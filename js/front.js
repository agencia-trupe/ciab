$(document).ready( function(){

    $('.fancybox').fancybox();

    $('.slideshow').cycle({
        timeout : 10000,
        pager : '#navegacao',
        activePagerClass : 'activeSlide',
        pauseOnPagerHover : true,
        pause : true,
        cleartype: true,
        cleartypeNoBg: true,
        filter : '',
        pagerAnchorBuilder : function(idx, slide){ return "<a class='bullet'></a>";},
        before: function(currSlideElement, nextSlideElement, options, forwardFlag){
                        $(nextSlideElement).find('.legenda').hide();
                    },
        after: function(currSlideElement, nextSlideElement, options, forwardFlag){
                    $(nextSlideElement).find('.legenda').slideDown();
        }
    });
    
    $('#palestrantes .palest:first').before($('#palestrantes .palest:last'));      
    
    $('#widget-nav-right').click( function(){
        pad = $(this).attr('data-pad');
        elem = $(this).attr('data-move');
        $('#widget-inner').animate({ left : '-='+pad }, 600, function(){
            $('#widget-inner .'+elem+':last').after($('#widget-inner .'+elem+':first'));
            $('#widget-inner').css({'left' : '0'});
        });
    });
    
    $('#widget-nav-left').click( function(){
        pad = $(this).attr('data-pad');
        elem = $(this).attr('data-move');
        $('#widget-inner').animate({ left : '+='+pad }, 600, function(){
            $('#widget-inner .'+elem+':first').before($('#widget-inner .'+elem+':last'));
            $('#widget-inner').css({'left' : '0'});
        });
    });
    
    if($('#tabela-hoteis').length){
        $('section').hide();
        
        $('#tabela-hoteis tr').click( function(){
            tgt = $('#'+$(this).attr('data-target'))
            visivel = $('section:visible');
            
            if(visivel.length){
                if(visivel.attr('id') != tgt.attr('id')){
                    visivel.hide('drop', { direction : 'right'}, 300,  function(){
                        tgt.show('drop', { direction : 'right'}, 300 );
                    });
                }else{
                    visivel.hide('drop', { direction : 'right'}, 300);
                }
            }else{
                tgt.show('drop', { direction : 'right'}, 300);
            }
        });
    }

    $('#link-todas, #link-anos a').click( function(event){
        event.preventDefault()
        ano = $(this).attr('data-target')
        $('#link-anos a').removeClass('link-anos-ativo')
        $(this).addClass('link-anos-ativo')
        var er = RegExp(/^[0-9]{4}\/[0-9]{2}-([a-zA-Z]{3}|[a-zA-Z]{5})([0-9]{4}).pdf$/)
        $.post( BASE + 'ajax/pegaThumbs', { pasta : $(this).attr('data-target')} , function(retorno){
            retorno = JSON.parse(retorno)
            $('#widget-inner').html('')
            resultado = retorno.map( function(self){
                exp = self.arquivo.match(er)
                titulo_arquivo = exp[1] + '/' + exp[2]
                $('#widget-inner').append("<div class='thumbs'><a target='_blank' href='"+BASE+"_pdfs/publicacoes/"+self.arquivo+"' class='fancy' title='"+titulo_arquivo+"'><img src='_imgs/publicacoes/capas/"+self.capa+"' alt='"+titulo_arquivo+"'></a></div>")
            })
        })
    });
    
    $('#newsletter-form').submit( function(event){
        event.preventDefault();
        var self = $(this);
        $('#form-resposta').html('').removeClass();
        $.post(BASE + 'ajax/cadastraNewsletter', { 'nome' : self.find('input[name=nome]').val(), 'email' : self.find('input[name=email]').val()}, function(retorno){
            retorno = JSON.parse(retorno)            
            self.slideUp('normal', function(){
                $('#form-resposta').html(retorno.mensagem).addClass(retorno.classe).slideDown('normal', function(){
                    self.find('input[name=nome]').val('').blur()
                    self.find('input[name=email]').val('').blur()
                    $(this).delay(5000).slideUp('normal', function(){
                        self.slideDown('normal')    
                    })
                })
            })
            
        });
    });
    
    $('#botao-voltar').click( function(event){
        event.preventDefault()
        history.back()
    });
    
    $('#banner-desconto').hover( function(){
        $(this).attr('src', '_imgs/inscricoes/hover-desconto.png')
    }, function(){
        $(this).attr('src', '_imgs/inscricoes/desconto.png')
    });
    
if (!$.browser.msie  || ($.browser.msie && parseInt($.browser.version, 10) > 8)) {
    
    $('.botao').hover( function(){
          $(this).stop().animate({ backgroundColor : '#F47E1E' }, 600)    
      }, function(){
          if($(this).hasClass('bt-cinza'))
              $(this).stop().animate({ backgroundColor : '#a3adb5' }, 600)
          else if($(this).hasClass('bt-azul-claro'))
              $(this).stop().animate({ backgroundColor : '#0095da' }, 600)
          else
              $(this).stop().animate({ backgroundColor : '#00427a' }, 600)
    });
  
    $('#lista-outrosanos li').hover( function(){
        $(this).stop().animate({ backgroundColor : '#8CC2EA' }, 300)    
    }, function(){
        $(this).stop().animate({ backgroundColor : '#f6f6f6' }, 300)    
    });
    
    $('.conteudo .lista-palestrante').hover( function(){
        $(this).stop()
                .animate({ backgroundColor : '#D3D3D3' }, 300)
                .find('.img-off').fadeOut(600)
    }, function(){
        $(this).stop()
                .animate({ backgroundColor : '#f6f6f6' }, 300)    
                .find('.img-off').fadeIn(600)
    });
    
    $('.noticia, .main-busca .lista-clipping').not('#colunas-home .noticia').hover( function(){
        $(this).stop().animate({ backgroundColor : '#D3D3D3' }, 300)    
    }, function(){
        $(this).stop().animate({ backgroundColor : '#f6f6f6' }, 300)    
    });
    
    $('#down-publi, .veja-tambem-link').hover( function(){
        $(this).find('img.tofade').fadeOut(300)
    }, function(){
        $(this).find('img.tofade').fadeIn(300)
    });

    $('#menu-lateral input[type="submit"]').hover( function(){
        $(this).stop().animate({ backgroundColor : '#F47E1E' }, 300)    
    }, function(){
        $(this).stop().animate({ backgroundColor : '#F6F6F6' }, 300)    
    });

    $('#colunas-home .coluna input[type="submit"]').hover( function(){
        $(this).stop().animate({ backgroundColor : '#F47E1E' }, 300)    
    }, function(){
        $(this).stop().animate({ backgroundColor : '#E2E2E2' }, 300)    
    });     
}

});