$(document).ready( function(){

    $('a.delete').click( function(){
        if(!confirm('Deseja realmente excluir o registro?'))
            return false
        else
            return true
    });
    
    $('a.troca-imagem').click( function(event){
        event.preventDefault()
        $(this).parent().find('.troca-imagem-box').toggle()
    });

    $('.btn-voltar').click( function(){
        window.history.back();
    });

    $('table tr:gt(1)').hover( function(){
        $(this).css('backgroundColor', '#CECECE');
    }, function(){
        $(this).css('backgroundColor', '#FFF');
    });

    $('#usuarios-form').submit( function(){
        if($('#edicao').length){
            if($('#senha').val() != ''){
                obrigatorios = {
                    'nome' : 'Nome de Usuário',
                    'senha' : 'Senha',
                    'senha_conf' : 'Confirmação de Senha'
                };
            }else{
                obrigatorios = {
                    'nome' : 'Nome de Usuário'
                };
            }
            if(verifica(obrigatorios)){
                if($('#senha').val() != ''){
                    if($('#senha').val() != $('#senha_conf').val() ){
                        alert('As senhas informadas não conferem.')
                        return false;
                    }else{
                        return true;
                    }
                }else{
                    return true;
                }
            }else{
                return false;
            }
        }else{
            obrigatorios = {
                'nome' : 'Nome de Usuário',
                'senha' : 'Senha',
                'senha_conf' : 'Confirmação de Senha'
            };
            if(verifica(obrigatorios)){
                if($('#senha').val() != $('#senha_conf').val() ){
                    alert('As senhas informadas não conferem.')
                    return false;
                }else{
                    return true;
                }
            }else{
                return false;
            }
        }

    });

    function verifica(obrigatorios){
        retorno = true;
        for(var i in obrigatorios){
            if($('#'+i).val() == ''){
                alert(obrigatorios[i] + ' � obrigatório.');
                $('#'+i).focus();
                retorno = false;
                break;
            }
        }
        return retorno;
    }

    $('#datepicker').datepicker();

});

tinyMCE.init({
    language : "pt",
    mode : "textareas",
    theme : "advanced",
    theme_advanced_buttons1 : "bold,italic,underline,link,unlink",
    theme_advanced_buttons2 : "",
    theme_advanced_buttons3 : "",
    theme_advanced_toolbar_location : "top",
    plugins: "paste",
    forced_root_block : "",
    force_br_newlines : true,
    force_p_newlines : false,
    oninit : 'setPlainText'
});

function setPlainText() {

        var ed = tinyMCE.get('elm1');

        ed.pasteAsPlainText = true;  

        if (tinymce.isOpera || /Firefox\/2/.test(navigator.userAgent)) {
            ed.onKeyDown.add(function (ed, e) {
                if (((tinymce.isMac ? e.metaKey : e.ctrlKey) && e.keyCode == 86) || (e.shiftKey && e.keyCode == 45))
                    ed.pasteAsPlainText = true;
            });

        } else{
            ed.onPaste.addToTop(function (ed, e) {
                ed.pasteAsPlainText = true;
            });
        }

        var ed2 = tinyMCE.get('elm2');

        ed2.pasteAsPlainText = true;  

        if (tinymce.isOpera || /Firefox\/2/.test(navigator.userAgent)) {
            ed2.onKeyDown.add(function (ed, e) {
                if (((tinymce.isMac ? e.metaKey : e.ctrlKey) && e.keyCode == 86) || (e.shiftKey && e.keyCode == 45))
                    ed2.pasteAsPlainText = true;
            });

        } else{
            ed2.onPaste.addToTop(function (ed, e) {
                ed2.pasteAsPlainText = true;
            });
        }        
}


glow.ready(function(){
        new glow.widgets.Sortable(
                '#content .grid_5, #content .grid_6',
                {
                        draggableOptions : {
                                handle : 'h2'
                        }
                }
        );
});