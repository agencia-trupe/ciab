$(document).ready( function(){

	function listaTemas(){
		var id_p = $('#id-palestrantes').val()
		var lang = $('#language').val()
		$.post(BASE + 'painel/ajax/listaTemas', { 'id_palestrantes' : id_p, 'language' : lang }, function(retorno){
			retorno = JSON.parse(retorno)
			result = '<table>'
			result += '<tr><th>titulo</th><th>data</th><th>texto</th><th></th></tr>'
			retorno.map( function(self){
                                switch(self.data){
                                    case '2013-06-12':
                                        self.dataExtens = '1&ordm; Dia'
                                        break;
                                    case '2013-06-13':
                                        self.dataExtens = '2&ordm; Dia'
                                        break;
                                    case '2013-06-14':
                                        self.dataExtens = '3&ordm; Dia'
                                        break;
                                }
				result += '<tr><td>'+self.titulo+'</td><td>'+self.dataExtens+'</td><td class=\'td-texto\'>'+self.texto+'</td><td><a href=\'#\' class=\'botao-excluirTema\' data-id=\''+self.id+'\'>excluir</a></td></tr>'
			})
			result += '</table>'
			$('#temas-target').html(result)
		})
	}

	listaTemas();
	
	$('#gravar-tema').live('click', function(){
		tinyMCE.triggerSave()
		var parent = $(this).parent()
		var tit = parent.find("input[name='titulo-tema']").val()
		var data = parent.find("select[name='data-tema']").val()
		var txt = parent.find("textarea[name='texto-tema']").val()
		var id_p = parent.find("input[name='id-palestrantes']").val()
		var language = parent.find("input[name='language']").val()
		$.post(BASE + 'painel/ajax/salvaTema', { 'id_palestrantes' : id_p, 'titulo' : tit, 'data' : data, 'texto' : txt, 'language' : language }, function(retorno){
			orig_content = parent.html()
			parent.fadeOut('normal', function(){
				parent.html(retorno).fadeIn('normal', function(){
                    parent.delay(2000).fadeOut('normal', function(){
						parent.html(orig_content)
						parent.find("input[name='titulo-tema']").val('')
						parent.find("select[name='data-tema']").val('')
						parent.find("textarea[name='texto-tema']").val('')
						parent.find("textarea[name='language']").val('')
						parent.fadeIn('normal')
						listaTemas()
					})
				})
			})
		})
	});
        
    $('.botao-excluirTema').live('click', function(event){
        event.preventDefault()
        if(confirm('Excluir o tema?')){
            var id_t = $(this).attr('data-id')
            var lang = $('#language').val()
            $.post(BASE+'painel/ajax/excluiTema', { id_temas : id_t, 'language' : lang }, function(retorno){
                listaTemas()
            })
        }
    })

});