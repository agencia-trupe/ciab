INSERT INTO site_stands(empresa, stand, ano) VALUES
("Acesso Digital", "E3", "2013"),
("Alog", "B19", "2013"),
("America Tecnologia", "H1", "2013"),
("Attps", "B12", "2013"),
("Banco do Brasil", "C1", "2013"),
("BR Token", "H9", "2013"),
("Brazil Mkt", "H2", "2013"),
("BRQ", "C13", "2013"),
("BSI Tecnologia", "F17", "2013"),
("Bull", "E11", "2013"),
("Burroughs", "R", "2013"),
("C&M", "F25+F26", "2013"),
("CA", "D6", "2013"),
("Capgemini", "D11", "2013"),
("CIS", "G", "2013"),
("Cisco", "D5", "2013"),
("Comprova.com", "S", "2013"),
("Compuprint", "H17", "2013"),
("Consist / DIBUTE", "C2", "2013"),
("Constat", "H8", "2013"),
("CPQD", "B9", "2013"),
("CRANE", "B", "2013"),
("Crivo TransUnion", "F24", "2013"),
("CRYPTERA", "D", "2013"),
("CTS", "J + K", "2013"),
("DDCom", "H11", "2013"),
("Diebold", "E12+E13", "2013"),
("Digital Check", "F", "2013"),
("EasySol", "E", "2013"),
("Easy-Way", "F23", "2013"),
("Edra", "G11", "2013"),
("Embratel", "E7", "2013"),
("EMC", "E9+E10", "2013"),
("Engetron", "F16", "2013"),
("E-Val", "H5", "2013"),
("FATO TI", "O", "2013"),
("Feedzai", "H14", "2013"),
("First Tech", "F12", "2013"),
("Foton", "G19", "2013"),
("Freesoft", "I", "2013"),
("Fujitsu", "D2", "2013"),
("G&P", "A13", "2013"),
("GetNet", "D3", "2013"),
("Glory", "D7", "2013"),
("Grupo HDI", "X", "2013"),
("Hess", "G10", "2013"),
("Honeywell", "C10", "2013"),
("HP", "D14+D15", "2013"),
("IBM", "D12+D13", "2013"),
("Icar", "P", "2013"),
("ID Tech", "H10", "2013"),
("In Metrics", "F19", "2013"),
("Itautec", "E14+E15", "2013"),
("JD Consultoria", "V", "2013"),
("Kodak", "B5+B6", "2013"),
("Lleida", "L", "2013"),
("Mastercoin", "F11", "2013"),
("Matera", "B18", "2013"),
("MEI", "C", "2013"),
("MicroFocus", "A", "2013"),
("Misys", "H15", "2013"),
("MT4", "T", "2013"),
("MUREX", "G21", "2013"),
("NCR", "B15", "2013"),
("Neurotech", "G18", "2013"),
("New Space", "E6", "2013"),
("Nonus", "A4", "2013"),
("Olivetti", "H13", "2013"),
("Oracle", "E5", "2013"),
("P3Image", "C9", "2013"),
("Panini", "A9", "2013"),
("PAX", "H", "2013"),
("Perto", "D9+D10", "2013"),
("Prime Informatica", "F18", "2013"),
("PromonLogicalis", "F15", "2013"),
("Prosegur", "C8", "2013"),
("Qualisoft", "Z", "2013"),
("Recognition", "F22", "2013"),
("Reiner", "B3b", "2013"),
("Resource", "C15+C16", "2013"),
("RTM", "C6", "2013"),
("Samsung", "A11", "2013"),
("SAP", "C21+C22", "2013"),
("SAS", "F13", "2013"),
("Scopus", "E8", "2013"),
("Seac Banche", "Q", "2013"),
("Sirsan", "A8", "2013"),
("SIS Consultoria", "C7", "2013"),
("Sistemas Críticos", "U", "2013"),
("Smartcon", "B14", "2013"),
("Stefanini", "C17+C18", "2013"),
("Symantec", "F9", "2013"),
("Symmetry", "N", "2013"),
("Tata", "C11", "2013"),
("Tecnocom", "A14", "2013"),
("Tecnometal", "G17", "2013"),
("THCS", "E2", "2013"),
("TIVIT", "C19+C20", "2013"),
("Totvs", "F20", "2013"),
("Tree Solution", "B11", "2013"),
("T-Systems", "B16", "2013"),
("Unisys", "F8", "2013"),
("Vasco", "F21", "2013"),
("Verint", "A10", "2013"),
("View", "A12", "2013"),
("WBA", "H4", "2013"),
("Wincor Nixdorf", "D8", "2013"),
("Wunderwerke", "H7", "2013");