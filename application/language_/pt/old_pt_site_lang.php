<?php
/* TOPO */
$lang['EVENTO'] = 'EVENTO';
$lang['EVENTO_SUBMENU_1'] = 'Apresentação';
$lang['EVENTO_SUBMENU_2'] = 'O Congresso';
$lang['EVENTO_SUBMENU_3'] = 'Espaços Temáticos';
$lang['EVENTO_SUBMENU_4'] = 'Exposição';
$lang['EVENTO_SUBMENU_5'] = 'Quero Visitar';
$lang['EVENTO_SUBMENU_6'] = 'Quero Expor';
$lang['EVENTO_SUBMENU_7'] = 'Quero Patrocinar';
$lang['EVENTO_SUBMENU_8'] = 'Planta da Exposição';
$lang['EVENTO_SUBMENU_9'] = 'Outros Anos';
$lang['PROGRAMACAO'] = 'PROGRAMAÇÃO';
$lang['PALESTRANTES'] = 'PALESTRANTES';
$lang['PREMIOS'] = 'PRÊMIOS';
$lang['PREMIOS_SUBMENU_1'] = 'Prêmio Ciab 2012';
$lang['PREMIOS_SUBMENU_2'] = 'Vencedores 2011';
$lang['PREMIOS_SUBMENU_3'] = 'Vencedores 2012';
$lang['INSCRICOES'] = 'INSCRIÇÕES';
$lang['INSCRICOES_SUBMENU_1'] = 'Valores';
$lang['INSCRICOES_SUBMENU_2'] = 'Informações Importantes';
$lang['INSCRICOES_BANNER_TEXTO'] = 'Inscrições com desconto prorrogadas até 10.06 para filiados e não filiados. Aproveite e faça sua inscrição.';
$lang['NOTICIAS'] = 'NOTÍCIAS';
$lang['PUBLICACOES'] = 'PUBLICAÇÕES';
$lang['IMPRENSA'] = 'IMPRENSA';
$lang['IMPRENSA_SUBMENU_1'] = 'Assessoria de Imprensa';
$lang['INFORMACOES'] = 'INFORMAÇÕES';
$lang['INFORMACOES_SUBMENU_1'] = 'Local do Evento';
$lang['INFORMACOES_SUBMENU_2'] = 'Hospedagem';
$lang['CONTATO'] = 'CONTATO';
$lang['BUSCA'] = 'BUSCA';
$lang['ling_portugues'] = 'português';
$lang['ling_ingles'] = 'english';
$lang['ling_espanhol'] = 'español';
$lang['VOLTAR'] = 'VOLTAR';
$lang['Compartilhe'] = 'Compartilhe';
$lang['Quarta-Feira'] = 'Quarta-Feira';
$lang['Quinta-Feira'] = 'Quinta-Feira';
$lang['Sexta-Feira'] = 'Sexta-Feira';

/* FOOTER */
$lang['PATROCINADORES'] = 'PATROCINADORES';
$lang['DIAMANTE'] = 'DIAMANTE';
$lang['OURO'] = 'OURO';
$lang['PRATA'] = 'PRATA';
$lang['APOIO'] = 'APOIO';
$lang['FALE CONOSCO'] = 'FALE CONOSCO';
$lang['POLITICA DE PRIVACIDADE'] = 'POLÍTICA DE PRIVACIDADE';
$lang['TERMO DE USO'] = 'TERMO DE USO';
$lang['MAPA DO SITE'] = 'MAPA DO SITE';
$lang['address-linha1'] = '&copy; FEBRABAN - Federação Brasileira de Bancos - '.date('Y').' - Todos os direitos reservados';
$lang['address-linha2'] = 'Av. Brig. Faria Lima, 1.485 - 14º andar • São Paulo • PABX .: 55 11 3244 9800 / 3186 9800 • FAX.: 55 11 3031 4106 •  <a href="http://www.febraban.org.br">WWW.FEBRABAN.ORG.BR</a>';

/* VEJA MAIS */
$lang['VEJA_MAIS_TITULO'] = "VEJA+MAIS";

/* PALESTRANTES */
$lang['PALESTRANTES_TEMAS_TITULO'] = 'Temas apresentados durante o evento';
$lang['PALESTRANTES_OUTROS'] = 'Outros Palestrantes';

/* NOTÍCIAS */
$lang['NOTICIAS_ULTIMAS_TITULO'] = 'Últimas Notícias';
$lang['NOTICIAS_VER_TODAS'] = 'VER TODAS';

/* HOME */
$lang['HOME_SLIDE_CMEP'] = 'Vem ai o CIAB FEBRABAN 2013<br>Agende-se: 12 a 14 de Junho de 2013';
$lang['HOME_SLIDE_1'] = 'O CIAB FEBRABAN - Congresso e Exposição de Tecnologia da Informação das Instituições Financeiras  é o maior evento da América Latina tanto para o setor financeiro quanto para a área de Tecnologia.';
$lang['HOME_SLIDE_2'] = 'Gestão e Recuperação de Crédito - PJ / PF, Prevenção sobre Crimes de Lavagem de Dinheiro, Metodologias de Análise e Inspeção em Fraudes Bancárias, Excelência nos Serviços de Atendimento a Clientes, Análise Setorial com análise de riscos, e ...';
$lang['HOME_SLIDE_3'] = 'Uma oportunidade única para discutir os desafios da indústria e questões específicas para a comunidade latino-americana, juntamente com uma atualização sobre as principais iniciativas SWIFT que exercem impacto na região.';
$lang['HOME_SLIDE_4'] = 'Em sua 13ª edição, o Congresso FEBRABAN de Auditoria Interna e Compliance traz à discussão a perspectiva tridimensional sobre o tema que compreende maior cobertura, maior profundidade nas análises e a garantia de qualidade ...';
$lang['BOX_INSCRICOES_TITULO'] = 'INSCRIÇÕES';
$lang['BOX_INSCRICOES_TEXTO'] = 'Inscrições com desconto prorrogadas até 10.06 para filiados e não filiados. Aproveite e faça sua inscrição.';
$lang['BOX_BLOG_TITULO'] = 'BLOG';
$lang['BOX_BLOG_TEXTO'] = 'Novo blog do Ciab FEBRABAN. Um espaço para discutir os mais variados temas ligados à Tecnologia da Informação. Participe!';
$lang['QUERO_PATROCINAR_TITULO'] = 'QUERO PATROCINAR';
$lang['QUERO_PATROCINAR_TEXTO'] = 'Sua empresa pode patrocinar o maior congresso e exposição da América Latina.';
$lang['QUERO_EXPOR_TITULO'] = 'QUERO EXPOR';
$lang['QUERO_EXPOR_TEXTO'] = 'CIAB FEBRABAN conta com a participação de centenas de empresas expositoras.';
$lang['QUERO_VISITAR_TITULO'] = 'QUERO VISITAR';
$lang['QUERO_VISITAR_TEXTO'] = 'Vá ao Transamérica Expo Center e faça seu cadastro de visitante do evento.';
$lang['AGENDA_TITULO'] = 'AGENDA';
$lang['AGENDA_TEXTO'] = 'Confira a agenda de palestrantes nacionais e internacionais que estarão presentes.';
$lang['NOTICIAS_TITULO'] = 'NOTÍCIAS';
$lang['NENHUMA_NOTICIA'] = 'Nenhuma notícia cadastrada';
$lang['EVENTO_TITULO'] = 'O EVENTO';
$lang['EVENTO_SUBTITULO'] = 'O que é CIAB FEBRABAN';
$lang['EVENTO_TEXTO'] = 'Congresso e Exposição de Tecnologia da Informação das Instituições Financeiras - o maior evento da América Latina...';
$lang['PUBLICACOES_TITULO'] = 'PUBLICAÇÕES';
$lang['PUBLICACOES_SUBTITULO'] = 'Aposta de R$ 18 bilhões em TI';
$lang['PUBLICACOES_TEXTO'] = 'Pesquisa Ciab FEBRABAN revela que despesas e investimentos dos bancos em tecnologia, em 2011, foram de R$ 18 bilhões e também aponta grande evolução no uso do Mobile Banking.';
$lang['TWITTER_TITULO'] = 'TWITTER CIABFEBRABAN';
$lang['NEWSLETTER_TITULO'] = 'NEWSLETTER';
$lang['NEWSLETTER_TEXTO'] = 'Assine e receba todas as notividades';
$lang['NEWSLETTER_PLACEHOLDER_NOME'] = 'Informe seu nome';
$lang['NEWSLETTER_PLACEHOLDER_EMAIL'] = 'seu e-mail';
$lang['NEWSLETTER_SUBMIT'] = 'ENVIAR CADASTRO';

/* INSCRIÇÕES */
$lang['INSCRICOES_TITULO'] = 'Preços de Inscrições e Bonificações de Inscrições';
$lang['INSCRICOES_TITULO_TABELA_1'] = 'Valores por pessoa para inscrições nos 3 dias do Congresso';
$lang['DATA_ATE'] = 'Até';
$lang['DATA_APOS'] = 'Após';
$lang['INSCRICOES_TABELA_INSCRICOES'] = 'Inscrições';
$lang['INSCRICOES_TABELA_FILIADOS'] = 'Bancos filiados';
$lang['INSCRICOES_TABELA_NAO_FILIADOS'] = 'Não filiados';
$lang['INSCRICOES_TABELA_LINHA_1'] = 'Até 5';
$lang['INSCRICOES_TABELA_LINHA_2'] = 'de 6 a 10';
$lang['INSCRICOES_TABELA_LINHA_3'] = 'de 11 a 20';
$lang['INSCRICOES_TABELA_LINHA_4'] = 'mais de 20';
$lang['INSCRICOES_TABELA_RODAPE'] = 'EMPRESAS COM O MESMO CNPJ VALORES POR PARTICIPANTE';
$lang['INSCRICOES_TITULO_TABELA_2'] = 'Valores por pessoa para inscrições para 1 dia do Congresso';
$lang['INSCRICOES_OBSERVACAO_TITULO'] = 'Observação';
$lang['INSCRICOES_OBSERVACAO_TEXTO'] = 'No valor da inscrição estão incluídos: almoços, coffee breaks , coquetel do dia 20/06 e estacionamento nos 3 dias de realização do evento.';
$lang['INSCRICOES_OBSERVACAO_ITEM_1'] = 'Os descontos são válidos somente para inscrições na mesma categoria (participação nos 3 dias do evento ou participação em apenas um dia do evento).';
$lang['INSCRICOES_OBSERVACAO_ITEM_2'] = 'Manteremos o valor da 1ª. Fase para o banco que fizer mais do que o equivalente a 50 inscrições válidas para os três dias do evento. (ex.: 50 inscrições para participação nos 3 dias x R$ 2.178,00 = R$ 108.900,00 ou 124 inscrições para participação em um dia = R$ 108.872,00).';
$lang['INSCRICOES_BONIFICACAO_TITULO'] = 'Bonificação';
$lang['INSCRICOES_BONIFICACAO_ITEM_1'] = 'A cada 20 inscrições a instituição recebe 01 (uma) inscrição cortesia de um dia;';
$lang['INSCRICOES_BONIFICACAO_ITEM_2'] = 'Instituições que fizerem mais de 50 (cinqüenta) inscrições e aumentarem o total de inscritos em relação a 2011 serão bonificadas, conforme abaixo:';
$lang['INSCRICOES_BONIFICACAO_TABELA_TITULO_1'] = 'até 10%';
$lang['INSCRICOES_BONIFICACAO_TABELA_TITULO_2'] = 'Acima de 10% até 20%';
$lang['INSCRICOES_BONIFICACAO_TABELA_TITULO_3'] = 'Acima de 20% até 30%';
$lang['INSCRICOES_BONIFICACAO_TABELA_TITULO_4'] = 'Acima de 30%';
$lang['INSCRICOES_BONIFICACAO_TABELA_TEXTO_1'] = '5 inscr. de 1 dia';
$lang['INSCRICOES_BONIFICACAO_TABELA_TEXTO_2'] = '10 inscr. de 1 dia';
$lang['INSCRICOES_BONIFICACAO_TABELA_TEXTO_3'] = '15 inscr. de 1 dia';
$lang['INSCRICOES_BONIFICACAO_TABELA_TEXTO_4'] = '20 inscr. de 1 dia';
$lang['INSCRICOES_BONIFICACAO_TABELA_LEGENDA'] = '(*) A Tabela de Bonificação é válida exclusivamente para os acréscimos de inscrições em relação a 2011, conforme as faixas estabelecidas, e somente para empresas que fizeram acima de 50 (cinquenta) inscrições no Ciab 2012 .<br>(**) Para consultar a quantidade de inscrições realizadas em 2011, pedimos contatar a Diretoria de<br>Eventos pelo telefone (11) 3186-9860.';
$lang['INSCRICOES_EXEMPLO_TITULO'] = 'Exemplo';
$lang['INSCRICOES_BONIFICACAO_TEXTO'] = 'Instituição Y realizou em 2011 50 inscrições para os 3 dias do evento.<br> Em 2012, realizou 66 inscrições para os 3 dias.<br> Isso quer dizer que o banco Y tem direito à bonificação de 30%.<br>';
$lang['INSCRICOES_BONIFICACAO_TEXTO_NEGRITO'] = 'As bonificações adquiridas pela Instituição Y são';
$lang['INSCRICOES_BONIFICACAO_ITEM_EXEMPLO_1'] = 'Desconto de 35% para a mesma categoria (3 dias ou 1 dia) até o último dia de realização do Congresso, conforme disponibilidade de vagas.';
$lang['INSCRICOES_BONIFICACAO_ITEM_EXEMPLO_2'] = 'Gratuidade de 3 inscrições para participação em 1 dia do Congresso (a cada 20 inscrições 01 gratuidade).';
$lang['INSCRICOES_BONIFICACAO_ITEM_EXEMPLO_3'] = 'Gratuidade de 20 inscrições para participação em 1 dia do Congresso, conforme tabela de bonificação';
$lang['INSCRICOES_INFORMACOES_TITULO'] = 'Informações Importantes';
$lang['INSCRICOES_OPCOES_TITULO'] = 'Opções de Pagamento';
$lang['INSCRICOES_OPCOES_ITEM_1'] = '- Boleto Bancário';
$lang['INSCRICOES_OPCOES_ITEM_2'] = '- Cartões de Crédito Nacionais e Internacionais';
$lang['INSCRICOES_REEMBOLSO_TITULO'] = 'Reembolsos e Cancelamentos';
$lang['INSCRICOES_REEMBOLSO_TEXTO'] = 'Os pedidos de reembolso ou cancelamento de inscrições serão acatados, mediante solicitação por escrito endereçada para o e-mail eventos@febraban.org.br até o dia 31 de maio de 2012.<br> Os reembolsos serão efetuados pelo valor líquido, deduzidas todas as despesas bancárias provenientes da transferência bancária.<br> A partir de 1º de junho de 2012 não serão aceitos pedidos de reembolso ou cancelamentos. ';
$lang['INSCRICOES_SUBSTITUICAO_TITULO'] = 'Substituição de Participante';
$lang['INSCRICOES_SUBSTITUICAO_TEXTO'] = 'A FEBRABAN somente aceitará a substituição de participante desde que a solicitação seja feita por e-mail dirigido ao endereço eletrônico eventos@febraban.org.br até o dia 15.06.2012.<br> Para substituições solicitadas durante a realização do evento a Organização cobrará uma taxa de R$ 250,00 pela reemissão do crachá. O respectivo pagamento deverá ser efetuado no ato da <br> solicitação. ';
$lang['INSCRICOES_CONFIRMACAO_TITULO'] = 'Confirmação de Inscrição Via E-Mail';
$lang['INSCRICOES_CONFIRMACAO_TEXTO'] = 'Após realizada a inscrição, o participante receberá automaticamente por e-mail disparado pelo sistema de inscrições a sua confirmação. <br> Caso não receba essa confirmação, o participante ou a área responsável pela inscrição poderá<br> entrar em contato com a Diretoria de Eventos da FEBRABAN pelo e-mail eventos@febraban.org.br,<br> solicitando a verificação.';
$lang['INSCRICOES_ENCERRAMENTO_TITULO'] = 'Encerramento das Inscrições';
$lang['INSCRICOES_ENCERRAMENTO_TEXTO'] = '15.06.2012 ou até o encerramento das vagas. ';

/* EVENTO */
$lang['EVENTO_APRESENTACAO_TITULO'] = 'Apresentação';
$lang['EVENTO_APRESENTACAO_TEXTO'] = <<<TEXTO
<p>
O CIAB FEBRABAN - Congresso e Exposição de Tecnologia da Informação das Instituições Financeiras - é o maior
evento da América Latina tanto para o setor financeiro quanto para a área de Tecnologia.
</p>
<p>
No seu lançamento, em Setembro de 2011, 76% do espaço destinado à exposição foi reservado por 74 
empresas da área de tecnologia. Isto representa uma venda 12% maior que no lançamento do CIAB FEBRABAN 2011
consolidando, mais uma vez, o seu sucesso.
</p>
<p>
A organização do congresso estima que o CIAB FEBRABAN 2012 seja visitado por 19 mil pessoas entre 
representantes de instituições financeiras e de empresas de TI, e registre 1,9 mil congressistas e que vá contar
com cerca de 190 empresas expositoras.
</p>
<p>
A 22ª edição do CIAB FEBRABAN terá como tema central “A Sociedade Conectada” e será realizada nos dias 20, 21 e
22 de Junho e 2012.
</p>
<p>
Números 2011, consolidação do sucesso<br>
Com o tema “A tecnologia além da web”, o CIAB FEBRABAN 2011 registrou, em sua 21ª edição, mais de 18 mil visitantes,
1.795 congressistas representantes de instituições financeiras do Brasil e de mais de 27 países, além de 174 empresas
expositoras, sendo 33 internacionais sem bases no Brasil.
</p>
<p>
O congresso ainda contou com o patrocínio e apoio de mais de 20 empresas, e teve ao todo 33 painéis, 81 
palestrantes, 5 debatedores, 27 moderadores e 06 keynotes.
</p>
TEXTO;
$lang['EVENTO_SOBRE_TITULO'] = 'Sobre a FEBRABAN';
$lang['EVENTO_SOBRE_TEXTO'] = <<<TEXTO
<p>
A Federação Brasileira de Bancos – FEBRABAN é a principal entidade representativa do setor bancário. Foi fundada em 1967 visando
o fortalecimento do sistema financeiro e de suas relações com a sociedade de modo. Reúne 120 bancos associados de um universo
de 172 instituições em operação no Brasil, os quais representam 97% dos ativos totais e 93% do patrimônio líquido do sistema 
bancário. Entre os objetivos estratégicos da Federação estão o desenvolvimento de iniciativas para a contínua melhoria da 
produtividade do sistema bancário e a redução dos seus níveis de risco, contribuindo dessa forma para o desenvolvimento econômico
e sustentável do País.
</p>
<p>
Os bancos também concentram esforços que viabilizam o crescente acesso da população aos seus produtos e serviços com qualidade
e transparência. A Federação consolidou em 2010 sua nova missão e seus novos valores, avançando em aspectos de governança corporativa.
São exemplos desse movimento, a posse do primeiro presidente não dirigente de instituição financeira de sua história, e o 
aprofundamento de discussões dos problemas brasileiros através do Conselho Consultivo da entidade, composto por membros representativos
de entidades empresariais ligadas ao comércio, indústria, educação e terceiro setor.
</p>
<p>
O diálogo com a sociedade vem se ampliando por meio de ações objetivas, como a criação do Sistema de Autorregulação Bancária e o
aperfeiçoamento de ferramentas de relacionamento com o consumidor: Orkut, Facebook, blogs e Twitter, e canais exclusivos de atendimento
como o Sac Bancos, Ouvidoria e Barômetro.
</p>
<p>
As novas exigências da sociedade, principalmente diante do novo estágio de desenvolvimento da economia brasileira, exigem que a
Federação e o setor bancário respondam aos novos desafios, valendo especial atenção à inclusão financeira de consumidores de baixa
renda e à educação financeira.
</p>
TEXTO;
$lang['EVENTO_CONGRESSO_TITULO'] = 'O Congresso';
$lang['EVENTO_CONGRESSO_TEXTO'] = <<<TEXTO
<p>
Realizado paralelamente à Exposição, o Congresso CIAB FEBRABAN será realizado em 3 auditórios que 
contarão com especialistas nacionais e internacionais de tecnologia e que debaterão sobre o tema principal do 
Congresso -  “A Sociedade Conectada”.
</p>
<p>
Em 2011, o Congresso CIAB contou com a participação de 1,8 mil congressistas, representantes de
instituições financeiras do Brasil e de outros 27 países, dentre eles: Alemanha, Argentina, Austrália,
Bélgica, China, Chile, Espanha, EUA, França, Israel, Índia, Itália, Japão, México, Singapura, Taiwan, e Uruguai.
</p>
<p>
Com o tema “A tecnologia além da web”, a edição de 2011 realizou 33 painéis com cerca de 100 keynote 
speakers, estudiosos e escritores internacionais como Michael D. Capellas, Tom Kelley, Tom Davenport,
além dos principais executivos das maiores instituições financeiras do Brasil.
</p>
TEXTO;
$lang['EVENTO_ESPACOS_TITULO'] = 'Espaços Temáticos';
$lang['EVENTO_ESPACOS_TEXTO'] = '<p> Ciab FEBRABAN reforça, novamente, suas ações voltadas para as pequenas empresas, visando estimular a presença desse segmento no congresso. </p>';
$lang['EVENTOS_ESPACOS_ITEM_1_TITULO'] = 'ESPAÇO EMPREENDEDOR';
$lang['EVENTOS_ESPACOS_ITEM_1_TEXTO'] = 'Espaço dedicado à exposição de empresas com capital 100% nacional, de pequeno porte e faturamento limitado a R$ 12 milhões. A empresa expositora conta com pacote composto por espaço na feira e montagem do estande a preços reduzidos.';
$lang['EVENTOS_ESPACOS_ITEM_2_TITULO'] = 'ESPAÇO INOVAÇÃO';
$lang['EVENTOS_ESPACOS_ITEM_2_TEXTO'] = 'A FEBRABAN e o ITS (Instituto de Tecnologia de Software e Serviços) vão promover durante o Ciab FEBRABAN 2012 a 8ª edição do “Espaço Inovação”. Divulgue esta iniciativa e participe do Processo de Seleção, inscrevendo sua inovação no site do ITS.';
$lang['EVENTOS_ESPACOS_ITEM_3_TITULO'] = 'ESPAÇO INTERNACIONAL';
$lang['EVENTOS_ESPACOS_ITEM_3_TEXTO'] = 'Espaço dedicado à exposição de empresas que não tem sede no Brasil, com montagem do estande e toda infraestrutura.';
$lang['EVENTOS_ESPACOS_ITEM_4_TITULO'] = 'PAVILHÃO FRANÇA';
$lang['EVENTOS_ESPACOS_ITEM_4_TEXTO'] = 'A FEBRABAN e a Embaixada da França no Brasil fecharam um acordo pelo qual empresas francesas da área de TI poderão participar de um espaço no 22º Ciab FEBRABAN. O Espaço França ocupará uma área de 27 metros quadrados, e abrigará até dez companhias. O intuito é intensificar o intercâmbio dos dois países.';
$lang['EVENTO_EXPOSICAO_TITULO'] = 'Exposição';
$lang['EVENTO_EXPOSICAO_TEXTO'] = <<<TEXTO
<p>
Simultaneamente ao congresso, o CIAB FEBRABAN realiza sua exposição que conta com a participação de centenas de empresas expositoras das áreas de finanças, tecnologia da informação (TI) e telecomunicações.
</p>

<p>
O visitante terá a oportunidade de conferir todas as novidades e soluções tecnológicas para 2012 e para os próximos anos. Visite também os Espaços Temáticos, uma oportunidade única de prestigiar as pequenas empresas nacionais da área de TI.
</p>

<p>
A visitação à exposição é gratuita e ocorre no horário das 9h às 20h. Basta fazer o seu cadastro no link abaixo ou diretamente no local, na área “Credenciamento de Visitantes”.
</p>

<p>
<span class="negrito">Importante:</span> o crachá de visitante é válido somente para acesso ao pavilhão de exposição. Em nenhuma hipótese será permitida a entrada de visitantes nas áreas exclusivas de congressistas e convidados.
</p>

<a href="http://www.credenciamento.com.br/2012/VisitanteCiab/Default_por.aspx" target="_blank" class="botao" style="color:#FFF;">CLIQUE AQUI E CADASTRE-SE</a>
TEXTO;
$lang['EVENTO_QUERO_VISITAR_TITULO'] = 'Quero Visitar';
$lang['EVENTO_QUERO_VISITAR_TEXTO_1'] = <<<TEXTO
<p>
Compareça no Transamérica Expo Center e faça o seu cadastro na área “Credenciamento de Visitantes”. Pegue o seu crachá e confira as soluções que os expositores da feira trazem até você.  A exposição Ciab FEBRABAN acontece simultaneamente ao congresso, no horário das 9h ás 20h.
</p>

<p>
<span class="negrito">Importante:</span> o crachá de visitante é válido somente para o acesso ao pavilhão de exposição. Em nenhuma hipótese será permitida a entrada de visitantes nas áreas exclusivas de congressistas e convidados.
</p>
TEXTO;
$lang['EVENTO_QUERO_VISITAR_TEXTO_2'] = <<<TEXTO
<p style="clear:left;">
Se preferir, clique no botão abaixo, preencha o formulário e faça agora mesmo seu cadastro.
</p>
<a href="http://www.credenciamento.com.br/2012/VisitanteCiab/Default_por.aspx" target="_blank" class="botao" style="color:#FFF;">CLIQUE AQUI E CADASTRE-SE</a>
TEXTO;
$lang['EVENTO_EXPOR_TITULO'] = 'Quero Expor';
$lang['EVENTO_EXPOR_TEXTO'] = <<<TEXTO
<p>
Centenas de empresas das áreas de finanças, tecnologia da informação (TI) e telecomunicações, anualmente expõem
no CIAB FEBRABAN e têm a oportunidade de estreitar seu relacionamento com milhares de pessoas que transitam
passam pelo evento.
</p>

<p>
Em 2011, 174 das maiores empresas desses setores (como IBM, Diebold, Microsoft, Itautec, HP, CA, Perto, Fujitsu
e CPM Braxis) participaram da exposição. No lançamento do CIAB 2012, que ocorreu em setembro de 2011, 76% da área
de exposição já havia sido reservada.
</p>
TEXTO;
$lang['EVENTO_EXPOR_FORM_TEXTO'] = 'Preencha o formulário abaixo, faça agora mesmo o seu cadastro e reserve o espaço da sua empresa no evento. Nossos assessores entrarão em contato para passar todas as informações de como se tornar um expositor.';
$lang['EVENTO_EXPOR_FORM_1'] = 'Escolha o assunto';
$lang['EVENTO_EXPOR_FORM_2'] = 'Nome (completo)';
$lang['EVENTO_EXPOR_FORM_3'] = 'E-mail ';
$lang['EVENTO_EXPOR_FORM_4'] = 'Empresa';
$lang['EVENTO_EXPOR_FORM_5'] = 'Site da Empresa';
$lang['EVENTO_EXPOR_FORM_6'] = 'Área de Atuação';
$lang['EVENTO_EXPOR_FORM_7'] = 'Seu Telefone';
$lang['EVENTO_EXPOR_FORM_8'] = 'Espaço/Metragem de interesse';
$lang['EVENTO_EXPOR_FORM_SUBMIT'] = 'ENVIAR';
$lang['EVENTO_EXPOR_FORM_OBS'] = 'Para informações adicionais e reservas de  Espaços fale com:';
$lang['EVENTO_EXPOR_FORM_OK'] = '<h1>Obrigado!<br>Email enviado com sucesso.entraremos em contato em breve.</h1>';
$lang['EVENTO_EXPOR_FORM_ERROR'] = '<h1>O email não pode ser enviado. <br> Tente novamente.</h1>';
$lang['EVENTO_PATROCINAR_TITULO'] = 'Quero Patrocinar';
$lang['EVENTO_PATROCINAR_TEXTO'] = <<<TEXTO
<p>
A sua empresa pode ser uma das patrocinadoras do maior congresso e exposição da América Latina em 
tecnologia da informação para instituições financeiras, e expor a sua marca para milhares de
congressistas e visitantes que frequentam os 3 dias do CIAB FEBRABAN.
</p>

<p>
A edição 2010 registrou 129 empresas expositoras (como IBM, Diebold, Microsoft, Itautec, HP, CA, Perto,
Fujitsu e CPM Braxis) e recebeu a visita de um público superior a 16 mil pessoas, altamente qualificado, 
composto por presidentes, diretores e gerentes de instituições financeiras, que representavam, em sua maioria,
bancos (70% do público) e empresas de TI (15%).
</p>

<p>
Já em 2011, esses números aumentaram. Foram 174 empresas expositoras (sendo 33 internacionais sem base no Brasil),
mais de 18 mil visitantes e 1.795 congressistas representantes de instituições financeiras do Brasil e de mais de
27 países.
</p>
TEXTO;
$lang['EVENTO_PATROCINAR_OBS'] = 'Para informações adicionais e reservas de espaços fale com:';
$lang['EVENTO_MAPA_EXPOSITORES'] = 'EXPOSITORES';
$lang['EVENTO_ANOS_TITULO'] = 'Outros Anos';
$lang['EVENTO_ANOS_SUBTITULO'] = 'Veja o que aconteceu nas edições anteriores do evento';
$lang['EVENTO_ANOS_ITEM_1'] = 'A Tecnologia Além da Web';
$lang['EVENTO_ANOS_ITEM_2'] = 'A geração Y<br> um novo banco para<br> um novo consumidor';
$lang['EVENTO_ANOS_ITEM_3'] = 'Uma ampla discussão sobre<br> a tecnologia e seus impactos<br> na inclusão bancária';
$lang['EVENTO_ANOS_ITEM_4'] = 'Tecnologia e Segurança';

/* PRÊMIOS */
$lang['PREMIOS_CIAB_TITULO'] = 'Prêmio Ciab 2012';
$lang['PREMIOS_CIAB_SUBTITULO'] = 'Este ano, o Prêmio CIAB traz uma novidade – premiação de protótipos para mobilidade.';
$lang['Menção Honrosa'] = 'Menção Honrosa';
$lang['PREMIOS_CIAB_TEXTO'] = <<<TEXTO
<p>Como a maior exposição de TI (Tecnologia da Informação) do País e um dos maiores congressos de bancos 
do mundo, o CIAB FEBRABAN traz uma novidade a cada ano. </p>

<p>Para a edição 2012 do Prêmio CIAB FEBRABAN, a novidade está no incentivo direto da capacidade de inovação
técnica de jovens e universitários que, pela primeira vez, deverão inscrever aplicativos para conectividade
móvel ou plataforma móvel. </p>

<p>Com o tema “Soluções para uma Sociedade Conectada”, o Prêmio CIAB FEBRABAN 2012 busca incluir os jovens e
universitários no debate dessas propostas inovadoras. Os trabalhos poderão explorar assuntos como a utilização
de redes sociais, convergência de tecnologias, smartphones, tablets, apps, novas formas de relacionamentos 
dos bancos com os clientes ou outros conteúdos relacionados ao tema do prêmio e deverão possuir a validação
de empresas ou instituições de ensino, e ser aplicáveis no mercado financeiro.</p>

<p>O principal quesito para avaliação e classificação do trabalho é a originalidade e ineditismo da proposta.
Podem participar grupos de até seis pessoas e os resultados serão divulgados dia 04 de junho de 2012.</p>

<p>O período de inscrições dos trabalhos é de 01 de março a 21 de maio de 2012.</p>
TEXTO;
$lang['PREMIOS_CIAB_CHAMADA'] = 'São R$ 25 mil em prêmios! Inscreva-se.';
$lang['PREMIOS_CIAB_BOTAO_1'] = 'CLIQUE AQUI PARA SE INSCREVER';
$lang['PREMIOS_CIAB_BOTAO_2'] = 'LEIA O REGULAMENTO';
$lang['PREMIOS_VENCEDORES_TITULO'] = 'Vencedores CIAB 2011';
$lang['PREMIOS_VENCEDORES_SUBTITULO'] = 'Vencedores do Prêmio CIAB FEBRABAN e Universia 2011 - A Tecnologia Além da Web';
$lang['PREMIOS_VENCEDORES_TEXTO_1'] = '<p>A edição 2011 do Prêmio CIAB FEBRABAN e Universia (que teve como tema “A Tecnologia Além da Web”)
            registrou 611 inscrições contra 223 na primeira edição do prêmio em 2010. Confira abaixo os vencedores.</p>';
$lang['PREMIOS_VENCEDORES_COMUNICADO'] = 'COMUNICADO – RECLASSIFICAÇÃO PRÊMIO CIAB FEBRABAN e UNIVERSIA 2011';
$lang['PREMIOS_VENCEDORES_TEXTO_2'] = <<<TEXTO
<p>A comissão organizadora do Ciab FEBRABAN 2011, com base nos critérios do regulamento do Prêmio
Ciab FEBRABAN e Universia 2011, reavaliando os concorrentes, concluiu que o trabalho classificado
anteriormente em primeiro lugar (2Whish) foi desclassificado por não cumprir com requisitos estabelecidos
no regulamento.</p>

<p>Portanto, a classificação final do prêmio, passa a ser a seguinte:</p>
TEXTO;
$lang['PREMIOS_VENCEDORES_TABELA_1'] = 'Classificação';
$lang['PREMIOS_VENCEDORES_TABELA_2'] = 'Nome';
$lang['PREMIOS_VENCEDORES_TABELA_3'] = 'Título do Trabalho';
$lang['PREMIOS_VENCEDORES_TWITTER_TITULO'] = 'Vencedores TWITTER 2011';
$lang['PREMIOS_VENCEDORES_TWITTER_SUBTITULO'] = 'Concurso Cultural Melhor Slogan CIAB no Twitter';
$lang['PREMIOS_VENCEDORES_TWITTER_CHAMADA'] = 'Quem seguiu o @CiabFEBRABAN e mandou o seu slogan se deu bem!';
$lang['PREMIOS_VENCEDORES_TWITTER_TEXTO'] = <<<TEXTO
<p>O Ciab FEBRABAN touxe mais uma novidade para a edição 2011: o concurso cultural "Melhor
Slogan Ciab no Twitter". Para participar, bastava seguir o perfil @CiabFEBRABAN e enviar um
slogan com até 140 caracteres.</p>

<p>Foram 37 slogans recebidos e 3 premiados com ingressos válidos para os três dias do Ciab
FEBRABAN 2011, com direito a participar da exposição e das palestras do congresso.</p>

<p>Continue seguindo o @CiabFEBRABAN e fique por dentro de tudo o que acontece no maior
Congresso e Exposição de Tecnologia da Informação da América Latina.</p>
TEXTO;

$lang['PREMIOS_VENCEDORES_2012_TITULO'] = 'Confira os vencedores do Prêmio CIAB 2012';
$lang['PREMIOS_VENCEDORES_2012_PROJETO'] = 'PROJETO';
$lang['PREMIOS_VENCEDORES_2012_GRUPO'] = 'GRUPO';
$lang['PREMIOS_VENCEDORES_2012_PROJETO_1'] = 'ProDeaf – Software Tradutor (Português/LIBRAS – LIBRAS/Português)';
$lang['PREMIOS_VENCEDORES_2012_GRUPO_1'] = <<<LIST
<p>- Marcelo Lucio Correia de Amorim</p>
<p>- Lucas Araújo Mello Soares</p>
<p>- João Paulo dos Santos Oliveira</p>
<p>- Luca Bezerra Dias</p>
<p>- Roberta Agra Coutelo</p>
<p>- Victor Rafael Nascimento dos Santos</p>
LIST;
$lang['PREMIOS_VENCEDORES_2012_PROJETO_2'] = 'Bidd – Plataforma móvel para contratação de crédito de consumo.';
$lang['PREMIOS_VENCEDORES_2012_GRUPO_2'] = <<<LIST
<p>- Daniel César Vieira Radicchi</p>
<p>- André Luis Ferreira Marques</p>
<p>- Willy Stadnick Neto</p>
<p>- Vinicius Abouhatem</p>
<p>- Daniel Coelho</p>
LIST;
$lang['PREMIOS_VENCEDORES_2012_PROJETO_3'] = 'My Tag – Rede social de consumo.';
$lang['PREMIOS_VENCEDORES_2012_GRUPO_3'] = <<<LIST
<p>- Holisson Soares da Cunha</p>
<p>- Sergio Luis Sardi Mergen</p>
<p>- Luciana Hikari Kuamoto</p>
<p>- Leonardo Seiji</p>
LIST;

/* IMPRENSA */
$lang['IMPRENSA_ASSESSORIA_TITULO'] = 'Assessoria de Imprensa';
$lang['IMPRENSA_ASSESSORIA_TEXTO_1'] = 'Para receber press releases, participar de coletivas e entrevistas, entre em contato conosco';
$lang['IMPRENSA_ASSESSORIA_TEXTO_2'] = 'Caso queira fazer a cobertura do evento, preencha o formulário abaixo e faça seu cadastro.<br> Nossos assessores entrarão em contato caso haja necessidade.';
$lang['IMPRENSA_ASSESSORIA_FORM_1'] = 'Nome (completo)';
$lang['IMPRENSA_ASSESSORIA_FORM_2'] = 'E-mail';
$lang['IMPRENSA_ASSESSORIA_FORM_3'] = 'MTB';
$lang['IMPRENSA_ASSESSORIA_FORM_4'] = 'Empresa';
$lang['IMPRENSA_ASSESSORIA_FORM_5'] = 'Site da Empresa';
$lang['IMPRENSA_ASSESSORIA_FORM_6'] = 'Área de Atuação';
$lang['IMPRENSA_ASSESSORIA_FORM_7'] = 'Seu Telefone';
$lang['IMPRENSA_ASSESSORIA_FORM_SUBMIT'] = 'ENVIAR MENSAGEM';
$lang['IMPRENSA_ASSESSORIA_FORM_OK'] = '<h1>Obrigado! <br> Email enviado com  ssucesso. Entraremos em contato em breve.</h1>';
$lang['IMPRENSA_ASSESSORIA_FORM_ERROR'] = '<h1>O email não pode ser enviado. <br> Tente novamente.</h1>';

/* INFORMAÇÕES */
$lang['INFO_PAVILHOES'] = 'Pavilhões A, B, C e D';
$lang['INFORMACOES_LOCAL_TITULO'] = 'Local do Evento';
$lang['INFORMACOES_LOCAL_VERMAPA'] = 'Exibir mapa ampliado';
$lang['INFORMACOES_HOTEIS_TITULO'] = 'Hotéis';
$lang['INFORMACOES_HOTEIS_SUBTITULO'] = 'Clique sobre o hotel para mais detalhes';
$lang['INFORMACOES_HOTEIS_LEGENDA'] = 'SGL: Acomoda 1 pessoa | DBL: Acomoda 2 pessoas';
$lang['INFORMACOES_HOTEIS_TABELA_TITULO_1'] = 'HOTEL';
$lang['INFORMACOES_HOTEIS_TABELA_TITULO_2'] = 'DIÁRIA SUPERIOR SGL';
$lang['INFORMACOES_HOTEIS_TABELA_TITULO_3'] = 'DIÁRIA SUPERIOR DBL';
$lang['INFORMACOES_HOTEIS_COMDESCONTO'] = 'já com desconto';
$lang['INFORMACOES_HOTEIS_LOCAL'] = 'Local';
$lang['INFORMACOES_HOTEIS_TEL'] = 'Tel';
$lang['INFORMACOES_HOTEIS_COMO_CHEGAR'] = 'Clique aqui para ver como chegar';
$lang['INFORMACOES_HOTEIS_CODIGO'] = 'Código para reserva';
$lang['INFORMACOES_HOTEIS_DIARIA_SGL'] = 'Diária de apartamento Superior SGL';
$lang['INFORMACOES_HOTEIS_DIARIA_DBL'] = 'Diária de apartamento Superior DBL';
$lang['INFORMACOES_HOTEIS_DIARIA'] = 'A diária (por adesão) inclui';
$lang['INFORMACOES_HOTEIS_OBSERVACOES'] = 'Observações';
$lang['INFORMACOES_HOTEIS_CHECK_1'] = '- Check-in a partir das 14h00<br>- Check-out até às 12h00';
$lang['INFORMACOES_HOTEIS_CHECK_2'] = 'Diárias iniciam às 12h e encerram às 12h';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_1'] = 'Café da manhã servido no restaurante (6h às 10h);';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_2'] = '- Café da manhã (quando servido no restaurante)<br>- Transfer Hotel/Transamérica Expo Center/Hotel<br>- 01 vaga na garagem por apartamento.';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_3'] = '- Café da manhã (quando servido no restaurante).<br>- Transfer Aeroporto de Congonhas / Hotel / <br>Aeroporto de Congonhas.';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_4'] = '- Café da manhã (quando servido no restaurante).<br>- 01 vaga na garagem por apartamento';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_5'] = '- Café da manhã (quando servido no restaurante).';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_1'] = 'O valor da diária será acrescido de 5% de ISS + R$ 4,50 de taxa de turismo e o pagamento das despesas de hospedagem deverá ser feita diretamente no Hotel Transamérica São Paulo com cartão de crédito ou cash.<br> Os apartamentos estão sujeito à disponibilidade.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_2'] = 'O valor da diária será acrescido de 5% de ISS + taxa de turismo e o pagamento das despesas de hospedagem deverá ser feita diretamente no Transamérica Executive Chácara Santo Antonio com cartão de Crédito ou Cash. <br> Os apartamentos estão sujeito à disponibilidade.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_3'] = 'O valor da diária será acrescido de 5% de ISS + taxa de turismo e o pagamento das despesas de hospedagem deverá ser feita diretamente no Transamérica Executive Congonhas com cartão de Crédito ou Cash<br> Os apartamentos estão sujeito à disponibilidade.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_4'] = 'O valor da diária será acrescido de 5% de ISS + taxa de turismo e o pagamento das despesas de hospedagem deverá ser feita diretamente no Transamérica Executive Congonhas com cartão de Crédito ou Cash.<br> Os apartamentos estão sujeito à disponibilidade.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_5'] = 'O valor da diária será acrescido de 5% de ISS + taxa de turismo e o pagamento das despesas de hospedagem deverá ser feita diretamente no Transamérica Executive Congonhas com cartão de Crédito ou Cash<br> Os apartamentos estão sujeito à disponibilidade.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_6'] = 'O valor da diária será acrescido de 5% de ISS + taxa de turismo e o pagamento das despesas de hospedagem deverá ser feita diretamente no Transamérica Executive Congonhas com cartão de Crédito ou Cash. <br> Os apartamentos estão sujeito à disponibilidade.';
$lang['CONTATO_INFORMACOES_GERAIS'] = 'Informações gerais';
$lang['CONTATO_COMO_CHEGAR'] = 'COMO CHEGAR';
$lang['CONTATO_RESERVAS'] = 'Reservas de espaços';
$lang['CONTATO_FORM_TITULO'] = 'Preencha o formulário abaixo para contato';
$lang['CONTATO_FORM_ASSUNTO_TITULO'] = 'Assunto';
$lang['CONTATO_FORM_ASSUNTO_1'] = 'Outros';
$lang['CONTATO_FORM_ASSUNTO_2'] = 'Congressista';
$lang['CONTATO_FORM_ASSUNTO_3'] = 'Expositor';
$lang['CONTATO_FORM_ASSUNTO_4'] = 'Visitante';
$lang['CONTATO_FORM_ASSUNTO_5'] = 'Patrocinador';
$lang['CONTATO_FORM_ASSUNTO_6'] = 'Imprensa';
$lang['CONTATO_FORM_1'] = 'Nome (completo)';
$lang['CONTATO_FORM_2'] = 'E-mail';
$lang['CONTATO_FORM_3'] = 'Empresa';
$lang['CONTATO_FORM_4'] = 'Site da Empresa';
$lang['CONTATO_FORM_5'] = 'Área de Atuação';
$lang['CONTATO_FORM_6'] = 'Seu Telefone';
$lang['CONTATO_FORM_7'] = 'Mensagem';
$lang['CONTATO_FORM_SUBMIT'] = 'ENVIAR';
$lang['CONTATO_FORM_OK'] = '<h1>Obrigado! <br>Email enviado com sucesso. Entraremos em contato em breve.</h1>';
$lang['CONTATO_FORM_ERROR'] = '<h1>O email não pode ser enviado. <br>Tente novamente.</h1>';

/***************/
/* PROGRAMAÇÃO */
/***************/

/* DIA 1 */
$lang['auditorio-1'] = <<<STR
AUDITÓRIO FEBRABAN
STR;

$lang['auditorio-2'] = <<<STR
AUDITÓRIO LINHA DE NEGÓCIOS
STR;

$lang['auditorio-3'] = <<<STR
AUDITÓRIO EFICIÊNCIA OPERACIONAL
STR;

$lang['obs-solenidade'] = <<<STR
Solenidade de abertura da Exposição e visita aos estandes dos Patrocinadores / Coffee break
STR;

$lang['1-sessao-1'] = <<<STR
1ª sessão manhã - das 09:00 às 10:30
STR;

$lang['1-sessao-2'] = <<<STR
2ª sessão manhã - das 11:00 às 12:30
STR;

$lang['1-sessao-3'] = <<<STR
1ª sessão tarde - das 14:00 às 15:30
STR;

$lang['1-sessao-4'] = <<<STR
2ª sessão tarde - das 16:00 às 18:00
STR;

$lang['DIA_1_MANHA_1_A'] = <<<STR
Abertura<br>
Murilo Portugal - FEBRABAN<br>
Luis Antonio Rodrigues - CIAB FEBRABAN<br>
STR;

$lang['DIA_1_MANHA_1_B'] = <<<STR
Transmissão simultânea da Abertura
STR;

$lang['DIA_1_MANHA_1_C'] = <<<STR
Transmissão simultânea da Abertura
STR;


$lang['DIA_1_MANHA_2_A'] = <<<STR
A importância da TI no Desenvolvimento do Sistema Financeiro<br>
Roberto Egydio Setúbal - Itaú Unibanco<br>
Moderador: Murilo Portugal - FEBRABAN<br>
STR;

$lang['DIA_1_MANHA_2_B'] = <<<STR
Transmissão simultânea
STR;

$lang['DIA_1_MANHA_2_C'] = <<<STR
Transmissão simultânea
STR;


$lang['DIA_1_TARDE_1_A'] = <<<STR
ArchiTechs: How to Live, Govern & Learn in a Hyper-connected World<br>
Rahaf Harfoush - Estrategista de Mídias Sociais<br>
Moderador: Cláudio Almeida Prado - Deutsche Bank
STR;

$lang['DIA_1_TARDE_1_B'] = <<<STR
O Banco na Nuvem - Discussão sobre a utilização de Cloud Computing pelos Bancos<br>
Peter Redshaw - Gartner<br>
Azeem Mohamed - HP<br>
Rodrigo Gazzaneo - EMC<br>
Moderador: Jair Vasconcelos Filho - CAIXA
STR;

$lang['DIA_1_TARDE_1_C'] = <<<STR
Consumerização<br>
Fernando Belfort - Frost & Sullivan<br>
Ghassan Dreibi Jr. - Cisco<br>
Moderadora: Francimara Teixeira Garcia Viotti - Banco do Brasil
STR;


$lang['DIA_1_TARDE_2_A'] = <<<STR
Inovação, Tecnologia da Informação e Comunicação<br>
Silvio Meira - Centro de Estudos e Sistemas Avançados do Recife (CESAR)<br>
Moderador: Milton Shizuo Noguchi - Itautec<br>
====================================== <br>
Delivering Convenience, Security and Innovation<br>
Thomas W. Swidarski - CEO Global da Diebold<br>
Moderador: Benito Luís Rossiti - TECBAN
STR;

$lang['DIA_1_TARDE_2_B'] = <<<STR
Big Data - Inteligência Analítica em cima de dados não estruturados<br>
Donald Feinberg - Gartner<br>
George Tziahanas - HP <br>
Patrícia Florissi - EMC<br>
Moderador: Armando Corrêa - Citibank
STR;

$lang['DIA_1_TARDE_2_C'] = <<<STR
Tecnologias Móveis na Era Conectada<br>
Paulo Marcelo Lessa Moreira - CPM Braxis Capgemini<br>
Michihiko Yoden - NTT Data<br>
Nuno Gomes - Booz & Company<br>
José Domingos Favoretto Jr. - CPqD<br>
Moderador: Jorge Vacarini Júnior - Deutsche Bank 
STR;



/* DIA 2 */
$lang['DIA_2_MANHA_1_A'] = <<<STR
O Banco e o Cliente do Futuro<br>
John Bates - Progress Software<br>
Maurício Machado de Minas - Bradesco<br>
Moderador: Ricardo Ribeiro Mandacaru Guerra - Itaú Unibanco 
STR;

$lang['DIA_2_MANHA_1_B'] = <<<STR
CIO's de Outras Indústrias e a Sociedade Conectada<br>
Pedro Paulo A.C. Cunha - Alelo<br>
Anderson Figueiredo – IDC<br>
João Lencioni  - GE<br>
Moderador: Keiji Sakai – BM&FBovespa
STR;

$lang['DIA_2_MANHA_1_C'] = <<<STR
Eficiência Operacional<br>
Ian Malone - EMC<br>
Walter Tadeu Pinto de Faria - FEBRABAN<br>
Marcelo Atique - CAIXA<br>
Moderador: Joaquim Kiyoshi Kavakama - CIP - Câmara Interbancária de Pagamentos
STR;



$lang['DIA_2_MANHA_2_A'] = <<<STR
Inovação e Criatividade<br>
Charles Bezerra - GAD'Innovation<br>
Moderador: Carlos Roberto Zanellato - HSBC
STR;

$lang['DIA_2_MANHA_2_B'] = <<<STR
O Banco na Era Conectada<br>
Rosie Fitzmaurice - RBR<br>
Dan Cohen - IBM <br>
Antranik Haroutiounian - Bradesco<br>
Moderador: David Alves de Melo - Itautec
STR;

$lang['DIA_2_MANHA_2_C'] = <<<STR
Desafios da formação de capital humano qualificado<br>
Prof. José Pastore<br>
Paulo Sérgio Sgobbi - Brasscom<br>
João Sabino - Fundação Bradesco<br>
Moderador: Fábio Cássio Costa Moraes - FEBRABAN
STR;


$lang['DIA_2_TARDE_1_A'] = <<<STR
As Áreas de Negócios dos Bancos falam sobre Tendências<br>
Paulo Nergi Boeira de Oliveira - Caixa<br>
Hideraldo Dwight Leitão - Banco do Brasil <br>
Arnaldo Nissental - Bradesco<br>
Gilberto de Abreu - Santander Brasil<br>
Moderador: Alexandre Gouvea - McKinsey
STR;

$lang['DIA_2_TARDE_1_B'] = <<<STR
Acessibilidade Digital<br>
Delfino Natal de Souza - Ministério do Planejamento, Orçamento e Gestão<br>
Karen Myers  - W3C - World Wide Web Consortium <br>
Sidinei Rossoni - CAIXA<br>
Liliane Vieira Moraes - CNPq - Conselho Nacional de Desenvolvimento Científico e Tecnológico<br>
Moderador: Carlos Alberto Brocchi de Oliveira Pádua - Diebold
STR;

$lang['DIA_2_TARDE_1_C'] = <<<STR
Segurança da Informação<br>
Kris Lovejoy - IBM<br>
Alberto Fávero - Ernst Young<br>
Americo Lobo Neto - BioLógica<br>
Moderador: Jorge Fernando Krug - Banrisul
STR;


$lang['DIA_2_TARDE_2_A'] = <<<STR
16h00 - 17h00<br>
O ambiente de Ameaça e a Nova Onda de TI<br>
Arthur W. Coviello Jr. - EMC<br>
Moderador: André Augusto de Lima Salgado - Citibank<br>
======================================<br>
17h00 - 18h00<br>
How IT Is Changing Our Future <br>
Miha Kralj - Microsoft<br>
Moderador: João Antonio Dantas Bezerra Leite - Itaú Unibanco
STR;

$lang['DIA_2_TARDE_2_B'] = <<<STR
Plataforma Analítica para Redes Sociais<br>
Katia Vaskys - IBM<br>
Zachary Aron - Deloitte<br>
Rodrigo Gonsales - Cisco<br>
Katia Furuie - Santander Brasil<br>
Moderador: Gustavo de Souza Fosse - Banco do Brasil
STR;

$lang['DIA_2_TARDE_2_C'] = <<<STR
Os Imperativos da Nova Gestão de Riscos<br>
Luis Arturo Diaz- Oracle (171)<br>
Oliver Cunningham - KPMG (172)<br>
Maria del Carmen Aleixandre - Accenture (173)<br>
Moderador: Ricardo Orlando - Itaú Unibanco
STR;



/* DIA 3 */
$lang['3-sessao-1'] = <<<STR
1ª sessão manhã - das 09:00 às 10:30
STR;

$lang['3-sessao-2'] = <<<STR
2ª sessão manhã - das 11:00 às 12:30
STR;

$lang['3-sessao-3'] = <<<STR
1ª sessão tarde - das 14:00 às 16:00
STR;

$lang['3-sessao-4'] = <<<STR
2ª sessão tarde - das 16:30 às 18:00
STR;

$lang['DIA_3_MANHA_1_A'] = <<<STR
Inovação na Indústria de TI para os Bancos<br>
Carlos Cunha - EMC<br>
Fábio Pessoa - IBM<br>
J. Thomas Elbling - Perto<br>
João Abud Junior - Diebold<br>
Luciano Corsini - HP<br>
Rui José Schoenberger - NTT Data<br>
Wilton Ruas da Silva - Itautec<br>
Moderadora: Cláudia Vassalo - Editora Abril
STR;


$lang['DIA_3_MANHA_1_B'] = <<<STR
Transmissão Simultânea
STR;

$lang['DIA_3_MANHA_1_C'] = <<<STR
Transmissão Simultânea
STR;


$lang['DIA_3_MANHA_2_A'] = <<<STR
Lideranças de TI debatem Tendências<br>
Aurélio Conrado Boni - Bradesco<br>
Joaquim Lima de Oliveira - Caixa<br>
Luis Antonio Rodrigues - Itaú Unibanco <br>
Marcelo Zerbinatti - Santander Brasil<br>
Moderador: Gustavo José Costa Roxo da Fonseca - Booz & Company
STR;

$lang['DIA_3_MANHA_2_B'] = <<<STR
Social Commerce<br>
Eduardo Galanternick - Magazine Luiza<br>
Luca Cavalcanti - Bradesco<br>
Gabriel Borges - Like Store<br>
Moderador: Ricardo Pomeranz - Rapp Brasil
STR;

$lang['DIA_3_MANHA_2_C'] = <<<STR
Desenvolvimento Ágil<br>
Pedro Britto - IBM<br>
Eduardo Mazon - Banco BMG<br>
Ricardo Ribeiro Mandacaru Guerra - Itaú Unibanco<br>
Moderador: Alberto Luiz Gerardi - Banco do Brasil
STR;


$lang['DIA_3_TARDE_1_A'] = <<<STR
O Brasil em Perspectiva<br>
Ilan Goldfajn - Itaú Unibanco<br>
Moderador: André Lahóz - Revista Exame

STR;

$lang['DIA_3_TARDE_1_B'] = <<<STR
Inovação em Auto Atendimento<br>
Carlos Alberto Brocchi de Oliveira Pádua - Diebold<br>
João Lo Ré Chagas - Itautec<br>
Fabrizio Pinna - Scopus<br>
Jorge Schtoltz - Banco do Brasil<br>
Moderadora: Kátia Militello - Info Exame
STR;

$lang['DIA_3_TARDE_1_C'] = <<<STR
Gamification - Como a mecânica do jogo transformará sua empresa e seu comportamento<br>
Peter Redshaw - Gartner<br>
Alcides de Francisco Ferreira - BM&Fbovespa<br>
Alexandre Winetzki - Woopi<br>
Moderador: Flávio Leomil Marietto - CPM Braxis Capgemini
STR;


$lang['DIA_3_TARDE_2_A'] = <<<STR
Palestra de encerramento
STR;

$lang['DIA_3_TARDE_2_B'] = <<<STR
Transmissão Simultânea
STR;

$lang['DIA_3_TARDE_2_C'] = <<<STR
Transmissão Simultânea
STR;

$lang['Auditórios Excelência em TI'] = 'Auditórios Excelência em TI';
$lang['Auditorios Horário'] = 'Horário';
$lang['Auditorios Palestrantes'] = 'Palestrantes';
$lang['Auditorios Tema'] = 'Tema';

/* DIA 1 */
$lang['dia_1 Auditorio 1'] = 'Auditório 3 - TALARIS';
$lang['dia_1 Auditorio 3'] = 'Auditório 3 - LÓGICA';
$lang['dia_1 Auditorio 2'] = 'Auditório 2 - Acesso Digital';

$lang['dia1_auditorio_2_horario_1'] = '14h30';
$lang['dia1_auditorio_2_horario_2'] = '14h40';
$lang['dia1_auditorio_2_horario_3'] = '15h00';
$lang['dia1_auditorio_2_horario_4'] = '15h30';
$lang['dia1_auditorio_2_horario_5'] = '16h00';
$lang['dia1_auditorio_2_palestrante_1'] = 'Antonio Augusto de Almeida Leite (Pancho), ACREFI';
$lang['dia1_auditorio_2_palestrante_2'] = 'Breno Costa, Sócio e Consultor da GoOn – A Evolução na Gestão do Risco';
$lang['dia1_auditorio_2_palestrante_3'] = 'Ricardo Batista, Superintendente de Crédito do Tribanco';
$lang['dia1_auditorio_2_palestrante_4'] = 'Roberto Jabali, Superintendente de Crédito do Grupo Citi - Credicard';
$lang['dia1_auditorio_2_palestrante_5'] = 'Alex Yamamoto,Consultor da Acesso Digital';
$lang['dia1_auditorio_2_tema_1'] = 'Abertura';
$lang['dia1_auditorio_2_tema_2'] = 'Case GoOn: Como aumentar o nível de decisão automática sem impacto na inadimplência.';
$lang['dia1_auditorio_2_tema_3'] = 'Case Tribanco: captura de documentos no varejo';
$lang['dia1_auditorio_2_tema_4'] = 'Case Grupo Citi: Pioneirismo na gestão eletrônica de documentos';
$lang['dia1_auditorio_2_tema_5'] = 'Formalização de crédito online nas instituições financeiras';

$lang['dia1_auditorio_3_horario_1'] = '09h00 - 10h00';
$lang['dia1_auditorio_3_horario_2'] = '11h00 – 12h00';
$lang['dia1_auditorio_3_horario_3'] = '14h00 - 15h10';
$lang['dia1_auditorio_3_horario_4'] = '15h20 – 16h30';
$lang['dia1_auditorio_3_horario_5'] = '16h40 – 17h50';
$lang['dia1_auditorio_3_horario_6'] = '17h50 -18h00';
$lang['dia1_auditorio_3_horario_7'] = ' ';
$lang['dia1_auditorio_3_palestrante_1'] = 'Paulo Martins, Diretor Comercial / Logica';
$lang['dia1_auditorio_3_palestrante_2'] = 'Reginaldo Silva, Portfólio / Logica';
$lang['dia1_auditorio_3_palestrante_3'] = 'Antonio Requejo, Regional Practice Leader Iberia & Latam – Security / Logica';
$lang['dia1_auditorio_3_palestrante_4'] = 'Júlio Gonçalves, Regional Practice Leader Iberia & Latam – IT Modernization / Logica';
$lang['dia1_auditorio_3_palestrante_5'] = 'Lode Snykers, Vice President, Global Financial Services / Logica';
$lang['dia1_auditorio_3_palestrante_6'] = 'Paulo Martins, Diretor Comercial / Logica';
$lang['dia1_auditorio_3_palestrante_7'] = ' ';
$lang['dia1_auditorio_3_tema_1'] = 'Compartilhando Idéias';
$lang['dia1_auditorio_3_tema_2'] = 'Mobilidade: Saiba como surpreender os seus clientes com soluções modernas e inovadoras';
$lang['dia1_auditorio_3_tema_3'] = 'De Securidad en TI para el Gerenciamento de Riesgo';
$lang['dia1_auditorio_3_tema_4'] = 'Modernização de Sistemas Legados';
$lang['dia1_auditorio_3_tema_5'] = 'The technological challenges of the Financial Sector in the XXI century';
$lang['dia1_auditorio_3_tema_6'] = 'Compartilhando Ideias II';
$lang['dia1_auditorio_3_tema_7'] = 'Sorteios para os participantes do dia';


/* DIA 2 */
$lang['dia_2 Auditorio 1'] = 'Auditório 1 - HP';
$lang['dia_2 Auditorio 2'] = 'Auditório 2 - Microsoft';
$lang['dia_2 Auditorio 3'] = 'Auditório 3 - CLM';

/* Auditorio 1 */
$lang['dia2_auditorio_1_horario_1'] = '10h00 – 11h00';
$lang['dia2_auditorio_1_horario_2'] = '11h10 – 12h10';
$lang['dia2_auditorio_1_horario_3'] = '14h30 – 15h30';
$lang['dia2_auditorio_1_horario_4'] = '15h40 – 16h00';
$lang['dia2_auditorio_1_palestrante_1'] = 'Mike Wright, HP';
$lang['dia2_auditorio_1_palestrante_2'] = 'Gladson Martins Russo, Nokia Siemens';
$lang['dia2_auditorio_1_palestrante_3'] = 'Andre Kalsing, HP';
$lang['dia2_auditorio_1_palestrante_4'] = 'Federico Grosso, Autonomy';
$lang['dia2_auditorio_1_tema_1'] = 'Tendências para Social CRM';
$lang['dia2_auditorio_1_tema_2'] = 'Novas tecnologias para Mobile Payment';
$lang['dia2_auditorio_1_tema_3'] = 'Benefícios da adoção de uma plataforma de serviços multicanal';
$lang['dia2_auditorio_1_tema_4'] = 'Uma nova Visão de Inovação e Big Data';


// Auditorio 2
$lang['dia2_auditorio_2_horario_1'] = '9h-10h';
$lang['dia2_auditorio_2_horario_2'] = '10h-11h30';
$lang['dia2_auditorio_2_horario_3'] = '11h30-12h30';
$lang['dia2_auditorio_2_horario_4'] = '14h-15h';
$lang['dia2_auditorio_2_horario_5'] = '15h-16h';
$lang['dia2_auditorio_2_horario_6'] = '16h-17h';
$lang['dia2_auditorio_2_horario_7'] = '17h-18h';
$lang['dia2_auditorio_2_palestrante_1'] = 'Fabio Souto';
$lang['dia2_auditorio_2_palestrante_2'] = 'Miha Krajl';
$lang['dia2_auditorio_2_palestrante_3'] = 'Jun Endo';
$lang['dia2_auditorio_2_palestrante_4'] = 'Eduardo Campos';
$lang['dia2_auditorio_2_palestrante_5'] = 'Roberto Prado';
$lang['dia2_auditorio_2_palestrante_6'] = 'Ricardo-Enrico Jahn';
$lang['dia2_auditorio_2_palestrante_7'] = 'Richard Chaves';
$lang['dia2_auditorio_2_tema_1'] = 'Sociedade Conectada e a Consumerização de TI';
$lang['dia2_auditorio_2_tema_2'] = 'Como TI está mudando nosso futuro';
$lang['dia2_auditorio_2_tema_3'] = 'Soluções Inovadoras para uma Experiência Única Multidevices & Consumerização de TI';
$lang['dia2_auditorio_2_tema_4'] = 'A nuvem nos seus termos - Estratégia de Computação na nuvem da Microsoft';
$lang['dia2_auditorio_2_tema_5'] = 'O profissional TI como protagonista da competitividade. Capacitação e Satisfação do cliente.';
$lang['dia2_auditorio_2_tema_6'] = 'Relacionamento e fidelização nas instituições financeiras com Dynamics CRM/XRM';
$lang['dia2_auditorio_2_tema_7'] = 'O poder da Inovação';

// Auditorio 3
$lang['dia2_auditorio_3_horario_1'] = '10h30 - 11h30';
$lang['dia2_auditorio_3_horario_2'] = '11h30 - 12h30';
$lang['dia2_auditorio_3_horario_3'] = '14h30 - 15h30';
$lang['dia2_auditorio_3_horario_4'] = '15h30 - 16h30';
$lang['dia2_auditorio_3_horario_5'] = '16h30 - 17h30';
$lang['dia2_auditorio_3_horario_6'] = '17h30 - 18h30';
$lang['dia2_auditorio_3_palestrante_1'] = 'Martin Roesch, SNORT';
$lang['dia2_auditorio_3_palestrante_2'] = 'Alex Silva, A10';
$lang['dia2_auditorio_3_palestrante_3'] = 'Carlos Fagundes, INTEGRALTRUST';
$lang['dia2_auditorio_3_palestrante_4'] = 'Sanjay Ramnath, BARRACUDA';
$lang['dia2_auditorio_3_palestrante_5'] = 'Dick Faulkner, SOPHOS';
$lang['dia2_auditorio_3_palestrante_6'] = 'Martin Roesch, SOURCEFIRE';
$lang['dia2_auditorio_3_tema_1'] = 'Open Source Security';
$lang['dia2_auditorio_3_tema_2'] = 'DDoS Attacks and Brute Force';
$lang['dia2_auditorio_3_tema_3'] = 'Segredos do Basileia III';
$lang['dia2_auditorio_3_tema_4'] = 'Application Security';
$lang['dia2_auditorio_3_tema_5'] = 'Cryptography and Mobile Security';
$lang['dia2_auditorio_3_tema_6'] = 'Next Generation Security';

/* DIA 3*/
$lang['dia_3 Auditorio 1'] = 'Auditório 1 - FEBRABAN';

$lang['dia3_auditorio_1_horario_1'] = '11h00 - 12h30';
$lang['dia3_auditorio_1_palestrante_1'] = 'Guilhermino Domiciano de Souza – Banco do Brasil;  Marcelo Ribeiro Câmara – Bradesco;  Cesar Faustino – Itaú Unibanco';
$lang['dia3_auditorio_1_tema_1'] = 'A Segurança Digital do Cidadão no Sistema Financeiro Brasileiro';
?>
