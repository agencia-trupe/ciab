<?php
/* TOPO */
$lang['EVENTO'] = 'EVENTO';
$lang['EVENTO_SUBMENU_1'] = 'Apresentação';
$lang['EVENTO_SUBMENU_2'] = 'O Congresso';
$lang['EVENTO_SUBMENU_3'] = 'Espaços Temáticos';
$lang['EVENTO_SUBMENU_4'] = 'Exposição';
$lang['EVENTO_SUBMENU_5'] = 'Quero Visitar';
$lang['EVENTO_SUBMENU_6'] = 'Quero Expor';
$lang['EVENTO_SUBMENU_7'] = 'Quero Patrocinar';
$lang['EVENTO_SUBMENU_8'] = 'Planta da Exposição';
$lang['EVENTO_SUBMENU_9'] = 'Outros Anos';
$lang['PROGRAMACAO'] = 'PROGRAMAÇÃO';
$lang['PALESTRANTES'] = 'PALESTRANTES';
$lang['PROMOÇÃO'] = 'PROMOÇÃO';
$lang['PROMOÇÃO2'] = 'Visite nosso estande no CIAB 2013, assista a uma demonstração e concorra a iPads. ESTANDE H15 - PAVILHÃO C';
$lang['PREMIOS'] = 'PRÊMIOS';
$lang['PREMIOS_SUBMENU_1'] = 'Prêmio Ciab 2013';
$lang['PREMIOS_SUBMENU_2'] = 'Vencedores 2011';
$lang['PREMIOS_SUBMENU_3'] = 'Vencedores 2012';
$lang['INSCRICOES'] = 'INSCRIÇÕES';
$lang['INSCRICOES_SUBMENU_1'] = 'Valores';
$lang['INSCRICOES_SUBMENU_2'] = 'Informações Importantes';
$lang['INSCRICOES_BANNER_TEXTO'] = 'Inscrições com desconto prorrogadas até 10.06 para filiados e não filiados. Aproveite e faça sua inscrição.';
$lang['NOTICIAS'] = 'NOTÍCIAS';
$lang['PUBLICACOES'] = 'PUBLICAÇÕES';
$lang['IMPRENSA'] = 'IMPRENSA';
$lang['IMPRENSA_SUBMENU_1'] = 'Assessoria de Imprensa';
$lang['INFORMACOES'] = 'INFORMAÇÕES';
$lang['INFORMACOES_SUBMENU_1'] = 'Local do Evento';
$lang['INFORMACOES_SUBMENU_2'] = 'Hospedagem';
$lang['CONTATO'] = 'CONTATO';
$lang['BUSCA'] = 'BUSCA';
$lang['ling_portugues'] = 'português';
$lang['ling_ingles'] = 'english';
$lang['ling_espanhol'] = 'español';
$lang['VOLTAR'] = 'VOLTAR';
$lang['Compartilhe'] = 'Compartilhe';
$lang['Quarta-Feira'] = 'Quarta-Feira';
$lang['Quinta-Feira'] = 'Quinta-Feira';
$lang['Sexta-Feira'] = 'Sexta-Feira';

/* FOOTER */
$lang['PATROCINADORES'] = 'PATROCINADORES';
$lang['DIAMANTE'] = 'DIAMANTE';
$lang['OURO'] = 'OURO';
$lang['PRATA'] = 'PRATA';
$lang['APOIO'] = 'APOIO';
$lang['FALE CONOSCO'] = 'FALE CONOSCO';
$lang['POLITICA DE PRIVACIDADE'] = 'POLÍTICA DE PRIVACIDADE';
$lang['TERMO DE USO'] = 'TERMO DE USO';
$lang['MAPA DO SITE'] = 'MAPA DO SITE';
$lang['address-linha1'] = '&copy; FEBRABAN - Brazilian Federation of Banks - 2013 - Todos os direitos reservados';
$lang['address-linha2'] = 'Av. Brig. Faria Lima, 1.485 - 14º andar • São Paulo • PABX .: 55 11 3244 9800 / 3186 9800 • FAX.: 55 11 3031 4106 •  <a href="http://www.febraban.org.br">WWW.FEBRABAN.ORG.BR</a>';

/* VEJA MAIS */
$lang['VEJA_MAIS_TITULO'] = "VEJA+MAIS";

/* PALESTRANTES */
$lang['PALESTRANTES_TEMAS_TITULO'] = 'Temas apresentados durante o evento';
$lang['PALESTRANTES_OUTROS'] = 'Outros Palestrantes';

/* NOTÍCIAS */
$lang['NOTICIAS_ULTIMAS_TITULO'] = 'Últimas Notícias';
$lang['NOTICIAS_VER_TODAS'] = 'VER TODAS';

/* HOME */
$lang['HOME_SLIDE_CMEP'] = 'Meios de Pagamento e a Sustentabilidade nas Relações de Consumo é o tema principal do 7º CMEP, promovido pela FEBRABAN.';
$lang['HOME_SLIDE_1'] = 'O CIAB FEBRABAN - Congresso e Exposição de Tecnologia da Informação das Instituições Financeiras  é o maior evento da América Latina tanto para o setor financeiro quanto para a área de Tecnologia.';
$lang['HOME_SLIDE_2'] = 'Gestão e Recuperação de Crédito - PJ / PF, Prevenção sobre Crimes de Lavagem de Dinheiro, Metodologias de Análise e Inspeção em Fraudes Bancárias, Excelência nos Serviços de Atendimento a Clientes, Análise Setorial com análise de riscos, e ...';
$lang['HOME_SLIDE_3'] = 'Uma oportunidade única para discutir os desafios da indústria e questões específicas para a comunidade latino-americana, juntamente com uma atualização sobre as principais iniciativas SWIFT que exercem impacto na região.';
$lang['HOME_SLIDE_4'] = 'Em sua 13ª edição, o Congresso FEBRABAN de Auditoria Interna e Compliance traz à discussão a perspectiva tridimensional sobre o tema que compreende maior cobertura, maior profundidade nas análises e a garantia de qualidade ...';
$lang['BOX_INSCRICOES_TITULO'] = 'PALESTRAS';
$lang['BOX_INSCRICOES_TEXTO'] = 'Congressista, clique aqui e faça o download das palestras do CIAB 2013.';
$lang['BOX_BLOG_TITULO'] = 'PROMOÇÃO';
$lang['BOX_BLOG_TEXTO'] = 'Visite nosso estande no CIAB 2013, assista a uma demonstração e concorra a iPads. ESTANDE H15 - PAVILHÃO C';
$lang['QUERO_PATROCINAR_TITULO'] = 'QUERO PATROCINAR';
$lang['QUERO_PATROCINAR_TEXTO'] = 'Sua empresa pode patrocinar o maior congresso e exposição da América Latina.';
$lang['QUERO_EXPOR_TITULO'] = 'QUERO EXPOR';
$lang['QUERO_EXPOR_TEXTO'] = 'CIAB FEBRABAN conta com a participação de centenas de empresas expositoras.';
$lang['QUERO_VISITAR_TITULO'] = 'BLOG CIAB';
$lang['QUERO_VISITAR_TEXTO'] = 'Acesse e discuta os mais variados temas ligados à Tecnologia da Informação.';
$lang['AGENDA_TITULO'] = 'AGENDA';
$lang['AGENDA_TEXTO'] = 'Confira a agenda de palestrantes nacionais e internacionais que estarão presentes.';
$lang['NOTICIAS_TITULO'] = 'NOTÍCIAS';
$lang['NENHUMA_NOTICIA'] = 'Nenhuma notícia cadastrada';
$lang['EVENTO_TITULO'] = 'O EVENTO';
$lang['EVENTO_SUBTITULO'] = 'O que é CIAB FEBRABAN';
$lang['EVENTO_TEXTO'] = 'Congresso e Exposição de Tecnologia da Informação das Instituições Financeiras - o maior evento da América Latina...';
/*****************************************************/
$lang['PUBLICACOES_TITULO'] = 'PUBLICAÇÕES';
$lang['PUBLICACOES_IMAGEM'] = '_imgs/home/ImgHome.jpg';
$lang['PUBLICACOES_SUBTITULO'] = 'Debates no Ciab FEBRABAN';
$lang['PUBLICACOES_TEXTO'] = 'Especialistas dos maiores bancos do Brasil e fornecedores de TI falaram sobre o futuro das instituições financeiras no País.';
/*****************************************************/
$lang['TWITTER_TITULO'] = 'TWITTER CIABFEBRABAN';
$lang['NEWSLETTER_TITULO'] = 'NEWSLETTER';
$lang['NEWSLETTER_TEXTO'] = 'Assine e receba todas as notividades';
$lang['NEWSLETTER_PLACEHOLDER_NOME'] = 'Informe seu nome';
$lang['NEWSLETTER_PLACEHOLDER_EMAIL'] = 'seu e-mail';
$lang['NEWSLETTER_SUBMIT'] = 'ENVIAR CADASTRO';

/* INSCRIÇÕES */
$lang['INSCRICOES_TITULO'] = 'Preços de Inscrições e Bonificações de Inscrições';
$lang['INSCRICOES_TITULO_TABELA_1'] = 'Valores por pessoa para inscrições nos 3 dias do Congresso';
$lang['DATA_ATE'] = 'Até';
$lang['DATA_APOS'] = 'Após';
$lang['INSCRICOES_TABELA_INSCRICOES'] = 'Inscrições';
$lang['INSCRICOES_TABELA_FILIADOS'] = 'Bancos filiados';
$lang['INSCRICOES_TABELA_NAO_FILIADOS'] = 'Não filiados';
$lang['INSCRICOES_TABELA_LINHA_1'] = 'Até 5';
$lang['INSCRICOES_TABELA_LINHA_2'] = 'de 6 a 10';
$lang['INSCRICOES_TABELA_LINHA_3'] = 'de 11 a 20';
$lang['INSCRICOES_TABELA_LINHA_4'] = 'mais de 20';
$lang['INSCRICOES_TABELA_RODAPE'] = 'EMPRESAS COM O MESMO CNPJ VALORES POR PARTICIPANTE';
$lang['INSCRICOES_TITULO_TABELA_2'] = 'Valores por pessoa para inscrições para 1 dia do Congresso';
$lang['INSCRICOES_OBSERVACAO_TITULO'] = 'Observação';
$lang['INSCRICOES_OBSERVACAO_TEXTO'] = 'No valor da inscrição estão incluídos: almoços, coffee breaks , coquetel do dia 20/06 e estacionamento nos 3 dias de realização do evento.';
$lang['INSCRICOES_OBSERVACAO_ITEM_1'] = 'Os descontos são válidos somente para inscrições na mesma categoria (participação nos 3 dias do evento ou participação em apenas um dia do evento).';
$lang['INSCRICOES_OBSERVACAO_ITEM_2'] = 'Manteremos o valor da 1ª. Fase para o banco que fizer mais do que o equivalente a 50 inscrições válidas para os três dias do evento. (ex.: 50 inscrições para participação nos 3 dias x R$ 2.178,00 = R$ 108.900,00 ou 124 inscrições para participação em um dia = R$ 108.872,00).';
$lang['INSCRICOES_BONIFICACAO_TITULO'] = 'Bonificação';
$lang['INSCRICOES_BONIFICACAO_ITEM_1'] = 'A cada 20 inscrições a instituição recebe 01 (uma) inscrição cortesia de um dia;';
$lang['INSCRICOES_BONIFICACAO_ITEM_2'] = 'Instituições que fizerem mais de 50 (cinqüenta) inscrições e aumentarem o total de inscritos em relação a 2011 serão bonificadas, conforme abaixo:';
$lang['INSCRICOES_BONIFICACAO_TABELA_TITULO_1'] = 'até 10%';
$lang['INSCRICOES_BONIFICACAO_TABELA_TITULO_2'] = 'Acima de 10% até 20%';
$lang['INSCRICOES_BONIFICACAO_TABELA_TITULO_3'] = 'Acima de 20% até 30%';
$lang['INSCRICOES_BONIFICACAO_TABELA_TITULO_4'] = 'Acima de 30%';
$lang['INSCRICOES_BONIFICACAO_TABELA_TEXTO_1'] = '5 inscr. de 1 dia';
$lang['INSCRICOES_BONIFICACAO_TABELA_TEXTO_2'] = '10 inscr. de 1 dia';
$lang['INSCRICOES_BONIFICACAO_TABELA_TEXTO_3'] = '15 inscr. de 1 dia';
$lang['INSCRICOES_BONIFICACAO_TABELA_TEXTO_4'] = '20 inscr. de 1 dia';
$lang['INSCRICOES_BONIFICACAO_TABELA_LEGENDA'] = '(*) A Tabela de Bonificação é válida exclusivamente para os acréscimos de inscrições em relação a 2011, conforme as faixas estabelecidas, e somente para empresas que fizeram acima de 50 (cinquenta) inscrições no Ciab 2012 .<br>(**) Para consultar a quantidade de inscrições realizadas em 2011, pedimos contatar a Diretoria de<br>Eventos pelo telefone (11) 3186-9860.';
$lang['INSCRICOES_EXEMPLO_TITULO'] = 'Exemplo';
$lang['INSCRICOES_BONIFICACAO_TEXTO'] = 'Instituição Y realizou em 2011 50 inscrições para os 3 dias do evento.<br> Em 2012, realizou 66 inscrições para os 3 dias.<br> Isso quer dizer que o banco Y tem direito à bonificação de 30%.<br>';
$lang['INSCRICOES_BONIFICACAO_TEXTO_NEGRITO'] = 'As bonificações adquiridas pela Instituição Y são';
$lang['INSCRICOES_BONIFICACAO_ITEM_EXEMPLO_1'] = 'Desconto de 35% para a mesma categoria (3 dias ou 1 dia) até o último dia de realização do Congresso, conforme disponibilidade de vagas.';
$lang['INSCRICOES_BONIFICACAO_ITEM_EXEMPLO_2'] = 'Gratuidade de 3 inscrições para participação em 1 dia do Congresso (a cada 20 inscrições 01 gratuidade).';
$lang['INSCRICOES_BONIFICACAO_ITEM_EXEMPLO_3'] = 'Gratuidade de 20 inscrições para participação em 1 dia do Congresso, conforme tabela de bonificação';
$lang['INSCRICOES_INFORMACOES_TITULO'] = 'Informações Importantes';
$lang['INSCRICOES_OPCOES_TITULO'] = 'Opções de Pagamento';
$lang['INSCRICOES_OPCOES_ITEM_1'] = '- Boleto Bancário';
$lang['INSCRICOES_OPCOES_ITEM_2'] = '- Cartões de Crédito Nacionais e Internacionais';
$lang['INSCRICOES_REEMBOLSO_TITULO'] = 'Reembolsos e Cancelamentos';
$lang['INSCRICOES_REEMBOLSO_TEXTO'] = 'Os pedidos de reembolso ou cancelamento de inscrições serão acatados, mediante solicitação por escrito endereçada para o e-mail eventos@febraban.org.br até o dia 31 de maio de 2012.<br> Os reembolsos serão efetuados pelo valor líquido, deduzidas todas as despesas bancárias provenientes da transferência bancária.<br> A partir de 1º de junho de 2012 não serão aceitos pedidos de reembolso ou cancelamentos. ';
$lang['INSCRICOES_SUBSTITUICAO_TITULO'] = 'Substituição de Participante';
$lang['INSCRICOES_SUBSTITUICAO_TEXTO'] = 'A FEBRABAN somente aceitará a substituição de participante desde que a solicitação seja feita por e-mail dirigido ao endereço eletrônico eventos@febraban.org.br até o dia 15.06.2012.<br> Para substituições solicitadas durante a realização do evento a Organização cobrará uma taxa de R$ 250,00 pela reemissão do crachá. O respectivo pagamento deverá ser efetuado no ato da <br> solicitação. ';
$lang['INSCRICOES_CONFIRMACAO_TITULO'] = 'Confirmação de Inscrição Via E-Mail';
$lang['INSCRICOES_CONFIRMACAO_TEXTO'] = 'Após realizada a inscrição, o participante receberá automaticamente por e-mail disparado pelo sistema de inscrições a sua confirmação. <br> Caso não receba essa confirmação, o participante ou a área responsável pela inscrição poderá<br> entrar em contato com a Diretoria de Eventos da FEBRABAN pelo e-mail eventos@febraban.org.br,<br> solicitando a verificação.';
$lang['INSCRICOES_ENCERRAMENTO_TITULO'] = 'Encerramento das Inscrições';
$lang['INSCRICOES_ENCERRAMENTO_TEXTO'] = '15.06.2012 ou até o encerramento das vagas. ';

/* EVENTO */
$lang['EVENTO_APRESENTACAO_TITULO'] = 'Apresentação';
$lang['EVENTO_APRESENTACAO_TEXTO'] = <<<TEXTO
<p>
O CIAB FEBRABAN - Congresso e Exposição de Tecnologia da Informação das Instituições Financeiras - é o maior evento da América Latina tanto para o setor financeiro quanto para a área de Tecnologia.
</p>
<p>
Foi criado em 1990 e desde a sua primeira edição em 1991, vem incentivando o desenvolvimento da tecnologia e inovação bancária. Anualmente, o congresso reúne cerca de 1,9 mil representantes de bancos do Brasil e de outros países. Apresenta cerca de 120 personalidades entre conferencistas e debatedores em mais de 30 painéis. 
</p>
<p>
A área de exposições reune cerca de 200 empresas fornecedoras de tecnologia e inovação corporativas, em um espaço total superior a quatro mil metros quadrados, atraindo uma visitação anual superior a 18 mil executivos e diretores de instituições financeiras e outras áreas de tecnologia e inovação. 
</p>
<p>
A 23ª edição do CIAB FEBRABAN terá como tema central “Os Novos Desafios do Setor Financeiro” e será realizada nos dias 12, 13 e 14 de Junho e 2013, no Transamérica Expo Center em São Paulo.
</p>
<p>
<p class="strong">O melhor do Ciab FEBRABAN 2012</p>
<p>
A 22ª edição do Ciab FEBRABAN, realizada entre os dias 20 e 22 de Junho de 2012, foi uma das maiores e mais concorridas de sua história. A área recorde de exposições de 4.500 m² teve uma visitação também recorde de 19 mil representantes de instituições financeiras, empresas de Tecnologia da Informação e parceiros. Foram 149 empresas expositoras que trouxeram muitas novidades, incluindo a presença de 31 empresas estrangeiras que estrearam no mercado brasileiro. 
</p>
<p>
Mais de 1.700 congressistas compareceram aos 28 painéis de palestras e debates ministrados por 110 especialistas, sendo que 19 deles foram keynote speakers internacionais, debatendo assuntos que permearam o tema central “A Sociedade Conectada”.
</p>
<p>
Também é destaque em 2012 a terceira edição do prêmio Ciab FEBRABAN, cujo objetivo reconhecer jovens talentos que desenvolvem soluções inovadoras, e o Prêmio do Espaço Inovação.
</p>
TEXTO;
$lang['EVENTO_SOBRE_TITULO'] = 'Sobre a FEBRABAN';
$lang['EVENTO_SOBRE_TEXTO'] = <<<TEXTO
<p>
A Federação Brasileira de Bancos – FEBRABAN é a principal entidade representativa do setor bancário. Foi fundada em 1967 visando o fortalecimento do sistema financeiro e de suas relações com a sociedade de modo. Reúne 120 bancos associados de um universo de 172 instituições em operação no Brasil, os quais representam 97% dos ativos totais e 93% do patrimônio líquido do sistema bancário. Entre os objetivos estratégicos da Federação estão o desenvolvimento de iniciativas para a contínua melhoria da produtividade do sistema bancário e a redução dos seus níveis de risco, contribuindo dessa forma para o desenvolvimento econômico, social e sustentável do País. 
</p>
<p>
	Saiba mais em <a href='http://www.febraban.org.br' title='FEBRABAN' target='_blank'>www.febraban.org.br</a>
</p>
TEXTO;
$lang['EVENTO_CONGRESSO_TITULO'] = 'O Congresso';
$lang['EVENTO_CONGRESSO_TEXTO'] = <<<TEXTO
<p>
Realizado paralelamente à Exposição, o Congresso Ciab FEBRABAN acontece em 3 auditórios que contam com os melhores e mais renomados especialistas nacionais e internacionais da área de tecnologia.
</p>
<p>
	<strong>Números do Congresso Ciab 2012 - A Sociedade Conectada</strong>
</p>
<p>
	&bull; 110 especialistas das áreas de tecnologia e bancária<br>
	&bull; 19 keynote speakers internacionais<br>
	&bull; 1.717 congressistas
</p>
<p>
Com estes números, o Congresso Ciab FEBRABAN 2012 debateu temas importantes como inovação, estratégia de mídias sociais, Cloud Computing, desenvolvimento ágil, Big Data, consumerização, tecnologias móveis, acessibilidade, segurança da informação, além de painéis com CIOs e diretores de demais áreas de negócios das instituições financeiras.
</p>
<p>
Entre as mais destacadas participações, os congressistas apontaram a conferência do presidente do Itaú Unibanco, Roberto Setubal, realizada dia 20 de junho sobre o painel “A importância da TI no desenvolvimento do sistema financeiro”.
</p>
TEXTO;
$lang['EVENTO_ESPACOS_TITULO'] = 'Espaços Temáticos';
$lang['EVENTO_ESPACOS_TEXTO'] = '<p> Ciab FEBRABAN reforça, novamente, suas ações voltadas para as pequenas empresas, visando estimular a presença desse segmento no congresso. </p>';
$lang['EVENTOS_ESPACOS_ITEM_1_TITULO'] = 'ESPAÇO EMPREENDEDOR';
$lang['EVENTOS_ESPACOS_ITEM_1_TEXTO'] = 'Espaço dedicado à exposição de empresas com capital 100% nacional, de pequeno porte e faturamento limitado a R$ 12 milhões. A empresa expositora conta com pacote composto por espaço na feira e montagem do estande a preços reduzidos.';
$lang['EVENTOS_ESPACOS_ITEM_2_TITULO'] = 'ESPAÇO INOVAÇÃO';
$lang['EVENTOS_ESPACOS_ITEM_2_TEXTO'] = "A FEBRABAN e o ITS (Instituto de Tecnologia de Software e Serviços) promovem, durante o Ciab FEBRABAN, o “Espaço Inovação”. Divulgue esta iniciativa e participe do processo de seleção! Para mais informações, envie email para <a href='mailto:eventos@febraban.org.br'>eventos@febraban.org.br</a>.";
$lang['EVENTOS_ESPACOS_ITEM_3_TITULO'] = 'ESPAÇO INTERNACIONAL';
$lang['EVENTOS_ESPACOS_ITEM_3_TEXTO'] = 'Espaço dedicado à exposição de empresas que não tem sede no Brasil, com montagem do estande e toda infraestrutura.';

$lang['EVENTO_EXPOSICAO_TITULO'] = 'Exposição';
$lang['EVENTO_EXPOSICAO_TEXTO'] = <<<TEXTO
<p>
Simultaneamente ao congresso, o CIAB FEBRABAN realiza sua exposição que conta com a participação de centenas de empresas expositoras das áreas de finanças, tecnologia da informação (TI) e telecomunicações.
</p>

<p>
O visitante terá a oportunidade de conferir todas as novidades e soluções tecnológicas para os próximos anos. Visite também os Espaços Temáticos, uma oportunidade única de prestigiar as pequenas empresas nacionais da área de TI. 
</p>

<p>
A visitação à exposição é gratuita e ocorre no horário das 9h às 20h. Basta fazer o seu cadastro no link abaixo ou diretamente no local, na área “Credenciamento de Visitantes”. 
</p>

<p>
<span class="negrito">Importante:</span> o crachá de visitante é válido somente para acesso ao pavilhão de exposição. Em nenhuma hipótese será permitida a entrada de visitantes nas áreas exclusivas de congressistas e convidados.
</p>
TEXTO;
$lang['EVENTO_QUERO_VISITAR_TITULO'] = 'Quero Visitar';
$lang['EVENTO_QUERO_VISITAR_TEXTO_1'] = <<<TEXTO
<p>
Compareça no Transamérica Expo Center e faça o seu cadastro na área “Credenciamento de Visitantes”. Pegue o seu crachá e confira as soluções que os expositores da feira trazem até você.  A exposição Ciab FEBRABAN acontece simultaneamente ao congresso, no horário das 9h ás 20h.
</p>

<p>
<span class="negrito">Importante:</span> o crachá de visitante é válido somente para o acesso ao pavilhão de exposição. Em nenhuma hipótese será permitida a entrada de visitantes nas áreas exclusivas de congressistas e convidados.
</p>
TEXTO;
$lang['EVENTO_QUERO_VISITAR_TEXTO_2'] = <<<TEXTO
<p style="clear:left;">
Se preferir, clique no botão abaixo, preencha o formulário e faça agora mesmo seu cadastro.
</p>
<a href="http://www.credenciamento.com.br/2012/VisitanteCiab/Default_por.aspx" target="_blank" class="botao" style="color:#FFF;">CLIQUE AQUI E CADASTRE-SE</a>
TEXTO;
$lang['EVENTO_EXPOR_TITULO'] = 'Quero Expor';
$lang['EVENTO_EXPOR_TEXTO'] = <<<TEXTO
<p>
Centenas de empresas das áreas de finanças, tecnologia da informação (TI) e telecomunicações, anualmente expõem no CIAB FEBRABAN e têm a oportunidade de estreitar seu relacionamento com milhares de pessoas que transitam passam pelo evento.
</p>
<p>
No lançamento do Ciab 2013, em outubro, 81 expositores garantiram seus espaços nos 3.770m² da área de exposição. 
</p>
TEXTO;
$lang['EVENTO_EXPOR_FORM_TEXTO'] = 'Preencha o formulário abaixo, faça agora mesmo o seu cadastro e reserve o espaço da sua empresa no evento. Nossos assessores entrarão em contato para passar todas as informações de como se tornar um expositor.';
$lang['EVENTO_EXPOR_FORM_1'] = 'Escolha o assunto';
$lang['EVENTO_EXPOR_FORM_2'] = 'Nome (completo)';
$lang['EVENTO_EXPOR_FORM_3'] = 'E-mail ';
$lang['EVENTO_EXPOR_FORM_4'] = 'Empresa';
$lang['EVENTO_EXPOR_FORM_5'] = 'Site da Empresa';
$lang['EVENTO_EXPOR_FORM_6'] = 'Área de Atuação';
$lang['EVENTO_EXPOR_FORM_7'] = 'Seu Telefone';
$lang['EVENTO_EXPOR_FORM_8'] = 'Espaço/Metragem de interesse';
$lang['EVENTO_EXPOR_FORM_SUBMIT'] = 'ENVIAR';
$lang['EVENTO_EXPOR_FORM_OBS'] = 'Para informações adicionais e reservas de  Espaços fale com:';
$lang['EVENTO_EXPOR_FORM_OK'] = '<h1>Obrigado!<br>Email enviado com sucesso.entraremos em contato em breve.</h1>';
$lang['EVENTO_EXPOR_FORM_ERROR'] = '<h1>O email não pode ser enviado. <br> Tente novamente.</h1>';
$lang['EVENTO_PATROCINAR_TITULO'] = 'Quero Patrocinar';
$lang['EVENTO_PATROCINAR_TEXTO'] = <<<TEXTO
<p>
A sua empresa pode ser uma das patrocinadoras do maior congresso e exposição da América Latina em tecnologia da informação para instituições financeiras, e expor a sua marca para milhares de congressistas e visitantes que frequentam os 3 dias do Ciab FEBRABAN.
</p>

<p>
Em 2012, a exposição do Ciab FEBRABAN recebeu a visita de um público superior a 17 mil pessoas, altamente qualificado, composto por presidentes, diretores e gerentes de instituições financeiras do Brasil e de mais de 26 países.
</p>
TEXTO;
$lang['EVENTO_PATROCINAR_OBS'] = 'Para informações adicionais e reservas de espaços fale com:';
$lang['EVENTO_MAPA_EXPOSITORES'] = 'EXPOSITORES';
$lang['EVENTO_ANOS_TITULO'] = 'Outros Anos';
$lang['EVENTO_ANOS_SUBTITULO'] = 'Veja o que aconteceu nas edições anteriores do evento';
$lang['EVENTO_ANOS_ITEM_1'] = 'A Tecnologia Além da Web';
$lang['EVENTO_ANOS_ITEM_2'] = 'A geração Y<br> um novo banco para<br> um novo consumidor';
$lang['EVENTO_ANOS_ITEM_3'] = 'Uma ampla discussão sobre<br> a tecnologia e seus impactos<br> na inclusão bancária';
$lang['EVENTO_ANOS_ITEM_4'] = 'Tecnologia e Segurança';

/* PRÊMIOS */
$lang['PREMIOS_CIAB_TITULO'] = 'Prêmio Ciab 2013';
$lang['PREMIOS_CIAB_SUBTITULO'] = 'PRÊMIO CIAB FEBRABAN! PREPARE-SE PARA SER O VENCEDOR DE 2013!';
$lang['Menção Honrosa'] = 'Menção Honrosa';
$lang['PREMIOS_CIAB_TEXTO'] = <<<TEXTO
<p>O Prêmio CIAB FEBRABAN visa o incentivo direto da capacidade de inovação técnica de jovens e universitários e o debate de propostas inovadoras. Fale com seus amigos e, sozinho ou em grupo, concorra a um total de R$ 25mil em prêmios, além do trabalho vencedor ser exposto para diversos especialistas e executivos das áreas de tecnologia e bancária.</p>
<p>O tema para 2013 é <strong>“Soluções para Eficiência Operacional”</strong></p>
<p style='color:red'><strong>Inscrições encerradas.</strong></p>
<p style='color:red'><strong>IMPORTANTE</strong>: somente para os inscritos no prêmio Ciab 2013, o prazo para envio dos trabalhos ou correções de trabalhos já enviados foi <strong>postergado para o dia 10.05.13.</strong></p>
TEXTO;
$lang['PREMIOS_CIAB_CHAMADA'] = 'Clique no botão abaixo e confira o regulamento.';
$lang['PREMIOS_CIAB_BOTAO_1'] = 'VER REGULAMENTO';
$lang['PREMIOS_CIAB_BOTAO_2'] = 'LEIA O REGULAMENTO';
$lang['PREMIOS_VENCEDORES_TITULO'] = 'Vencedores CIAB 2011';
$lang['PREMIOS_VENCEDORES_SUBTITULO'] = 'Vencedores do Prêmio CIAB FEBRABAN e Universia 2011 - A Tecnologia Além da Web';
$lang['PREMIOS_VENCEDORES_TEXTO_1'] = '<p>A edição 2011 do Prêmio CIAB FEBRABAN e Universia (que teve como tema “A Tecnologia Além da Web”)
            registrou 611 inscrições contra 223 na primeira edição do prêmio em 2010. Confira abaixo os vencedores.</p>';
$lang['PREMIOS_VENCEDORES_COMUNICADO'] = 'COMUNICADO – RECLASSIFICAÇÃO PRÊMIO CIAB FEBRABAN e UNIVERSIA 2011';
$lang['PREMIOS_VENCEDORES_TEXTO_2'] = <<<TEXTO
<p>A comissão organizadora do Ciab FEBRABAN 2011, com base nos critérios do regulamento do Prêmio
Ciab FEBRABAN e Universia 2011, reavaliando os concorrentes, concluiu que o trabalho classificado
anteriormente em primeiro lugar (2Whish) foi desclassificado por não cumprir com requisitos estabelecidos
no regulamento.</p>

<p>Portanto, a classificação final do prêmio, passa a ser a seguinte:</p>
TEXTO;
$lang['PREMIOS_VENCEDORES_TABELA_1'] = 'Classificação';
$lang['PREMIOS_VENCEDORES_TABELA_2'] = 'Nome';
$lang['PREMIOS_VENCEDORES_TABELA_3'] = 'Título do Trabalho';
$lang['PREMIOS_VENCEDORES_TWITTER_TITULO'] = 'Vencedores TWITTER 2011';
$lang['PREMIOS_VENCEDORES_TWITTER_SUBTITULO'] = 'Concurso Cultural Melhor Slogan CIAB no Twitter';
$lang['PREMIOS_VENCEDORES_TWITTER_CHAMADA'] = 'Quem seguiu o @CiabFEBRABAN e mandou o seu slogan se deu bem!';
$lang['PREMIOS_VENCEDORES_TWITTER_TEXTO'] = <<<TEXTO
<p>O Ciab FEBRABAN touxe mais uma novidade para a edição 2011: o concurso cultural "Melhor
Slogan Ciab no Twitter". Para participar, bastava seguir o perfil @CiabFEBRABAN e enviar um
slogan com até 140 caracteres.</p>

<p>Foram 37 slogans recebidos e 3 premiados com ingressos válidos para os três dias do Ciab
FEBRABAN 2011, com direito a participar da exposição e das palestras do congresso.</p>

<p>Continue seguindo o @CiabFEBRABAN e fique por dentro de tudo o que acontece no maior
Congresso e Exposição de Tecnologia da Informação da América Latina.</p>
TEXTO;

$lang['PREMIOS_VENCEDORES_2012_TITULO'] = 'Confira os vencedores do Prêmio CIAB 2012';
$lang['PREMIOS_VENCEDORES_2012_PROJETO'] = 'PROJETO';
$lang['PREMIOS_VENCEDORES_2012_GRUPO'] = 'GRUPO';
$lang['PREMIOS_VENCEDORES_2012_PROJETO_1'] = 'ProDeaf – Software Tradutor (Português/LIBRAS – LIBRAS/Português)';
$lang['PREMIOS_VENCEDORES_2012_GRUPO_1'] = <<<LIST
<p>- Marcelo Lucio Correia de Amorim</p>
<p>- Lucas Araújo Mello Soares</p>
<p>- João Paulo dos Santos Oliveira</p>
<p>- Luca Bezerra Dias</p>
<p>- Roberta Agra Coutelo</p>
<p>- Victor Rafael Nascimento dos Santos</p>
LIST;
$lang['PREMIOS_VENCEDORES_2012_PROJETO_2'] = 'Bidd – Plataforma móvel para contratação de crédito de consumo.';
$lang['PREMIOS_VENCEDORES_2012_GRUPO_2'] = <<<LIST
<p>- Daniel César Vieira Radicchi</p>
<p>- André Luis Ferreira Marques</p>
<p>- Willy Stadnick Neto</p>
<p>- Vinicius Abouhatem</p>
<p>- Daniel Coelho</p>
LIST;
$lang['PREMIOS_VENCEDORES_2012_PROJETO_3'] = 'My Tag – Rede social de consumo.';
$lang['PREMIOS_VENCEDORES_2012_GRUPO_3'] = <<<LIST
<p>- Holisson Soares da Cunha</p>
<p>- Sergio Luis Sardi Mergen</p>
<p>- Luciana Hikari Kuamoto</p>
<p>- Leonardo Seiji</p>
LIST;

/* IMPRENSA */
$lang['IMPRENSA_ASSESSORIA_TITULO'] = 'Assessoria de Imprensa';
$lang['IMPRENSA_ASSESSORIA_TEXTO_1'] = 'Para receber press releases, participar de coletivas e entrevistas, entre em contato conosco';
$lang['IMPRENSA_ASSESSORIA_TEXTO_2'] = 'Caso queira fazer a cobertura do evento, preencha o formulário abaixo e faça seu cadastro.<br> Nossos assessores entrarão em contato caso haja necessidade.';
$lang['IMPRENSA_ASSESSORIA_FORM_1'] = 'Nome (completo)';
$lang['IMPRENSA_ASSESSORIA_FORM_2'] = 'E-mail';
$lang['IMPRENSA_ASSESSORIA_FORM_3'] = 'MTB';
$lang['IMPRENSA_ASSESSORIA_FORM_4'] = 'Empresa';
$lang['IMPRENSA_ASSESSORIA_FORM_5'] = 'Site da Empresa';
$lang['IMPRENSA_ASSESSORIA_FORM_6'] = 'Área de Atuação';
$lang['IMPRENSA_ASSESSORIA_FORM_7'] = 'Seu Telefone';
$lang['IMPRENSA_ASSESSORIA_FORM_SUBMIT'] = 'ENVIAR MENSAGEM';
$lang['IMPRENSA_ASSESSORIA_FORM_OK'] = '<h1>Obrigado! <br> Email enviado com  ssucesso. Entraremos em contato em breve.</h1>';
$lang['IMPRENSA_ASSESSORIA_FORM_ERROR'] = '<h1>O email não pode ser enviado. <br> Tente novamente.</h1>';

/* INFORMAÇÕES */
$lang['INFO_PAVILHOES'] = 'Pavilhões A, B, C e D';
$lang['INFORMACOES_LOCAL_TITULO'] = 'Local do Evento';
$lang['INFORMACOES_LOCAL_VERMAPA'] = 'Exibir mapa ampliado';
$lang['INFORMACOES_HOTEIS_TITULO'] = 'Hotéis';
$lang['INFORMACOES_HOTEIS_SUBTITULO'] = 'Clique sobre o hotel para mais detalhes';
$lang['INFORMACOES_HOTEIS_LEGENDA'] = 'SGL: Acomoda 1 pessoa | DBL: Acomoda 2 pessoas';
$lang['INFORMACOES_HOTEIS_TABELA_TITULO_1'] = 'HOTEL';
$lang['INFORMACOES_HOTEIS_TABELA_TITULO_2'] = 'DIÁRIA SUPERIOR SGL';
$lang['INFORMACOES_HOTEIS_TABELA_TITULO_3'] = 'DIÁRIA SUPERIOR DBL';
$lang['INFORMACOES_HOTEIS_COMDESCONTO'] = 'já com desconto';
$lang['INFORMACOES_HOTEIS_LOCAL'] = 'Local';
$lang['INFORMACOES_HOTEIS_TEL'] = 'Tel';
$lang['INFORMACOES_HOTEIS_COMO_CHEGAR'] = 'Clique aqui para ver como chegar';
$lang['INFORMACOES_HOTEIS_CODIGO'] = 'Código para reserva';
$lang['INFORMACOES_HOTEIS_DIARIA_SGL'] = 'Diária de apartamento Superior SGL';
$lang['INFORMACOES_HOTEIS_DIARIA_DBL'] = 'Diária de apartamento Superior DBL';
$lang['INFORMACOES_HOTEIS_DIARIA'] = 'A diária (por adesão) inclui';
$lang['INFORMACOES_HOTEIS_OBSERVACOES'] = 'Observações';
$lang['INFORMACOES_HOTEIS_CHECK_1'] = '- Check-in a partir das 14h00<br>- Check-out até às 12h00';
$lang['INFORMACOES_HOTEIS_CHECK_2'] = 'Diárias iniciam às 12h e encerram às 12h';
$lang['INFORMACOES_HOTEIS_CHECK_3'] = 'Diárias iniciam às 14h e encerram às 12h';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_1'] = 'Café da manhã servido no restaurante (6h às 10h);';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_2'] = '- Café da manhã (quando servido no restaurante)<br>- 01 vaga na garagem por apartamento.';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_3'] = '- Café da manhã (quando servido no restaurante).<br>- Transfer das 4h50 às 23h10 do hotel<br> Transamérica Executive Congonhas /<br> Aeroporto de Congonhas / Hotel a cada  vinte minutos.';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_4'] = '- Café da manhã (quando servido no restaurante).<br>- 01 vaga na garagem por apartamento';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_5'] = '- Café da manhã (quando servido no restaurante).';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_6'] = '- Café da manhã (quando servido no restaurante).<br>- Internet';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_1'] = 'O valor da diária será acrescido de 5% de ISS + R$ 4,50 de taxa de turismo e o pagamento das despesas de hospedagem deverá ser feita diretamente no Hotel Transamérica São Paulo com cartão de crédito ou cash.<br> Os apartamentos estão sujeito à disponibilidade.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_2'] = 'O valor da diária será acrescido de 5% de ISS + taxa de turismo de R$ 3,20 e o pagamento das despesas de hospedagem deverá ser feita diretamente no Transamérica Executive Chácara Santo Antonio com cartão de Crédito ou Cash. Os apartamentos estão sujeito à disponibilidade.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_3'] = 'O valor da diária será acrescido de 5% de ISS + taxa de turismo de R$ 3,20 e o pagamento das despesas de hospedagem deverá ser feita diretamente no Transamérica Executive Congonhas com cartão de Crédito ou Cash. Os apartamentos estão sujeito à disponibilidade.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_4'] = 'O valor da diária será acrescido de 5% de ISS + taxa de turismo e o pagamento das despesas de hospedagem deverá ser feita diretamente no Transamérica Executive Congonhas com cartão de Crédito ou Cash.<br> Os apartamentos estão sujeito à disponibilidade.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_5'] = 'O valor da diária será acrescido de 5% de ISS + taxa de turismo de R$ 3,20 e o pagamento das despesas de hospedagem deverá ser feita diretamente no Blue Tree Premium Morumbi com cartão de Crédito ou Cash. Os apartamentos estão sujeito à disponibilidade.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_6'] = 'O valor da diária será acrescido de 5% de ISS + taxa de turismo e o pagamento das despesas de hospedagem deverá ser feita diretamente no Hotel Blue Tree Premium Verbo Divino com cartão de Crédito ou Cash. Os apartamentos estão sujeito à disponibilidade.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_7'] = 'O valor da diária será acrescido de 5% de ISS + taxa de turismo e o pagamento das despesas de hospedagem deverá ser feita diretamente no Hotel Quality Berrini com cartão de Crédito ou Cash. Os apartamentos estão sujeito à disponibilidade.';
$lang['CONTATO_INFORMACOES_GERAIS'] = 'Informações gerais';
$lang['CONTATO_COMO_CHEGAR'] = 'COMO CHEGAR';
$lang['CONTATO_RESERVAS'] = 'Reservas de espaços';
$lang['CONTATO_FORM_TITULO'] = 'Preencha o formulário abaixo para contato';
$lang['CONTATO_FORM_ASSUNTO_TITULO'] = 'Assunto';
$lang['CONTATO_FORM_ASSUNTO_1'] = 'Outros';
$lang['CONTATO_FORM_ASSUNTO_2'] = 'Congressista';
$lang['CONTATO_FORM_ASSUNTO_3'] = 'Expositor';
$lang['CONTATO_FORM_ASSUNTO_4'] = 'Visitante';
$lang['CONTATO_FORM_ASSUNTO_5'] = 'Patrocinador';
$lang['CONTATO_FORM_ASSUNTO_6'] = 'Imprensa';
$lang['CONTATO_FORM_1'] = 'Nome (completo)';
$lang['CONTATO_FORM_2'] = 'E-mail';
$lang['CONTATO_FORM_3'] = 'Empresa';
$lang['CONTATO_FORM_4'] = 'Site da Empresa';
$lang['CONTATO_FORM_5'] = 'Área de Atuação';
$lang['CONTATO_FORM_6'] = 'Seu Telefone';
$lang['CONTATO_FORM_7'] = 'Mensagem';
$lang['CONTATO_FORM_SUBMIT'] = 'ENVIAR';
$lang['CONTATO_FORM_OK'] = '<h1>Obrigado! <br>Email enviado com sucesso. Entraremos em contato em breve.</h1>';
$lang['CONTATO_FORM_ERROR'] = '<h1>O email não pode ser enviado. <br>Tente novamente.</h1>';

/***************/
/* PROGRAMAÇÃO */
/***************/

/* DIA 1 */
$lang['auditorio-1'] = <<<STR
AUDITÓRIO FEBRABAN
STR;

$lang['auditorio-2'] = <<<STR
AUDITÓRIO LINHA DE NEGÓCIOS
STR;

$lang['auditorio-3'] = <<<STR
AUDITÓRIO EFICIÊNCIA OPERACIONAL
STR;

$lang['obs-solenidade'] = <<<STR
Solenidade de abertura da Exposição e visita aos estandes dos Patrocinadores / Coffee break
STR;

$lang['1-sessao-1'] = <<<STR
1ª sessão manhã - das 09:00 às 10:30
STR;

$lang['1-sessao-2'] = <<<STR
2ª sessão manhã - das 11:00 às 12:30
STR;

$lang['1-sessao-3'] = <<<STR
1ª sessão tarde - das 14:00 às 15:30
STR;

$lang['1-sessao-4'] = <<<STR
2ª sessão tarde - das 16:00 às 17:00 e das 17:15 às 18:15
STR;

$lang['DIA_1_MANHA_1_A'] = <<<STR
Abertura
STR;

$lang['DIA_1_MANHA_1_B'] = <<<STR
Transmissão simultânea da Abertura
STR;

$lang['DIA_1_MANHA_1_C'] = <<<STR
Transmissão simultânea da Abertura
STR;


$lang['DIA_1_MANHA_2_A'] = <<<STR
Jorge Hereda - Presidente da CAIXA
STR;

$lang['DIA_1_MANHA_2_B'] = <<<STR
Transmissão simultânea
STR;

$lang['DIA_1_MANHA_2_C'] = <<<STR
Transmissão simultânea
STR;


$lang['DIA_1_TARDE_1_A'] = <<<STR
Keynote Speaker
STR;

$lang['DIA_1_TARDE_1_B'] = <<<STR
Interagindo com Clientes e Funcionários com Mobilidade
STR;

$lang['DIA_1_TARDE_1_C'] = <<<STR
Appification - Das aplicações para aplicativos
STR;


$lang['DIA_1_TARDE_2_A'] = <<<STR
A definir
STR;

$lang['DIA_1_TARDE_2_B'] = <<<STR
Impacto das Megatendências globais nos Serviços Financeiros<br>
<br>
<br>
==================================== <br>
Intervalo das 17:00 às 17:15<br>
==================================== <br>
Converting data into a strategic business asset for Banks<br>
<br>
<br>
<br>
Moderador: Jorge Vacarini - Deutsche Bank
STR;

$lang['DIA_1_TARDE_2_C'] = <<<STR
Cloud Computing na Prática
STR;



/* DIA 2 */
$lang['DIA_2_MANHA_1_A'] = <<<STR
Keynote Speaker
STR;

$lang['DIA_2_MANHA_1_B'] = <<<STR
BPO - Business Process Outsourcing
STR;

$lang['DIA_2_MANHA_1_C'] = <<<STR
Estratégia para o desenvolvimento de um modelo de Governança de TI 
STR;



$lang['DIA_2_MANHA_2_A'] = <<<STR
Expansão do Crédito - Formas e Limites 
STR;

$lang['DIA_2_MANHA_2_B'] = <<<STR
Projetos de Inovação ou Cultura em Inovação
STR;

$lang['DIA_2_MANHA_2_C'] = <<<STR
Big Data
STR;


$lang['DIA_2_TARDE_1_A'] = <<<STR
Painel das Áreas de Negócios dos Bancos
STR;

$lang['DIA_2_TARDE_1_B'] = <<<STR
Segurança da informação
STR;

$lang['DIA_2_TARDE_1_C'] = <<<STR
A Definir
STR;


$lang['DIA_2_TARDE_2_A'] = <<<STR
Infraestrutura do Brasil e Inovação
STR;

$lang['DIA_2_TARDE_2_B'] = <<<STR
Experiencia de Clientes vs Fidelização<br>
<br>
<br>
=====================================<br>
Intervalo das 17:00 às 17:15<br>
==================================== <br>
Costumer Excellence in Banking
STR;

$lang['DIA_2_TARDE_2_C'] = <<<STR
Eficiência Operacional e Redução de Custos Operacionais
STR;



/* DIA 3 */
$lang['3-sessao-1'] = <<<STR
1ª sessão manhã - das 09:00 às 10:30
STR;

$lang['3-sessao-2'] = <<<STR
2ª sessão manhã - das 11:00 às 12:30
STR;

$lang['3-sessao-3'] = <<<STR
1ª sessão tarde - das 14:00 às 16:00
STR;

$lang['3-sessao-4'] = <<<STR
2ª sessão tarde - das 16:30 às 18:00
STR;

$lang['DIA_3_MANHA_1_A'] = <<<STR
Inovação na Indústria de TI para os Bancos
STR;


$lang['DIA_3_MANHA_1_B'] = <<<STR
Transmissão Simultânea
STR;

$lang['DIA_3_MANHA_1_C'] = <<<STR
Transmissão Simultânea
STR;


$lang['DIA_3_MANHA_2_A'] = <<<STR
Lideranças de TI debatem Tendências
STR;

$lang['DIA_3_MANHA_2_B'] = <<<STR
Basiléia III
STR;

$lang['DIA_3_MANHA_2_C'] = <<<STR
Inovação em Auto Atendimento
STR;


$lang['DIA_3_TARDE_1_A'] = <<<STR
A definir

STR;

$lang['DIA_3_TARDE_1_B'] = <<<STR
A definir
STR;

$lang['DIA_3_TARDE_1_C'] = <<<STR
A definir
STR;


$lang['DIA_3_TARDE_2_A'] = <<<STR
A definir
STR;

$lang['DIA_3_TARDE_2_B'] = <<<STR
Transmissão Simultânea
STR;

$lang['DIA_3_TARDE_2_C'] = <<<STR
Transmissão Simultânea
STR;

$lang['Auditórios Excelência em TI'] = 'Auditórios Excelência em TI';
$lang['Auditorios Horário'] = 'Horário';
$lang['Auditorios Palestrantes'] = 'Palestrantes';
$lang['Auditorios Tema'] = 'Tema';

/* DIA 1 */
$lang['dia_1 Auditorio 1'] = 'Auditório 3 - TALARIS';
$lang['dia_1 Auditorio 3'] = 'Auditório 3 - LÓGICA';
$lang['dia_1 Auditorio 2'] = 'Auditório 2 - Acesso Digital';

$lang['dia1_auditorio_2_horario_1'] = '14h30';
$lang['dia1_auditorio_2_horario_2'] = '14h40';
$lang['dia1_auditorio_2_horario_3'] = '15h00';
$lang['dia1_auditorio_2_horario_4'] = '15h30';
$lang['dia1_auditorio_2_horario_5'] = '16h00';
$lang['dia1_auditorio_2_palestrante_1'] = 'Antonio Augusto de Almeida Leite (Pancho), ACREFI';
$lang['dia1_auditorio_2_palestrante_2'] = 'Breno Costa, Sócio e Consultor da GoOn – A Evolução na Gestão do Risco';
$lang['dia1_auditorio_2_palestrante_3'] = 'Ricardo Batista, Superintendente de Crédito do Tribanco';
$lang['dia1_auditorio_2_palestrante_4'] = 'Roberto Jabali, Superintendente de Crédito do Grupo Citi - Credicard';
$lang['dia1_auditorio_2_palestrante_5'] = 'Alex Yamamoto,Consultor da Acesso Digital';
$lang['dia1_auditorio_2_tema_1'] = 'Abertura';
$lang['dia1_auditorio_2_tema_2'] = 'Case GoOn: Como aumentar o nível de decisão automática sem impacto na inadimplência.';
$lang['dia1_auditorio_2_tema_3'] = 'Case Tribanco: captura de documentos no varejo';
$lang['dia1_auditorio_2_tema_4'] = 'Case Grupo Citi: Pioneirismo na gestão eletrônica de documentos';
$lang['dia1_auditorio_2_tema_5'] = 'Formalização de crédito online nas instituições financeiras';

$lang['dia1_auditorio_3_horario_1'] = '09h00 - 10h00';
$lang['dia1_auditorio_3_horario_2'] = '11h00 – 12h00';
$lang['dia1_auditorio_3_horario_3'] = '14h00 - 15h10';
$lang['dia1_auditorio_3_horario_4'] = '15h20 – 16h30';
$lang['dia1_auditorio_3_horario_5'] = '16h40 – 17h50';
$lang['dia1_auditorio_3_horario_6'] = '17h50 -18h00';
$lang['dia1_auditorio_3_horario_7'] = ' ';
$lang['dia1_auditorio_3_palestrante_1'] = 'Paulo Martins, Diretor Comercial / Logica';
$lang['dia1_auditorio_3_palestrante_2'] = 'Reginaldo Silva, Portfólio / Logica';
$lang['dia1_auditorio_3_palestrante_3'] = 'Antonio Requejo, Regional Practice Leader Iberia & Latam – Security / Logica';
$lang['dia1_auditorio_3_palestrante_4'] = 'Júlio Gonçalves, Regional Practice Leader Iberia & Latam – IT Modernization / Logica';
$lang['dia1_auditorio_3_palestrante_5'] = 'Lode Snykers, Vice President, Global Financial Services / Logica';
$lang['dia1_auditorio_3_palestrante_6'] = 'Paulo Martins, Diretor Comercial / Logica';
$lang['dia1_auditorio_3_palestrante_7'] = ' ';
$lang['dia1_auditorio_3_tema_1'] = 'Compartilhando Idéias';
$lang['dia1_auditorio_3_tema_2'] = 'Mobilidade: Saiba como surpreender os seus clientes com soluções modernas e inovadoras';
$lang['dia1_auditorio_3_tema_3'] = 'De Securidad en TI para el Gerenciamento de Riesgo';
$lang['dia1_auditorio_3_tema_4'] = 'Modernização de Sistemas Legados';
$lang['dia1_auditorio_3_tema_5'] = 'The technological challenges of the Financial Sector in the XXI century';
$lang['dia1_auditorio_3_tema_6'] = 'Compartilhando Ideias II';
$lang['dia1_auditorio_3_tema_7'] = 'Sorteios para os participantes do dia';


/* DIA 2 */
$lang['dia_2 Auditorio 1'] = 'Auditório 1 - HP';
$lang['dia_2 Auditorio 2'] = 'Auditório 2 - Microsoft';
$lang['dia_2 Auditorio 3'] = 'Auditório 3 - CLM';

/* Auditorio 1 */
$lang['dia2_auditorio_1_horario_1'] = '10h00 – 11h00';
$lang['dia2_auditorio_1_horario_2'] = '11h10 – 12h10';
$lang['dia2_auditorio_1_horario_3'] = '14h30 – 15h30';
$lang['dia2_auditorio_1_horario_4'] = '15h40 – 16h00';
$lang['dia2_auditorio_1_palestrante_1'] = 'Mike Wright, HP';
$lang['dia2_auditorio_1_palestrante_2'] = 'Gladson Martins Russo, Nokia Siemens';
$lang['dia2_auditorio_1_palestrante_3'] = 'Andre Kalsing, HP';
$lang['dia2_auditorio_1_palestrante_4'] = 'Federico Grosso, Autonomy';
$lang['dia2_auditorio_1_tema_1'] = 'Tendências para Social CRM';
$lang['dia2_auditorio_1_tema_2'] = 'Novas tecnologias para Mobile Payment';
$lang['dia2_auditorio_1_tema_3'] = 'Benefícios da adoção de uma plataforma de serviços multicanal';
$lang['dia2_auditorio_1_tema_4'] = 'Uma nova Visão de Inovação e Big Data';


// Auditorio 2
$lang['dia2_auditorio_2_horario_1'] = '9h-10h';
$lang['dia2_auditorio_2_horario_2'] = '10h-11h30';
$lang['dia2_auditorio_2_horario_3'] = '11h30-12h30';
$lang['dia2_auditorio_2_horario_4'] = '14h-15h';
$lang['dia2_auditorio_2_horario_5'] = '15h-16h';
$lang['dia2_auditorio_2_horario_6'] = '16h-17h';
$lang['dia2_auditorio_2_horario_7'] = '17h-18h';
$lang['dia2_auditorio_2_palestrante_1'] = 'Fabio Souto';
$lang['dia2_auditorio_2_palestrante_2'] = 'Miha Krajl';
$lang['dia2_auditorio_2_palestrante_3'] = 'Jun Endo';
$lang['dia2_auditorio_2_palestrante_4'] = 'Eduardo Campos';
$lang['dia2_auditorio_2_palestrante_5'] = 'Roberto Prado';
$lang['dia2_auditorio_2_palestrante_6'] = 'Ricardo-Enrico Jahn';
$lang['dia2_auditorio_2_palestrante_7'] = 'Richard Chaves';
$lang['dia2_auditorio_2_tema_1'] = 'Sociedade Conectada e a Consumerização de TI';
$lang['dia2_auditorio_2_tema_2'] = 'Como TI está mudando nosso futuro';
$lang['dia2_auditorio_2_tema_3'] = 'Soluções Inovadoras para uma Experiência Única Multidevices & Consumerização de TI';
$lang['dia2_auditorio_2_tema_4'] = 'A nuvem nos seus termos - Estratégia de Computação na nuvem da Microsoft';
$lang['dia2_auditorio_2_tema_5'] = 'O profissional TI como protagonista da competitividade. Capacitação e Satisfação do cliente.';
$lang['dia2_auditorio_2_tema_6'] = 'Relacionamento e fidelização nas instituições financeiras com Dynamics CRM/XRM';
$lang['dia2_auditorio_2_tema_7'] = 'O poder da Inovação';

// Auditorio 3
$lang['dia2_auditorio_3_horario_1'] = '10h30 - 11h30';
$lang['dia2_auditorio_3_horario_2'] = '11h30 - 12h30';
$lang['dia2_auditorio_3_horario_3'] = '14h30 - 15h30';
$lang['dia2_auditorio_3_horario_4'] = '15h30 - 16h30';
$lang['dia2_auditorio_3_horario_5'] = '16h30 - 17h30';
$lang['dia2_auditorio_3_horario_6'] = '17h30 - 18h30';
$lang['dia2_auditorio_3_palestrante_1'] = 'Martin Roesch, SNORT';
$lang['dia2_auditorio_3_palestrante_2'] = 'Alex Silva, A10';
$lang['dia2_auditorio_3_palestrante_3'] = 'Carlos Fagundes, INTEGRALTRUST';
$lang['dia2_auditorio_3_palestrante_4'] = 'Sanjay Ramnath, BARRACUDA';
$lang['dia2_auditorio_3_palestrante_5'] = 'Dick Faulkner, SOPHOS';
$lang['dia2_auditorio_3_palestrante_6'] = 'Martin Roesch, SOURCEFIRE';
$lang['dia2_auditorio_3_tema_1'] = 'Open Source Security';
$lang['dia2_auditorio_3_tema_2'] = 'DDoS Attacks and Brute Force';
$lang['dia2_auditorio_3_tema_3'] = 'Segredos do Basileia III';
$lang['dia2_auditorio_3_tema_4'] = 'Application Security';
$lang['dia2_auditorio_3_tema_5'] = 'Cryptography and Mobile Security';
$lang['dia2_auditorio_3_tema_6'] = 'Next Generation Security';

/* DIA 3*/
$lang['dia_3 Auditorio 1'] = 'Auditório 1 - FEBRABAN';

$lang['dia3_auditorio_1_horario_1'] = '11h00 - 12h30';
$lang['dia3_auditorio_1_palestrante_1'] = 'Guilhermino Domiciano de Souza – Banco do Brasil;  Marcelo Ribeiro Câmara – Bradesco;  Cesar Faustino – Itaú Unibanco';
$lang['dia3_auditorio_1_tema_1'] = 'A Segurança Digital do Cidadão no Sistema Financeiro Brasileiro';

$lang['Programação em Breve'] = "Aguarde, em breve publicaremos a programação do congresso.";

$lang['CIA AÉREA OFICIAL'] = "CIA AÉREA OFICIAL";
$lang['Parceria TAM'] = "Parceria TAM";
$lang['A TAM é a Cia Aérea Oficial do Ciab 2013 e quem ganha o desconto é você!'] = "A TAM é a Cia Aérea Oficial do Ciab 2013 e quem ganha o desconto é você!";
$lang["A TAM é a CIA Aérea Oficial do Ciab 2013 e quem ganha o desconto é você. Clique aqui e veja as condições especiais oferecidas."] = "";
$lang['Em parceria com o Ciab 2013, a TAM como Cia Aérea Oficial do congresso disponibiliza passagens aéreas com condições especiais para você comparecer ao Ciab e ficar por dentro de tudo que as empresas de ponta em tecnologia estão preparando para superar “Os Novos Desafios do Setor Financeiro”.'] = "Em parceria com o Ciab 2013, a TAM como Cia Aérea Oficial do congresso disponibiliza passagens aéreas com condições especiais para você comparecer ao Ciab e ficar por dentro de tudo que as empresas de ponta em tecnologia estão preparando para superar “Os Novos Desafios do Setor Financeiro”.";
$lang['Confira abaixo as condições que preparamos para você, válidos para todos os trechos <strong>Brasil/São Paulo/Brasil</strong> no período de <strong>embarque</strong> entre <strong>09 e 17.06.2013:</strong>'] = "Confira abaixo as condições que preparamos para você, válidos para todos os trechos <strong>Brasil/São Paulo/Brasil</strong> no período de <strong>embarque</strong> entre <strong>09 e 17.06.2013:</strong>";
$lang['- 25%* de desconto na tarifa da classe disponível<br><br><strong>ou</strong><br><br>- 10%* de desconto nas tarifas Mega Promo e nas classes W e N'] = "- 25%* de desconto na tarifa da classe disponível<br><br><strong>ou</strong><br><br>- 10%* de desconto nas tarifas Mega Promo e nas classes W e N";
$lang["Para conseguir o seu desconto é fácil:<br>Clique no botão abaixo ou acesse <a href='http://www.tam.com.br' title='TAM' target='_blank'>www.tam.com.br</a> e ao escolher o <strong>trecho</strong> e a <strong>data</strong>, digite o número do Código Promocional <strong>413709</strong>."] = "Para conseguir o seu desconto é fácil:<br>Clique no botão abaixo ou acesse <a href='http://www.tam.com.br' title='TAM' target='_blank'>www.tam.com.br</a> e ao escolher o <strong>trecho</strong> e a <strong>data</strong>, digite o número do Código Promocional <strong>413709</strong>.";
$lang['Clique e Compre sua Passagem'] = "Clique e Compre sua Passagem";
$lang['* valores sujeitos à disponibilidade de assentos e regras/restrições específicas de cada tarifa, válidos para embarque três dias antes e três após a data do evento no trecho Brasil/São Paulo/Brasil.'] = "* valores sujeitos à disponibilidade de assentos e regras/restrições específicas de cada tarifa, válidos para embarque três dias antes e três após a data do evento no trecho Brasil/São Paulo/Brasil.";


// ARQUIVO : ../www/ciab/application/views/programacao//primeiro.php
$lang['Quarta-Feira'] = "";
$lang['AUDITÓRIO FEBRABAN'] = "";
$lang['sessão'] = "";
$lang['horário'] = "";
$lang['palestra'] = "";
$lang['palestrante'] = "";
$lang['manhã'] = "";
$lang['às'] = "";
$lang['Abertura'] = "";
$lang['Jorge Hereda - Presidente da CAIXA'] = "";
$lang['tarde'] = "";
$lang['Keynote Speaker'] = "";
$lang['A definir'] = " ";
$lang['obs-solenidade'] = "";
$lang['AUDITÓRIO LINHA DE NEGÓCIOS'] = "";
$lang['Transmissão simultânea da Abertura'] = "";
$lang['Transmissão simultânea'] = "";
$lang['Interagindo com Clientes e Funcionários com Mobilidade'] = "";
$lang['Impacto das Megatendências globais nos Serviços Financeiros'] = "";
$lang['intervalo'] = "";
$lang['Converting data into a strategic business asset for Banks'] = "";
$lang['Moderador: Jorge Vacarini - Deutsche Bank'] = "";
$lang['AUDITÓRIO EFICIÊNCIA OPERACIONAL'] = "";
$lang['Appification - Das aplicações para aplicativos'] = "";
$lang['Cloud Computing na Prática'] = "";
$lang['Auditórios Excelência em TI'] = "";
$lang['dia_1 Auditorio 2'] = "";
$lang['Auditorios Horário'] = "";
$lang['Auditorios Palestrantes'] = "";
$lang['Auditorios Tema'] = "";
$lang['dia1_auditorio_2_horario_1'] = "";
$lang['dia1_auditorio_2_palestrante_1'] = "";
$lang['dia1_auditorio_2_tema_1'] = "";
$lang['dia1_auditorio_2_horario_2'] = "";
$lang['dia1_auditorio_2_palestrante_2'] = "";
$lang['dia1_auditorio_2_tema_2'] = "";
$lang['dia1_auditorio_2_horario_3'] = "";
$lang['dia1_auditorio_2_palestrante_3'] = "";
$lang['dia1_auditorio_2_tema_3'] = "";
$lang['dia1_auditorio_2_horario_4'] = "";
$lang['dia1_auditorio_2_palestrante_4'] = "";
$lang['dia1_auditorio_2_tema_4'] = "";
$lang['dia1_auditorio_2_horario_5'] = "";
$lang['dia1_auditorio_2_palestrante_5'] = "";
$lang['dia1_auditorio_2_tema_5'] = "";
$lang['dia_1 Auditorio 3'] = "";
$lang['dia1_auditorio_3_horario_1'] = "";
$lang['dia1_auditorio_3_palestrante_1'] = "";
$lang['dia1_auditorio_3_tema_1'] = "";
$lang['dia1_auditorio_3_horario_2'] = "";
$lang['dia1_auditorio_3_palestrante_2'] = "";
$lang['dia1_auditorio_3_tema_2'] = "";
$lang['dia1_auditorio_3_horario_3'] = "";
$lang['dia1_auditorio_3_palestrante_3'] = "";
$lang['dia1_auditorio_3_tema_3'] = "";
$lang['dia1_auditorio_3_horario_4'] = "";
$lang['dia1_auditorio_3_palestrante_4'] = "";
$lang['dia1_auditorio_3_tema_4'] = "";
$lang['dia1_auditorio_3_horario_5'] = "";
$lang['dia1_auditorio_3_palestrante_5'] = "";
$lang['dia1_auditorio_3_tema_5'] = "";
$lang['dia1_auditorio_3_horario_6'] = "";
$lang['dia1_auditorio_3_palestrante_6'] = "";
$lang['dia1_auditorio_3_tema_6'] = "";
$lang['dia1_auditorio_3_horario_7'] = "";
$lang['dia1_auditorio_3_palestrante_7'] = "";
$lang['dia1_auditorio_3_tema_7'] = "";

// ARQUIVO : ../www/ciab/application/views/programacao//segundo.php
$lang['Quinta-Feira'] = "";
$lang['Expansão do Crédito - Formas e Limites'] = "";
$lang['Painel das Áreas de Negócios dos Bancos'] = "";
$lang['Infraestrutura do Brasil e Inovação'] = "";
$lang['BPO - Business Process Outsourcing'] = "";
$lang['Projetos de Inovação ou Cultura em Inovação'] = "";
$lang['Segurança da informação'] = "";
$lang['Experiencia de Clientes vs Fidelização'] = "";
$lang['Costumer Excellence in Banking'] = "";
$lang['Estratégia para o desenvolvimento de um modelo de Governança de TI'] = "";
$lang['Big Data'] = "";
$lang['A Definir'] = "";
$lang['Eficiência Operacional e Redução de Custos Operacionais'] = "";

// ARQUIVO : ../www/ciab/application/views/programacao//terceiro.php
$lang['Sexta-Feira'] = "";
$lang['Inovação na Indústria de TI para os Bancos'] = "";
$lang['Lideranças de TI debatem Tendências'] = "";
$lang['Transmissão Simultânea'] = "";
$lang['Basiléia III'] = "";
$lang['Inovação em Auto Atendimento'] = "";

?>