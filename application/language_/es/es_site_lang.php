<?php
/* TOPO */
$lang['EVENTO'] = 'EVENT';
$lang['EVENTO_SUBMENU_1'] = 'Presentación';
$lang['EVENTO_SUBMENU_2'] = 'El Congreso';
$lang['EVENTO_SUBMENU_3'] = 'Espacios Temáticos';
$lang['EVENTO_SUBMENU_4'] = 'Exposicion';
$lang['EVENTO_SUBMENU_5'] = 'Quiero Visitar';
$lang['EVENTO_SUBMENU_6'] = 'Quiero Esponer';
$lang['EVENTO_SUBMENU_7'] = 'Quiero Apoyar';
$lang['EVENTO_SUBMENU_8'] = 'Planta de La Exposición';
$lang['EVENTO_SUBMENU_9'] = 'Otros Años';
$lang['PROGRAMACAO'] = 'PROGRAMACIÓN';
$lang['PROMOÇÃO'] = 'PROMOCIÓN';
$lang['PROMOÇÃO2'] = 'Visite nuestro stand en CIAB 2013, vea una demostración y participe para ganar iPads. SOPORTE H15 - FLAG C';
$lang['PALESTRANTES'] = 'CONFERENCISTAS';
$lang['PREMIOS'] = 'PREMIOS';
$lang['PREMIOS_SUBMENU_1'] = 'Prêmio Ciab 2013';
$lang['PREMIOS_SUBMENU_2'] = 'Vencedores 2011';
$lang['PREMIOS_SUBMENU_3'] = 'Vencedores 2012';
$lang['INSCRICOES'] = 'INSCRIPCIONES';
$lang['INSCRICOES_SUBMENU_1'] = 'Valores';
$lang['INSCRICOES_SUBMENU_2'] = 'Informaciones Importantes';
$lang['INSCRICOES_BANNER_TEXTO'] = 'Aproveche los descuentos promocionales hasta el 10/06 para afiliados y no afiliados. Inscriba su empresa y gane descuentos por grupo.';
$lang['NOTICIAS'] = 'NOTICIAS';
$lang['PUBLICACOES'] = 'PUBLICACIONES';
$lang['IMPRENSA'] = 'PRENSA';
$lang['IMPRENSA_SUBMENU_1'] = 'Prensa';
$lang['INFORMACOES'] = 'INFORMACIONES';
$lang['INFORMACOES_SUBMENU_1'] = 'Local del Evento';
$lang['INFORMACOES_SUBMENU_2'] = 'Hospedaje';
$lang['CONTATO'] = 'CONTACTO';
$lang['BUSCA'] = 'BUSCA';
$lang['ling_portugues'] = 'português';
$lang['ling_ingles'] = 'english';
$lang['ling_espanhol'] = 'español';
$lang['VOLTAR'] = 'VOLVER';
$lang['Compartilhe'] = 'Compártelo';
$lang['Quarta-Feira'] = 'Miércoles';
$lang['Quinta-Feira'] = 'Jueves';
$lang['Sexta-Feira'] = 'Viernes';

/* FOOTER */
$lang['PATROCINADORES'] = 'PATROCINADORES';
$lang['DIAMANTE'] = 'DIAMANTE';
$lang['OURO'] = 'ORO';
$lang['PRATA'] = 'PLATA';
$lang['APOIO'] = 'APOYO';
$lang['FALE CONOSCO'] = 'COMUNÍQUESE CON NOSOTROS';
$lang['POLITICA DE PRIVACIDADE'] = 'POLÍTICA DE PRIVACIDAD';
$lang['TERMO DE USO'] = 'TÉRMINO DE USO';
$lang['MAPA DO SITE'] = 'MAPA DEL SITIO';
$lang['address-linha1'] = '&copy; FEBRABAN - Federação Brasileira de Bancos (Federación Brasileña de Bancos) - 2013 - Todos Los Derechos Reservados';
$lang['address-linha2'] = 'Av. Brig. Faria Lima, 1.485 - 14º andar • São Paulo • PABX .: 55 11 3244 9800 / 3186 9800 • FAX.: 55 11 3031 4106 •  <a href="http://www.febraban.org.br">WWW.FEBRABAN.ORG.BR</a>';

/* VEJA MAIS */
$lang['VEJA_MAIS_TITULO'] = "VER+MÁS";

/* PALESTRANTES */
$lang['PALESTRANTES_TEMAS_TITULO'] = 'Temas presentados durante el evento';
$lang['PALESTRANTES_OUTROS'] = 'Otros Conferencistas';

/* NOTÍCIAS */
$lang['NOTICIAS_ULTIMAS_TITULO'] = 'Últimas Notícias';
$lang['NOTICIAS_VER_TODAS'] = 'VER TODAS';

/* HOME */
$lang['HOME_SLIDE_CMEP'] = 'Medios de Pago y Sostenibilidad en las Relaciones de Consumo es el tema principal del 7º CMEP organizado por FEBRABAN.';
$lang['HOME_SLIDE_1'] = 'CIAB FEBRABAN - Congreso y Exposición de Tecnología de la Información de las Instituciones Financieras, es el mayor evento de América Latina tanto para el sector financiero como para el área de Tecnología.';
$lang['HOME_SLIDE_2'] = '';
$lang['HOME_SLIDE_3'] = '';
$lang['HOME_SLIDE_4'] = '';
$lang['BOX_INSCRICOES_TITULO'] = 'PALESTRAS';

$lang['BOX_INSCRICOES_TEXTO'] = 'Congressista, clique aqui e faça o download das palestras do CIAB 2013.';
$lang['BOX_BLOG_TITULO'] = 'BLOG';
$lang['BOX_BLOG_TEXTO'] = 'Nuevo blog del Ciab FEBRABAN. Un espacio para debates sobre los temas relacionados con la tecnología de la información.!';
$lang['QUERO_PATROCINAR_TITULO'] = 'QUIERO PATROCINAR';
$lang['QUERO_PATROCINAR_TEXTO'] = 'Su empresa puede patrocinar el mayor congreso y exposición de América Latina.';
$lang['QUERO_EXPOR_TITULO'] = 'QUIERO EXPONER';
$lang['QUERO_EXPOR_TEXTO'] = 'CIAB FEBRABAN cuenta con la participación de centenas de empresas expositoras.';
$lang['QUERO_VISITAR_TITULO'] = 'BLOG CIAB';
$lang['QUERO_VISITAR_TEXTO'] = 'Vaya al Transamérica Expo Center y efectúe su registro como visitante del evento.';
$lang['AGENDA_TITULO'] = 'AGENDA';
$lang['AGENDA_TEXTO'] = 'Verifique la agenda de conferencistas nacionales e internacionales que estarán presentes.';
$lang['NOTICIAS_TITULO'] = 'NOTICIAS';
$lang['NENHUMA_NOTICIA'] = 'Nenhuma notícia cadastrada';
$lang['EVENTO_TITULO'] = 'EL EVENTO';
$lang['EVENTO_SUBTITULO'] = 'Qué es Ciab Febraban';
$lang['EVENTO_TEXTO'] = 'Congreso y Exposición de Tecnología de la Información de las Instituciones Financieras – el mayor evento de América Latina...';
/*****************************************************/
$lang['PUBLICACOES_TITULO'] = 'PUBLICACIONES';
$lang['PUBLICACOES_IMAGEM'] = '_imgs/home/ImgHome.jpg';
$lang['PUBLICACOES_SUBTITULO'] = 'A vez da Sociedade Conectada';
$lang['PUBLICACOES_TEXTO'] = 'Iniciativa do governo federal quer garantir o acesso de deficientes físicos aos seus sites. Outros públicos, como a terceira idade, também serão beneficiados';
/*****************************************************/
$lang['TWITTER_TITULO'] = 'TWITTER CIABFEBRABAN';
$lang['NEWSLETTER_TITULO'] = 'NEWSLETTER';
$lang['NEWSLETTER_TEXTO'] = 'Firme y reciba todas las novedades';
$lang['NEWSLETTER_PLACEHOLDER_NOME'] = 'Informe su Nombre';
$lang['NEWSLETTER_PLACEHOLDER_EMAIL'] = 'Su email';
$lang['NEWSLETTER_SUBMIT'] = 'ENVIAR REGISTRO';

/* INSCRIÇÕES */
$lang['INSCRICOES_TITULO'] = 'Preços de Inscrições e Bonificações de Inscrições';
$lang['INSCRICOES_TITULO_TABELA_1'] = 'Valores por pessoa para inscrições nos 3 dias do Congresso';
$lang['DATA_ATE'] = 'Até';
$lang['DATA_APOS'] = 'Após';
$lang['INSCRICOES_TABELA_INSCRICOES'] = 'Inscrições';
$lang['INSCRICOES_TABELA_FILIADOS'] = 'Bancos filiados';
$lang['INSCRICOES_TABELA_NAO_FILIADOS'] = 'Não filiados';
$lang['INSCRICOES_TABELA_LINHA_1'] = 'Até 5';
$lang['INSCRICOES_TABELA_LINHA_2'] = 'de 6 a 10';
$lang['INSCRICOES_TABELA_LINHA_3'] = 'de 11 a 20';
$lang['INSCRICOES_TABELA_LINHA_4'] = 'mais de 20';
$lang['INSCRICOES_TABELA_RODAPE'] = 'EMPRESAS COM O MESMO CNPJ VALORES POR PARTICIPANTE';
$lang['INSCRICOES_TITULO_TABELA_2'] = 'Valores por pessoa para inscrições para 1 dia do Congresso';
$lang['INSCRICOES_OBSERVACAO_TITULO'] = 'Observação';
$lang['INSCRICOES_OBSERVACAO_TEXTO'] = 'No valor da inscrição estão incluídos: almoços, coffee breaks , coquetel do dia 20/06 e estacionamento nos 3 dias de realização do evento.';
$lang['INSCRICOES_OBSERVACAO_ITEM_1'] = 'Os descontos são válidos somente para inscrições na mesma categoria (participação nos 3 dias do evento ou participação em apenas um dia do evento).';
$lang['INSCRICOES_OBSERVACAO_ITEM_2'] = 'Manteremos o valor da 1ª. Fase para o banco que fizer mais do que o equivalente a 50 inscrições válidas para os três dias do evento. (ex.: 50 inscrições para participação nos 3 dias x R$ 2.178,00 = R$ 108.900,00 ou 124 inscrições para participação em um dia = R$ 108.872,00).';
$lang['INSCRICOES_BONIFICACAO_TITULO'] = 'Bonificação';
$lang['INSCRICOES_BONIFICACAO_ITEM_1'] = 'A cada 20 inscrições a instituição recebe 01 (uma) inscrição cortesia de um dia;';
$lang['INSCRICOES_BONIFICACAO_ITEM_2'] = 'Instituições que fizerem mais de 50 (cinqüenta) inscrições e aumentarem o total de inscritos em relação a 2011 serão bonificadas, conforme abaixo:';
$lang['INSCRICOES_BONIFICACAO_TABELA_TITULO_1'] = 'até 10%';
$lang['INSCRICOES_BONIFICACAO_TABELA_TITULO_2'] = 'Acima de 10% até 20%';
$lang['INSCRICOES_BONIFICACAO_TABELA_TITULO_3'] = 'Acima de 20% até 30%';
$lang['INSCRICOES_BONIFICACAO_TABELA_TITULO_4'] = 'Acima de 30%';
$lang['INSCRICOES_BONIFICACAO_TABELA_TEXTO_1'] = '5 inscr. de 1 dia';
$lang['INSCRICOES_BONIFICACAO_TABELA_TEXTO_2'] = '10 inscr. de 1 dia';
$lang['INSCRICOES_BONIFICACAO_TABELA_TEXTO_3'] = '15 inscr. de 1 dia';
$lang['INSCRICOES_BONIFICACAO_TABELA_TEXTO_4'] = '20 inscr. de 1 dia';
$lang['INSCRICOES_BONIFICACAO_TABELA_LEGENDA'] = '(*) A Tabela de Bonificação é válida exclusivamente para os acréscimos de inscrições em relação a 2011, conforme as faixas estabelecidas, e somente para empresas que fizeram acima de 50 (cinquenta) inscrições no Ciab 2012 .<br>(**) Para consultar a quantidade de inscrições realizadas em 2011, pedimos contatar a Diretoria de<br>Eventos pelo telefone (11) 3186-9860.';
$lang['INSCRICOES_EXEMPLO_TITULO'] = 'Exemplo';
$lang['INSCRICOES_BONIFICACAO_TEXTO'] = 'Instituição Y realizou em 2011 50 inscrições para os 3 dias do evento.<br> Em 2012, realizou 66 inscrições para os 3 dias.<br> Isso quer dizer que o banco Y tem direito à bonificação de 30%.<br>';
$lang['INSCRICOES_BONIFICACAO_TEXTO_NEGRITO'] = 'As bonificações adquiridas pela Instituição Y são';
$lang['INSCRICOES_BONIFICACAO_ITEM_EXEMPLO_1'] = 'Desconto de 35% para a mesma categoria (3 dias ou 1 dia) até o último dia de realização do Congresso, conforme disponibilidade de vagas.';
$lang['INSCRICOES_BONIFICACAO_ITEM_EXEMPLO_2'] = 'Gratuidade de 3 inscrições para participação em 1 dia do Congresso (a cada 20 inscrições 01 gratuidade).';
$lang['INSCRICOES_BONIFICACAO_ITEM_EXEMPLO_3'] = 'Gratuidade de 20 inscrições para participação em 1 dia do Congresso, conforme tabela de bonificação';
$lang['INSCRICOES_INFORMACOES_TITULO'] = 'Informaciones Importantes';
$lang['INSCRICOES_OPCOES_TITULO'] = 'Opciones de Pago';
$lang['INSCRICOES_OPCOES_ITEM_1'] = '- Tarjeta de Crédito Internacional';
$lang['INSCRICOES_OPCOES_ITEM_2'] = ' ';
$lang['INSCRICOES_REEMBOLSO_TITULO'] = 'Reembolsos y Cancelaciones';
$lang['INSCRICOES_REEMBOLSO_TEXTO'] = 'Los pedidos de reembolso o de cancelaciones de inscripciones serán recibidos, a través de una solicitación por escrito destinado para el e-mail eventos@febraban.org.br hasta el día 31 de mayo de 2012.<br> Los reembolsos serán efectuados en el valor neto, descontadas los gastos bancarios provenientes de la transferencia bancaria. A partir del 1º de junio de 2011 no serán más aceptados los pedidos de reembolso ó de cancelaciones.';
$lang['INSCRICOES_SUBSTITUICAO_TITULO'] = 'Sustitución de Participante';
$lang['INSCRICOES_SUBSTITUICAO_TEXTO'] = 'FEBRABAN, solamente aceptará la  sustitución del participante desde que la solicitación sea hecha vía e-mail y dirigido para la dirección electrónica:  eventos@febraban.org.br hasta el día 15 de junio de 2012.<br> Para  las sustituciones solicitadas durante la realización del evento la Organización cobrará una tasa de R$ 250,00 por la reemisión del gafete.  El respectivo pago deberá ser efectuado en el acto de la solicitación.';
$lang['INSCRICOES_CONFIRMACAO_TITULO'] = 'Confirmación de La Inscripción Vía E-Mail';
$lang['INSCRICOES_CONFIRMACAO_TEXTO'] = 'Una vez realizada la inscripción, el sistema de inscripciones de la FEBRABAN enviará un e-mail de confirmación de inscripción del participante. <br>En caso de que no se tenga una respuesta de confirmación de la inscripción, el participante o el área responsable por la inscripción debe entrar en contacto con la Dirección de Eventos de la FEBRABAN a través del e-mail: eventos@febraban.org.br, solicitando la verificación de la inscripción.';
$lang['INSCRICOES_ENCERRAMENTO_TITULO'] = 'Encerramiento de Las Inscripciones';
$lang['INSCRICOES_ENCERRAMENTO_TEXTO'] = 'Las inscripciones serán encerradas el 15 de junio de 2012 o hasta que el número de vacantes sea ocupado.';

/* EVENTO */
$lang['EVENTO_APRESENTACAO_TITULO'] = 'Presentación';
$lang['EVENTO_APRESENTACAO_TEXTO'] = <<<TEXTO
<p>
CIAB FEBRABAN - Congreso y Exposición de Tecnología de la Información de las Instituciones Financieras – es el mayor evento de América Latina tanto para el sector financiero como para el área de Tecnología.
</p>
<p>
Fue creado en 1990 y desde su primera edición en 1991, es alentador del desarrollo de la tecnología e innovación bancaria. Anualmente, el congreso reúne alrededor de 1,9 mil representantes de bancos de Brasil y de otros países. Presenta cerca de 120 personalidades entre conferencistas y polemistas en más de 30 paneles. 
</p>
<p>
El área de exposiciones reúne alrededor de 200 empresas proveedoras de tecnología e innovación corporativa, en un espacio total superior a cuatro mil metros cuadrados, que atrae visitas anuales superiores a 18 mil ejecutivos y directores de instituciones financieras y otras áreas de tecnología e innovación. 
</p>
<p>
La 23ª edición de CIAB FEBRABAN tendrá como tema central “Los Nuevos Desafíos del Sector Financiero” y será realizada los días 12, 13 y 14 de Junio de 2013, en el Transamérica Expo Center, en São Paulo.
</p>
<p>
<p class="strong">Lo mejor de Ciab FEBRABAN 2012</p>
La 22ª edición de Ciab FEBRABAN, realizada entre los días 20 y 22 de Junio de 2012, fue una de las mayores y más concurridas de su historia. El área record de exposiciones, con 4.500 m² tuvo un público también record de 19 mil representantes de instituciones financieras, empresas de Tecnología de la Información y aliados. Fueron 149 empresas expositoras que trajeron muchas novedades, incluyendo la presencia de 31 empresas extranjeras que estrenaron en el mercado brasileño.
</p>
<p>
Más de 1.700 congresistas se presentaron en los 28 paneles de charlas y debates realizados por 110 especialistas, siendo que 19 de ellos fueron keynote speakers internacionales, debatiendo asuntos que profundizaron el tema central “La Sociedad Conectada”.
</p>
<p>
También es destaque en 2012 la tercera edición del premio Ciab FEBRABAN, cuyo objetivo de reconocer jóvenes talentos que desarrollen soluciones innovadoras, y el Premio del Espacio Innovación.
</p>
TEXTO;
$lang['EVENTO_SOBRE_TITULO'] = 'En FEBRABAN';
$lang['EVENTO_SOBRE_TEXTO'] = <<<TEXTO
<p>
La Federação Brasileira de Bancos (Federación Brasileña de Bancos) – FEBRABAN es la principal entidad representativa del sector bancario. Fue fundada en 1967 buscando el fortalecimiento del sistema financiero y de sus relaciones con la sociedad de modo. Reúne 120 bancos asociados de un universo de 172 instituciones en operación en Brasil, los cuales representan el 97% de los activos totales y el 93% del patrimonio líquido del sistema bancario. Entre los objetivos estratégicos de la Federación están el desarrollo de iniciativas para la continua mejoría de la productividad del sistema bancario y la reducción de sus niveles de riesgo, contribuyendo de esta forma al desarrollo económico, social y sustentable del País. 
</p>
<p>
	Sepa más en <a href='http://www.febraban.org.br' title='FEBRABAN' target='_blank'>www.febraban.org.br</a>
</p>
TEXTO;
$lang['EVENTO_CONGRESSO_TITULO'] = 'El Congreso';
$lang['EVENTO_CONGRESSO_TEXTO'] = <<<TEXTO
<p>
Realizado juntamente a la Exposición, el Congreso Ciab FEBRABAN se lleva a cabo en 3 auditorios que cuentan con los mejores y más renombrados especialistas nacionales e internacionales del área de tecnología.
</p>
<p>
	<strong>Números del Congreso Ciab 2012 – La Sociedad Conectada</strong>
</p>
<p>
	&bull; 110 especialistas de las áreas de tecnología y bancaria<br>
	&bull; 19 keynote speakers internacionales<br>
	&bull; 1.717 congresistas
</p>
<p>
Con estos números, el Congreso Ciab FEBRABAN 2012 debatió temas importantes como innovación, estrategia de medias sociales, Cloud Computing, desarrollo ágil, Big Data, consumerización, tecnologías móviles, accesibilidad, seguridad de la información, además de paneles con CIOs y directores de las demás áreas de negocios de las instituciones financieras.
</p>
<p>
Entre las más destacadas participaciones, los congresistas apuntaron la conferencia del presidente de Itaú Unibanco, Roberto Setubal, realizada el día 20 de junio sobre el panel “La importancia de TI en el desarrollo del sistema financiero”.
</p>
TEXTO;
$lang['EVENTO_ESPACOS_TITULO'] = 'Espacios Temáticos';
$lang['EVENTO_ESPACOS_TEXTO'] = '<p> Ciab FEBRABAN 2011 refuerza, nuevamente, sus acciones destinadas para las pequeñas empresas, con el propósito de estimular la presencia de ese segmento en el congreso. </p>';
$lang['EVENTOS_ESPACOS_ITEM_1_TITULO'] = 'ESPACIO EMPREENDEDOR';
$lang['EVENTOS_ESPACOS_ITEM_1_TEXTO'] = 'Espacio dedicado a la exposición de empresas con capital el100% nacional, de pequeño porte y facturación limitada a R$ 12 millones. La empresa expositora cuenta con un paquete compuesto por espacio en la feria y montaje del stand a precios reducidos. ';
$lang['EVENTOS_ESPACOS_ITEM_2_TITULO'] = 'ESPACIO DE INNOVACIÓN';
$lang['EVENTOS_ESPACOS_ITEM_2_TEXTO'] = "FEBRABAN e ITS (Instituto de Tecnología de Software y Servicios) promovido durante la FEBRABAN Ciab, el \"Espacio de Innovación\". Ponga esta iniciativa y participar en el proceso de selección. Para obtener más información, envíe un correo electrónico a <a href='mailto:eventos@febraban.org.br'>eventos@febraban.org.br</a>.";
$lang['EVENTOS_ESPACOS_ITEM_3_TITULO'] = 'ESPACIO INTERNACIONAL';
$lang['EVENTOS_ESPACOS_ITEM_3_TEXTO'] = 'Espacio dedicado a la exposición de empresas que no tienen su sede en Brasil, con montaje del stand y toda la infraestructura.';

$lang['EVENTO_EXPOSICAO_TITULO'] = 'Exposicion';
$lang['EVENTO_EXPOSICAO_TEXTO'] = <<<TEXTO
<p>
Simultáneamente al congreso, CIAB FEBRABAN realiza su exposición que cuenta con la participación de centenas de empresas expositoras de las áreas de finanzas, tecnología de la información (TI) y telecomunicaciones. 
</p>

<p>
El visitante tendrá la oportunidad de ver todas las novedades y soluciones tecnológicas para los próximos años. Visite también los Espacios Temáticos, una oportunidad única de prestigiar a las pequeñas empresas nacionales del área de TI. 
</p>

<p>
Las visitas a la exposición son gratuitas y ocurren en el horario de 9h a 20h. Basta registrarse en el link abajo o directamente en el local, en el área “Credenciamento de Visitantes”. 
</p>

<p>
<span class="negrito">Importante:</span> la tarjeta de identificación de visitante es válida solamente para acceso al pabellón de la exposición. Bajo ninguna hipótesis será permitida la entrada de visitantes a las áreas exclusivas de congresistas e invitados. 
</p>
TEXTO;
$lang['EVENTO_QUERO_VISITAR_TITULO'] = 'Quiero Visitar';
$lang['EVENTO_QUERO_VISITAR_TEXTO_1'] = <<<TEXTO
<p>
También puedes visitar la exposición del Ciab FEBRABAN. La entrada es gratis. Par el cual, basta llene el formulario abajo con tus datos y enseguida imprimir tu invitación. Preséntalo en la entrada del Ciab FEBRABAN para puedas retirar tu gafete de visitante y tener el acceso a la exposición.
</p>

<p>
<span class="negrito">Aviso Importante:</span> el gafete de visitante solamente tendrá validez para el acceso a los pabellones de exposición. De ninguna será permitida la entrada de visitantes en áreas exclusivas para los congresistas e invitados.
</p>
TEXTO;
$lang['EVENTO_QUERO_VISITAR_TEXTO_2'] = <<<TEXTO
<p style="clear:left;">
Se preferir, clique no botão abaixo, preencha o formulário e faça agora mesmo seu cadastro.
</p>
<a href="http://www.credenciamento.com.br/2012/VisitanteCiab/Default_por.aspx" target="_blank" class="botao" style="color:#FFF;">HAGA CLIC AQUÍ PARA INSCRIBIRSE</a>
TEXTO;
$lang['EVENTO_EXPOR_TITULO'] = 'Quiero Esponer';
$lang['EVENTO_EXPOR_TEXTO'] = <<<TEXTO
<p>
Centenas de empresas de las áreas de finanzas, tecnología de la información (TI) y telecomunicaciones, anualmente exponen en CIAB FEBRABAN y tienen la oportunidad de estrechar sus relaciones con millares de personas que transitan por el evento.
</p>

<p>
En el lanzamiento de Ciab 2013, en octubre, 81 expositores garantizaron sus espacios en los 3.770m² del área de exposición. 
</p>
TEXTO;
$lang['EVENTO_EXPOR_FORM_TEXTO'] = 'Llene el formulario abajo, haga ahora mismo su registro y reserve el espacio de su empresa en el evento. Nuestros asesores lo contactarán para pasar todas las informaciones sobre cómo ser un expositor.';
$lang['EVENTO_EXPOR_FORM_1'] = 'Elija el Asunto';
$lang['EVENTO_EXPOR_FORM_2'] = 'Nombre (Completo)';
$lang['EVENTO_EXPOR_FORM_3'] = 'E-mail ';
$lang['EVENTO_EXPOR_FORM_4'] = 'Empresa';
$lang['EVENTO_EXPOR_FORM_5'] = 'Sitio de la Empresa';
$lang['EVENTO_EXPOR_FORM_6'] = 'Área de Actuación';
$lang['EVENTO_EXPOR_FORM_7'] = 'Su Teléfono';
$lang['EVENTO_EXPOR_FORM_8'] = 'Espacio/Metraje de interés';
$lang['EVENTO_EXPOR_FORM_SUBMIT'] = 'ENVIAR MENSAJE';
$lang['EVENTO_EXPOR_FORM_OBS'] = 'Para más información y reservas de espacio de exposición, contactar a:';
$lang['EVENTO_EXPOR_FORM_OK'] = '<h1>Obrigado!<br>Email enviado com sucesso.entraremos em contato em breve.</h1>';
$lang['EVENTO_EXPOR_FORM_ERROR'] = '<h1>O email não pode ser enviado. <br> Tente novamente.</h1>';
$lang['EVENTO_PATROCINAR_TITULO'] = 'Quiero Apoyar';
$lang['EVENTO_PATROCINAR_TEXTO'] = <<<TEXTO
<p>
Su empresa puede ser una de las patrocinadoras del mayor congreso y exposición de América Latina en tecnología de la información para instituciones financieras, y exponer su marca para millares de congresistas y visitantes que frecuentan los 3 días de Ciab FEBRABAN.

<p>
En 2012, la exposición de Ciab FEBRABAN recibió la visita de un público superior a 17 mil personas, altamente calificado, compuesto por presidentes, directores y gerentes de instituciones financieras de Brasil y de más de 26 países.
</p>
TEXTO;
$lang['EVENTO_PATROCINAR_OBS'] = 'Para más información y reservas de espacio de exposición, contactar a:';
$lang['EVENTO_MAPA_EXPOSITORES'] = 'EXPOSITORES';
$lang['EVENTO_ANOS_TITULO'] = 'Otros Años';
$lang['EVENTO_ANOS_SUBTITULO'] = 'Vea lo que sucedió en las ediciones anteriores del evento';
$lang['EVENTO_ANOS_ITEM_1'] = 'La tecnología más allá de la web';
$lang['EVENTO_ANOS_ITEM_2'] = 'Generación Y<br> un nuevo banco para<br> un nuevo consumidor';
$lang['EVENTO_ANOS_ITEM_3'] = 'Un amplio debate sobre<br> la tecnología y su impacto<br> en la inclusión bancaria';
$lang['EVENTO_ANOS_ITEM_4'] = 'Tecnología y Seguridad';

/* PRÊMIOS */
$lang['PREMIOS_CIAB_TITULO'] = 'Prêmio Ciab 2013';
$lang['PREMIOS_CIAB_SUBTITULO'] = 'PRÊMIO CIAB FEBRABAN! PREPARE-SE PARA SER O VENCEDOR DE 2013!';
$lang['Menção Honrosa'] = 'Menção Honrosa';
$lang['PREMIOS_CIAB_TEXTO'] = <<<TEXTO
<p>O Prêmio CIAB FEBRABAN visa o incentivo direto da capacidade de inovação técnica de jovens e universitários e o debate de propostas inovadoras. Fale com seus amigos e, sozinho ou em grupo, concorra a um total de R$ 25mil em prêmios, além do trabalho vencedor ser exposto para diversos especialistas e executivos das áreas de tecnologia e bancária.</p>
<p>O tema para 2013 é <strong>“Soluções para Eficiência Operacional”</strong></p>
<p>As <strong>inscrições poderão ser realizadas a partir de 06.03.2013</strong>. Prepare-se para ser o vencedor de 2013!</p>
TEXTO;
$lang['PREMIOS_CIAB_CHAMADA'] = 'Clique no botão abaixo e confira o regulamento.';
$lang['PREMIOS_CIAB_BOTAO_1'] = 'VER REGULAMENTO';
$lang['PREMIOS_CIAB_BOTAO_2'] = 'LEA EL REGLAMENTO';
$lang['PREMIOS_VENCEDORES_TITULO'] = 'Vencedores CIAB 2011';
$lang['PREMIOS_VENCEDORES_SUBTITULO'] = 'Ciab FEBRABAN y Universia lanzan la segunda edición de premio para jóvenes.';
$lang['PREMIOS_VENCEDORES_TEXTO_1'] = '<p>La edición 2011 del Premio CIAB FEBRABAN y Universia (que tuvo como tema “La Tecnología Además de la Web”) registró 611 inscripciones contra 223 en la primera edición del premio en el 2010. Verifique abajo los vencedores.</p>';
$lang['PREMIOS_VENCEDORES_COMUNICADO'] = 'COMUNICADO – RECLASIFICACIÓN PREMIO CIAB FEBRABAN y UNIVERSIA 2011';
$lang['PREMIOS_VENCEDORES_TEXTO_2'] = <<<TEXTO
<p>La comisión organizadora del Ciab FEBRABAN 2011, con base en los criterios del reglamento del Premio Ciab FEBRABAN y Universia 2011, reevaluando a los competidores, concluyó que el trabajo clasificado anteriormente en primer lugar (2Whish) fue desclasificado por no cumplir con requisitos establecidos en el reglamento.</p>

<p>Por lo tanto, la clasificación final del premio, pasa a ser la siguiente:</p>
TEXTO;
$lang['PREMIOS_VENCEDORES_TABELA_1'] = 'Clasificación';
$lang['PREMIOS_VENCEDORES_TABELA_2'] = 'Nombre';
$lang['PREMIOS_VENCEDORES_TABELA_3'] = 'Título';
$lang['PREMIOS_VENCEDORES_TWITTER_TITULO'] = 'Vencedores TWITTER 2011';
$lang['PREMIOS_VENCEDORES_TWITTER_SUBTITULO'] = 'Concurso Cultural Mejor Slogan CIAB en el Twitter';
$lang['PREMIOS_VENCEDORES_TWITTER_CHAMADA'] = '¡Quien siguió el @CiabFEBRABAN y mandó su slogan hizo bien!';
$lang['PREMIOS_VENCEDORES_TWITTER_TEXTO'] = <<<TEXTO
<p>El Ciab FEBRABAN trajo una novedad más para la edición 2011: el concurso cultural "Mejor Slogan Ciab en el Twitter". Para participar, bastaba seguir el perfil @CiabFEBRABAN y enviar un slogan con hasta 140 caracteres.</p>

<p>Fueron 37 slogans recibidos y 3 premiados con ingresos válidos para los tres días del Ciab FEBRABAN 2011, con derecho a participar de la exposición y de las conferencias del congreso.</p>

<p>Continúe siguiendo al @CiabFEBRABAN y entérese de todo lo que acontece en el mayor Congreso y Exposición de Tecnología de la Información de América Latina.</p>
TEXTO;


$lang['PREMIOS_VENCEDORES_2012_TITULO'] = 'Confira os vencedores do Prêmio CIAB 2012';
$lang['PREMIOS_VENCEDORES_2012_PROJETO'] = 'PROJETO';
$lang['PREMIOS_VENCEDORES_2012_GRUPO'] = 'GRUPO';
$lang['PREMIOS_VENCEDORES_2012_PROJETO_1'] = 'ProDeaf – Software Tradutor (Português/LIBRAS – LIBRAS/Português)';
$lang['PREMIOS_VENCEDORES_2012_GRUPO_1'] = <<<LIST
<p>- Marcelo Lucio Correia de Amorim</p>
<p>- Lucas Araújo Mello Soares</p>
<p>- João Paulo dos Santos Oliveira</p>
<p>- Luca Bezerra Dias</p>
<p>- Roberta Agra Coutelo</p>
<p>- Victor Rafael Nascimento dos Santos</p>
LIST;
$lang['PREMIOS_VENCEDORES_2012_PROJETO_2'] = 'Bidd – Plataforma móvel para contratação de crédito de consumo.';
$lang['PREMIOS_VENCEDORES_2012_GRUPO_2'] = <<<LIST
<p>- Daniel César Vieira Radicchi</p>
<p>- André Luis Ferreira Marques</p>
<p>- Willy Stadnick Neto</p>
<p>- Vinicius Abouhatem</p>
<p>- Daniel Coelho</p>
LIST;
$lang['PREMIOS_VENCEDORES_2012_PROJETO_3'] = 'My Tag – Rede social de consumo.';
$lang['PREMIOS_VENCEDORES_2012_GRUPO_3'] = <<<LIST
<p>- Holisson Soares da Cunha</p>
<p>- Sergio Luis Sardi Mergen</p>
<p>- Luciana Hikari Kuamoto</p>
<p>- Leonardo Seiji</p>
LIST;

/* IMPRENSA */
$lang['IMPRENSA_ASSESSORIA_TITULO'] = 'Prensa';
$lang['IMPRENSA_ASSESSORIA_TEXTO_1'] = 'Para recibir press releases, participar de colectivas y entrevistas, contáctese con nosotros';
$lang['IMPRENSA_ASSESSORIA_TEXTO_2'] = 'Si desea tomar la cobertura del evento, llene el siguiente formulario y haga su registro. Nuestros asesores se comunicarán cuando sea necesario.';
$lang['IMPRENSA_ASSESSORIA_FORM_1'] = 'Nombre (Completo)';
$lang['IMPRENSA_ASSESSORIA_FORM_2'] = 'E-mail';
$lang['IMPRENSA_ASSESSORIA_FORM_3'] = 'MTB';
$lang['IMPRENSA_ASSESSORIA_FORM_4'] = 'Empresa';
$lang['IMPRENSA_ASSESSORIA_FORM_5'] = 'Sitio de la Empresa';
$lang['IMPRENSA_ASSESSORIA_FORM_6'] = 'Área de Actuación';
$lang['IMPRENSA_ASSESSORIA_FORM_7'] = 'Su Teléfono';
$lang['IMPRENSA_ASSESSORIA_FORM_SUBMIT'] = 'ENVIAR MENSAJE';
$lang['IMPRENSA_ASSESSORIA_FORM_OK'] = '<h1>Obrigado! <br> Email enviado com  ssucesso. Entraremos em contato em breve.</h1>';
$lang['IMPRENSA_ASSESSORIA_FORM_ERROR'] = '<h1>O email não pode ser enviado. <br> Tente novamente.</h1>';

/* INFORMAÇÕES */
$lang['INFO_PAVILHOES'] = 'Pabellones A, B, C y D';
$lang['INFORMACOES_LOCAL_TITULO'] = 'Local del Evento';
$lang['INFORMACOES_LOCAL_VERMAPA'] = 'ver mapa más grande';
$lang['INFORMACOES_HOTEIS_TITULO'] = 'Hoteles';
$lang['INFORMACOES_HOTEIS_SUBTITULO'] = 'Clique sobre o hotel para mais detalhes';
$lang['INFORMACOES_HOTEIS_LEGENDA'] = 'SGL: Acomoda 1 pessoa | DBL: Acomoda 2 pessoas';
$lang['INFORMACOES_HOTEIS_TABELA_TITULO_1'] = 'HOTEL';
$lang['INFORMACOES_HOTEIS_TABELA_TITULO_2'] = 'Costo diario SUPERIOR SGL';
$lang['INFORMACOES_HOTEIS_TABELA_TITULO_3'] = 'Costo diario SUPERIOR DBL';
$lang['INFORMACOES_HOTEIS_COMDESCONTO'] = 'ya con descuento';
$lang['INFORMACOES_HOTEIS_LOCAL'] = 'Local';
$lang['INFORMACOES_HOTEIS_TEL'] = 'Tel';
$lang['INFORMACOES_HOTEIS_COMO_CHEGAR'] = 'Pulse aquí para ver cómo llegar';
$lang['INFORMACOES_HOTEIS_CODIGO'] = 'Código para reservar';
$lang['INFORMACOES_HOTEIS_DIARIA_SGL'] = 'Costo diario del apartamento Superior SGL';
$lang['INFORMACOES_HOTEIS_DIARIA_DBL'] = 'Costo diario del apartamento Superior DBL';
$lang['INFORMACOES_HOTEIS_DIARIA'] = 'El costo diario incluye (por adhesión)';
$lang['INFORMACOES_HOTEIS_OBSERVACOES'] = 'Observaciones';
$lang['INFORMACOES_HOTEIS_CHECK_1'] = '- Registro a partir de las 2.00 p.m.<br> - Salida del hotel hasta 12.00 a.m.';
$lang['INFORMACOES_HOTEIS_CHECK_2'] = 'los días comienzan y se encierran a las 12.00 a.m.';
$lang['INFORMACOES_HOTEIS_CHECK_3'] = 'los días comienzan a las 14.00<br> y encierran a las 12.00';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_1'] = 'Desayuno servido en el restaurante<br> (6.00 a.m. a 10.00 a.m.);';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_2'] = '- Desayuno (servido en el mismo restaurante)<br>- 01 lugar en la cochera por apartamento';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_3'] = '- Desayuno (servido en el restaurante)<br> - Traslado del Aeropuerto de Congonhas<br> / Hotel / Aeropuerto de Congonhas';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_4'] = '- Desayuno (servido en el restaurante)<br> - 01 lugar en la cochera por apartamento';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_5'] = '- Desayuno (servido en el restaurante)';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_6'] = '- Desayuno (servido en el restaurante)<br>- Internet';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_1'] = 'El valor del costo diario será aumentando en 5% de ISS + R$ 4,50 de taza de turismo y el pago de los gastos de hospedaje será realizada con tarjeta de crédito o cash directamente en el Hotel Transamérica São Paulo. Los apartamentos están sujetos a disponibilidad.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_2'] = 'El valor del costo diario será aumentando en 5% de ISS + taza de turismo y el pago de los gastos de hospedaje será realizada con tarjeta de crédito o cash directamente en el Transamérica Executive Chácara Santo Antonio. Los apartamentos están sujetos a disponibilidad.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_3'] = 'El valor del costo diario será aumentando en 5% de ISS + taza de turismo y el pago de los gastos de hospedaje será realizada con tarjeta de crédito o cash directamente en el Transamérica Executive Congonhas. Los apartamentos están sujetos a disponibilidad.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_4'] = 'El valor del costo diario será aumentando en 5% de ISS + taza de turismo y el pago de los gastos de hospedaje será realizada con tarjeta de crédito o cash directamente en el Transamérica Prime International Plaza. Los apartamentos están sujetos a disponibilidad.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_5'] = 'El valor del costo diario será aumentando en 5% de ISS + taza de turismo y el pago de los gastos de hospedaje será realizada con tarjeta de crédito o cash directamente en el Blue Tree Premium Morumbi. Los apartamentos están sujetos a disponibilidad.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_6'] = 'El valor del costo diario será aumentando en 5% de ISS + taza de turismo y el pago de los gastos de hospedaje será realizada con tarjeta de crédito o cash directamente en el Blue Tree Premium Verbo Divino. Los apartamentos están sujetos a disponibilidad.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_7'] = 'El valor del costo diario será aumentando en 5% de ISS + taza de turismo y el pago de los gastos de hospedaje será realizada con tarjeta de crédito o cash directamente en el Hotel Quality Berrini. Los apartamentos están sujetos a disponibilidad.';
$lang['CONTATO_INFORMACOES_GERAIS'] = 'INFORMACIONES GENERALES';
$lang['CONTATO_COMO_CHEGAR'] = 'CÓMO LLEGAR';
$lang['CONTATO_RESERVAS'] = 'Reservas de espacios';
$lang['CONTATO_FORM_TITULO'] = 'Llene el siguiente formulario  para contacto';
$lang['CONTATO_FORM_ASSUNTO_TITULO'] = 'Assunto';
$lang['CONTATO_FORM_ASSUNTO_1'] = 'Otros';
$lang['CONTATO_FORM_ASSUNTO_2'] = 'Conferencista';
$lang['CONTATO_FORM_ASSUNTO_3'] = 'Expositor';
$lang['CONTATO_FORM_ASSUNTO_4'] = 'Visitante';
$lang['CONTATO_FORM_ASSUNTO_5'] = 'Apoyo';
$lang['CONTATO_FORM_ASSUNTO_6'] = 'Prensa';
$lang['CONTATO_FORM_1'] = 'Nombre (Completo)';
$lang['CONTATO_FORM_2'] = 'E-mail';
$lang['CONTATO_FORM_3'] = 'Empresa';
$lang['CONTATO_FORM_4'] = 'Sitio de la Empresa';
$lang['CONTATO_FORM_5'] = 'Área de Especialización';
$lang['CONTATO_FORM_6'] = 'Su Teléfono';
$lang['CONTATO_FORM_7'] = 'Mensaje';
$lang['CONTATO_FORM_SUBMIT'] = 'ENVIAR MENSAJE';
$lang['CONTATO_FORM_OK'] = '<h1>Obrigado! <br>Email enviado com sucesso. Entraremos em contato em breve.</h1>';
$lang['CONTATO_FORM_ERROR'] = '<h1>O email não pode ser enviado. <br>Tente novamente.</h1>';

/***************/
/* PROGRAMAÇÃO */
/***************/

/* DIA 1 */
$lang['auditorio-1'] = <<<STR
AUDITÓRIO FEBRABAN
STR;

$lang['auditorio-2'] = <<<STR
AUDITÓRIO LINHA DE NEGÓCIOS
STR;

$lang['auditorio-3'] = <<<STR
AUDITÓRIO EFICIÊNCIA OPERACIONAL
STR;

$lang['obs-solenidade'] = <<<STR
Solenidade de abertura da Exposição e visita aos estandes dos Patrocinadores / Coffee break
STR;

$lang['1-sessao-1'] = <<<STR
1ª sessão manhã - das 09:00 às 10:30
STR;

$lang['1-sessao-2'] = <<<STR
2ª sessão manhã - das 11:00 às 12:30
STR;

$lang['1-sessao-3'] = <<<STR
1ª sessão tarde - das 14:00 às 15:30
STR;

$lang['1-sessao-4'] = <<<STR
2ª sessão tarde - das 16:00 às 17:00 e das 17:15 às 18:15
STR;

$lang['DIA_1_MANHA_1_A'] = <<<STR
Abertura
STR;

$lang['DIA_1_MANHA_1_B'] = <<<STR
Transmissão simultânea da Abertura
STR;

$lang['DIA_1_MANHA_1_C'] = <<<STR
Transmissão simultânea da Abertura
STR;


$lang['DIA_1_MANHA_2_A'] = <<<STR
Jorge Hereda - Presidente da CAIXA
STR;

$lang['DIA_1_MANHA_2_B'] = <<<STR
Transmissão simultânea
STR;

$lang['DIA_1_MANHA_2_C'] = <<<STR
Transmissão simultânea
STR;


$lang['DIA_1_TARDE_1_A'] = <<<STR
Keynote Speaker
STR;

$lang['DIA_1_TARDE_1_B'] = <<<STR
Interagindo com Clientes e Funcionários com Mobilidade
STR;

$lang['DIA_1_TARDE_1_C'] = <<<STR
Appification - Das aplicações para aplicativos
STR;


$lang['DIA_1_TARDE_2_A'] = <<<STR
A definir
STR;

$lang['DIA_1_TARDE_2_B'] = <<<STR
Impacto das Megatendências globais nos Serviços Financeiros<br>
<br>
<br>
==================================== <br>
Intervalo das 17:00 às 17:15<br>
==================================== <br>
Converting data into a strategic business asset for Banks<br>
<br>
<br>
<br>
Moderador: Jorge Vacarini - Deutsche Bank
STR;

$lang['DIA_1_TARDE_2_C'] = <<<STR
Cloud Computing na Prática
STR;



/* DIA 2 */
$lang['DIA_2_MANHA_1_A'] = <<<STR
Keynote Speaker
STR;

$lang['DIA_2_MANHA_1_B'] = <<<STR
BPO - Business Process Outsourcing
STR;

$lang['DIA_2_MANHA_1_C'] = <<<STR
Estratégia para o desenvolvimento de um modelo de Governança de TI 
STR;



$lang['DIA_2_MANHA_2_A'] = <<<STR
Expansão do Crédito - Formas e Limites 
STR;

$lang['DIA_2_MANHA_2_B'] = <<<STR
Projetos de Inovação ou Cultura em Inovação
STR;

$lang['DIA_2_MANHA_2_C'] = <<<STR
Big Data
STR;


$lang['DIA_2_TARDE_1_A'] = <<<STR
Painel das Áreas de Negócios dos Bancos
STR;

$lang['DIA_2_TARDE_1_B'] = <<<STR
Segurança da informação
STR;

$lang['DIA_2_TARDE_1_C'] = <<<STR
A Definir
STR;


$lang['DIA_2_TARDE_2_A'] = <<<STR
Infraestrutura do Brasil e Inovação
STR;

$lang['DIA_2_TARDE_2_B'] = <<<STR
Experiencia de Clientes vs Fidelização<br>
<br>
<br>
=====================================<br>
Intervalo das 17:00 às 17:15<br>
==================================== <br>
Costumer Excellence in Banking
STR;

$lang['DIA_2_TARDE_2_C'] = <<<STR
Eficiência Operacional e Redução de Custos Operacionais
STR;



/* DIA 3 */
$lang['3-sessao-1'] = <<<STR
1ª sessão manhã - das 09:00 às 10:30
STR;

$lang['3-sessao-2'] = <<<STR
2ª sessão manhã - das 11:00 às 12:30
STR;

$lang['3-sessao-3'] = <<<STR
1ª sessão tarde - das 14:00 às 16:00
STR;

$lang['3-sessao-4'] = <<<STR
2ª sessão tarde - das 16:30 às 18:00
STR;

$lang['DIA_3_MANHA_1_A'] = <<<STR
Inovação na Indústria de TI para os Bancos
STR;


$lang['DIA_3_MANHA_1_B'] = <<<STR
Transmissão Simultânea
STR;

$lang['DIA_3_MANHA_1_C'] = <<<STR
Transmissão Simultânea
STR;


$lang['DIA_3_MANHA_2_A'] = <<<STR
Lideranças de TI debatem Tendências
STR;

$lang['DIA_3_MANHA_2_B'] = <<<STR
Basiléia III
STR;

$lang['DIA_3_MANHA_2_C'] = <<<STR
Inovação em Auto Atendimento
STR;


$lang['DIA_3_TARDE_1_A'] = <<<STR
A definir

STR;

$lang['DIA_3_TARDE_1_B'] = <<<STR
A definir
STR;

$lang['DIA_3_TARDE_1_C'] = <<<STR
A definir
STR;


$lang['DIA_3_TARDE_2_A'] = <<<STR
A definir
STR;

$lang['DIA_3_TARDE_2_B'] = <<<STR
Transmissão Simultânea
STR;

$lang['DIA_3_TARDE_2_C'] = <<<STR
Transmissão Simultânea
STR;

$lang['Auditórios Excelência em TI'] = 'Auditórios Excelência em TI';
$lang['Auditorios Horário'] = 'Horário';
$lang['Auditorios Palestrantes'] = 'Palestrantes';
$lang['Auditorios Tema'] = 'Tema';

/* DIA 1 */
$lang['dia_1 Auditorio 1'] = 'Auditório 3 - TALARIS';
$lang['dia_1 Auditorio 3'] = 'Auditório 3 - LÓGICA';
$lang['dia_1 Auditorio 2'] = 'Auditório 2 - Acesso Digital';

$lang['dia1_auditorio_2_horario_1'] = '14h30';
$lang['dia1_auditorio_2_horario_2'] = '14h40';
$lang['dia1_auditorio_2_horario_3'] = '15h00';
$lang['dia1_auditorio_2_horario_4'] = '15h30';
$lang['dia1_auditorio_2_horario_5'] = '16h00';
$lang['dia1_auditorio_2_palestrante_1'] = 'Antonio Augusto de Almeida Leite (Pancho), ACREFI';
$lang['dia1_auditorio_2_palestrante_2'] = 'Breno Costa, Sócio e Consultor da GoOn – A Evolução na Gestão do Risco';
$lang['dia1_auditorio_2_palestrante_3'] = 'Ricardo Batista, Superintendente de Crédito do Tribanco';
$lang['dia1_auditorio_2_palestrante_4'] = 'Roberto Jabali, Superintendente de Crédito do Grupo Citi - Credicard';
$lang['dia1_auditorio_2_palestrante_5'] = 'Alex Yamamoto,Consultor da Acesso Digital';
$lang['dia1_auditorio_2_tema_1'] = 'Abertura';
$lang['dia1_auditorio_2_tema_2'] = 'Case GoOn: Como aumentar o nível de decisão automática sem impacto na inadimplência.';
$lang['dia1_auditorio_2_tema_3'] = 'Case Tribanco: captura de documentos no varejo';
$lang['dia1_auditorio_2_tema_4'] = 'Case Grupo Citi: Pioneirismo na gestão eletrônica de documentos';
$lang['dia1_auditorio_2_tema_5'] = 'Formalização de crédito online nas instituições financeiras';

$lang['dia1_auditorio_3_horario_1'] = '09h00 - 10h00';
$lang['dia1_auditorio_3_horario_2'] = '11h00 – 12h00';
$lang['dia1_auditorio_3_horario_3'] = '14h00 - 15h10';
$lang['dia1_auditorio_3_horario_4'] = '15h20 – 16h30';
$lang['dia1_auditorio_3_horario_5'] = '16h40 – 17h50';
$lang['dia1_auditorio_3_horario_6'] = '17h50 -18h00';
$lang['dia1_auditorio_3_horario_7'] = ' ';
$lang['dia1_auditorio_3_palestrante_1'] = 'Paulo Martins, Diretor Comercial / Logica';
$lang['dia1_auditorio_3_palestrante_2'] = 'Reginaldo Silva, Portfólio / Logica';
$lang['dia1_auditorio_3_palestrante_3'] = 'Antonio Requejo, Regional Practice Leader Iberia & Latam – Security / Logica';
$lang['dia1_auditorio_3_palestrante_4'] = 'Júlio Gonçalves, Regional Practice Leader Iberia & Latam – IT Modernization / Logica';
$lang['dia1_auditorio_3_palestrante_5'] = 'Lode Snykers, Vice President, Global Financial Services / Logica';
$lang['dia1_auditorio_3_palestrante_6'] = 'Paulo Martins, Diretor Comercial / Logica';
$lang['dia1_auditorio_3_palestrante_7'] = ' ';
$lang['dia1_auditorio_3_tema_1'] = 'Compartilhando Idéias';
$lang['dia1_auditorio_3_tema_2'] = 'Mobilidade: Saiba como surpreender os seus clientes com soluções modernas e inovadoras';
$lang['dia1_auditorio_3_tema_3'] = 'De Securidad en TI para el Gerenciamento de Riesgo';
$lang['dia1_auditorio_3_tema_4'] = 'Modernização de Sistemas Legados';
$lang['dia1_auditorio_3_tema_5'] = 'The technological challenges of the Financial Sector in the XXI century';
$lang['dia1_auditorio_3_tema_6'] = 'Compartilhando Ideias II';
$lang['dia1_auditorio_3_tema_7'] = 'Sorteios para os participantes do dia';


/* DIA 2 */
$lang['dia_2 Auditorio 1'] = 'Auditório 1 - HP';
$lang['dia_2 Auditorio 2'] = 'Auditório 2 - Microsoft';
$lang['dia_2 Auditorio 3'] = 'Auditório 3 - CLM';

/* Auditorio 1 */
$lang['dia2_auditorio_1_horario_1'] = '10h00 – 11h00';
$lang['dia2_auditorio_1_horario_2'] = '11h10 – 12h10';
$lang['dia2_auditorio_1_horario_3'] = '14h30 – 15h30';
$lang['dia2_auditorio_1_horario_4'] = '15h40 – 16h00';
$lang['dia2_auditorio_1_palestrante_1'] = 'Mike Wright, HP';
$lang['dia2_auditorio_1_palestrante_2'] = 'Gladson Martins Russo, Nokia Siemens';
$lang['dia2_auditorio_1_palestrante_3'] = 'Andre Kalsing, HP';
$lang['dia2_auditorio_1_palestrante_4'] = 'Federico Grosso, Autonomy';
$lang['dia2_auditorio_1_tema_1'] = 'Tendências para Social CRM';
$lang['dia2_auditorio_1_tema_2'] = 'Novas tecnologias para Mobile Payment';
$lang['dia2_auditorio_1_tema_3'] = 'Benefícios da adoção de uma plataforma de serviços multicanal';
$lang['dia2_auditorio_1_tema_4'] = 'Uma nova Visão de Inovação e Big Data';


// Auditorio 2
$lang['dia2_auditorio_2_horario_1'] = '9h-10h';
$lang['dia2_auditorio_2_horario_2'] = '10h-11h30';
$lang['dia2_auditorio_2_horario_3'] = '11h30-12h30';
$lang['dia2_auditorio_2_horario_4'] = '14h-15h';
$lang['dia2_auditorio_2_horario_5'] = '15h-16h';
$lang['dia2_auditorio_2_horario_6'] = '16h-17h';
$lang['dia2_auditorio_2_horario_7'] = '17h-18h';
$lang['dia2_auditorio_2_palestrante_1'] = 'Fabio Souto';
$lang['dia2_auditorio_2_palestrante_2'] = 'Miha Krajl';
$lang['dia2_auditorio_2_palestrante_3'] = 'Jun Endo';
$lang['dia2_auditorio_2_palestrante_4'] = 'Eduardo Campos';
$lang['dia2_auditorio_2_palestrante_5'] = 'Roberto Prado';
$lang['dia2_auditorio_2_palestrante_6'] = 'Ricardo-Enrico Jahn';
$lang['dia2_auditorio_2_palestrante_7'] = 'Richard Chaves';
$lang['dia2_auditorio_2_tema_1'] = 'Sociedade Conectada e a Consumerização de TI';
$lang['dia2_auditorio_2_tema_2'] = 'Como TI está mudando nosso futuro';
$lang['dia2_auditorio_2_tema_3'] = 'Soluções Inovadoras para uma Experiência Única Multidevices & Consumerização de TI';
$lang['dia2_auditorio_2_tema_4'] = 'A nuvem nos seus termos - Estratégia de Computação na nuvem da Microsoft';
$lang['dia2_auditorio_2_tema_5'] = 'O profissional TI como protagonista da competitividade. Capacitação e Satisfação do cliente.';
$lang['dia2_auditorio_2_tema_6'] = 'Relacionamento e fidelização nas instituições financeiras com Dynamics CRM/XRM';
$lang['dia2_auditorio_2_tema_7'] = 'O poder da Inovação';

// Auditorio 3
$lang['dia2_auditorio_3_horario_1'] = '10h30 - 11h30';
$lang['dia2_auditorio_3_horario_2'] = '11h30 - 12h30';
$lang['dia2_auditorio_3_horario_3'] = '14h30 - 15h30';
$lang['dia2_auditorio_3_horario_4'] = '15h30 - 16h30';
$lang['dia2_auditorio_3_horario_5'] = '16h30 - 17h30';
$lang['dia2_auditorio_3_horario_6'] = '17h30 - 18h30';
$lang['dia2_auditorio_3_palestrante_1'] = 'Martin Roesch, SNORT';
$lang['dia2_auditorio_3_palestrante_2'] = 'Alex Silva, A10';
$lang['dia2_auditorio_3_palestrante_3'] = 'Carlos Fagundes, INTEGRALTRUST';
$lang['dia2_auditorio_3_palestrante_4'] = 'Sanjay Ramnath, BARRACUDA';
$lang['dia2_auditorio_3_palestrante_5'] = 'Dick Faulkner, SOPHOS';
$lang['dia2_auditorio_3_palestrante_6'] = 'Martin Roesch, SOURCEFIRE';
$lang['dia2_auditorio_3_tema_1'] = 'Open Source Security';
$lang['dia2_auditorio_3_tema_2'] = 'DDoS Attacks and Brute Force';
$lang['dia2_auditorio_3_tema_3'] = 'Segredos do Basileia III';
$lang['dia2_auditorio_3_tema_4'] = 'Application Security';
$lang['dia2_auditorio_3_tema_5'] = 'Cryptography and Mobile Security';
$lang['dia2_auditorio_3_tema_6'] = 'Next Generation Security';

/* DIA 3*/
$lang['dia_3 Auditorio 1'] = 'Auditório 1 - FEBRABAN';

$lang['dia3_auditorio_1_horario_1'] = '11h00 - 12h30';
$lang['dia3_auditorio_1_palestrante_1'] = 'Guilhermino Domiciano de Souza – Banco do Brasil;  Marcelo Ribeiro Câmara – Bradesco;  Cesar Faustino – Itaú Unibanco';
$lang['dia3_auditorio_1_tema_1'] = 'A Segurança Digital do Cidadão no Sistema Financeiro Brasileiro';


$lang['Programação em Breve'] = "Espere, en breve publicaremos la programación del congreso.";


$lang['CIA AÉREA OFICIAL'] = "LÍNEA AÉREA OFICIAL";
$lang['Parceria TAM'] = "TAM asociación";
$lang['A TAM é a Cia Aérea Oficial do Ciab 2013 e quem ganha o desconto é você!'] = "¡Tam es la Línea Aérea Oficial del Ciab FEBRABAN 2013 y quién gana el descuento es Usted!";
$lang['Em parceria com o Ciab 2013, a TAM como Cia Aérea Oficial do congresso disponibiliza passagens aéreas com condições especiais para você comparecer ao Ciab e ficar por dentro de tudo que as empresas de ponta em tecnologia estão preparando para superar “Os Novos Desafios do Setor Financeiro”.'] = "En aparcería con el Ciab FEBRABAN 2013, Tam como la Línea Aérea Oficial del congreso ofrece billetes aéreos con condiciones especiales para Usted participar del Ciab FEBRABAN y enterarse de todo cuanto las empresas de punta en tecnología de la información están preparando para superar “Los Nuevos Desafíos del Sector Financiero”.";
$lang['Confira abaixo as condições que preparamos para você, válidos para todos os trechos <strong>Brasil/São Paulo/Brasil</strong> no período de <strong>embarque</strong> entre <strong>09 e 17.06.2013:</strong>'] = "Confiera abajo las condiciones disponibles, válidas para todos los trechos <strong>Brasil/São Paulo/Brasil</strong> en el período de <strong>embarque</strong> entre <strong>09 y 17.06.2013:</strong>";
$lang["A TAM é a CIA Aérea Oficial do Ciab e quem ganha o desconto é você. Clique aqui e veja as condições especiais oferecidas."] = "TAM és la Línea Aérea Oficial del Ciab 2013 y quién gana el descuento es Usted. Haga aqui para ver las condiciones especiales ofrecidas.";
$lang['- 25%* de desconto na tarifa da classe disponível<br><br><strong>ou</strong><br><br>- 10%* de desconto nas tarifas Mega Promo e nas classes W e N'] = "- 25%* de descuento en la tarifa de la clase disponible<br><br><strong>or</strong><br><br>- 10%* de descuento en las tarifas Mega Promo y en las clases W y N";
$lang["Para conseguir o seu desconto é fácil:<br>Clique no botão abaixo ou acesse <a href='http://www.tam.com.br' title='TAM' target='_blank'>www.tam.com.br</a> e ao escolher o <strong>trecho</strong> e a <strong>data</strong>, digite o número do Código Promocional <strong>413709</strong>."] = "Obtener este beneficio es fácil:<br>Click en el botón abajo o accede <a href='http://www.tam.com.br' title='TAM' target='_blank'>www.tam.com.br</a> y al definir el <strong>trecho</strong> y la <strong>fecha</strong>, digite el número del Código Promocional <strong>413709</strong>.";
$lang['Clique e Compre sua Passagem'] = "Comprar un pase";
$lang['* valores sujeitos à disponibilidade de assentos e regras/restrições específicas de cada tarifa, válidos para embarque três dias antes e três após a data do evento no trecho Brasil/São Paulo/Brasil.'] = "* valores sujetos a la disponibilidad de asientos y reglas/restricciones específicas de cada tarifa, validos para embarque tres días antes y tres después de la fecha del evento en el trecho Brasil/São Paulo/Brasil.";


// ARQUIVO : ../www/ciab/application/views/programacao//primeiro.php
$lang['Quarta-Feira'] = "";
$lang['AUDITÓRIO FEBRABAN'] = "";
$lang['sessão'] = "";
$lang['horário'] = "";
$lang['palestra'] = "";
$lang['palestrante'] = "";
$lang['manhã'] = "";
$lang['às'] = "";
$lang['Abertura'] = "";
$lang['Jorge Hereda - Presidente da CAIXA'] = "";
$lang['tarde'] = "";
$lang['Keynote Speaker'] = "";
$lang['A definir'] = " ";
$lang['obs-solenidade'] = "";
$lang['AUDITÓRIO LINHA DE NEGÓCIOS'] = "";
$lang['Transmissão simultânea da Abertura'] = "";
$lang['Transmissão simultânea'] = "";
$lang['Interagindo com Clientes e Funcionários com Mobilidade'] = "";
$lang['Impacto das Megatendências globais nos Serviços Financeiros'] = "";
$lang['intervalo'] = "";
$lang['Converting data into a strategic business asset for Banks'] = "";
$lang['Moderador: Jorge Vacarini - Deutsche Bank'] = "";
$lang['AUDITÓRIO EFICIÊNCIA OPERACIONAL'] = "";
$lang['Appification - Das aplicações para aplicativos'] = "";
$lang['Cloud Computing na Prática'] = "";
$lang['Auditórios Excelência em TI'] = "";
$lang['dia_1 Auditorio 2'] = "";
$lang['Auditorios Horário'] = "";
$lang['Auditorios Palestrantes'] = "";
$lang['Auditorios Tema'] = "";
$lang['dia1_auditorio_2_horario_1'] = "";
$lang['dia1_auditorio_2_palestrante_1'] = "";
$lang['dia1_auditorio_2_tema_1'] = "";
$lang['dia1_auditorio_2_horario_2'] = "";
$lang['dia1_auditorio_2_palestrante_2'] = "";
$lang['dia1_auditorio_2_tema_2'] = "";
$lang['dia1_auditorio_2_horario_3'] = "";
$lang['dia1_auditorio_2_palestrante_3'] = "";
$lang['dia1_auditorio_2_tema_3'] = "";
$lang['dia1_auditorio_2_horario_4'] = "";
$lang['dia1_auditorio_2_palestrante_4'] = "";
$lang['dia1_auditorio_2_tema_4'] = "";
$lang['dia1_auditorio_2_horario_5'] = "";
$lang['dia1_auditorio_2_palestrante_5'] = "";
$lang['dia1_auditorio_2_tema_5'] = "";
$lang['dia_1 Auditorio 3'] = "";
$lang['dia1_auditorio_3_horario_1'] = "";
$lang['dia1_auditorio_3_palestrante_1'] = "";
$lang['dia1_auditorio_3_tema_1'] = "";
$lang['dia1_auditorio_3_horario_2'] = "";
$lang['dia1_auditorio_3_palestrante_2'] = "";
$lang['dia1_auditorio_3_tema_2'] = "";
$lang['dia1_auditorio_3_horario_3'] = "";
$lang['dia1_auditorio_3_palestrante_3'] = "";
$lang['dia1_auditorio_3_tema_3'] = "";
$lang['dia1_auditorio_3_horario_4'] = "";
$lang['dia1_auditorio_3_palestrante_4'] = "";
$lang['dia1_auditorio_3_tema_4'] = "";
$lang['dia1_auditorio_3_horario_5'] = "";
$lang['dia1_auditorio_3_palestrante_5'] = "";
$lang['dia1_auditorio_3_tema_5'] = "";
$lang['dia1_auditorio_3_horario_6'] = "";
$lang['dia1_auditorio_3_palestrante_6'] = "";
$lang['dia1_auditorio_3_tema_6'] = "";
$lang['dia1_auditorio_3_horario_7'] = "";
$lang['dia1_auditorio_3_palestrante_7'] = "";
$lang['dia1_auditorio_3_tema_7'] = "";

// ARQUIVO : ../www/ciab/application/views/programacao//segundo.php
$lang['Quinta-Feira'] = "";
$lang['Expansão do Crédito - Formas e Limites'] = "";
$lang['Painel das Áreas de Negócios dos Bancos'] = "";
$lang['Infraestrutura do Brasil e Inovação'] = "";
$lang['BPO - Business Process Outsourcing'] = "";
$lang['Projetos de Inovação ou Cultura em Inovação'] = "";
$lang['Segurança da informação'] = "";
$lang['Experiencia de Clientes vs Fidelização'] = "";
$lang['Costumer Excellence in Banking'] = "";
$lang['Estratégia para o desenvolvimento de um modelo de Governança de TI'] = "";
$lang['Big Data'] = "";
$lang['A Definir'] = "";
$lang['Eficiência Operacional e Redução de Custos Operacionais'] = "";

// ARQUIVO : ../www/ciab/application/views/programacao//terceiro.php
$lang['Sexta-Feira'] = "";
$lang['Inovação na Indústria de TI para os Bancos'] = "";
$lang['Lideranças de TI debatem Tendências'] = "";
$lang['Transmissão Simultânea'] = "";
$lang['Basiléia III'] = "";
$lang['Inovação em Auto Atendimento'] = "";
?>