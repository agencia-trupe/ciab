<?php
/* TOPO */
$lang['EVENTO'] = 'EVENT';
$lang['EVENTO_SUBMENU_1'] = 'Presentación';
$lang['EVENTO_SUBMENU_2'] = 'El Congreso';
$lang['EVENTO_SUBMENU_3'] = 'Espacios Temáticos';
$lang['EVENTO_SUBMENU_4'] = 'Exposicion';
$lang['EVENTO_SUBMENU_5'] = 'Quiero Visitar';
$lang['EVENTO_SUBMENU_6'] = 'Quiero Esponer';
$lang['EVENTO_SUBMENU_7'] = 'Quiero Apoyar';
$lang['EVENTO_SUBMENU_8'] = 'Planta de La Exposición';
$lang['EVENTO_SUBMENU_9'] = 'Otros Años';
$lang['PROGRAMACAO'] = 'PROGRAMACIÓN';
$lang['PALESTRANTES'] = 'CONFERENCISTAS';
$lang['PREMIOS'] = 'PREMIOS';
$lang['PREMIOS_SUBMENU_1'] = 'Prêmio Ciab 2012';
$lang['PREMIOS_SUBMENU_2'] = 'Vencedores 2011';
$lang['PREMIOS_SUBMENU_3'] = 'Vencedores 2012';
$lang['INSCRICOES'] = 'INSCRIPCIONES';
$lang['INSCRICOES_SUBMENU_1'] = 'Valores';
$lang['INSCRICOES_SUBMENU_2'] = 'Informaciones Importantes';
$lang['INSCRICOES_BANNER_TEXTO'] = 'Aproveche los descuentos promocionales hasta el 10/06 para afiliados y no afiliados. Inscriba su empresa y gane descuentos por grupo.';
$lang['NOTICIAS'] = 'NOTICIAS';
$lang['PUBLICACOES'] = 'PUBLICACIONES';
$lang['IMPRENSA'] = 'PRENSA';
$lang['IMPRENSA_SUBMENU_1'] = 'Prensa';
$lang['INFORMACOES'] = 'INFORMACIONES';
$lang['INFORMACOES_SUBMENU_1'] = 'Local del Evento';
$lang['INFORMACOES_SUBMENU_2'] = 'Hospedaje';
$lang['CONTATO'] = 'CONTACTO';
$lang['BUSCA'] = 'BUSCA';
$lang['ling_portugues'] = 'português';
$lang['ling_ingles'] = 'english';
$lang['ling_espanhol'] = 'español';
$lang['VOLTAR'] = 'VOLVER';
$lang['Compartilhe'] = 'Compártelo';
$lang['Quarta-Feira'] = 'Miércoles';
$lang['Quinta-Feira'] = 'Jueves';
$lang['Sexta-Feira'] = 'Viernes';

/* FOOTER */
$lang['PATROCINADORES'] = 'PATROCINADORES';
$lang['DIAMANTE'] = 'DIAMANTE';
$lang['OURO'] = 'ORO';
$lang['PRATA'] = 'PLATA';
$lang['APOIO'] = 'APOYO';
$lang['FALE CONOSCO'] = 'COMUNÍQUESE CON NOSOTROS';
$lang['POLITICA DE PRIVACIDADE'] = 'POLÍTICA DE PRIVACIDAD';
$lang['TERMO DE USO'] = 'TÉRMINO DE USO';
$lang['MAPA DO SITE'] = 'MAPA DEL SITIO';
$lang['address-linha1'] = '&copy; FEBRABAN - Federação Brasileira de Bancos - '.date('Y').' - Todos Los Derechos Reservados';
$lang['address-linha2'] = 'Av. Brig. Faria Lima, 1.485 - 14º andar • São Paulo • PABX .: 55 11 3244 9800 / 3186 9800 • FAX.: 55 11 3031 4106 •  <a href="http://www.febraban.org.br">WWW.FEBRABAN.ORG.BR</a>';

/* VEJA MAIS */
$lang['VEJA_MAIS_TITULO'] = "VER+MÁS";

/* PALESTRANTES */
$lang['PALESTRANTES_TEMAS_TITULO'] = 'Temas presentados durante el evento';
$lang['PALESTRANTES_OUTROS'] = 'Otros Conferencistas';

/* NOTÍCIAS */
$lang['NOTICIAS_ULTIMAS_TITULO'] = 'Últimas Notícias';
$lang['NOTICIAS_VER_TODAS'] = 'VER TODAS';

/* HOME */
$lang['HOME_SLIDE_CMEP'] = 'Ahí viene el CIAB FEBRABAN 2013<br>Agendese: del 12 al 14 de Junio';
$lang['HOME_SLIDE_1'] = 'CIAB FEBRABAN - Congreso y Exposición de Tecnología de la Información de las Instituciones Financieras, es el mayor evento de América Latina tanto para el sector financiero como para el área de Tecnología.';
$lang['HOME_SLIDE_2'] = '';
$lang['HOME_SLIDE_3'] = '';
$lang['HOME_SLIDE_4'] = '';
$lang['BOX_INSCRICOES_TITULO'] = 'INSCRIPCIONES';

$lang['BOX_INSCRICOES_TEXTO'] = 'Aproveche los descuentos promocionales hasta el 10/06 para afiliados y no afiliados. Inscriba su empresa y gane descuentos por grupo.';
$lang['BOX_BLOG_TITULO'] = 'BLOG';
$lang['BOX_BLOG_TEXTO'] = 'Nuevo blog del Ciab FEBRABAN. Un espacio para debates sobre  los más distintos temas relacionados con la tecnología de la información.! Participe!';
$lang['QUERO_PATROCINAR_TITULO'] = 'QUIERO PATROCINAR';
$lang['QUERO_PATROCINAR_TEXTO'] = 'Su empresa puede patrocinar el mayor congreso y exposición de América Latina.';
$lang['QUERO_EXPOR_TITULO'] = 'QUIERO EXPONER';
$lang['QUERO_EXPOR_TEXTO'] = 'CIAB FEBRABAN cuenta con la participación de centenas de empresas expositoras.';
$lang['QUERO_VISITAR_TITULO'] = 'QUIERO VISITAR';
$lang['QUERO_VISITAR_TEXTO'] = 'Vaya al Transamérica Expo Center y efectúe su registro como visitante del evento.';
$lang['AGENDA_TITULO'] = 'AGENDA';
$lang['AGENDA_TEXTO'] = 'Verifique la agenda de conferencistas nacionales e internacionales que estarán presentes.';

$lang['NOTICIAS_TITULO'] = 'NOTICIAS';
$lang['NENHUMA_NOTICIA'] = 'Nenhuma notícia cadastrada';
$lang['EVENTO_TITULO'] = 'EL EVENTO';
$lang['EVENTO_SUBTITULO'] = 'Qué es Ciab Febraban';
$lang['EVENTO_TEXTO'] = 'Congreso y Exposición de Tecnología de la Información de las Instituciones Financieras – el mayor evento de América Latina...';
$lang['PUBLICACOES_TITULO'] = 'PUBLICACIONES';
$lang['PUBLICACOES_SUBTITULO'] = 'Aposta de R$ 18 bilhões em TI';
$lang['PUBLICACOES_TEXTO'] = 'Pesquisa Ciab FEBRABAN revela que despesas e investimentos dos bancos em tecnologia, em 2011, foram de R$ 18 bilhões e também aponta grande evolução no uso do Mobile Banking.';
$lang['TWITTER_TITULO'] = 'TWITTER CIABFEBRABAN';
$lang['NEWSLETTER_TITULO'] = 'NEWSLETTER';
$lang['NEWSLETTER_TEXTO'] = 'Firme y reciba todas las novedades';
$lang['NEWSLETTER_PLACEHOLDER_NOME'] = 'Informe su Nombre';
$lang['NEWSLETTER_PLACEHOLDER_EMAIL'] = 'Su email';
$lang['NEWSLETTER_SUBMIT'] = 'ENVIAR REGISTRO';

/* INSCRIÇÕES */
$lang['INSCRICOES_TITULO'] = 'Preços de Inscrições e Bonificações de Inscrições';
$lang['INSCRICOES_TITULO_TABELA_1'] = 'Valores por pessoa para inscrições nos 3 dias do Congresso';
$lang['DATA_ATE'] = 'Até';
$lang['DATA_APOS'] = 'Após';
$lang['INSCRICOES_TABELA_INSCRICOES'] = 'Inscrições';
$lang['INSCRICOES_TABELA_FILIADOS'] = 'Bancos filiados';
$lang['INSCRICOES_TABELA_NAO_FILIADOS'] = 'Não filiados';
$lang['INSCRICOES_TABELA_LINHA_1'] = 'Até 5';
$lang['INSCRICOES_TABELA_LINHA_2'] = 'de 6 a 10';
$lang['INSCRICOES_TABELA_LINHA_3'] = 'de 11 a 20';
$lang['INSCRICOES_TABELA_LINHA_4'] = 'mais de 20';
$lang['INSCRICOES_TABELA_RODAPE'] = 'EMPRESAS COM O MESMO CNPJ VALORES POR PARTICIPANTE';
$lang['INSCRICOES_TITULO_TABELA_2'] = 'Valores por pessoa para inscrições para 1 dia do Congresso';
$lang['INSCRICOES_OBSERVACAO_TITULO'] = 'Observação';
$lang['INSCRICOES_OBSERVACAO_TEXTO'] = 'No valor da inscrição estão incluídos: almoços, coffee breaks , coquetel do dia 20/06 e estacionamento nos 3 dias de realização do evento.';
$lang['INSCRICOES_OBSERVACAO_ITEM_1'] = 'Os descontos são válidos somente para inscrições na mesma categoria (participação nos 3 dias do evento ou participação em apenas um dia do evento).';
$lang['INSCRICOES_OBSERVACAO_ITEM_2'] = 'Manteremos o valor da 1ª. Fase para o banco que fizer mais do que o equivalente a 50 inscrições válidas para os três dias do evento. (ex.: 50 inscrições para participação nos 3 dias x R$ 2.178,00 = R$ 108.900,00 ou 124 inscrições para participação em um dia = R$ 108.872,00).';
$lang['INSCRICOES_BONIFICACAO_TITULO'] = 'Bonificação';
$lang['INSCRICOES_BONIFICACAO_ITEM_1'] = 'A cada 20 inscrições a instituição recebe 01 (uma) inscrição cortesia de um dia;';
$lang['INSCRICOES_BONIFICACAO_ITEM_2'] = 'Instituições que fizerem mais de 50 (cinqüenta) inscrições e aumentarem o total de inscritos em relação a 2011 serão bonificadas, conforme abaixo:';
$lang['INSCRICOES_BONIFICACAO_TABELA_TITULO_1'] = 'até 10%';
$lang['INSCRICOES_BONIFICACAO_TABELA_TITULO_2'] = 'Acima de 10% até 20%';
$lang['INSCRICOES_BONIFICACAO_TABELA_TITULO_3'] = 'Acima de 20% até 30%';
$lang['INSCRICOES_BONIFICACAO_TABELA_TITULO_4'] = 'Acima de 30%';
$lang['INSCRICOES_BONIFICACAO_TABELA_TEXTO_1'] = '5 inscr. de 1 dia';
$lang['INSCRICOES_BONIFICACAO_TABELA_TEXTO_2'] = '10 inscr. de 1 dia';
$lang['INSCRICOES_BONIFICACAO_TABELA_TEXTO_3'] = '15 inscr. de 1 dia';
$lang['INSCRICOES_BONIFICACAO_TABELA_TEXTO_4'] = '20 inscr. de 1 dia';
$lang['INSCRICOES_BONIFICACAO_TABELA_LEGENDA'] = '(*) A Tabela de Bonificação é válida exclusivamente para os acréscimos de inscrições em relação a 2011, conforme as faixas estabelecidas, e somente para empresas que fizeram acima de 50 (cinquenta) inscrições no Ciab 2012 .<br>(**) Para consultar a quantidade de inscrições realizadas em 2011, pedimos contatar a Diretoria de<br>Eventos pelo telefone (11) 3186-9860.';
$lang['INSCRICOES_EXEMPLO_TITULO'] = 'Exemplo';
$lang['INSCRICOES_BONIFICACAO_TEXTO'] = 'Instituição Y realizou em 2011 50 inscrições para os 3 dias do evento.<br> Em 2012, realizou 66 inscrições para os 3 dias.<br> Isso quer dizer que o banco Y tem direito à bonificação de 30%.<br>';
$lang['INSCRICOES_BONIFICACAO_TEXTO_NEGRITO'] = 'As bonificações adquiridas pela Instituição Y são';
$lang['INSCRICOES_BONIFICACAO_ITEM_EXEMPLO_1'] = 'Desconto de 35% para a mesma categoria (3 dias ou 1 dia) até o último dia de realização do Congresso, conforme disponibilidade de vagas.';
$lang['INSCRICOES_BONIFICACAO_ITEM_EXEMPLO_2'] = 'Gratuidade de 3 inscrições para participação em 1 dia do Congresso (a cada 20 inscrições 01 gratuidade).';
$lang['INSCRICOES_BONIFICACAO_ITEM_EXEMPLO_3'] = 'Gratuidade de 20 inscrições para participação em 1 dia do Congresso, conforme tabela de bonificação';
$lang['INSCRICOES_INFORMACOES_TITULO'] = 'Informaciones Importantes';
$lang['INSCRICOES_OPCOES_TITULO'] = 'Opciones de Pago';
$lang['INSCRICOES_OPCOES_ITEM_1'] = '- Tarjeta de Crédito Internacional';
$lang['INSCRICOES_OPCOES_ITEM_2'] = ' ';
$lang['INSCRICOES_REEMBOLSO_TITULO'] = 'Reembolsos y Cancelaciones';
$lang['INSCRICOES_REEMBOLSO_TEXTO'] = 'Los pedidos de reembolso o de cancelaciones de inscripciones serán recibidos, a través de una solicitación por escrito destinado para el e-mail eventos@febraban.org.br hasta el día 31 de mayo de 2012.<br> Los reembolsos serán efectuados en el valor neto, descontadas los gastos bancarios provenientes de la transferencia bancaria. A partir del 1º de junio de 2011 no serán más aceptados los pedidos de reembolso ó de cancelaciones.';
$lang['INSCRICOES_SUBSTITUICAO_TITULO'] = 'Sustitución de Participante';
$lang['INSCRICOES_SUBSTITUICAO_TEXTO'] = 'FEBRABAN, solamente aceptará la  sustitución del participante desde que la solicitación sea hecha vía e-mail y dirigido para la dirección electrónica:  eventos@febraban.org.br hasta el día 15 de junio de 2012.<br> Para  las sustituciones solicitadas durante la realización del evento la Organización cobrará una tasa de R$ 250,00 por la reemisión del gafete.  El respectivo pago deberá ser efectuado en el acto de la solicitación.';
$lang['INSCRICOES_CONFIRMACAO_TITULO'] = 'Confirmación de La Inscripción Vía E-Mail';
$lang['INSCRICOES_CONFIRMACAO_TEXTO'] = 'Una vez realizada la inscripción, el sistema de inscripciones de la FEBRABAN enviará un e-mail de confirmación de inscripción del participante. <br>En caso de que no se tenga una respuesta de confirmación de la inscripción, el participante o el área responsable por la inscripción debe entrar en contacto con la Dirección de Eventos de la FEBRABAN a través del e-mail: eventos@febraban.org.br, solicitando la verificación de la inscripción.';
$lang['INSCRICOES_ENCERRAMENTO_TITULO'] = 'Encerramiento de Las Inscripciones';
$lang['INSCRICOES_ENCERRAMENTO_TEXTO'] = 'Las inscripciones serán encerradas el 15 de junio de 2012 o hasta que el número de vacantes sea ocupado.';

/* EVENTO */
$lang['EVENTO_APRESENTACAO_TITULO'] = 'Presentación';
$lang['EVENTO_APRESENTACAO_TEXTO'] = <<<TEXTO
<p>
CIAB FEBRABAN - Congreso y Exposición de Tecnología de la Información para Instituciones Financieras— es el principal encuentro
 de América Latina, tanto en el sector financiero como en el área tecnológica. 
</p>
<p>
En la ceremonia de lanzamiento, que tuvo lugar en septiembre de 2011, un 76 % del espacio de exposición fue reservado por 74
 empresas del área tecnológica. Esta cifra representa un aumento del 12 % en comparación con la obtenida en el lanzamiento de
  la última versión de CIAB FEBRABAN, lo que consolida su éxito.
</p>
<p>
La organización del congreso estima que CIAB FEBRABAN 2012 recibirá un púbico de 19 000 personas, compuesto por representantes
 de instituciones financieras y empresas de TI, y 1900 participantes en el congreso, y que contará con alrededor de 190 expositores.
</p>
<p>
La 22.ª edición de CIAB FEBRABAN tendrá como tema central La sociedad conectada y se celebrará del 20 al el 22 de junio de 2012.
</p>
<p>
Cifras de 2011, consolidación del êxito<br>
Con el tema La tecnología más allá de la web, la 21.ª edición de CIAB FEBRABAN, celebrada en 2011, recibió un público de más de
 18 000 personas y 1795 participantes en el congreso, que representaban instituciones financieras de Brasil y de más de 27 países
  extranjeros, además de 174 empresas expositoras, de las cuales 33 extranjeras.
</p>
<p>
El congreso contó con el patrocinio y el apoyo de más de 20 empresas y con un total de 33 mesas redondas, 81 oradores, 5 panelistas,
 27 moderadores y 6 conferencias magistrales.
</p>
TEXTO;
$lang['EVENTO_SOBRE_TITULO'] = 'Sobre a FEBRABAN';
$lang['EVENTO_SOBRE_TEXTO'] = <<<TEXTO
<p>
A Federação Brasileira de Bancos – FEBRABAN é a principal entidade representativa do setor bancário. Foi fundada em 1967 visando
o fortalecimento do sistema financeiro e de suas relações com a sociedade de modo. Reúne 120 bancos associados de um universo
de 172 instituições em operação no Brasil, os quais representam 97% dos ativos totais e 93% do patrimônio líquido do sistema 
bancário. Entre os objetivos estratégicos da Federação estão o desenvolvimento de iniciativas para a contínua melhoria da 
produtividade do sistema bancário e a redução dos seus níveis de risco, contribuindo dessa forma para o desenvolvimento econômico
e sustentável do País.
</p>
<p>
Os bancos também concentram esforços que viabilizam o crescente acesso da população aos seus produtos e serviços com qualidade
e transparência. A Federação consolidou em 2010 sua nova missão e seus novos valores, avançando em aspectos de governança corporativa.
São exemplos desse movimento, a posse do primeiro presidente não dirigente de instituição financeira de sua história, e o 
aprofundamento de discussões dos problemas brasileiros através do Conselho Consultivo da entidade, composto por membros representativos
de entidades empresariais ligadas ao comércio, indústria, educação e terceiro setor.
</p>
<p>
O diálogo com a sociedade vem se ampliando por meio de ações objetivas, como a criação do Sistema de Autorregulação Bancária e o
aperfeiçoamento de ferramentas de relacionamento com o consumidor: Orkut, Facebook, blogs e Twitter, e canais exclusivos de atendimento
como o Sac Bancos, Ouvidoria e Barômetro.
</p>
<p>
As novas exigências da sociedade, principalmente diante do novo estágio de desenvolvimento da economia brasileira, exigem que a
Federação e o setor bancário respondam aos novos desafios, valendo especial atenção à inclusão financeira de consumidores de baixa
renda e à educação financeira.
</p>
TEXTO;
$lang['EVENTO_CONGRESSO_TITULO'] = 'El Congreso';
$lang['EVENTO_CONGRESSO_TEXTO'] = <<<TEXTO
<p>
En paralelo a la exposición, se celebrará el Congreso CIAB FEBRABAN, en tres auditorios, que contará con la participación de
 expertos en tecnología brasileños y extranjeros, que debatirán el tema principal del Congreso: La sociedad conectada.
</p>
<p>
En 2011, asistieron al Congreso CIAB 1800 participantes, que representaban a instituciones financieras brasileñas y de 27 países
 extranjeros, entre los cuales: Alemania, Argentina, Australia, Bélgica, China, Chile, España, EE. UU., Francia, Israel, India,
  Italia, Japón, México, Singapur, Taiwán y Uruguay.
</p>
<p>
Bajo el tema principal La tecnología más allá de la web, la edición de 2011 contó con 33 mesas redondas con 100 ponentes, expertos y
 escritores internacionales de la talla de Michael D. Capellas, Tom Kelley, Tom Davenport, además de los más altos estamentos de las
  principales instituciones financieras de Brasil.
</p>
TEXTO;
$lang['EVENTO_ESPACOS_TITULO'] = 'Espacios Temáticos';
$lang['EVENTO_ESPACOS_TEXTO'] = '<p> Ciab FEBRABAN 2011 refuerza, nuevamente, sus acciones destinadas para las pequeñas empresas, con el propósito de estimular la presencia de ese segmento en el congreso. </p>';
$lang['EVENTOS_ESPACOS_ITEM_1_TITULO'] = 'ESPACIO EMPREENDEDOR';
$lang['EVENTOS_ESPACOS_ITEM_1_TEXTO'] = 'Dedicado para la exposición de empresas con capital 100% nacional, de pequeño porte y facturación aproximada a R$ 12 millones de reales. La empresa expositora tendrá un paquete compuesto por el espacio en la feria y montaje del stand a precios reducidos.';
$lang['EVENTOS_ESPACOS_ITEM_2_TITULO'] = 'ESPACIO DE INNOVACIÓN';
$lang['EVENTOS_ESPACOS_ITEM_2_TEXTO'] = 'A FEBRABAN e o ITS (Instituto de Tecnologia de Software e Serviços) vão promover durante o Ciab FEBRABAN 2012 a 8ª edição do “Espaço Inovação”. Divulgue esta iniciativa e participe do Processo de Seleção, inscrevendo sua inovação no site do ITS.';
$lang['EVENTOS_ESPACOS_ITEM_3_TITULO'] = 'ESPACIO INTERNACIONAL';
$lang['EVENTOS_ESPACOS_ITEM_3_TEXTO'] = 'Dedicado a la exposición de empresas que no tienen sede en Brasil, con montaje del stand y toda la infraestructura.';
$lang['EVENTOS_ESPACOS_ITEM_4_TITULO'] = 'PABELLÓN FRANCIA - UBIFRANCE';
$lang['EVENTOS_ESPACOS_ITEM_4_TEXTO'] = 'A FEBRABAN e a Embaixada da França no Brasil fecharam um acordo pelo qual empresas francesas da área de TI poderão participar de um espaço no 22º Ciab FEBRABAN. O Espaço França ocupará uma área de 27 metros quadrados, e abrigará até dez companhias. O intuito é intensificar o intercâmbio dos dois países.';
$lang['EVENTO_EXPOSICAO_TITULO'] = 'Exposicion';
$lang['EVENTO_EXPOSICAO_TEXTO'] = <<<TEXTO
<p>
De forma simultánea al congreso, el CIAB FEBRABAN realizará su exposición que contará con la participación de centenas de empresas expositoras de las áreas de finanzas, tecnología de la información (TI) y de telecomunicaciones.
</p>

<p>
El visitante tendrá la oportunidad de ver todas las novedades y soluciones tecnológicas para 2012 y los años posteriores. Visite también los Espacios Temáticos, una oportunidad única de prestigiar las pequeñas empresas nacionales del área de TI.
</p>

<p>
La visita para la exposición es gratuita. Basta realizar su registro en el Enlace abajo o sino directamente en el local, del área “Credenciamento de Visitantes” (Gafete de visitantes).
</p>

<p>
<span class="negrito">Aviso Importante:</span> el gafete de visitante es válido solamente para su ingreso libre al pabellón de exposición. El visitante, de ninguna forma tendrá acceso a las áreas exclusivas de los congresistas y de los invitados.
</p>

<a href="http://www.credenciamento.com.br/2012/VisitanteCiab/Default_por.aspx" target="_blank" class="botao" style="color:#FFF;">HAGA CLIC AQUÍ PARA INSCRIBIRSE</a>
TEXTO;
$lang['EVENTO_QUERO_VISITAR_TITULO'] = 'Quiero Visitar';
$lang['EVENTO_QUERO_VISITAR_TEXTO_1'] = <<<TEXTO
<p>
También puedes visitar la exposición del Ciab FEBRABAN. La entrada es gratis. Par el cual, basta llene el formulario abajo con tus datos y enseguida imprimir tu invitación. Preséntalo en la entrada del Ciab FEBRABAN para puedas retirar tu gafete de visitante y tener el acceso a la exposición.
</p>

<p>
<span class="negrito">Aviso Importante:</span> el gafete de visitante solamente tendrá validez para el acceso a los pabellones de exposición. De ninguna será permitida la entrada de visitantes en áreas exclusivas para los congresistas e invitados.
</p>
TEXTO;
$lang['EVENTO_QUERO_VISITAR_TEXTO_2'] = <<<TEXTO
<p style="clear:left;">
Se preferir, clique no botão abaixo, preencha o formulário e faça agora mesmo seu cadastro.
</p>
<a href="http://www.credenciamento.com.br/2012/VisitanteCiab/Default_por.aspx" target="_blank" class="botao" style="color:#FFF;">HAGA CLIC AQUÍ PARA INSCRIBIRSE</a>
TEXTO;
$lang['EVENTO_EXPOR_TITULO'] = 'Quiero Esponer';
$lang['EVENTO_EXPOR_TEXTO'] = <<<TEXTO
<p>
Llene ahora mismo su registro y reserve el espacio de su empresa en el evento. Nuestros asesores se comunicarán para proporcionarle todas las informaciones de cómo ser un expositor.
</p>

<p>
Si desea, puede llenar el formulario abajo y registrarse en estos momentos. Nuestros asesores entrarán en contacto cuando sea necesario.
</p>
TEXTO;
$lang['EVENTO_EXPOR_FORM_TEXTO'] = 'Cuando quiera hacer la cobertura del evento, llene el formulario abajo y haga su registro. Nuestros asesores se comunicarán cuando sea necesario.';
$lang['EVENTO_EXPOR_FORM_1'] = 'Elija el Asunto';
$lang['EVENTO_EXPOR_FORM_2'] = 'Nombre (Completo)';
$lang['EVENTO_EXPOR_FORM_3'] = 'E-mail ';
$lang['EVENTO_EXPOR_FORM_4'] = 'Empresa';
$lang['EVENTO_EXPOR_FORM_5'] = 'Sitio de la Empresa';
$lang['EVENTO_EXPOR_FORM_6'] = 'Área de Actuación';
$lang['EVENTO_EXPOR_FORM_7'] = 'Su Teléfono';
$lang['EVENTO_EXPOR_FORM_8'] = 'Espacio/Metraje de interés';
$lang['EVENTO_EXPOR_FORM_SUBMIT'] = 'ENVIAR MENSAJE';
$lang['EVENTO_EXPOR_FORM_OBS'] = 'Para más información y reservas de espacio de exposición, contactar a:';
$lang['EVENTO_EXPOR_FORM_OK'] = '<h1>Obrigado!<br>Email enviado com sucesso.entraremos em contato em breve.</h1>';
$lang['EVENTO_EXPOR_FORM_ERROR'] = '<h1>O email não pode ser enviado. <br> Tente novamente.</h1>';
$lang['EVENTO_PATROCINAR_TITULO'] = 'Quiero Apoyar';
$lang['EVENTO_PATROCINAR_TEXTO'] = <<<TEXTO
<p>
Su empresa puede ser uno de los patrocinadores del principal congreso y exposición de América Latina en tecnología de la información 
para las instituciones financieras, y exponer su marca a miles de participantes en el congreso y al público de la exposición.
</p>

<p>
La edición 2010 contó con 129 expositores (tales como IBM, Diebold, Microsoft, Itautec, HP, CA, Perto, Fujitsu y CPM Braxis) y fue visitada
 por un público, altamente calificado, de más de 16 000 personas, compuesto por presidentes, directores y gerentes de instituciones
  financieras, que representaban, en su mayoría, a la banca (70 % del público) y empresas de TI (15 %). 
</p>

<p>
En 2011, estas cifras se incrementaron: hubo 174 expositores (de las cuales 33 extranjeros), un público de más de 18 000 personas y 
1795 participantes en el congreso, que representaban a instituciones financieras de Brasil y de más de 27 países extranjeros.
</p>
TEXTO;
$lang['EVENTO_PATROCINAR_OBS'] = 'Para más información y reservas de espacio de exposición, contactar a:';
$lang['EVENTO_MAPA_EXPOSITORES'] = 'EXPOSITORES';
$lang['EVENTO_ANOS_TITULO'] = 'Otros Años';
$lang['EVENTO_ANOS_SUBTITULO'] = 'Vea lo que sucedió en las ediciones anteriores del evento';
$lang['EVENTO_ANOS_ITEM_1'] = 'La tecnología más allá de la web';
$lang['EVENTO_ANOS_ITEM_2'] = 'Generación Y<br> un nuevo banco para<br> un nuevo consumidor';
$lang['EVENTO_ANOS_ITEM_3'] = 'Un amplio debate sobre<br> la tecnología y su impacto<br> en la inclusión bancaria';
$lang['EVENTO_ANOS_ITEM_4'] = 'Tecnología y Seguridad';

/* PRÊMIOS */
$lang['PREMIOS_CIAB_TITULO'] = 'Prêmio Ciab 2012';
$lang['PREMIOS_CIAB_SUBTITULO'] = 'Este año, el Premio CIAB trae una novedad – Premiación de prototipos para movilidad.';
$lang['Menção Honrosa'] = 'Menção Honrosa';
$lang['PREMIOS_CIAB_TEXTO'] = <<<TEXTO
<p>Como la mayor exposición de TI (Tecnología de la Información) del país y uno de los mayores congresos de bancos del mundo, el
 CIAB FEBRABAN trae novedades cada año.</p>

<p>Para la edición 2012 del Premio CIAB FEBRABAN, la novedad está en el incentivo directo de la capacidad de innovación técnica
 de jóvenes y universitarios que por primera vez deben inscribir aplicativos para la conectividad móvil o plataforma móvil.</p>

<p>Con el tema “Soluciones para una Sociedad Conectada” el Premio CIAB FEBRABAN 2012 pretende incluir a los jóvenes y universitarios
 en el debate de estas propuestas innovadoras. Los trabajos podrán explorar asuntos como el uso de redes sociales, convergencia de
  tecnologías, smartphones, tablets, apps, nuevas formas de relaciones de los bancos con los clientes u otros contenidos relacionados
   al tema del premio y deberán tener la validación de empresas o instituciones de enseñanza y ser aplicables en el mercado financiero.</p>

<p>El principal requisito para evaluación y clasificación del trabajo es la originalidad y ser inédito en la propuesta. Pueden participar grupos de hasta seis personas y los resultados serán divulgados el día 25 de mayo de 2012.</p>

<p>El período de inscripciones de los trabajos es del 01 de Marzo al 20 de Abril del 2012.</p>
TEXTO;
$lang['PREMIOS_CIAB_CHAMADA'] = '¡Son R$ 25 mil en premios! Inscríbase.';
$lang['PREMIOS_CIAB_BOTAO_1'] = 'HAGA CLIC AQUÍ PARA INSCRIBIRSE';
$lang['PREMIOS_CIAB_BOTAO_2'] = 'LEA EL REGLAMENTO';
$lang['PREMIOS_VENCEDORES_TITULO'] = 'Vencedores CIAB 2011';
$lang['PREMIOS_VENCEDORES_SUBTITULO'] = 'Ciab FEBRABAN y Universia lanzan la segunda edición de premio para jóvenes.';
$lang['PREMIOS_VENCEDORES_TEXTO_1'] = '<p>La edición 2011 del Premio CIAB FEBRABAN y Universia (que tuvo como tema “La Tecnología Además de la Web”) registró 611 inscripciones contra 223 en la primera edición del premio en el 2010. Verifique abajo los vencedores.</p>';
$lang['PREMIOS_VENCEDORES_COMUNICADO'] = 'COMUNICADO – RECLASIFICACIÓN PREMIO CIAB FEBRABAN y UNIVERSIA 2011';
$lang['PREMIOS_VENCEDORES_TEXTO_2'] = <<<TEXTO
<p>La comisión organizadora del Ciab FEBRABAN 2011, con base en los criterios del reglamento del Premio Ciab FEBRABAN y Universia 2011, reevaluando a los competidores, concluyó que el trabajo clasificado anteriormente en primer lugar (2Whish) fue desclasificado por no cumplir con requisitos establecidos en el reglamento.</p>

<p>Por lo tanto, la clasificación final del premio, pasa a ser la siguiente:</p>
TEXTO;
$lang['PREMIOS_VENCEDORES_TABELA_1'] = 'Clasificación';
$lang['PREMIOS_VENCEDORES_TABELA_2'] = 'Nombre';
$lang['PREMIOS_VENCEDORES_TABELA_3'] = 'Título';
$lang['PREMIOS_VENCEDORES_TWITTER_TITULO'] = 'Vencedores TWITTER 2011';
$lang['PREMIOS_VENCEDORES_TWITTER_SUBTITULO'] = 'Concurso Cultural Mejor Slogan CIAB en el Twitter';
$lang['PREMIOS_VENCEDORES_TWITTER_CHAMADA'] = '¡Quien siguió el @CiabFEBRABAN y mandó su slogan hizo bien!';
$lang['PREMIOS_VENCEDORES_TWITTER_TEXTO'] = <<<TEXTO
<p>El Ciab FEBRABAN trajo una novedad más para la edición 2011: el concurso cultural "Mejor Slogan Ciab en el Twitter". Para participar, bastaba seguir el perfil @CiabFEBRABAN y enviar un slogan con hasta 140 caracteres.</p>

<p>Fueron 37 slogans recibidos y 3 premiados con ingresos válidos para los tres días del Ciab FEBRABAN 2011, con derecho a participar de la exposición y de las conferencias del congreso.</p>

<p>Continúe siguiendo al @CiabFEBRABAN y entérese de todo lo que acontece en el mayor Congreso y Exposición de Tecnología de la Información de América Latina.</p>
TEXTO;


$lang['PREMIOS_VENCEDORES_2012_TITULO'] = 'Confira os vencedores do Prêmio CIAB 2012';
$lang['PREMIOS_VENCEDORES_2012_PROJETO'] = 'PROJETO';
$lang['PREMIOS_VENCEDORES_2012_GRUPO'] = 'GRUPO';
$lang['PREMIOS_VENCEDORES_2012_PROJETO_1'] = 'ProDeaf – Software Tradutor (Português/LIBRAS – LIBRAS/Português)';
$lang['PREMIOS_VENCEDORES_2012_GRUPO_1'] = <<<LIST
<p>- Marcelo Lucio Correia de Amorim</p>
<p>- Lucas Araújo Mello Soares</p>
<p>- João Paulo dos Santos Oliveira</p>
<p>- Luca Bezerra Dias</p>
<p>- Roberta Agra Coutelo</p>
<p>- Victor Rafael Nascimento dos Santos</p>
LIST;
$lang['PREMIOS_VENCEDORES_2012_PROJETO_2'] = 'Bidd – Plataforma móvel para contratação de crédito de consumo.';
$lang['PREMIOS_VENCEDORES_2012_GRUPO_2'] = <<<LIST
<p>- Daniel César Vieira Radicchi</p>
<p>- André Luis Ferreira Marques</p>
<p>- Willy Stadnick Neto</p>
<p>- Vinicius Abouhatem</p>
<p>- Daniel Coelho</p>
LIST;
$lang['PREMIOS_VENCEDORES_2012_PROJETO_3'] = 'My Tag – Rede social de consumo.';
$lang['PREMIOS_VENCEDORES_2012_GRUPO_3'] = <<<LIST
<p>- Holisson Soares da Cunha</p>
<p>- Sergio Luis Sardi Mergen</p>
<p>- Luciana Hikari Kuamoto</p>
<p>- Leonardo Seiji</p>
LIST;

/* IMPRENSA */
$lang['IMPRENSA_ASSESSORIA_TITULO'] = 'Prensa';
$lang['IMPRENSA_ASSESSORIA_TEXTO_1'] = 'Para recibir press releases, participar de colectivas y entrevistas, contáctese con nosotros';
$lang['IMPRENSA_ASSESSORIA_TEXTO_2'] = 'Si desea tomar la cobertura del evento, llene el siguiente formulario y haga su registro. Nuestros asesores se comunicarán cuando sea necesario.';
$lang['IMPRENSA_ASSESSORIA_FORM_1'] = 'Nombre (Completo)';
$lang['IMPRENSA_ASSESSORIA_FORM_2'] = 'E-mail';
$lang['IMPRENSA_ASSESSORIA_FORM_3'] = 'MTB';
$lang['IMPRENSA_ASSESSORIA_FORM_4'] = 'Empresa';
$lang['IMPRENSA_ASSESSORIA_FORM_5'] = 'Sitio de la Empresa';
$lang['IMPRENSA_ASSESSORIA_FORM_6'] = 'Área de Actuación';
$lang['IMPRENSA_ASSESSORIA_FORM_7'] = 'Su Teléfono';
$lang['IMPRENSA_ASSESSORIA_FORM_SUBMIT'] = 'ENVIAR MENSAJE';
$lang['IMPRENSA_ASSESSORIA_FORM_OK'] = '<h1>Obrigado! <br> Email enviado com  ssucesso. Entraremos em contato em breve.</h1>';
$lang['IMPRENSA_ASSESSORIA_FORM_ERROR'] = '<h1>O email não pode ser enviado. <br> Tente novamente.</h1>';

/* INFORMAÇÕES */
$lang['INFO_PAVILHOES'] = 'Pabellones A, B, C y D';
$lang['INFORMACOES_LOCAL_TITULO'] = 'Local del Evento';
$lang['INFORMACOES_LOCAL_VERMAPA'] = 'ver mapa más grande';
$lang['INFORMACOES_HOTEIS_TITULO'] = 'Hoteles';
$lang['INFORMACOES_HOTEIS_SUBTITULO'] = 'Clique sobre o hotel para mais detalhes';
$lang['INFORMACOES_HOTEIS_LEGENDA'] = 'SGL: Acomoda 1 pessoa | DBL: Acomoda 2 pessoas';
$lang['INFORMACOES_HOTEIS_TABELA_TITULO_1'] = 'HOTEL';
$lang['INFORMACOES_HOTEIS_TABELA_TITULO_2'] = 'Costo diario SUPERIOR SGL';
$lang['INFORMACOES_HOTEIS_TABELA_TITULO_3'] = 'Costo diario SUPERIOR DBL';
$lang['INFORMACOES_HOTEIS_COMDESCONTO'] = 'ya con descuento';
$lang['INFORMACOES_HOTEIS_LOCAL'] = 'Local';
$lang['INFORMACOES_HOTEIS_TEL'] = 'Tel';
$lang['INFORMACOES_HOTEIS_COMO_CHEGAR'] = 'Pulse aquí para ver cómo llegar';
$lang['INFORMACOES_HOTEIS_CODIGO'] = 'Código para reservar';
$lang['INFORMACOES_HOTEIS_DIARIA_SGL'] = 'Costo diario del apartamento Superior SGL';
$lang['INFORMACOES_HOTEIS_DIARIA_DBL'] = 'Costo diario del apartamento Superior DBL';
$lang['INFORMACOES_HOTEIS_DIARIA'] = 'El costo diario incluye (por adhesión)';
$lang['INFORMACOES_HOTEIS_OBSERVACOES'] = 'Observaciones';
$lang['INFORMACOES_HOTEIS_CHECK_1'] = '- Registro a partir de las 2.00 p.m.<br> - Salida del hotel hasta 12.00 a.m.';
$lang['INFORMACOES_HOTEIS_CHECK_2'] = 'los días comienzan y se encierran a las 12.00 a.m.';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_1'] = 'Desayuno servido en el restaurante (6.00 a.m. a 10.00 a.m.);';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_2'] = '- Desayuno (servido en el mismo restaurante)<br> - Traslado del Hotel/Transamérica Expo Center/Hotel<br> - 01 lugar en la cochera por apartamento';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_3'] = '- Desayuno (servido en el restaurante)<br> - Traslado del Aeropuerto de Congonhas / Hotel / Aeropuerto de Congonhas';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_4'] = '- Desayuno (servido en el restaurante)<br> - 01 lugar en la cochera por apartamento';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_5'] = '- Desayuno (servido en el restaurante)';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_1'] = 'El valor del costo diario será aumentando en 5% de ISS + R$ 4,50 de taza de turismo y el pago de los gastos de hospedaje será realizada con tarjeta de crédito o cash directamente en el Hotel Transamérica São Paulo. Los apartamentos están sujetos a disponibilidad.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_2'] = 'El valor del costo diario será aumentando en 5% de ISS + taza de turismo y el pago de los gastos de hospedaje será realizada con tarjeta de crédito o cash directamente en el Transamérica Executive Chácara Santo Antonio. Los apartamentos están sujetos a disponibilidad.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_3'] = 'El valor del costo diario será aumentando en 5% de ISS + taza de turismo y el pago de los gastos de hospedaje será realizada con tarjeta de crédito o cash directamente en el Transamérica Executive Congonhas. Los apartamentos están sujetos a disponibilidad.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_4'] = 'El valor del costo diario será aumentando en 5% de ISS + taza de turismo y el pago de los gastos de hospedaje será realizada con tarjeta de crédito o cash directamente en el Transamérica Prime International Plaza. Los apartamentos están sujetos a disponibilidad.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_5'] = 'El valor del costo diario será aumentando en 5% de ISS + taza de turismo y el pago de los gastos de hospedaje será realizada con tarjeta de crédito o cash directamente en el Blue Tree Premium Morumbi. Los apartamentos están sujetos a disponibilidad.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_6'] = 'El valor del costo diario será aumentando en 5% de ISS + taza de turismo y el pago de los gastos de hospedaje será realizada con tarjeta de crédito o cash directamente en el Blue Tree Premium Verbo Divino. Los apartamentos están sujetos a disponibilidad.';
$lang['CONTATO_INFORMACOES_GERAIS'] = 'INFORMACIONES GENERALES';
$lang['CONTATO_COMO_CHEGAR'] = 'CÓMO LLEGAR';
$lang['CONTATO_RESERVAS'] = 'Reservas de espacios';
$lang['CONTATO_FORM_TITULO'] = 'Llene el siguiente formulario  para contacto';
$lang['CONTATO_FORM_ASSUNTO_TITULO'] = 'Assunto';
$lang['CONTATO_FORM_ASSUNTO_1'] = 'Otros';
$lang['CONTATO_FORM_ASSUNTO_2'] = 'Conferencista';
$lang['CONTATO_FORM_ASSUNTO_3'] = 'Expositor';
$lang['CONTATO_FORM_ASSUNTO_4'] = 'Visitante';
$lang['CONTATO_FORM_ASSUNTO_5'] = 'Apoyo';
$lang['CONTATO_FORM_ASSUNTO_6'] = 'Prensa';
$lang['CONTATO_FORM_1'] = 'Nombre (Completo)';
$lang['CONTATO_FORM_2'] = 'E-mail';
$lang['CONTATO_FORM_3'] = 'Empresa';
$lang['CONTATO_FORM_4'] = 'Sitio de la Empresa';
$lang['CONTATO_FORM_5'] = 'Área de Especialización';
$lang['CONTATO_FORM_6'] = 'Su Teléfono';
$lang['CONTATO_FORM_7'] = 'Mensaje';
$lang['CONTATO_FORM_SUBMIT'] = 'ENVIAR MENSAJE';
$lang['CONTATO_FORM_OK'] = '<h1>Obrigado! <br>Email enviado com sucesso. Entraremos em contato em breve.</h1>';
$lang['CONTATO_FORM_ERROR'] = '<h1>O email não pode ser enviado. <br>Tente novamente.</h1>';

/***************/
/* PROGRAMAÇÃO */
/***************/

/* DIA 1 */
$lang['1-sessao-1'] = <<<STR
1ª sessão manhã - das 09:00 às 10:30
STR;

$lang['auditorio-1'] = <<<STR
AUDITORIO FEBRABAN
STR;

$lang['auditorio-2'] = <<<STR
AUDITORIO LÍNEA DE NEGOCIOS
STR;

$lang['auditorio-3'] = <<<STR
AUDITORIO EFICIENCIA OPERACIONAL
STR;

$lang['1-sessao-1'] = <<<STR
1ª sesión mañana - de las 09:00 a las 10:30
STR;

$lang['1-sessao-2'] = <<<STR
2ª sesión mañana - de las 11:00 a las 12:30
STR;

$lang['1-sessao-3'] = <<<STR
1ª sesión tarde - de las 14:00 a las 15:30
STR;

$lang['1-sessao-4'] = <<<STR
2ª sesión tarde - de las 16:00 a las 18:00
STR;

$lang['obs-solenidade'] = <<<STR
Solemnidad de apertura de la Exposición y visita a los stands de los Auspiciadores / Coffee break
STR;

$lang['DIA_1_MANHA_1_A'] = <<<STR
Apertura<br>
Murilo Portugal - FEBRABAN<br>
Luis Antonio Rodrigues - CIAB FEBRABAN<br>
STR;

$lang['DIA_1_MANHA_1_B'] = <<<STR
Transmisión simultánea de la apertura
STR;

$lang['DIA_1_MANHA_1_C'] = <<<STR
Transmisión simultánea de la apertura
STR;


$lang['DIA_1_MANHA_2_A'] = <<<STR
La importancia de la TI en el desarrollo del sistema financiero<br>
Roberto Egydio Setúbal - Itaú Unibanco<br>
Moderador: Murilo Portugal - FEBRABAN<br>
STR;

$lang['DIA_1_MANHA_2_B'] = <<<STR
Transmisión simultánea 
STR;

$lang['DIA_1_MANHA_2_C'] = <<<STR
Transmisión simultánea 
STR;


$lang['DIA_1_TARDE_1_A'] = <<<STR
ArchiTechs: How to Live, Govern & Learn in a Hyper-connected World<br>
Rahaf Harfoush - Estrategista de Medias Sociales<br>
Moderador: Cláudio Almeida Prado - Deutsche Bank
STR;

$lang['DIA_1_TARDE_1_B'] = <<<STR
El banco en la nube - Discusión sobre el uso de Cloud Computing por los bancos<br>
Peter Redshaw - Gartner<br>
Azeem Mohamed - HP<br>
Rodrigo Gazzaneo - EMC<br>
Moderador: Jair Vasconcelos Filho - CAIXA
STR;

$lang['DIA_1_TARDE_1_C'] = <<<STR
Consumerización<br>
Fernando Belfort - Frost & Sullivan<br>
Ghassan Dreibi Jr. - Cisco<br>
Moderadora: Francimara Teixeira Garcia Viotti - Banco do Brasil 
STR;


$lang['DIA_1_TARDE_2_A'] = <<<STR
de las 16:00 a las 17:00<br>
Innovación, Tecnología de la Información y comunicación<br>
Silvio Meira - Centro de Estudios y Sistemas Avanzados de Recife (CESAR)<br>
Moderador: Milton Shizuo Noguchi - Itautec<br>
====================================== <br>
de las 17:00 a las 18:00<br>
Delivering Convenience, Security and Innovation<br>
Thomas W. Swidarski - CEO Global da Diebold<br>
Moderador: Benito Luís Rossiti - TECBAN<br>
STR;

$lang['DIA_1_TARDE_2_B'] = <<<STR
Big Data - Inteligencia analítica sobre datos no estructurados<br>
Donald Feinberg - Gartner<br>
George Tziahanas - HP<br>
Patrícia Florissi - EMC<br>
Moderador: Armando Corrêa - Citibank
STR;

$lang['DIA_1_TARDE_2_C'] = <<<STR
Tecnologías móviles en la era conectada<br>
Paulo Marcelo Lessa Moreira - CPM Braxis Capgemini<br>
Michihiko Yoden - NTT Data<br>
Nuno Gomes - Booz & Company<br>
José Domingos Favoretto Jr. - CPqD<br>
Moderador: Jorge Vacarini Júnior - Deutsche Bank 
STR;



/* DIA 2 */
$lang['DIA_2_MANHA_1_A'] = <<<STR
El banco y el cliente del futuro<br>
John Bates - Progress Software<br>
Maurício Machado de Minas - Bradesco<br>
Moderador: Ricardo Ribeiro Mandacaru Guerra - Itaú Unibanco
STR;

$lang['DIA_2_MANHA_1_B'] = <<<STR
CIO de otras industrias y la sociedad conectada<br>
Pedro Paulo A.C. Cunha - Alelo<br>
Anderson Figueiredo – IDC<br>
João Lencioni  - GE<br>
Moderador: Keiji Sakai – BM&FBovespa
STR;

$lang['DIA_2_MANHA_1_C'] = <<<STR
Eficiencia operacional<br>
Ian Malone - EMC<br>
Walter Tadeu Pinto de Faria - FEBRABAN<br>
Marcelo Atique - CAIXA<br>
Moderador: Joaquim Kiyoshi Kavakama - CIP - Cámara Interbancaria de Pagos
STR;


$lang['DIA_2_MANHA_2_A'] = <<<STR
Innovación y creatividad<br>
Charles Bezerra - GAD'Innovation<br>
Moderador: Carlos Roberto Zanellato - HSBC
STR;

$lang['DIA_2_MANHA_2_B'] = <<<STR
El banco en la era conectad<br>
Rosie Fitzmaurice - RBR<br>
Dan Cohen - IBM<br>
Antranik Haroutiounian - Bradesco<br>
Moderador: David Alves de Melo - Itautec
STR;

$lang['DIA_2_MANHA_2_C'] = <<<STR
Desafíos de la formación del capital humano calificado<br>
Prof. José Pastore<br>
Paulo Sérgio Sgobbi - Brasscom<br>
João Sabino - Fundação Bradesco<br>
Moderador: Fábio Cássio Costa Moraes - FEBRABAN
STR;


$lang['DIA_2_TARDE_1_A'] = <<<STR
Las áreas de negocios de los bancos hablan sobre tendencias<br>
Paulo Nergi Boeira de Oliveira - Caixa<br>
Hideraldo Dwight Leitão - Banco do Brasil<br>
Arnaldo Nissental - Bradesco<br>
Gilberto de Abreu - Santander Brasil<br>
Moderador: Alexandre Gouvea - McKinsey
STR;

$lang['DIA_2_TARDE_1_B'] = <<<STR
Accesibilidad digital<br>
Delfino Natal de Souza - Ministerio de Planificación, Presupuesto y Gestión<br>
Karen Myers  - W3C - World Wide Web Consortium<br>
Sidinei Rossoni - CAIXA<br>
Liliane Vieira Moraes - CNPq - Consejo Nacional de Desarrollo Científico y Tecnológico<br>
Moderador: Carlos Alberto Brocchi de Oliveira Pádua - Diebold
STR;

$lang['DIA_2_TARDE_1_C'] = <<<STR
Seguridad de la información<br>
Kris Lovejoy - IBM<br>
Alberto Fávero - Ernst Young<br>
Americo Lobo Neto - BioLógica<br>
Moderador: Jorge Fernando Krug - Banrisul
STR;


$lang['DIA_2_TARDE_2_A'] = <<<STR
de las 16:00 a las 17:00<br>
El ambiente de amenaza y la Nueva onda de TI<br>
Arthur W. Coviello Jr. - EMC<br>
Moderador: André Augusto de Lima Salgado - Citibank<br>
======================================<br>
de las 17:00 a las 18:00<br>
How IT Is Changing Our Future<br>
Miha Kralj - Microsoft<br>
Moderador: João Antonio Dantas Bezerra Leite - Itaú Unibanco
STR;

$lang['DIA_2_TARDE_2_B'] = <<<STR
Plataforma analítica para redes sociales<br>
Katia Vaskys - IBM<br>
Zachary Aron - Deloitte<br>
Rodrigo Gonsales - Cisco<br>
Katia Furie - Santander Brasil<br>
Moderador: Gustavo de Souza Fosse - Banco do Brasil
STR;

$lang['DIA_2_TARDE_2_C'] = <<<STR
Los imperativos de la nueva gestión de riesgos<br>
Luis Arturo Diaz- Oracle<br>
Oliver Cunningham - KPMG<br>
Maria del Carmen Aleixandre - Accenture<br>
Moderador: Ricardo Orlando - Itaú Unibanco
STR;



/* DIA 3 */
$lang['3-sessao-1'] = <<<STR
1ª sesión mañana - de las 09:00 a las 10:30
STR;

$lang['3-sessao-2'] = <<<STR
2ª sesión mañana - de las 11:00 a las 12:30
STR;

$lang['3-sessao-3'] = <<<STR
1ª sesión tarde - de las 14:00 a las 16:00
STR;

$lang['3-sessao-4'] = <<<STR
2ª sesión tarde - de las 16:30 a las 18:00
STR;

$lang['DIA_3_MANHA_1_A'] = <<<STR
Innovación en la industria de TI para los bancos<br>
Carlos Cunha - EMC<br>
Fábio Pessoa - IBM<br>
J. Thomas Elbling - Perto<br>
João Abud Junior - Diebold<br>
Luciano Corsini - HP<br>
Rui José Schoenberger - NTT Data<br>
Wilton Ruas da Silva - Itautec<br>
Moderadora: Cláudia Vassalo - Editora Abril<br>
STR;

$lang['DIA_3_MANHA_1_B'] = <<<STR
Transmisión simultánea
STR;

$lang['DIA_3_MANHA_1_C'] = <<<STR
Transmisión simultánea
STR;


$lang['DIA_3_MANHA_2_A'] = <<<STR
Liderazgos de TI debaten tendencias<br>
Aurélio Conrado Boni - Bradesco<br>
Joaquim Lima de Oliveira - Caixa<br>
Luis Antonio Rodrigues - Itaú Unibanco<br>
Marcelo Zerbinatti - Santander Brasil<br>
Moderador: Gustavo José Costa Roxo da Fonseca - Booz & Company
STR;

$lang['DIA_3_MANHA_2_B'] = <<<STR
Social Commerce<br>
Eduardo Galanternick - Magazine Luiza<br>
Luca Cavalcanti - Bradesco<br>
Gabriel Borges - Like Store<br>
Moderador: Ricardo Pomeranz - Rapp Brasil
STR;

$lang['DIA_3_MANHA_2_C'] = <<<STR
Desarrollo ágil<br>
Pedro Britto - IBM<br>
Eduardo Mazon - Banco BMG<br>
Ricardo Ribeiro Mandacaru Guerra - Itaú Unibanco<br>
Moderador: Alberto Luiz Gerardi - Banco do Brasil
STR;


$lang['DIA_3_TARDE_1_A'] = <<<STR
Brasil en perspectiva<br>
Ilan Goldfajn - Itaú Unibanco<br>
Moderador: André Lahóz - Revista Exame
STR;

$lang['DIA_3_TARDE_1_B'] = <<<STR
Innovación en autoatención<br>
Carlos Alberto Brocchi de Oliveira Pádua - Diebold<br>
João Lo Ré Chagas - Itautec<br>
Fabrizio Pinna - Scopus<br>
Jorge Schtoltz - Banco do Brasil<br>
Moderadora: Kátia Militello - Info Exame
STR;

$lang['DIA_3_TARDE_1_C'] = <<<STR
Gamification - Cómo la mecánica del juego transformará su empresa y su comportamiento<br>
Peter Redshaw - Gartner<br>
Alcides de Francisco Ferreira - BM&Fbovespa<br>
Alexandre Winetzki - Woopi<br>
Moderador: Flávio Leomil Marietto - CPM Braxis Capgemini
STR;


$lang['DIA_3_TARDE_2_A'] = <<<STR
Charla de encerramiento
STR;

$lang['DIA_3_TARDE_2_B'] = <<<STR
Transmisión simultánea
STR;

$lang['DIA_3_TARDE_2_C'] = <<<STR
Transmisión simultánea
STR;

$lang['Auditórios Excelência em TI'] = 'Auditórios Excelência em TI';
$lang['Auditorios Horário'] = 'Horário';
$lang['Auditorios Palestrantes'] = 'Palestrantes';
$lang['Auditorios Tema'] = 'Tema';

/* DIA 1 */
$lang['dia_1 Auditorio 1'] = 'Auditório 3 - TALARIS';
$lang['dia_1 Auditorio 3'] = 'Auditório 3 - LÓGICA';
$lang['dia_1 Auditorio 2'] = 'Auditório 2 - Acesso Digital';

$lang['dia1_auditorio_2_horario_1'] = '14h30';
$lang['dia1_auditorio_2_horario_2'] = '14h40';
$lang['dia1_auditorio_2_horario_3'] = '15h00';
$lang['dia1_auditorio_2_horario_4'] = '15h30';
$lang['dia1_auditorio_2_horario_5'] = '16h00';
$lang['dia1_auditorio_2_palestrante_1'] = 'Antonio Augusto de Almeida Leite (Pancho), ACREFI';
$lang['dia1_auditorio_2_palestrante_2'] = 'Breno Costa, Sócio e Consultor da GoOn – A Evolução na Gestão do Risco';
$lang['dia1_auditorio_2_palestrante_3'] = 'Ricardo Batista, Superintendente de Crédito do Tribanco';
$lang['dia1_auditorio_2_palestrante_4'] = 'Roberto Jabali, Superintendente de Crédito do Grupo Citi - Credicard';
$lang['dia1_auditorio_2_palestrante_5'] = 'Alex Yamamoto,Consultor da Acesso Digital';
$lang['dia1_auditorio_2_tema_1'] = 'Abertura';
$lang['dia1_auditorio_2_tema_2'] = 'Case GoOn: Como aumentar o nível de decisão automática sem impacto na inadimplência.';
$lang['dia1_auditorio_2_tema_3'] = 'Case Tribanco: captura de documentos no varejo';
$lang['dia1_auditorio_2_tema_4'] = 'Case Grupo Citi: Pioneirismo na gestão eletrônica de documentos';
$lang['dia1_auditorio_2_tema_5'] = 'Formalização de crédito online nas instituições financeiras';

$lang['dia1_auditorio_3_horario_1'] = '09h00 - 10h00';
$lang['dia1_auditorio_3_horario_2'] = '11h00 – 12h00';
$lang['dia1_auditorio_3_horario_3'] = '14h00 - 15h10';
$lang['dia1_auditorio_3_horario_4'] = '15h20 – 16h30';
$lang['dia1_auditorio_3_horario_5'] = '16h40 – 17h50';
$lang['dia1_auditorio_3_horario_6'] = '17h50 -18h00';
$lang['dia1_auditorio_3_horario_7'] = ' ';
$lang['dia1_auditorio_3_palestrante_1'] = 'Paulo Martins, Diretor Comercial / Logica';
$lang['dia1_auditorio_3_palestrante_2'] = 'Reginaldo Silva, Portfólio / Logica';
$lang['dia1_auditorio_3_palestrante_3'] = 'Antonio Requejo, Regional Practice Leader Iberia & Latam – Security / Logica';
$lang['dia1_auditorio_3_palestrante_4'] = 'Júlio Gonçalves, Regional Practice Leader Iberia & Latam – IT Modernization / Logica';
$lang['dia1_auditorio_3_palestrante_5'] = 'Lode Snykers, Vice President, Global Financial Services / Logica';
$lang['dia1_auditorio_3_palestrante_6'] = 'Paulo Martins, Diretor Comercial / Logica';
$lang['dia1_auditorio_3_palestrante_7'] = ' ';
$lang['dia1_auditorio_3_tema_1'] = 'Compartilhando Idéias';
$lang['dia1_auditorio_3_tema_2'] = 'Mobilidade: Saiba como surpreender os seus clientes com soluções modernas e inovadoras';
$lang['dia1_auditorio_3_tema_3'] = 'De Securidad en TI para el Gerenciamento de Riesgo';
$lang['dia1_auditorio_3_tema_4'] = 'Modernização de Sistemas Legados';
$lang['dia1_auditorio_3_tema_5'] = 'The technological challenges of the Financial Sector in the XXI century';
$lang['dia1_auditorio_3_tema_6'] = 'Compartilhando Ideias II';
$lang['dia1_auditorio_3_tema_7'] = 'Sorteios para os participantes do dia';


/* DIA 2 */
$lang['dia_2 Auditorio 1'] = 'Auditório 1 - HP';
$lang['dia_2 Auditorio 2'] = 'Auditório 2 - Microsoft';
$lang['dia_2 Auditorio 3'] = 'Auditório 3 - CLM';

/* Auditorio 1 */
$lang['dia2_auditorio_1_horario_1'] = '10h00 – 11h00';
$lang['dia2_auditorio_1_horario_2'] = '11h10 – 12h10';
$lang['dia2_auditorio_1_horario_3'] = '14h30 – 15h30';
$lang['dia2_auditorio_1_horario_4'] = '15h40 – 16h00';
$lang['dia2_auditorio_1_palestrante_1'] = 'Mike Wright, HP';
$lang['dia2_auditorio_1_palestrante_2'] = 'Gladson Martins Russo, Nokia Siemens';
$lang['dia2_auditorio_1_palestrante_3'] = 'Andre Kalsing, HP';
$lang['dia2_auditorio_1_palestrante_4'] = 'Federico Grosso, Autonomy';
$lang['dia2_auditorio_1_tema_1'] = 'Tendências para Social CRM';
$lang['dia2_auditorio_1_tema_2'] = 'Novas tecnologias para Mobile Payment';
$lang['dia2_auditorio_1_tema_3'] = 'Benefícios da adoção de uma plataforma de serviços multicanal';
$lang['dia2_auditorio_1_tema_4'] = 'Uma nova Visão de Inovação e Big Data';


// Auditorio 2
$lang['dia2_auditorio_2_horario_1'] = '9h-10h';
$lang['dia2_auditorio_2_horario_2'] = '10h-11h30';
$lang['dia2_auditorio_2_horario_3'] = '11h30-12h30';
$lang['dia2_auditorio_2_horario_4'] = '14h-15h';
$lang['dia2_auditorio_2_horario_5'] = '15h-16h';
$lang['dia2_auditorio_2_horario_6'] = '16h-17h';
$lang['dia2_auditorio_2_horario_7'] = '17h-18h';
$lang['dia2_auditorio_2_palestrante_1'] = 'Fabio Souto';
$lang['dia2_auditorio_2_palestrante_2'] = 'Miha Krajl';
$lang['dia2_auditorio_2_palestrante_3'] = 'Jun Endo';
$lang['dia2_auditorio_2_palestrante_4'] = 'Eduardo Campos';
$lang['dia2_auditorio_2_palestrante_5'] = 'Roberto Prado';
$lang['dia2_auditorio_2_palestrante_6'] = 'Ricardo-Enrico Jahn';
$lang['dia2_auditorio_2_palestrante_7'] = 'Richard Chaves';
$lang['dia2_auditorio_2_tema_1'] = 'Sociedade Conectada e a Consumerização de TI';
$lang['dia2_auditorio_2_tema_2'] = 'Como TI está mudando nosso futuro';
$lang['dia2_auditorio_2_tema_3'] = 'Soluções Inovadoras para uma Experiência Única Multidevices & Consumerização de TI';
$lang['dia2_auditorio_2_tema_4'] = 'A nuvem nos seus termos - Estratégia de Computação na nuvem da Microsoft';
$lang['dia2_auditorio_2_tema_5'] = 'O profissional TI como protagonista da competitividade. Capacitação e Satisfação do cliente.';
$lang['dia2_auditorio_2_tema_6'] = 'Relacionamento e fidelização nas instituições financeiras com Dynamics CRM/XRM';
$lang['dia2_auditorio_2_tema_7'] = 'O poder da Inovação';

// Auditorio 3
$lang['dia2_auditorio_3_horario_1'] = '10h30 - 11h30';
$lang['dia2_auditorio_3_horario_2'] = '11h30 - 12h30';
$lang['dia2_auditorio_3_horario_3'] = '14h30 - 15h30';
$lang['dia2_auditorio_3_horario_4'] = '15h30 - 16h30';
$lang['dia2_auditorio_3_horario_5'] = '16h30 - 17h30';
$lang['dia2_auditorio_3_horario_6'] = '17h30 - 18h30';
$lang['dia2_auditorio_3_palestrante_1'] = 'Martin Roesch, SNORT';
$lang['dia2_auditorio_3_palestrante_2'] = 'Alex Silva, A10';
$lang['dia2_auditorio_3_palestrante_3'] = 'Carlos Fagundes, INTEGRALTRUST';
$lang['dia2_auditorio_3_palestrante_4'] = 'Sanjay Ramnath, BARRACUDA';
$lang['dia2_auditorio_3_palestrante_5'] = 'Dick Faulkner, SOPHOS';
$lang['dia2_auditorio_3_palestrante_6'] = 'Martin Roesch, SOURCEFIRE';
$lang['dia2_auditorio_3_tema_1'] = 'Open Source Security';
$lang['dia2_auditorio_3_tema_2'] = 'DDoS Attacks and Brute Force';
$lang['dia2_auditorio_3_tema_3'] = 'Segredos do Basileia III';
$lang['dia2_auditorio_3_tema_4'] = 'Application Security';
$lang['dia2_auditorio_3_tema_5'] = 'Cryptography and Mobile Security';
$lang['dia2_auditorio_3_tema_6'] = 'Next Generation Security';

/* DIA 3*/
$lang['dia_3 Auditorio 1'] = 'Auditório 1 - FEBRABAN';

$lang['dia3_auditorio_1_horario_1'] = '11h00 - 12h30';
$lang['dia3_auditorio_1_palestrante_1'] = 'Guilhermino Domiciano de Souza – Banco do Brasil;  Marcelo Ribeiro Câmara – Bradesco;  Cesar Faustino – Itaú Unibanco';
$lang['dia3_auditorio_1_tema_1'] = 'A Segurança Digital do Cidadão no Sistema Financeiro Brasileiro';

?>
