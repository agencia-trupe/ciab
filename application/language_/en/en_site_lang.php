<?php
/* TOPO */
$lang['EVENTO'] = 'EVENT';
$lang['EVENTO_SUBMENU_1'] = 'Presentation';
$lang['EVENTO_SUBMENU_2'] = 'The Congress';
$lang['EVENTO_SUBMENU_3'] = 'Thematic Spaces';
$lang['EVENTO_SUBMENU_4'] = 'Exhibition';
$lang['EVENTO_SUBMENU_5'] = 'I Want To Visit';
$lang['EVENTO_SUBMENU_6'] = 'I Want To Exhibit';
$lang['EVENTO_SUBMENU_7'] = 'I Want To Sponsor';
$lang['EVENTO_SUBMENU_8'] = 'Exhibition Plan';
$lang['EVENTO_SUBMENU_9'] = 'Other Years';
$lang['PROGRAMACAO'] = 'PROGRAM';
$lang['PROMOÇÃO'] = 'PROMOTION';
$lang['PROMOÇÃO2'] = 'Visit our booth at CIAB 2013, watch a demonstration and be entered to win iPads. STAND H15 - FLAG C';
$lang['PALESTRANTES'] = 'SPEAKERS';
$lang['PREMIOS'] = 'AWARDS';
$lang['PREMIOS_SUBMENU_1'] = 'Award Ciab 2013';
$lang['PREMIOS_SUBMENU_2'] = 'Ciab 2011 Winners';
$lang['PREMIOS_SUBMENU_3'] = 'Ciab 2012 Winners';
$lang['INSCRICOES'] = 'REGISTRATIONS';
$lang['INSCRICOES_SUBMENU_1'] = 'Values';
$lang['INSCRICOES_SUBMENU_2'] = 'More Information';
$lang['INSCRICOES_BANNER_TEXTO'] = 'Take advantage of promotional discounts until 10/06. Register your company and get group discounts.';
$lang['NOTICIAS'] = 'NEWS';
$lang['PUBLICACOES'] = 'PUBLICATIONS';
$lang['IMPRENSA'] = 'PRESS';
$lang['IMPRENSA_SUBMENU_1'] = 'Press Office';
$lang['INFORMACOES'] = 'INFORMATION';
$lang['INFORMACOES_SUBMENU_1'] = 'Event Location';
$lang['INFORMACOES_SUBMENU_2'] = 'Accommodation';
$lang['CONTATO'] = 'CONTACT';
$lang['BUSCA'] = 'SEARCH';
$lang['ling_portugues'] = 'português';
$lang['ling_ingles'] = 'english';
$lang['ling_espanhol'] = 'español';
$lang['VOLTAR'] = 'BACK';
$lang['Compartilhe'] = 'Share';
$lang['Quarta-Feira'] = 'Wednesday';
$lang['Quinta-Feira'] = 'Thursday';
$lang['Sexta-Feira'] = 'Friday';

/* FOOTER */
$lang['PATROCINADORES'] = 'SPONSORS';
$lang['DIAMANTE'] = 'DIAMOND';
$lang['OURO'] = 'GOLD';
$lang['PRATA'] = 'SILVER';
$lang['APOIO'] = 'SUPPORT';
$lang['FALE CONOSCO'] = 'CONTACT';
$lang['POLITICA DE PRIVACIDADE'] = 'PRIVACY POLICY';
$lang['TERMO DE USO'] = 'LEGAL INFORMATION';
$lang['MAPA DO SITE'] = 'SITE MAP';
$lang['address-linha1'] = '&copy; FEBRABAN - Federação Brasileira de Bancos - 2013 - All rights reserved';
$lang['address-linha2'] = 'Av. Brig. Faria Lima, 1.485 - 14º andar • São Paulo • PABX .: 55 11 3244 9800 / 3186 9800 • FAX.: 55 11 3031 4106 •  <a href="http://www.febraban.org.br">WWW.FEBRABAN.ORG.BR</a>';

/* VEJA MAIS */
$lang['VEJA_MAIS_TITULO'] = "SEE+MORE";

/* PALESTRANTES */
$lang['PALESTRANTES_TEMAS_TITULO'] = 'Themes presented during the congress';
$lang['PALESTRANTES_OUTROS'] = 'Other Speakers';

/* NOTÍCIAS */
$lang['NOTICIAS_ULTIMAS_TITULO'] = 'Latest News';
$lang['NOTICIAS_VER_TODAS'] = 'VIEW ALL';

/* HOME */
$lang['HOME_SLIDE_CMEP'] = 'Payment Means and Sustainability in Consumer Relationships is the principal theme of the 7º CMEP,  promoted by FEBRABAN.';
$lang['HOME_SLIDE_1'] = 'CIAB FEBRABAN – Congress and Expo of Information Technology for Financial Institutions – is the biggest event in Latin America for the financial sector and for technology.';
$lang['HOME_SLIDE_2'] = 'Gestão e Recuperação de Crédito - PJ / PF, Prevenção sobre Crimes de Lavagem de Dinheiro, Metodologias de Análise e Inspeção em Fraudes Bancárias, Excelência nos Serviços de Atendimento a Clientes, Análise Setorial com análise de riscos, e ...';
$lang['HOME_SLIDE_3'] = 'This unique event will provide an opportunity to discuss the key industry opportunities and issues specific to the Latin American community, together with an update on key SWIFT initiatives which impact the region.';
$lang['HOME_SLIDE_4'] = 'Em sua 13ª edição, o Congresso FEBRABAN de Auditoria Interna e Compliance traz à discussão a perspectiva tridimensional sobre o tema que compreende maior cobertura, maior profundidade nas análises e a garantia de qualidade ...';
$lang['BOX_INSCRICOES_TITULO'] = 'PALESTRAS';
$lang['BOX_INSCRICOES_TEXTO'] = 'Congressista, clique aqui e faça o download das palestras do CIAB 2013.';
$lang['BOX_BLOG_TITULO'] = 'BLOG';
$lang['BOX_BLOG_TEXTO'] = 'New Ciab FEBRABAN blog. A place to discuss various themes related to Information Technology. Join it!';
$lang['QUERO_PATROCINAR_TITULO'] = 'I WANT TO SPONSOR';
$lang['QUERO_PATROCINAR_TEXTO'] = 'Sponsor the biggest event and exhibition in Latin America.';
$lang['QUERO_EXPOR_TITULO'] = 'I WANT TO EXHIBIT';
$lang['QUERO_EXPOR_TEXTO'] = 'Become an exhibitor and connect with more than 20,000 people that visit the three days of CIAB 2012.';
$lang['QUERO_VISITAR_TITULO'] = 'I WANT TO VISIT';
$lang['QUERO_VISITAR_TEXTO'] = 'Come to the Transamérica Expo Center and check out the innovative solutions that the CIAB exhibitors offer.';
$lang['AGENDA_TITULO'] = 'PROGRAM';
$lang['AGENDA_TEXTO'] = 'Check out the national and international speakers present in the panels of the 2012 edition of CIAB FEBRABAN';
$lang['NOTICIAS_TITULO'] = 'NEWS';
$lang['NENHUMA_NOTICIA'] = 'No recent news';
$lang['EVENTO_TITULO'] = 'THE EVENT';
$lang['EVENTO_SUBTITULO'] = 'What´s CIAB FEBRABAN?';
$lang['EVENTO_TEXTO'] = 'Congress and Expo of Information Technology for Financial Institutions – CIAB FEBRABAN is the biggest event in Latin America for the financial and technology sectors.';
/*****************************************************/
$lang['PUBLICACOES_TITULO'] = 'PUBLICATIONS';
$lang['PUBLICACOES_IMAGEM'] = '_imgs/home/ImgHome.jpg';
$lang['PUBLICACOES_SUBTITULO'] = 'A vez da Sociedade Conectada';
$lang['PUBLICACOES_TEXTO'] = 'Iniciativa do governo federal quer garantir o acesso de deficientes físicos aos seus sites. Outros públicos, como a terceira idade, também serão beneficiados';
/*****************************************************/
$lang['TWITTER_TITULO'] = 'TWITTER CIABFEBRABAN';
$lang['NEWSLETTER_TITULO'] = 'NEWSLETTER';
$lang['NEWSLETTER_TEXTO'] = 'Register and receive all news';
$lang['NEWSLETTER_PLACEHOLDER_NOME'] = 'Inform Your Name';
$lang['NEWSLETTER_PLACEHOLDER_EMAIL'] = 'Your email ';
$lang['NEWSLETTER_SUBMIT'] = 'REGISTER';

/* INSCRIÇÕES */
$lang['INSCRICOES_TITULO'] = 'Preços de Inscrições e Bonificações de Inscrições';
$lang['INSCRICOES_TITULO_TABELA_1'] = 'Valores por pessoa para inscrições nos 3 dias do Congresso';
$lang['DATA_ATE'] = 'Até';
$lang['DATA_APOS'] = 'Após';
$lang['INSCRICOES_TABELA_INSCRICOES'] = 'Inscrições';
$lang['INSCRICOES_TABELA_FILIADOS'] = 'Bancos filiados';
$lang['INSCRICOES_TABELA_NAO_FILIADOS'] = 'Não filiados';
$lang['INSCRICOES_TABELA_LINHA_1'] = 'Até 5';
$lang['INSCRICOES_TABELA_LINHA_2'] = 'de 6 a 10';
$lang['INSCRICOES_TABELA_LINHA_3'] = 'de 11 a 20';
$lang['INSCRICOES_TABELA_LINHA_4'] = 'mais de 20';
$lang['INSCRICOES_TABELA_RODAPE'] = 'EMPRESAS COM O MESMO CNPJ VALORES POR PARTICIPANTE';
$lang['INSCRICOES_TITULO_TABELA_2'] = 'Valores por pessoa para inscrições para 1 dia do Congresso';
$lang['INSCRICOES_OBSERVACAO_TITULO'] = 'Observação';
$lang['INSCRICOES_OBSERVACAO_TEXTO'] = 'No valor da inscrição estão incluídos: almoços, coffee breaks , coquetel do dia 20/06 e estacionamento nos 3 dias de realização do evento.';
$lang['INSCRICOES_OBSERVACAO_ITEM_1'] = 'Os descontos são válidos somente para inscrições na mesma categoria (participação nos 3 dias do evento ou participação em apenas um dia do evento).';
$lang['INSCRICOES_OBSERVACAO_ITEM_2'] = 'Manteremos o valor da 1ª. Fase para o banco que fizer mais do que o equivalente a 50 inscrições válidas para os três dias do evento. (ex.: 50 inscrições para participação nos 3 dias x R$ 2.178,00 = R$ 108.900,00 ou 124 inscrições para participação em um dia = R$ 108.872,00).';
$lang['INSCRICOES_BONIFICACAO_TITULO'] = 'Bonificação';
$lang['INSCRICOES_BONIFICACAO_ITEM_1'] = 'A cada 20 inscrições a instituição recebe 01 (uma) inscrição cortesia de um dia;';
$lang['INSCRICOES_BONIFICACAO_ITEM_2'] = 'Instituições que fizerem mais de 50 (cinqüenta) inscrições e aumentarem o total de inscritos em relação a 2011 serão bonificadas, conforme abaixo:';
$lang['INSCRICOES_BONIFICACAO_TABELA_TITULO_1'] = 'até 10%';
$lang['INSCRICOES_BONIFICACAO_TABELA_TITULO_2'] = 'Acima de 10% até 20%';
$lang['INSCRICOES_BONIFICACAO_TABELA_TITULO_3'] = 'Acima de 20% até 30%';
$lang['INSCRICOES_BONIFICACAO_TABELA_TITULO_4'] = 'Acima de 30%';
$lang['INSCRICOES_BONIFICACAO_TABELA_TEXTO_1'] = '5 inscr. de 1 dia';
$lang['INSCRICOES_BONIFICACAO_TABELA_TEXTO_2'] = '10 inscr. de 1 dia';
$lang['INSCRICOES_BONIFICACAO_TABELA_TEXTO_3'] = '15 inscr. de 1 dia';
$lang['INSCRICOES_BONIFICACAO_TABELA_TEXTO_4'] = '20 inscr. de 1 dia';
$lang['INSCRICOES_BONIFICACAO_TABELA_LEGENDA'] = '(*) A Tabela de Bonificação é válida exclusivamente para os acréscimos de inscrições em relação a 2011, conforme as faixas estabelecidas, e somente para empresas que fizeram acima de 50 (cinquenta) inscrições no Ciab 2012 .<br>(**) Para consultar a quantidade de inscrições realizadas em 2011, pedimos contatar a Diretoria de<br>Eventos pelo telefone (11) 3186-9860.';
$lang['INSCRICOES_EXEMPLO_TITULO'] = 'Exemplo';
$lang['INSCRICOES_BONIFICACAO_TEXTO'] = 'Instituição Y realizou em 2011 50 inscrições para os 3 dias do evento.<br> Em 2012, realizou 66 inscrições para os 3 dias.<br> Isso quer dizer que o banco Y tem direito à bonificação de 30%.<br>';
$lang['INSCRICOES_BONIFICACAO_TEXTO_NEGRITO'] = 'As bonificações adquiridas pela Instituição Y são';
$lang['INSCRICOES_BONIFICACAO_ITEM_EXEMPLO_1'] = 'Desconto de 35% para a mesma categoria (3 dias ou 1 dia) até o último dia de realização do Congresso, conforme disponibilidade de vagas.';
$lang['INSCRICOES_BONIFICACAO_ITEM_EXEMPLO_2'] = 'Gratuidade de 3 inscrições para participação em 1 dia do Congresso (a cada 20 inscrições 01 gratuidade).';
$lang['INSCRICOES_BONIFICACAO_ITEM_EXEMPLO_3'] = 'Gratuidade de 20 inscrições para participação em 1 dia do Congresso, conforme tabela de bonificação';
$lang['INSCRICOES_INFORMACOES_TITULO'] = 'IMPORTANT INFORMATION';
$lang['INSCRICOES_OPCOES_TITULO'] = 'Payment option';
$lang['INSCRICOES_OPCOES_ITEM_1'] = '- International Credit Cards';
$lang['INSCRICOES_OPCOES_ITEM_2'] = ' ';
$lang['INSCRICOES_REEMBOLSO_TITULO'] = 'Refunds and Cancellations';
$lang['INSCRICOES_REEMBOLSO_TEXTO'] = 'Requests for refunds or cancellations of registrations will be accepted when requested in writing and addressed to the email: eventos@febraban.org.br by May 31, 2012.<br> The refunds will be executed at the net amount, deducting all bank charges arising from bank transfers.<br> Requests for refunds or cancellations will not be accepted after June 1st, 2012.';
$lang['INSCRICOES_SUBSTITUICAO_TITULO'] = 'Substitution of Participant';
$lang['INSCRICOES_SUBSTITUICAO_TEXTO'] = 'FEBRABAN will only accept the substitution of a participant provided that the request is made by email eventos@febraban.org.br and sent before June 15, 2012.<br>Any replacement requested during the event, will be charged a fee of R$ 250.00, for re-issuing the name tag. The respective payment shall be made upon replacement request.';
$lang['INSCRICOES_CONFIRMACAO_TITULO'] = 'Registration Confirmation by Email';
$lang['INSCRICOES_CONFIRMACAO_TEXTO'] = 'After the registration is done, the system will send a confirmation by email.<br>If this confirmation is not received, the participant or area in charge shall contact the Events department of FEBRABAN by email eventos@febraban.org.br, requesting verification.';
$lang['INSCRICOES_ENCERRAMENTO_TITULO'] = 'Closing of Registrations';
$lang['INSCRICOES_ENCERRAMENTO_TEXTO'] = 'Registrations end on June 15, 2012 or when sold out.';

/* EVENTO */
$lang['EVENTO_APRESENTACAO_TITULO'] = 'Presentation';
$lang['EVENTO_APRESENTACAO_TEXTO'] = <<<TEXTO
<p>
The CIAB FEBRABAN – Congress and Exhibition of Information Technology for Financial Institutions – it’s the largest Latin American event for both the financial sector and the Technology area.
</p>
<p>
Created in 1990, since its first edition in 1991 the event has been encouraging the development of technology and banking innovation. Annually, the congress reunites about 1.9 thousand bank representatives of Brazil and abroad. Presents around 120 personalities among lecturers and debaters in more than 30 panels. 
</p>
<p>
The exhibition area gathers about 200 technology supplier companies and corporative innovation, in a total space superior to four thousand square meters, luring annual visits superior to 18 thousand executives and directors from financial institutions and other areas of technology and innovation.
</p>
<p>
The 23rd edition of CIAB FEBRABAN will have as its main theme “The New Challenges of the Financial Sector” and will be held on June 12, 13 and 14 of 2013, at the Transamérica Expo Center in Sao Paulo.
</p>
<p>
<p class="strong">The best of Ciab FEBRABAN 2012</p>
<p>
The 22nd edition of the Ciab FEBRABAN, held between June 20 and 22 of 2012 was one of the biggest and busiest from its history. The record area of expositions of 4.500 m² also had an audience record with 19 thousand representatives from financial institutions, Information Technology companies and partners. It was 149 exhibition companies that brought plenty of novelties, including the presence of 31 companies from abroad, which debuted in the Brazilian market.
</p>
<p>
More than 1.700 congress participants attended the 28 lecture panels and debates ministered by 110 specialists, 19 of who were international keynote speakers, debating subjects that permeated the main theme “The Connected Society”.
</p>
<p>
Also a highlight in 2012 is the third edition of the Ciab FEBRABAN award, which aims at recognizing young talents that develop innovative solutions, and the Innovation Space award.
</p>
TEXTO;
$lang['EVENTO_SOBRE_TITULO'] = 'About FEBRABAN';
$lang['EVENTO_SOBRE_TEXTO'] = <<<TEXTO
<p>
The Brazilian Federation of Banks – FEBRABAN is the main representative entity of the banking sector. It was founded in 1967 viewing the fortification of the financial system and its relations with the mode society. It gathers 120 associated banks from a universe with 172 institutions in operation in Brazil, which represent 97% of the total assets and 93% of the banking system’s liquid patrimony. Among the strategic objectives of the Federation is the development of initiatives for the ongoing improvement of the banking system’s productivity and the reduction of its risk levels, hence contributing for the economical, social and sustainable development of the country. 
</p>
<p>
	Read more at <a href='http://www.febraban.org.br' title='FEBRABAN' target='_blank'>www.febraban.org.br</a>
</p>
TEXTO;
$lang['EVENTO_CONGRESSO_TITULO'] = 'The Congress';
$lang['EVENTO_CONGRESSO_TEXTO'] = <<<TEXTO
<p>
Held alongside the Exposition, the Ciab FEBRABAN Congress occurs in three auditoriums that have the best and most renowned national and international specialists from the technology area.
</p>
<p>
	<strong>Numbers from the Ciab 2012 Congress – The Connected Society</strong>
</p>
<p>
	&bull; 110 specialists from the areas of technology and banking<br>
	&bull; 19 international keynote speakers<br>
	&bull; 1,717 congress participants
</p>
<p>
With these numbers, the Ciab FEBRABAN 2012 Congress debated important subjects such as innovation, social medias strategy, Cloud Computing, agile development, Big Data, consumerization, mobile technologies, accessibility, information security, besides panels with CIOs and directors of other business areas from financial institutions.
</p>
<p>
Among the most prominent participations, the congress participants pointed out the conference of the Itaú Unibanco CEO, Roberto Setubal, held on June 20 about the panel “The importance of IT on the financial system development”.
</p>
TEXTO;
$lang['EVENTO_ESPACOS_TITULO'] = 'Thematic Spaces';
$lang['EVENTO_ESPACOS_TEXTO'] = '<p>CIAB FEBRABAN once again reinforces its actions geared towards small companies aimed at stimulating the presence of this segment at the congress.</p>';
$lang['EVENTOS_ESPACOS_ITEM_1_TITULO'] = 'ENTREPREUNER AREA';
$lang['EVENTOS_ESPACOS_ITEM_1_TEXTO'] = 'Area dedicated to the exposition of companies with 100% national capital, of small size and billing limited to R$ 12 million. The exhibiting company has a package comprised of the area at the fair and stand assembly at lower prices.';
$lang['EVENTOS_ESPACOS_ITEM_2_TITULO'] = 'INNOVATION SPACE';
$lang['EVENTOS_ESPACOS_ITEM_2_TEXTO'] = "FEBRABAN and ITS (Technology Institute of Software and Services) promote, during Ciab FEBRABAN, the INNOVATION SPACE. Disclose this initiative and participate in the selection process! For more information, send an email to <a href='mailto:eventos@febraban.org.br'>eventos@febraban.org.br</a>.";
$lang['EVENTOS_ESPACOS_ITEM_3_TITULO'] = 'INTERNATIONAL AREA';
$lang['EVENTOS_ESPACOS_ITEM_3_TEXTO'] = 'Area dedicated to the exposition of companies without headquarters in Brazil, with stand assembly and its entire infrastructure.';

$lang['EVENTO_EXPOSICAO_TITULO'] = 'Exhibition';
$lang['EVENTO_EXPOSICAO_TEXTO'] = <<<TEXTO
<p>
Simultaneously to the congress, the CIAB FEBRABAN performs its exposition, which has the participation of hundreds of companies from financial areas, information technology (IT) and telecommunications. 
</p>
<p>
The visitor will have the opportunity to check all the technology novelties and solutions for the years to come. Also visit the Thematic Areas, a unique opportunity of honoring the small national companies from the IT area. 
</p>
<p>
The visit to the expo is free and happens from 9am to 8pm. Just sign up on the link below or directly at the site, at the “Visitors Accreditation” area. 
</p>
<p>
<span class="negrito">Important:</span> the visitor badge is only valid to access the expo’s pavilion. In no circumstances will the entry of visitors in the exclusive areas of congress participants and guests be allowed.
</p>
TEXTO;
$lang['EVENTO_QUERO_VISITAR_TITULO'] = 'I Want To Visit';
$lang['EVENTO_QUERO_VISITAR_TEXTO_1'] = <<<TEXTO
<p>
You can also visit the exhibition of the Ciab FEBRABAN. Entry is free. You only need to fill in the form below with your data and print your invitation. Go to the entrance of the Ciab FEBRABAN to obtain your visitor’s name tag and have access to the exhibition. 
</p>
<p>
<span class="negrito">Important:</span> the visitor’s name tag is only valid for access to the exhibition pavilion. Under no circumstances will visitors be allowed to enter the exclusive areas of entities taking part in the congress and guests.
</p>
TEXTO;
$lang['EVENTO_QUERO_VISITAR_TEXTO_2'] = <<<TEXTO
<p style="clear:left;">
If you prefer, click the button below and register. Our advisors team will contact you, if necessary.
</p>
<a href="http://www.credenciamento.com.br/2012/VisitanteCiab/Default_por.aspx" target="_blank" class="botao" style="color:#FFF;">CLICK HERE AND REGISTER</a>
TEXTO;
$lang['EVENTO_EXPOR_TITULO'] = 'I Want To Exhibit';
$lang['EVENTO_EXPOR_TEXTO'] = <<<TEXTO
<p>
Hundreds of companies from financial areas, information technology (IT) and telecommunications expose annually at the CIAB FEBRABAN and have the opportunity of narrowing their relationship with thousands of people that walk through the event. 
</p>
<p>
At the launch of the 2013 Ciab, in October, 81 exhibitors guaranteed their slots on the 3,770m² exposition area. 
</p>
TEXTO;
$lang['EVENTO_EXPOR_FORM_TEXTO'] = 'Fill out the form below, make your registration right now and book your company’s slot at the event. Our advisors will get in touch to pass on all information of how to become an expositor.';
$lang['EVENTO_EXPOR_FORM_1'] = 'Choose The Topic';
$lang['EVENTO_EXPOR_FORM_2'] = 'Full Name';
$lang['EVENTO_EXPOR_FORM_3'] = 'E-mail ';
$lang['EVENTO_EXPOR_FORM_4'] = 'Company';
$lang['EVENTO_EXPOR_FORM_5'] = 'Company Website ';
$lang['EVENTO_EXPOR_FORM_6'] = 'Field';
$lang['EVENTO_EXPOR_FORM_7'] = 'Phone number';
$lang['EVENTO_EXPOR_FORM_8'] = 'Area/ Space of Interest';
$lang['EVENTO_EXPOR_FORM_SUBMIT'] = 'SEND';
$lang['EVENTO_EXPOR_FORM_OBS'] = 'For additional information and space reservations contact:';
$lang['EVENTO_EXPOR_FORM_OK'] = '<h1>Obrigado!<br>Email enviado com sucesso.entraremos em contato em breve.</h1>';
$lang['EVENTO_EXPOR_FORM_ERROR'] = '<h1>O email não pode ser enviado. <br> Tente novamente.</h1>';
$lang['EVENTO_PATROCINAR_TITULO'] = 'I Want To Sponsor';
$lang['EVENTO_PATROCINAR_TEXTO'] = <<<TEXTO
<p>
Your company can be one of the sponsors of the largest Latin American expo and congress in information technology for financial institutions, and expose your brand to thousands of congress participants and visitors that attend all 3 days of Ciab FEBRABAN.
</p>

<p>
In 2012, the Ciab FEBRABAN expo received the visit of an audience superior to 17 thousand people, highly qualified, composed of CEO’s, directors and managers of financial institutions from Brazil and more than 26 countries. 
</p>
TEXTO;
$lang['EVENTO_PATROCINAR_OBS'] = 'For additional information and space reservations contact:';
$lang['EVENTO_MAPA_EXPOSITORES'] = 'Expositores';

$lang['EVENTO_ANOS_TITULO'] = 'Other Years';
$lang['EVENTO_ANOS_SUBTITULO'] = 'See what happened in previous editions of the event';
$lang['EVENTO_ANOS_ITEM_1'] = 'Technology beyond the web';
$lang['EVENTO_ANOS_ITEM_2'] = 'Generation Y<br> A new bank for<br> a new consumer';
$lang['EVENTO_ANOS_ITEM_3'] = 'A broad discussion about<br> technology and the impact<br> on banking inclusion';  
$lang['EVENTO_ANOS_ITEM_4'] = 'Technology and Security';

/* PRÊMIOS */
$lang['PREMIOS_CIAB_TITULO'] = 'Prêmio Ciab 2013';
$lang['PREMIOS_CIAB_SUBTITULO'] = 'PRÊMIO CIAB FEBRABAN! PREPARE-SE PARA SER O VENCEDOR DE 2013!';
$lang['Menção Honrosa'] = 'Menção Honrosa';
$lang['PREMIOS_CIAB_TEXTO'] = <<<TEXTO
<p>O Prêmio CIAB FEBRABAN visa o incentivo direto da capacidade de inovação técnica de jovens e universitários e o debate de propostas inovadoras. Fale com seus amigos e, sozinho ou em grupo, concorra a um total de R$ 25mil em prêmios, além do trabalho vencedor ser exposto para diversos especialistas e executivos das áreas de tecnologia e bancária.</p>
<p>O tema para 2013 é <strong>“Soluções para Eficiência Operacional”</strong></p>
<p>As <strong>inscrições poderão ser realizadas a partir de 06.03.2013</strong>. Prepare-se para ser o vencedor de 2013!</p>
TEXTO;
$lang['PREMIOS_CIAB_CHAMADA'] = 'Clique no botão abaixo e confira o regulamento.';
$lang['PREMIOS_CIAB_BOTAO_1'] = 'VER REGULAMENTO';
$lang['PREMIOS_CIAB_BOTAO_2'] = 'READ THE REGULATION';
$lang['PREMIOS_VENCEDORES_TITULO'] = 'CIAB WINNERS 2011';
$lang['PREMIOS_VENCEDORES_SUBTITULO'] = 'Award Winners of CIAB FEBRABAN and Universia 2011 - Technology In The Web';
$lang['PREMIOS_VENCEDORES_TEXTO_1'] = '<p>The 2011 edition of the Award CIAB FEBRABAN and Universia (which had the theme "Technology In The Web") recorded against  611 versus 223 entries in the first edition of the award in 2010. Check out the winners.</p>';
$lang['PREMIOS_VENCEDORES_COMUNICADO'] = 'ANNOUNCEMENT - RECLASSIFICATION AWARD CIAB FEBRABAN and UNIVERSIA 2011';
$lang['Menção Honrosa'] = 'Honors Mention';
$lang['PREMIOS_VENCEDORES_TEXTO_2'] = <<<TEXTO
<p>
The organizing committee of Ciab FEBRABAN 2011 based on the criteria of Regulation Award Ciab FEBRABAN Universia and 2011, re-evaluating the competitors, concluded that the work previously classified in the first place (2Whish) was disqualified for not complying with requirements of regulation.
</p>

<p>
Therefore, the final award, shall be as follows:
</p>
TEXTO;
$lang['PREMIOS_VENCEDORES_TABELA_1'] = 'Classification';
$lang['PREMIOS_VENCEDORES_TABELA_2'] = 'Name';
$lang['PREMIOS_VENCEDORES_TABELA_3'] = 'Title of the work';
$lang['PREMIOS_VENCEDORES_TWITTER_TITULO'] = 'TWITTER 2011 Winners';
$lang['PREMIOS_VENCEDORES_TWITTER_SUBTITULO'] = 'Contest Cultural Best Slogan CIAB on Twitter';
$lang['PREMIOS_VENCEDORES_TWITTER_CHAMADA'] = 'Who followed the @ CiabFEBRABAN and sent his slogan did well!';
$lang['PREMIOS_VENCEDORES_TWITTER_TEXTO'] = <<<TEXTO
<p>Ciab FEBRABAN brought one more novelty for 2011 edition: the cultural contest "Best Slogan Ciab on Twitter." To participate, was enough follow the profile @ CiabFEBRABAN and send a slogan up to 140 characters.</p>

<p>37 slogans were received and three winners were awarded with tickets valid for three days of Ciab FEBRABAN 2011, entitled to participate in the exhibition and lectures at the congress.</p>

<p>Keep following @ CiabFEBRABAN and stay on top of everything that happens in the biggest Congress and Exhibition of Information Technology in Latin America.</p>
TEXTO;

$lang['PREMIOS_VENCEDORES_2012_TITULO'] = 'Confira os vencedores do Prêmio CIAB 2012';
$lang['PREMIOS_VENCEDORES_2012_PROJETO'] = 'PROJETO';
$lang['PREMIOS_VENCEDORES_2012_GRUPO'] = 'GRUPO';
$lang['PREMIOS_VENCEDORES_2012_PROJETO_1'] = 'ProDeaf – Software Tradutor (Português/LIBRAS – LIBRAS/Português)';
$lang['PREMIOS_VENCEDORES_2012_GRUPO_1'] = <<<LIST
<p>- Marcelo Lucio Correia de Amorim</p>
<p>- Lucas Araújo Mello Soares</p>
<p>- João Paulo dos Santos Oliveira</p>
<p>- Luca Bezerra Dias</p>
<p>- Roberta Agra Coutelo</p>
<p>- Victor Rafael Nascimento dos Santos</p>
LIST;
$lang['PREMIOS_VENCEDORES_2012_PROJETO_2'] = 'Bidd – Plataforma móvel para contratação de crédito de consumo.';
$lang['PREMIOS_VENCEDORES_2012_GRUPO_2'] = <<<LIST
<p>- Daniel César Vieira Radicchi</p>
<p>- André Luis Ferreira Marques</p>
<p>- Willy Stadnick Neto</p>
<p>- Vinicius Abouhatem</p>
<p>- Daniel Coelho</p>
LIST;
$lang['PREMIOS_VENCEDORES_2012_PROJETO_3'] = 'My Tag – Rede social de consumo.';
$lang['PREMIOS_VENCEDORES_2012_GRUPO_3'] = <<<LIST
<p>- Holisson Soares da Cunha</p>
<p>- Sergio Luis Sardi Mergen</p>
<p>- Luciana Hikari Kuamoto</p>
<p>- Leonardo Seiji</p>
LIST;





/* IMPRENSA */
$lang['IMPRENSA_ASSESSORIA_TITULO'] = 'Press Room';
$lang['IMPRENSA_ASSESSORIA_TEXTO_1'] = 'To receive press releases, participate of press conference and interviews, please contact us';
$lang['IMPRENSA_ASSESSORIA_TEXTO_2'] = 'If you want to cover the event, complete the form below and register. Our team will contact you if necessary.';
$lang['IMPRENSA_ASSESSORIA_FORM_1'] = 'Full Name';
$lang['IMPRENSA_ASSESSORIA_FORM_2'] = 'E-mail';
$lang['IMPRENSA_ASSESSORIA_FORM_3'] = 'MTB';
$lang['IMPRENSA_ASSESSORIA_FORM_4'] = 'Company';
$lang['IMPRENSA_ASSESSORIA_FORM_5'] = 'Company Website';
$lang['IMPRENSA_ASSESSORIA_FORM_6'] = 'Field';
$lang['IMPRENSA_ASSESSORIA_FORM_7'] = 'Phone number';
$lang['IMPRENSA_ASSESSORIA_FORM_SUBMIT'] = 'SEND';
$lang['IMPRENSA_ASSESSORIA_FORM_OK'] = '<h1>Obrigado! <br> Email enviado com  ssucesso. Entraremos em contato em breve.</h1>';
$lang['IMPRENSA_ASSESSORIA_FORM_ERROR'] = '<h1>O email não pode ser enviado. <br> Tente novamente.</h1>';

/* INFORMAÇÕES */
$lang['INFO_PAVILHOES'] = 'Pavillions A, B, C and D';
$lang['INFORMACOES_LOCAL_TITULO'] = 'Event Location';
$lang['INFORMACOES_LOCAL_VERMAPA'] = 'view larger map';
$lang['INFORMACOES_HOTEIS_TITULO'] = 'Hotels';
$lang['INFORMACOES_HOTEIS_SUBTITULO'] = 'Clique sobre o hotel para mais detalhes';
$lang['INFORMACOES_HOTEIS_LEGENDA'] = 'SGL: Acomoda 1 pessoa | DBL: Acomoda 2 pessoas';
$lang['INFORMACOES_HOTEIS_TABELA_TITULO_1'] = 'HOTEL';
$lang['INFORMACOES_HOTEIS_TABELA_TITULO_2'] = 'SGL SUPERIOR DAILY';
$lang['INFORMACOES_HOTEIS_TABELA_TITULO_3'] = 'DBL SUPERIOR DAILY';
$lang['INFORMACOES_HOTEIS_COMDESCONTO'] = 'including discount';
$lang['INFORMACOES_HOTEIS_LOCAL'] = 'Address';
$lang['INFORMACOES_HOTEIS_TEL'] = 'Phone number';
$lang['INFORMACOES_HOTEIS_COMO_CHEGAR'] = 'Click here to see how to get';
$lang['INFORMACOES_HOTEIS_CODIGO'] = 'Reservation code';
$lang['INFORMACOES_HOTEIS_DIARIA_SGL'] = 'Daily rate for Superior SGL apartment';
$lang['INFORMACOES_HOTEIS_DIARIA_DBL'] = 'Daily rate for Superior DBL apartment';
$lang['INFORMACOES_HOTEIS_DIARIA'] = 'The daily rate (by adherence) includes';
$lang['INFORMACOES_HOTEIS_OBSERVACOES'] = 'Notes';
$lang['INFORMACOES_HOTEIS_CHECK_1'] = '- Check-in from 14:00 PM<br>- Check-out by 12:00 PM';
$lang['INFORMACOES_HOTEIS_CHECK_2'] = 'Daily rates begin at 12:00 PM and finish at 12:00 PM';
$lang['INFORMACOES_HOTEIS_CHECK_3'] = 'Check-in from 02:00 PM<br>- Check-out by 12:00 PM';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_1'] = 'Breakfast served in the restaurant<br> (06:00 AM to 10:00 AM)';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_2'] = '- Breakfast (when served in the restaurant)<br>- 01 garage space per apartment.';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_3'] = '- Breakfast (when served in the restaurant)<br>- Transfer Congonhas Airport / Hotel /<br> Congonhas Airport';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_4'] = '- Breakfast (when served in the restaurant).<br>- 1 garage space per apartment';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_5'] = '-Breakfast (when served in the restaurant).';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_6'] = '-Breakfast (when served in the restaurant).<br>- Internet';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_1'] = 'The value of the daily rate will be increased by 5% for ISS + R$ 4,50 tourism tax. The payment of the accommodation costs should be made directly at Hotel Transamérica São Paulo with cash or credit card. The apartments are subject to availability.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_2'] = 'The value of the daily rate will be increased by 5% for ISS + tourism tax. The payment of the accommodation costs should be made directly at Transamérica Executive Chácara Santo Antonio with cash or credit card. The apartments are subject to availability.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_3'] = 'The value of the daily rate will be increased by 5% for ISS + tourism tax. The payment of the accommodation costs should be made directly at Transamérica Executive Congonhas with cash or credit card. The apartments are subject to availability.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_4'] = 'The value of the daily rate will be increased by 5% for ISS + tourism tax. The payment of the accommodation costs should be made directly at Transamérica Prime International Plaza with cash or credit card. The apartments are subject to availability.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_5'] = 'The value of the daily rate will be increased by 5% for ISS + tourism tax. The payment of the accommodation costs should be made directly at Blue Tree Premium Morumbi with cash or credit card. The apartments are subject to availability.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_6'] = 'The value of the daily rate will be increased by 5% for ISS + tourism tax. The payment of the accommodation costs should be made directly at Blue Tree Premium Verbo Divino with cash or credit card. The apartments are subject to availability.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_7'] = 'The value of the daily rate will be increased by 5% for ISS + tourism tax. The payment of the accommodation costs should be made directly at Berrini Quality Hotel o with cash or credit card. The apartments are subject to availability.';

/* CONTATO */
$lang['CONTATO_INFORMACOES_GERAIS'] = 'General Information';
$lang['CONTATO_COMO_CHEGAR'] = 'HOW TO GET THERE';
$lang['CONTATO_RESERVAS'] = 'Reservations';
$lang['CONTATO_FORM_TITULO'] = 'Fill out the form below';
$lang['CONTATO_FORM_ASSUNTO_TITULO'] = 'Topic';
$lang['CONTATO_FORM_ASSUNTO_1'] = 'Other';
$lang['CONTATO_FORM_ASSUNTO_2'] = 'Panelist';
$lang['CONTATO_FORM_ASSUNTO_3'] = 'Exhibit';
$lang['CONTATO_FORM_ASSUNTO_4'] = 'Visitor';
$lang['CONTATO_FORM_ASSUNTO_5'] = 'Sponsor';
$lang['CONTATO_FORM_ASSUNTO_6'] = 'Press';
$lang['CONTATO_FORM_1'] = 'Full Name';
$lang['CONTATO_FORM_2'] = 'E-mail';
$lang['CONTATO_FORM_3'] = 'Company';
$lang['CONTATO_FORM_4'] = 'Company Site';
$lang['CONTATO_FORM_5'] = 'Field';
$lang['CONTATO_FORM_6'] = 'Phone number';
$lang['CONTATO_FORM_7'] = 'Message';
$lang['CONTATO_FORM_SUBMIT'] = 'SEND';
$lang['CONTATO_FORM_OK'] = '<h1>Obrigado! <br>Email enviado com sucesso. Entraremos em contato em breve.</h1>';
$lang['CONTATO_FORM_ERROR'] = '<h1>O email não pode ser enviado. <br>Tente novamente.</h1>';

/***************/
/* PROGRAMAÇÃO */
/***************/

/* DIA 1 */
$lang['auditorio-1'] = <<<STR
AUDITÓRIO FEBRABAN
STR;

$lang['auditorio-2'] = <<<STR
AUDITÓRIO LINHA DE NEGÓCIOS
STR;

$lang['auditorio-3'] = <<<STR
AUDITÓRIO EFICIÊNCIA OPERACIONAL
STR;

$lang['obs-solenidade'] = <<<STR
Solenidade de abertura da Exposição e visita aos estandes dos Patrocinadores / Coffee break
STR;

$lang['1-sessao-1'] = <<<STR
1ª sessão manhã - das 09:00 às 10:30
STR;

$lang['1-sessao-2'] = <<<STR
2ª sessão manhã - das 11:00 às 12:30
STR;

$lang['1-sessao-3'] = <<<STR
1ª sessão tarde - das 14:00 às 15:30
STR;

$lang['1-sessao-4'] = <<<STR
2ª sessão tarde - das 16:00 às 17:00 e das 17:15 às 18:15
STR;

$lang['DIA_1_MANHA_1_A'] = <<<STR
Abertura
STR;

$lang['DIA_1_MANHA_1_B'] = <<<STR
Transmissão simultânea da Abertura
STR;

$lang['DIA_1_MANHA_1_C'] = <<<STR
Transmissão simultânea da Abertura
STR;


$lang['DIA_1_MANHA_2_A'] = <<<STR
Jorge Hereda - Presidente da CAIXA
STR;

$lang['DIA_1_MANHA_2_B'] = <<<STR
Transmissão simultânea
STR;

$lang['DIA_1_MANHA_2_C'] = <<<STR
Transmissão simultânea
STR;


$lang['DIA_1_TARDE_1_A'] = <<<STR
Keynote Speaker
STR;

$lang['DIA_1_TARDE_1_B'] = <<<STR
Interagindo com Clientes e Funcionários com Mobilidade
STR;

$lang['DIA_1_TARDE_1_C'] = <<<STR
Appification - Das aplicações para aplicativos
STR;


$lang['DIA_1_TARDE_2_A'] = <<<STR
A definir
STR;

$lang['DIA_1_TARDE_2_B'] = <<<STR
Impacto das Megatendências globais nos Serviços Financeiros<br>
<br>
<br>
==================================== <br>
Intervalo das 17:00 às 17:15<br>
==================================== <br>
Converting data into a strategic business asset for Banks<br>
<br>
<br>
<br>
Moderador: Jorge Vacarini - Deutsche Bank
STR;

$lang['DIA_1_TARDE_2_C'] = <<<STR
Cloud Computing na Prática
STR;



/* DIA 2 */
$lang['DIA_2_MANHA_1_A'] = <<<STR
Keynote Speaker
STR;

$lang['DIA_2_MANHA_1_B'] = <<<STR
BPO - Business Process Outsourcing
STR;

$lang['DIA_2_MANHA_1_C'] = <<<STR
Estratégia para o desenvolvimento de um modelo de Governança de TI 
STR;



$lang['DIA_2_MANHA_2_A'] = <<<STR
Expansão do Crédito - Formas e Limites 
STR;

$lang['DIA_2_MANHA_2_B'] = <<<STR
Projetos de Inovação ou Cultura em Inovação
STR;

$lang['DIA_2_MANHA_2_C'] = <<<STR
Big Data
STR;


$lang['DIA_2_TARDE_1_A'] = <<<STR
Painel das Áreas de Negócios dos Bancos
STR;

$lang['DIA_2_TARDE_1_B'] = <<<STR
Segurança da informação
STR;

$lang['DIA_2_TARDE_1_C'] = <<<STR
A Definir
STR;


$lang['DIA_2_TARDE_2_A'] = <<<STR
Infraestrutura do Brasil e Inovação
STR;

$lang['DIA_2_TARDE_2_B'] = <<<STR
Experiencia de Clientes vs Fidelização<br>
<br>
<br>
=====================================<br>
Intervalo das 17:00 às 17:15<br>
==================================== <br>
Costumer Excellence in Banking
STR;

$lang['DIA_2_TARDE_2_C'] = <<<STR
Eficiência Operacional e Redução de Custos Operacionais
STR;



/* DIA 3 */
$lang['3-sessao-1'] = <<<STR
1ª sessão manhã - das 09:00 às 10:30
STR;

$lang['3-sessao-2'] = <<<STR
2ª sessão manhã - das 11:00 às 12:30
STR;

$lang['3-sessao-3'] = <<<STR
1ª sessão tarde - das 14:00 às 16:00
STR;

$lang['3-sessao-4'] = <<<STR
2ª sessão tarde - das 16:30 às 18:00
STR;

$lang['DIA_3_MANHA_1_A'] = <<<STR
Inovação na Indústria de TI para os Bancos
STR;


$lang['DIA_3_MANHA_1_B'] = <<<STR
Transmissão Simultânea
STR;

$lang['DIA_3_MANHA_1_C'] = <<<STR
Transmissão Simultânea
STR;


$lang['DIA_3_MANHA_2_A'] = <<<STR
Lideranças de TI debatem Tendências
STR;

$lang['DIA_3_MANHA_2_B'] = <<<STR
Basiléia III
STR;

$lang['DIA_3_MANHA_2_C'] = <<<STR
Inovação em Auto Atendimento
STR;


$lang['DIA_3_TARDE_1_A'] = <<<STR
A definir

STR;

$lang['DIA_3_TARDE_1_B'] = <<<STR
A definir
STR;

$lang['DIA_3_TARDE_1_C'] = <<<STR
A definir
STR;


$lang['DIA_3_TARDE_2_A'] = <<<STR
A definir
STR;

$lang['DIA_3_TARDE_2_B'] = <<<STR
Transmissão Simultânea
STR;

$lang['DIA_3_TARDE_2_C'] = <<<STR
Transmissão Simultânea
STR;

$lang['Auditórios Excelência em TI'] = 'Auditórios Excelência em TI';
$lang['Auditorios Horário'] = 'Horário';
$lang['Auditorios Palestrantes'] = 'Palestrantes';
$lang['Auditorios Tema'] = 'Tema';

/* DIA 1 */
$lang['dia_1 Auditorio 1'] = 'Auditório 3 - TALARIS';
$lang['dia_1 Auditorio 3'] = 'Auditório 3 - LÓGICA';
$lang['dia_1 Auditorio 2'] = 'Auditório 2 - Acesso Digital';

$lang['dia1_auditorio_2_horario_1'] = '14h30';
$lang['dia1_auditorio_2_horario_2'] = '14h40';
$lang['dia1_auditorio_2_horario_3'] = '15h00';
$lang['dia1_auditorio_2_horario_4'] = '15h30';
$lang['dia1_auditorio_2_horario_5'] = '16h00';
$lang['dia1_auditorio_2_palestrante_1'] = 'Antonio Augusto de Almeida Leite (Pancho), ACREFI';
$lang['dia1_auditorio_2_palestrante_2'] = 'Breno Costa, Sócio e Consultor da GoOn – A Evolução na Gestão do Risco';
$lang['dia1_auditorio_2_palestrante_3'] = 'Ricardo Batista, Superintendente de Crédito do Tribanco';
$lang['dia1_auditorio_2_palestrante_4'] = 'Roberto Jabali, Superintendente de Crédito do Grupo Citi - Credicard';
$lang['dia1_auditorio_2_palestrante_5'] = 'Alex Yamamoto,Consultor da Acesso Digital';
$lang['dia1_auditorio_2_tema_1'] = 'Abertura';
$lang['dia1_auditorio_2_tema_2'] = 'Case GoOn: Como aumentar o nível de decisão automática sem impacto na inadimplência.';
$lang['dia1_auditorio_2_tema_3'] = 'Case Tribanco: captura de documentos no varejo';
$lang['dia1_auditorio_2_tema_4'] = 'Case Grupo Citi: Pioneirismo na gestão eletrônica de documentos';
$lang['dia1_auditorio_2_tema_5'] = 'Formalização de crédito online nas instituições financeiras';

$lang['dia1_auditorio_3_horario_1'] = '09h00 - 10h00';
$lang['dia1_auditorio_3_horario_2'] = '11h00 – 12h00';
$lang['dia1_auditorio_3_horario_3'] = '14h00 - 15h10';
$lang['dia1_auditorio_3_horario_4'] = '15h20 – 16h30';
$lang['dia1_auditorio_3_horario_5'] = '16h40 – 17h50';
$lang['dia1_auditorio_3_horario_6'] = '17h50 -18h00';
$lang['dia1_auditorio_3_horario_7'] = ' ';
$lang['dia1_auditorio_3_palestrante_1'] = 'Paulo Martins, Diretor Comercial / Logica';
$lang['dia1_auditorio_3_palestrante_2'] = 'Reginaldo Silva, Portfólio / Logica';
$lang['dia1_auditorio_3_palestrante_3'] = 'Antonio Requejo, Regional Practice Leader Iberia & Latam – Security / Logica';
$lang['dia1_auditorio_3_palestrante_4'] = 'Júlio Gonçalves, Regional Practice Leader Iberia & Latam – IT Modernization / Logica';
$lang['dia1_auditorio_3_palestrante_5'] = 'Lode Snykers, Vice President, Global Financial Services / Logica';
$lang['dia1_auditorio_3_palestrante_6'] = 'Paulo Martins, Diretor Comercial / Logica';
$lang['dia1_auditorio_3_palestrante_7'] = ' ';
$lang['dia1_auditorio_3_tema_1'] = 'Compartilhando Idéias';
$lang['dia1_auditorio_3_tema_2'] = 'Mobilidade: Saiba como surpreender os seus clientes com soluções modernas e inovadoras';
$lang['dia1_auditorio_3_tema_3'] = 'De Securidad en TI para el Gerenciamento de Riesgo';
$lang['dia1_auditorio_3_tema_4'] = 'Modernização de Sistemas Legados';
$lang['dia1_auditorio_3_tema_5'] = 'The technological challenges of the Financial Sector in the XXI century';
$lang['dia1_auditorio_3_tema_6'] = 'Compartilhando Ideias II';
$lang['dia1_auditorio_3_tema_7'] = 'Sorteios para os participantes do dia';


/* DIA 2 */
$lang['dia_2 Auditorio 1'] = 'Auditório 1 - HP';
$lang['dia_2 Auditorio 2'] = 'Auditório 2 - Microsoft';
$lang['dia_2 Auditorio 3'] = 'Auditório 3 - CLM';

/* Auditorio 1 */
$lang['dia2_auditorio_1_horario_1'] = '10h00 – 11h00';
$lang['dia2_auditorio_1_horario_2'] = '11h10 – 12h10';
$lang['dia2_auditorio_1_horario_3'] = '14h30 – 15h30';
$lang['dia2_auditorio_1_horario_4'] = '15h40 – 16h00';
$lang['dia2_auditorio_1_palestrante_1'] = 'Mike Wright, HP';
$lang['dia2_auditorio_1_palestrante_2'] = 'Gladson Martins Russo, Nokia Siemens';
$lang['dia2_auditorio_1_palestrante_3'] = 'Andre Kalsing, HP';
$lang['dia2_auditorio_1_palestrante_4'] = 'Federico Grosso, Autonomy';
$lang['dia2_auditorio_1_tema_1'] = 'Tendências para Social CRM';
$lang['dia2_auditorio_1_tema_2'] = 'Novas tecnologias para Mobile Payment';
$lang['dia2_auditorio_1_tema_3'] = 'Benefícios da adoção de uma plataforma de serviços multicanal';
$lang['dia2_auditorio_1_tema_4'] = 'Uma nova Visão de Inovação e Big Data';


// Auditorio 2
$lang['dia2_auditorio_2_horario_1'] = '9h-10h';
$lang['dia2_auditorio_2_horario_2'] = '10h-11h30';
$lang['dia2_auditorio_2_horario_3'] = '11h30-12h30';
$lang['dia2_auditorio_2_horario_4'] = '14h-15h';
$lang['dia2_auditorio_2_horario_5'] = '15h-16h';
$lang['dia2_auditorio_2_horario_6'] = '16h-17h';
$lang['dia2_auditorio_2_horario_7'] = '17h-18h';
$lang['dia2_auditorio_2_palestrante_1'] = 'Fabio Souto';
$lang['dia2_auditorio_2_palestrante_2'] = 'Miha Krajl';
$lang['dia2_auditorio_2_palestrante_3'] = 'Jun Endo';
$lang['dia2_auditorio_2_palestrante_4'] = 'Eduardo Campos';
$lang['dia2_auditorio_2_palestrante_5'] = 'Roberto Prado';
$lang['dia2_auditorio_2_palestrante_6'] = 'Ricardo-Enrico Jahn';
$lang['dia2_auditorio_2_palestrante_7'] = 'Richard Chaves';
$lang['dia2_auditorio_2_tema_1'] = 'Sociedade Conectada e a Consumerização de TI';
$lang['dia2_auditorio_2_tema_2'] = 'Como TI está mudando nosso futuro';
$lang['dia2_auditorio_2_tema_3'] = 'Soluções Inovadoras para uma Experiência Única Multidevices & Consumerização de TI';
$lang['dia2_auditorio_2_tema_4'] = 'A nuvem nos seus termos - Estratégia de Computação na nuvem da Microsoft';
$lang['dia2_auditorio_2_tema_5'] = 'O profissional TI como protagonista da competitividade. Capacitação e Satisfação do cliente.';
$lang['dia2_auditorio_2_tema_6'] = 'Relacionamento e fidelização nas instituições financeiras com Dynamics CRM/XRM';
$lang['dia2_auditorio_2_tema_7'] = 'O poder da Inovação';

// Auditorio 3
$lang['dia2_auditorio_3_horario_1'] = '10h30 - 11h30';
$lang['dia2_auditorio_3_horario_2'] = '11h30 - 12h30';
$lang['dia2_auditorio_3_horario_3'] = '14h30 - 15h30';
$lang['dia2_auditorio_3_horario_4'] = '15h30 - 16h30';
$lang['dia2_auditorio_3_horario_5'] = '16h30 - 17h30';
$lang['dia2_auditorio_3_horario_6'] = '17h30 - 18h30';
$lang['dia2_auditorio_3_palestrante_1'] = 'Martin Roesch, SNORT';
$lang['dia2_auditorio_3_palestrante_2'] = 'Alex Silva, A10';
$lang['dia2_auditorio_3_palestrante_3'] = 'Carlos Fagundes, INTEGRALTRUST';
$lang['dia2_auditorio_3_palestrante_4'] = 'Sanjay Ramnath, BARRACUDA';
$lang['dia2_auditorio_3_palestrante_5'] = 'Dick Faulkner, SOPHOS';
$lang['dia2_auditorio_3_palestrante_6'] = 'Martin Roesch, SOURCEFIRE';
$lang['dia2_auditorio_3_tema_1'] = 'Open Source Security';
$lang['dia2_auditorio_3_tema_2'] = 'DDoS Attacks and Brute Force';
$lang['dia2_auditorio_3_tema_3'] = 'Segredos do Basileia III';
$lang['dia2_auditorio_3_tema_4'] = 'Application Security';
$lang['dia2_auditorio_3_tema_5'] = 'Cryptography and Mobile Security';
$lang['dia2_auditorio_3_tema_6'] = 'Next Generation Security';

/* DIA 3*/
$lang['dia_3 Auditorio 1'] = 'Auditório 1 - FEBRABAN';

$lang['dia3_auditorio_1_horario_1'] = '11h00 - 12h30';
$lang['dia3_auditorio_1_palestrante_1'] = 'Guilhermino Domiciano de Souza – Banco do Brasil;  Marcelo Ribeiro Câmara – Bradesco;  Cesar Faustino – Itaú Unibanco';
$lang['dia3_auditorio_1_tema_1'] = 'A Segurança Digital do Cidadão no Sistema Financeiro Brasileiro';


$lang['Programação em Breve'] = "Please wait; soon we will publish the congress schedule.";


$lang['CIA AÉREA OFICIAL'] = "OFFICIAL AIRLINE COMPANY";
$lang['Parceria TAM'] = "TAM Partnership";
$lang["A TAM é a CIA Aérea Oficial do Ciab 2013 e quem ganha o desconto é você. Clique aqui e veja as condições especiais oferecidas."] = "TAM was elected the Official Airline Company for Ciab 2013 and who wins the discount is you. Click here to see the special offer.";
$lang['A TAM é a Cia Aérea Oficial do Ciab 2013 e quem ganha o desconto é você!'] = "Tam was elected the Official Airline Company for Ciab 2013 and who wins the discount is you!";
$lang['Em parceria com o Ciab 2013, a TAM como Cia Aérea Oficial do congresso disponibiliza passagens aéreas com condições especiais para você comparecer ao Ciab e ficar por dentro de tudo que as empresas de ponta em tecnologia estão preparando para superar “Os Novos Desafios do Setor Financeiro”.'] = "Partner of Ciab 2013 and Official Airline of congress, TAM offers airline tickets with special conditions for your participation at Ciab - be on top with all of the leading companies that are preparing to surpass “The New Challenges of the Financial Sector”.";
$lang['Confira abaixo as condições que preparamos para você, válidos para todos os trechos <strong>Brasil/São Paulo/Brasil</strong> no período de <strong>embarque</strong> entre <strong>09 e 17.06.2013:</strong>'] = "See below the special offers prepared for you, valid for all flights in stretch <strong>Brazil/São Paulo/Brazil</strong> on period for <strong>boarding</strong> between <strong>09 and 17.06.2013:</strong>";
$lang['- 25%* de desconto na tarifa da classe disponível<br><br><strong>ou</strong><br><br>- 10%* de desconto nas tarifas Mega Promo e nas classes W e N'] = "- 25% * discount on the rate class available<br><br><strong>or</strong><br><br>- 10% * discount on rates Mega Promo and classes N and W";
$lang["Para conseguir o seu desconto é fácil:<br>Clique no botão abaixo ou acesse <a href='http://www.tam.com.br' title='TAM' target='_blank'>www.tam.com.br</a> e ao escolher o <strong>trecho</strong> e a <strong>data</strong>, digite o número do Código Promocional <strong>413709</strong>."] = "To get your discount it's easy.<br> Click on the bottom below or go to webpage <a href='http://www.tam.com.br' title='TAM' target='_blank'>www.tam.com.br</a>, choose the <strong>rate</strong> and the <strong>date</strong> and type the number of promotional code <strong>413709</strong>.";
$lang['Clique e Compre sua Passagem'] = "Click here and buy your ticket";
$lang['* valores sujeitos à disponibilidade de assentos e regras/restrições específicas de cada tarifa, válidos para embarque três dias antes e três após a data do evento no trecho Brasil/São Paulo/Brasil.'] = "* values subject to availability of seats and rules / restrictions specific to each fare, valid for boarding three days before and three days after the date of the congress in stretch Brazil / Sao Paulo / Brazil, and Valid for domestic flights only.";

// ARQUIVO : ../www/ciab/application/views/programacao//primeiro.php
$lang['Quarta-Feira'] = "";
$lang['AUDITÓRIO FEBRABAN'] = "";
$lang['sessão'] = "";
$lang['horário'] = "";
$lang['palestra'] = "";
$lang['palestrante'] = "";
$lang['manhã'] = "";
$lang['às'] = "";
$lang['Abertura'] = "";
$lang['Jorge Hereda - Presidente da CAIXA'] = "";
$lang['tarde'] = "";
$lang['Keynote Speaker'] = "";
$lang['A definir'] = " ";
$lang['obs-solenidade'] = "";
$lang['AUDITÓRIO LINHA DE NEGÓCIOS'] = "";
$lang['Transmissão simultânea da Abertura'] = "";
$lang['Transmissão simultânea'] = "";
$lang['Interagindo com Clientes e Funcionários com Mobilidade'] = "";
$lang['Impacto das Megatendências globais nos Serviços Financeiros'] = "";
$lang['intervalo'] = "";
$lang['Converting data into a strategic business asset for Banks'] = "";
$lang['Moderador: Jorge Vacarini - Deutsche Bank'] = "";
$lang['AUDITÓRIO EFICIÊNCIA OPERACIONAL'] = "";
$lang['Appification - Das aplicações para aplicativos'] = "";
$lang['Cloud Computing na Prática'] = "";
$lang['Auditórios Excelência em TI'] = "";
$lang['dia_1 Auditorio 2'] = "";
$lang['Auditorios Horário'] = "";
$lang['Auditorios Palestrantes'] = "";
$lang['Auditorios Tema'] = "";
$lang['dia1_auditorio_2_horario_1'] = "";
$lang['dia1_auditorio_2_palestrante_1'] = "";
$lang['dia1_auditorio_2_tema_1'] = "";
$lang['dia1_auditorio_2_horario_2'] = "";
$lang['dia1_auditorio_2_palestrante_2'] = "";
$lang['dia1_auditorio_2_tema_2'] = "";
$lang['dia1_auditorio_2_horario_3'] = "";
$lang['dia1_auditorio_2_palestrante_3'] = "";
$lang['dia1_auditorio_2_tema_3'] = "";
$lang['dia1_auditorio_2_horario_4'] = "";
$lang['dia1_auditorio_2_palestrante_4'] = "";
$lang['dia1_auditorio_2_tema_4'] = "";
$lang['dia1_auditorio_2_horario_5'] = "";
$lang['dia1_auditorio_2_palestrante_5'] = "";
$lang['dia1_auditorio_2_tema_5'] = "";
$lang['dia_1 Auditorio 3'] = "";
$lang['dia1_auditorio_3_horario_1'] = "";
$lang['dia1_auditorio_3_palestrante_1'] = "";
$lang['dia1_auditorio_3_tema_1'] = "";
$lang['dia1_auditorio_3_horario_2'] = "";
$lang['dia1_auditorio_3_palestrante_2'] = "";
$lang['dia1_auditorio_3_tema_2'] = "";
$lang['dia1_auditorio_3_horario_3'] = "";
$lang['dia1_auditorio_3_palestrante_3'] = "";
$lang['dia1_auditorio_3_tema_3'] = "";
$lang['dia1_auditorio_3_horario_4'] = "";
$lang['dia1_auditorio_3_palestrante_4'] = "";
$lang['dia1_auditorio_3_tema_4'] = "";
$lang['dia1_auditorio_3_horario_5'] = "";
$lang['dia1_auditorio_3_palestrante_5'] = "";
$lang['dia1_auditorio_3_tema_5'] = "";
$lang['dia1_auditorio_3_horario_6'] = "";
$lang['dia1_auditorio_3_palestrante_6'] = "";
$lang['dia1_auditorio_3_tema_6'] = "";
$lang['dia1_auditorio_3_horario_7'] = "";
$lang['dia1_auditorio_3_palestrante_7'] = "";
$lang['dia1_auditorio_3_tema_7'] = "";

// ARQUIVO : ../www/ciab/application/views/programacao//segundo.php
$lang['Quinta-Feira'] = "";
$lang['Expansão do Crédito - Formas e Limites'] = "";
$lang['Painel das Áreas de Negócios dos Bancos'] = "";
$lang['Infraestrutura do Brasil e Inovação'] = "";
$lang['BPO - Business Process Outsourcing'] = "";
$lang['Projetos de Inovação ou Cultura em Inovação'] = "";
$lang['Segurança da informação'] = "";
$lang['Experiencia de Clientes vs Fidelização'] = "";
$lang['Costumer Excellence in Banking'] = "";
$lang['Estratégia para o desenvolvimento de um modelo de Governança de TI'] = "";
$lang['Big Data'] = "";
$lang['A Definir'] = "";
$lang['Eficiência Operacional e Redução de Custos Operacionais'] = "";

// ARQUIVO : ../www/ciab/application/views/programacao//terceiro.php
$lang['Sexta-Feira'] = "";
$lang['Inovação na Indústria de TI para os Bancos'] = "";
$lang['Lideranças de TI debatem Tendências'] = "";
$lang['Transmissão Simultânea'] = "";
$lang['Basiléia III'] = "";
$lang['Inovação em Auto Atendimento'] = "";
?>