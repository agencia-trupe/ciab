<?php

function traduz($label, $ajax = FALSE){
    
    $CI =& get_instance();

    if($ajax)
        $CI->lang->load($CI->session->userdata('language').'_site', $CI->session->userdata('language'));
    
    $return = $CI->lang->line($label);
    
    if($return)
        return $return;
    else
        //return prefixo(str_repeat('non', (strlen($label) - 3) / 3));
        return $label;
}

/*
 * Função para adicionar o prefixo definido na sessão de acordo com a linguagem
 */
function prefixo($arg){
    $CI =& get_instance();
    return $CI->session->userdata('prefixo').$arg;
}

function atualizaImagens(){
    $CI =& get_instance();
    $qry_pt = $CI->db->get('palestrantes')->result();
    foreach($qry_pt as $k => $v){
        if($v->imagem_on != '' && strpos($v->imagem_on, 'placehold') === FALSE){
            $CI->db->set('imagem_on', $v->imagem_on)->where('nome', $v->nome)->update('en_palestrantes');
            $CI->db->set('imagem_on', $v->imagem_on)->where('nome', $v->nome)->update('es_palestrantes');
        }
        if($v->imagem_off != '' && strpos($v->imagem_off, 'placehold') === FALSE){
            $CI->db->set('imagem_off', $v->imagem_off)->where('nome', $v->nome)->update('en_palestrantes');
            $CI->db->set('imagem_off', $v->imagem_off)->where('nome', $v->nome)->update('es_palestrantes');
        }
        if($v->imagem_ampliada != '' && strpos($v->imagem_ampliada, 'placehold') === FALSE){
            $CI->db->set('imagem_ampliada', $v->imagem_ampliada)->where('nome', $v->nome)->update('en_palestrantes');
            $CI->db->set('imagem_ampliada', $v->imagem_ampliada)->where('nome', $v->nome)->update('es_palestrantes');
        }
    }
}
?>