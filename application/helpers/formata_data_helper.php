<?php

function formataData($data, $tipo){
    if($tipo == 'br2mysql'){
        list($dia,$mes,$ano) = explode('/', $data);
        return $ano.'-'.$mes.'-'.$dia;
    }elseif($tipo == 'mysql2br'){
        list($ano,$mes,$dia) = explode('-', $data);
        return $dia.'/'.$mes.'/'.$ano;
    }
}

?>
