<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class MY_Frontcontroller extends CI_controller {

    var $headeropt;
    var $linguagem;

    function __construct() {
        parent::__construct();
        
        //$this->output->enable_profiler(TRUE);
        
        $this->linguagem = TRUE;
        $this->headeropt = array('marcar_menu' => $this->router->class);            
    }
    
    function _output($output){
        echo $this->load->view('common/header', $this->headeropt, TRUE) . $output . $this->load->view('common/footer', false, TRUE);
    }

}
?>
