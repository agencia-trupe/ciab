<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class MY_Admin extends CI_controller {

    var $linguagem;

    function __construct() {
        parent::__construct();
        
        $this->linguagem = FALSE;

        if(!$this->session->userdata('logged_in'))
            redirect('painel/login');
    }

}
?>
