<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="pt-BR"> <![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  
  <title>CIAB 2012</title>
  <meta name="description" content="Congresso e Exposição de Tecnologia da Informação das Instituições Financeiras">
  <meta name="keywords" content="Congresso de TI, TI para Instituições Financeiras, Febraban, Ciab, " />
  <meta name="robots" content="index, follow" />
  
    <!-- FACEBOOK -->
    <meta property="og:title" content="<? echo isset($fb_title) ? $fb_title : 'CIAB Febraban 2012' ?>" />
    <meta property="og:description" content="<? echo isset($fb_description) ? $fb_description : 'Congresso e Exposição de Tecnologia da Informação das Instituições Financeiras' ?>" />
    <meta property="og:image" content="<? echo isset($fb_image) ? $fb_image : base_url('_imgs/layout/logomarca.png') ?>" />    
    
    <meta property="og:type" content="activity" />
    <meta property="og:url" content="<?=current_url()?>" />
    <meta property="og:site_name" content="CIAB 2012" />
    <meta property="fb:admins" content="100002297057504" />
    
    <!-- GOOGLE PLUS -->
    <script type="text/javascript">
      window.___gcfg = {lang: 'pt-BR'};
    
      (function() {
        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
        po.src = 'https://apis.google.com/js/plusone.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
      })();
    </script>  
  
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <base href="<?= base_url() ?>">

  <script>
    var BASE = '<?=base_url()?>'
  </script>
  
  <!-- CSS -->
  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="css/fontface/stylesheet.css">
  <link rel="stylesheet" href="css/base.css">
  
  <link rel="stylesheet" href="css/fancybox/jquery.fancybox-1.3.4.css">
  <!--[if lt IE 9]><link rel="stylesheet" href="css/ie8.css"><![endif]-->
  
  <!-- JAVASCRIPT -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="js/jquery-1.6.4.min.js"><\/script>')</script>
  <script src="js/jquery-ui-1.8.18.custom.min.js"></script>
  <script src="js/modernizr-2.0.6.min.js"></script>
  <script>Array.prototype.map || document.write('<script src="js/map.js"><\/script>')</script>
  <script src="js/jquery.fancybox-1.3.4.js"></script>
  <script src="js/placeholder.js"></script>
  <script src="js/cycle.js"></script>
  <script defer src="js/front.js"></script>
    
</head>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=179472998836941";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="faixa-topo">
    <div class="verde cor"></div><div class="azul cor"></div>
    <div class="vermelho cor"></div><div class="laranja cor"></div>
</div>

<div id="superior">
    
    <div id="topo" class="centro">
    
        <img src="_imgs/layout/img-topo.png" alt="Febraban" class="imagem-topo">    
    
        <a href="home" class="logo"><img src="_imgs/layout/logomarca.png" alt="Home"></a>
        
        <div>        
            <form method="post" action="busca">
                <input type="text" name="busca" autocomplete="off" class="placeholder" placeholder="<?=traduz('BUSCA')?>//">
                <input type="submit" value="">
            </form>
            
            <a href="<?=base_url('rss')?>" target="_blank" id="mn-rss" class="icones"><img src="_imgs/layout/rss-btn.png" alt="RSS Feed"></a>
            <a href="http://www.twitter.com/CiabFEBRABAN" id="mn-twitter" class="icones divisao"><img src="_imgs/layout/twitter-btn.png" alt="Twitter"></a>
            <a href="linguagem/pt" id="mn-pt" class="icones"><img src="_imgs/layout/lang-btn-pt.png" alt="<?=traduz('ling_portugues')?>"></a>
            <a href="linguagem/es" id="mn-es" class="icones"><img src="_imgs/layout/lang-btn-es.png" alt="<?=traduz('ling_espanhol')?>"></a>
            <a href="linguagem/en" id="mn-en" class="icones ultimo"><img src="_imgs/layout/lang-btn-en.png" alt="<?=traduz('ling_ingles')?>"></a>
        </div>
        
    </div>
    
    <nav class="centro">
        <ul>
            <li id="mn-evento"><a href="evento"><?=traduz('EVENTO')?></a></li>
            <li id="mn-programacao"><a href="programacao"><?=traduz('PROGRAMACAO')?></a></li>
            <li id="mn-palestrantes"><a href="palestrantes"><?=traduz('PALESTRANTES')?></a></li>
            <li id="mn-premios"><a href="premios"><?=traduz('PREMIOS')?></a></li>
            <li id="mn-inscricoes"><a href="inscricoes"><?=traduz('INSCRICOES')?></a></li>
            <li id="mn-noticias"><a href="noticias"><?=traduz('NOTICIAS')?></a></li>
            <li id="mn-publicacoes"><a href="publicacoes"><?=traduz('PUBLICACOES')?></a></li>
            <li id="mn-imprensa"><a href="imprensa"><?=traduz('IMPRENSA')?></a></li>
            <li id="mn-informacoes"><a href="informacoes"><?=traduz('INFORMACOES')?></a></li>
            <li id="mn-contato"><a href="contato"><?=traduz('CONTATO')?></a></li>
        </ul>        
    </nav>

    	


		<div class="naoencontrado">


			<style>

			.naoencontrado h1{
				height:150px;
				line-height:150px;
				text-align:center;
				width:100%;
				font-size:25px;
			}

			</style>

			<h1>Página Não Encontrada</h1>

		</div>
	</div>
</div>

<footer>
    <div class="centropad">
        <h1 class="titulo-patrocinio"><?=traduz('PATROCINADORES')?></h1>
        
        <h2><?=traduz('DIAMANTE')?></h2>
        <div class="sombra-grande-fundo">
            <img src="_imgs/patrocinio/diamante.png" alt="Patrocinadores Diamante">
        </div>
        
        <h2><?=traduz('OURO')?></h2>
        <div class="sombra-grande-fundo">
            <img src="_imgs/patrocinio/ouro.png" alt="Patrocinadores Ouro">
        </div>
        
        <!-- <h2><=traduz('PRATA')?></h2>
        <div class="sombra-grande-fundo">
            <img src="_imgs/patrocinio/prata.jpg" alt="Patrocinadores Prata">
        </div> -->
        
        <h2><?=traduz('APOIO')?></h2>
        <div class="sombra-grande-fundo">
            <img src="_imgs/patrocinio/apoio.png" alt="Patrocinadores Apoio">
        </div>
    </div>
    <address class="centro">
        <div class="borda-wrapper">
            <ul>
                <li>
                    <a href="contato"><?=traduz('FALE CONOSCO')?></a>&nbsp;&#124;&nbsp;
                </li>
                <li>
                    <a href="sobre/privacidade"><?=traduz('POLITICA DE PRIVACIDADE')?></a>&nbsp;&#124;&nbsp;
                </li>
                <li>
                    <a href="sobre/termos"><?=traduz('TERMO DE USO')?></a>&nbsp;&#124;&nbsp;
                </li>
                <li>
                    <a href="sobre/mapa"><?=traduz('MAPA DO SITE')?></a>
                </li>
            </ul>
            
            <?=traduz('address-linha1')?><br>
            <?=traduz('address-linha2')?>
        </div>
    </address>
</footer>


<!-- GOOGLE ANALYTICS -->
<script type="text/javascript">

    var _gaq = _gaq || [];

    _gaq.push(['_setAccount', 'UA-3461385-24']);

    _gaq.push(['_trackPageview']);

    (function () {

        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

    })();

</script>

</body>
</html>