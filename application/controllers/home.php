<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Home extends MY_Frontcontroller {

    function __construct() {
        parent::__construct();
    }
    
    function index(){
        
        $data['noticias'] = $this->db->order_by('data','desc')->get('noticias', 3)->result();

        $this->load->view('home', $data);
    }

    function notfound(){
    	$this->load->view('404');	
    }

}
?>
