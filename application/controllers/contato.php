<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Contato extends MY_Frontcontroller {

    function __construct() {
        parent::__construct();
    }
    
    function index(){
        $menu['submenu'] = FALSE;
        $menu['titulo'] = "CONTATO";

        $data['envio'] = $this->session->flashdata('envio');
        $data['status'] = $this->session->flashdata('status');          
        
        $this->load->view('common/menu-lateral', $menu);
        $this->load->view('contato', $data);
        $this->load->view('common/veja-mais');
    }

    function enviar(){
        $emailconf['charset'] = 'utf-8';
        $emailconf['mailtype'] = 'html';

        $this->load->library('email');

        $this->email->initialize($emailconf);
        
        $from = 'noreply@febraban.com.br';
        $fromname = 'Site Ciab';
        $to = 'eventos@febraban.org.br';
        $bcc = 'bruno@trupe.net';
        $assunto = 'Ciab - Contato';
        
        $u_assunto = $this->input->post('assunto');
        $u_nome = $this->input->post('nome');
        $u_email = $this->input->post('email');
        $u_empresa = $this->input->post('empresa');
        $u_empresa_site = $this->input->post('empresa-site');
        $u_area_atuacao = $this->input->post('area-atuacao');
        $u_telefone = $this->input->post('telefone');
        $u_mensagem = $this->input->post('mensagem');
        
        $email = "<html>
                    <head>
                        <style type='text/css'>
                            .tit{
                                font-weight:bold;
                                font-size:16px;
                                color:#00B69C;
                                font-family:Verdana;
                            }
                            .val{
                                color:#000;
                                font-size:14px;
                                font-family:Verdana;
                            }
                        </style>
                    </head>
                    <body>
                    <span class='tit'>Assunto: </span> <span class='val'>$u_assunto</span><br>
                    <span class='tit'>Nome :</span> <span class='val'>$u_nome</span><br>
                    <span class='tit'>E-mail :</span> <span class='val'>$u_email</span><br>
                    <span class='tit'>Empresa :</span> <span class='val'>$u_empresa</span><br>
                    <span class='tit'>Site da Empresa :</span> <span class='val'><a href='$u_empresa_site'>$u_empresa_site</a></span><br>
                    <span class='tit'>Área de Atuação :</span> <span class='val'>$u_area_atuacao</span><br>
                    <span class='tit'>Telefone :</span> <span class='val'>$u_telefone</span><br>
                    <span class='tit'>Mensagem :</span> <span class='val'>$u_mensagem</span>
                    </body>
                    </html>";

        $this->email->from($from, $fromname);
        $this->email->to($to);
        if($bcc)
            $this->email->bcc($bcc);
        $this->email->reply_to($u_email);

        $this->email->subject($assunto);
        $this->email->message($email);

        if(!$this->email->send())
            $this->session->set_flashdata('status', FALSE);
        else
            $this->session->set_flashdata('status', TRUE);

        $this->session->set_flashdata('envio', TRUE);
        redirect('contato');
    }       
}
?>
