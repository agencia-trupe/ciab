<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Noticias extends MY_Frontcontroller {

    var $pag_options;

    function __construct() {
        parent::__construct();
        
        $this->pag_options = array(
            'per_page' => '5',
            'first_link' => FALSE,
            'last_link' => FALSE,
            'next_tag_open' => "<div class='seta' id='nav-direita'>",
            'next_tag_close' => "</div>",
            'next_link' => '',
            'prev_tag_open' => "<div class='seta' id='nav-esquerda'>",
            'prev_tag_close' => "</div>",
            'prev_link' => '',
            'display_pages' => TRUE,
            'current_tag_open' => "<div class='pagina-ativa'>",
            'current_tag_close' => "</div>",
            'num_links' => 15
        );        
    }
    
    function index($pag = 0){

        $this->pag_options['base_url'] = base_url('noticias/index');
        $this->pag_options['total_rows'] = $this->db->count_all_results('noticias');
        $this->pag_options['uri_segment'] = 3;
        $this->load->library('pagination', $this->pag_options);
        
        $menu['categorias'] = $this->db->order_by('titulo')->get('noticias_categorias')->result();
        
        $data['noticias'] = $this->db->order_by('data', 'DESC')->order_by('id', 'desc')->get('noticias', $this->pag_options['per_page'], $pag)->result();        
        $data['pagination'] = $this->pagination->create_links();
            
        $this->load->view('noticias/submenu-noticias',$menu);
        $this->load->view('common/menu-lateral');
        $this->load->view('noticias/lista', $data);
        $this->load->view('common/veja-mais');
    }

    function detalhe($id = TRUE){
        if(!$id)
            redirect('noticias');
        
        $this->load->helper('text');
            
        $menu['categorias'] = $this->db->order_by('titulo')->get('noticias_categorias')->result();
        $data['registro'] = $this->db->get_where('noticias', array('id' => $id))->result();

        if(!isset($data['registro'][0]))
            show_404(current_url(), FALSE);
        
        $query = $this->db->get_where('noticias_categorias', array('id' => $data['registro'][0]->id_noticias_categorias))->result();
        $menu['slug'] = $query[0]->slug;
        
        $data['ultimas'] = $this->db->order_by('id', 'DESC')->where('id !=', $id)->get('noticias', 3)->result();
        foreach($data['ultimas'] as $k => $v){
            $v->olho = strip_tags($v->olho);
        }
        
        $this->headeropt['fb_title'] = 'Ciab 2013 - '.$data['registro'][0]->titulo;
        $this->headeropt['fb_description'] = word_wrap(strip_tags($data['registro'][0]->texto), 40) . '...';
        $this->headeropt['fb_image'] = base_url('_imgs/noticias/'.$data['registro'][0]->imagem);
        
        $this->load->view('noticias/submenu-noticias',$menu);
        $this->load->view('common/menu-lateral');
        $this->load->view('noticias/detalhe', $data);
        $this->load->view('common/veja-mais');
    }
    
    function categoria($slug = false, $pag = 0){
        if(!$slug)
            redirect('noticias');
            
        $query = $this->db->get_where('noticias_categorias', array('slug' => $slug))->result();
        
        if(!$query[0])
            redirect('noticias');
        
        $menu['slug'] = $slug;
        
        $this->pag_options['base_url'] = base_url('noticias/categoria/'.$slug);
        $this->pag_options['total_rows'] = $this->db->where('id_noticias_categorias', $query[0]->id)->count_all_results('noticias');
        $this->pag_options['uri_segment'] = 4;
        $this->load->library('pagination', $this->pag_options);        
        
        $menu['categorias'] = $this->db->order_by('titulo')->get('noticias_categorias')->result();        
        $data['noticias'] = $this->db->order_by('data', 'DESC')->get_where('noticias', array('id_noticias_categorias' => $query[0]->id), $this->pag_options['per_page'], $pag)->result();
        $data['pagination'] = $this->pagination->create_links();
        
        $this->load->view('noticias/submenu-noticias',$menu);
        $this->load->view('common/menu-lateral');
        $this->load->view('noticias/lista', $data);
        $this->load->view('common/veja-mais');        
    }
}
?>
