<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Programacao extends MY_Frontcontroller {

    function __construct() {
        parent::__construct();
    }
    
    function index(){
        redirect('programacao/primeiroDia');
    }
    
    function primeiroDia(){
        $this->load->view('programacao/submenu-programacao');
        $this->load->view('common/menu-lateral');
        $this->load->view('programacao/'.prefixo('primeiro'));
        $this->load->view('common/veja-mais');        
    }
    
    function segundoDia(){
        $this->load->view('programacao/submenu-programacao');
        $this->load->view('common/menu-lateral');
        $this->load->view('programacao/'.prefixo('segundo'));
        $this->load->view('common/veja-mais');        
    }
    
    function terceiroDia(){
        $this->load->view('programacao/submenu-programacao');
        $this->load->view('common/menu-lateral');
        $this->load->view('programacao/'.prefixo('terceiro'));
        $this->load->view('common/veja-mais');        
    }
    
}
?>
