  <?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    
class Ajax extends CI_Controller{
    
    var $linguagem;    
    
    function __construct(){
        parent::__construct();
        
        $this->linguagem = FALSE;
        
        if(!$this->input->is_ajax_request())
            redirect('home');
    }
    
    function index(){redirect('home');}
    
    function cadastraNewsletter(){
        $nome = $this->input->post('nome');
        $email = $this->input->post('email');
        
        $reg = '/^([0-9a-zA-Z]+([_.-]?[0-9a-zA-Z]+)*@[0-9a-zA-Z]+[0-9,a-z,A-Z,.,-]*(.){1}[a-zA-Z]{2,4})+$/';
        
        if(preg_match($reg, $email)){
            if($this->db->where('email', $email)->count_all_results('cadastro_newsletter') > 0)
                echo json_encode(array('mensagem' => "E-mail já cadastrado!", 'classe' => "falha"));
            else{
                $this->db->set('nome', $nome);
                $this->db->set('email', $email);
                $this->db->set('data_inclusao', date('Y-m-d H:i:s'));
                $this->db->insert('cadastro_newsletter');
                echo json_encode(array('mensagem' => "E-mail cadastrado com sucesso!", 'classe' => "sucesso"));
            }
        }else{
            echo json_encode(array('mensagem' => "Email inválido!", 'classe' => "aviso"));
        }
    }

    function pegaThumbs(){
        $ano = $this->input->post('pasta');
        
        $publicacoes = array(
            '2013' => array(
                        array('capa' => 'CapaRevista44.jpg', 'arquivo' => '2013/44-Fev2013.pdf'),
            ),
            '2012' => array(
                        array('capa' => 'CapaRevista43.jpg', 'arquivo' => '2012/43-Dez2012.pdf'),
                        array('capa' => 'CapaRevista42.jpg', 'arquivo' => '2012/42-Out2012.pdf'),              
                        array('capa' => 'CapaRevista41.jpg', 'arquivo' => '2012/41-Ago2012.pdf'),
                        array('capa' => 'CapaRevista40.jpg', 'arquivo' => '2012/40-Jun2012.pdf'),
                        array('capa' => 'CapaRevista39.jpg', 'arquivo' => '2012/39-Mai2012.pdf'),
                        array('capa' => 'CapaRevista38.jpg', 'arquivo' => '2012/38-Fev2012.pdf')
            ),
            '2011' => array(
                        array('capa' => 'CapaRevista37.jpg', 'arquivo' => '2011/37-Dez2011.pdf'),
                        array('capa' => 'CapaRevista36.jpg', 'arquivo' => '2011/36-Out2011.pdf'),
                        array('capa' => 'CapaRevista35.jpg', 'arquivo' => '2011/35-Ago2011.pdf'),
                        array('capa' => 'CapaRevista34.jpg', 'arquivo' => '2011/34-Jun2011.pdf'),
                        array('capa' => 'CapaRevista33.jpg', 'arquivo' => '2011/33-Mai2011.pdf'),
                        array('capa' => 'CapaRevista32.jpg', 'arquivo' => '2011/32-Fev2011.pdf')
            ),
            '2010' => array(
                        array('capa' => 'CapaRevista31.jpg', 'arquivo' => '2010/31-Nov2010.pdf'),
                        array('capa' => 'CapaRevista30.jpg', 'arquivo' => '2010/30-Ago2010.pdf'),
                        array('capa' => 'CapaRevista29.jpg', 'arquivo' => '2010/29-Jun2010.pdf'),
                        array('capa' => 'CapaRevista28.jpg', 'arquivo' => '2010/28-Mai2010.pdf'),
                        array('capa' => 'CapaRevista27.jpg', 'arquivo' => '2010/27-Fev2010.pdf')
            ),
            '2009' => array(
                        array('capa' => 'CapaRevista26.jpg', 'arquivo' => '2009/26-Dez2009.pdf'),
                        array('capa' => 'CapaRevista25.jpg', 'arquivo' => '2009/25-Out2009.pdf'),
                        array('capa' => 'CapaRevista24.jpg', 'arquivo' => '2009/24-Ago2009.pdf'),
                        array('capa' => 'CapaRevista23.jpg', 'arquivo' => '2009/23-Jun2009.pdf'),
                        array('capa' => 'CapaRevista22.jpg', 'arquivo' => '2009/22-Abr2009.pdf'),
                        array('capa' => 'CapaRevista21.jpg', 'arquivo' => '2009/21-Fev2009.pdf')
            ),
            '2008' => array(
                        array('capa' => 'CapaRevista20.jpg', 'arquivo' => '2008/20-Dez2008.pdf'),
                        array('capa' => 'CapaRevista19.jpg', 'arquivo' => '2008/19-Nov2008.pdf'),
                        array('capa' => 'CapaRevista18.jpg', 'arquivo' => '2008/18-Ago2008.pdf'),
                        array('capa' => 'CapaRevista17.jpg', 'arquivo' => '2008/17-Jun2008.pdf'),
                        array('capa' => 'CapaRevista16.jpg', 'arquivo' => '2008/16-Abr2008.pdf'),
                        array('capa' => 'CapaRevista15.jpg', 'arquivo' => '2008/15-Fev2008.pdf')
            ),
            '2007' => array(
                        array('capa' => 'CapaRevista14.jpg', 'arquivo' => '2007/14-Dez2007.pdf'),
                        array('capa' => 'CapaRevista13.jpg', 'arquivo' => '2007/13-Out2007.pdf'),
                        array('capa' => 'CapaRevista12.jpg', 'arquivo' => '2007/12-Ago2007.pdf'),
                        array('capa' => 'CapaRevista11.jpg', 'arquivo' => '2007/11-Jun2007.pdf'),
                        array('capa' => 'CapaRevista10.jpg', 'arquivo' => '2007/10-Abril2007.pdf'),
                        array('capa' => 'CapaRevista09.jpg', 'arquivo' => '2007/09-Fev2007.pdf')
            ),
            '2006' => array(
                        array('capa' => 'CapaRevista08.jpg', 'arquivo' => '2006/08-Dez2006.pdf'),
                        array('capa' => 'CapaRevista07.jpg', 'arquivo' => '2006/07-Out2006.pdf'),
                        array('capa' => 'CapaRevista06.jpg', 'arquivo' => '2006/06-Ago2006.pdf'),
                        array('capa' => 'CapaRevista05.jpg', 'arquivo' => '2006/05-Jun2006.pdf'),
                        array('capa' => 'CapaRevista04.jpg', 'arquivo' => '2006/04-Abr2006.pdf'),
                        array('capa' => 'CapaRevista03.jpg', 'arquivo' => '2006/03-Fev2006.pdf')
            ),
            '2005' => array(
                        array('capa' => 'CapaRevista02.jpg', 'arquivo' => '2005/02-Dez2005.pdf'),
                        array('capa' => 'CapaRevista01.jpg', 'arquivo' => '2005/01-Out2005.pdf')
            )            
        );
        
        if($ano != 0)
            echo json_encode($publicacoes[$ano]);
        else{
            $retorno = array();
            foreach($publicacoes as $k => $v){
                foreach($v as $v2){
                    $retorno[] = $v2;
                }
            }
            echo json_encode($retorno);
        }
    }
    
    function linksVejaMais(){
        switch($this->input->post('lang')){
          case 'pt_':
            $links = array(
                array( 'imagem'     => 'o-ciab.png',
                       'titulo'     => 'CIAB FEBRABAN',
                       'subtitulo'  => 'Conecte-se e Saiba tudo sobre o Ciab!',
                       'destino'    => 'evento/apresentacao',
                       'texto'      => 'Participe do maior evento da América Latina tanto para o setor financeiro quanto para a área de TI, CIAB FEBRABAN. ',
                       'classe'     => ''),
           
                array( 'imagem'     => 'programacao.png',
                       'titulo'     => 'PROGRAMAÇÃO',
                       'subtitulo'  => 'Os temas mais atuais do setor.',
                       'destino'    => 'programacao/primeiroDia',
                       'texto'      => 'Mais de 20 palestras e debates sobre os temas mais atuais do setor de tecnologia bancária. Confira a programação',
                       'classe'     => ''),
                
                array( 'imagem'     => 'noticias.png',
                       'titulo'     => 'NOTÍCIAS',
                       'subtitulo'  => 'Confira as novidades do CIAB.',
                       'destino'    => 'noticias',
                       'texto'      => 'Aqui você fica conectado com as novidades e os acontecimentos do CIAB.',
                       'classe'     => ''),
                
                array( 'imagem'     => 'quero-visitar.png',
                       'titulo'     => 'QUERO VISITAR',
                       'subtitulo'  => 'Visite a exposição do CIAB.',
                       'destino'    => 'evento/querovisitar',
                       'texto'      => 'Venha ao Transamérica Expo Center e confira as soluções inovadoras que os expositores do CIAB trazem até você.',
                       'classe'     => ''),
                
                array( 'imagem'     => 'quero-expor.png',
                       'titulo'     => 'QUERO EXPOR',
                       'subtitulo'  => 'Quer ser um expositor no CIAB FEBRABAN?',
                       'destino'    => 'evento/queroexpor',
                       'texto'      => 'Seja um expositor e conecte-se com mais de 20.000 pessoas que frequentam os três dias do CIAB.',
                       'classe'     => ''),
           
                array( 'imagem'     => 'espacotematico.png',
                       'titulo'     => 'ESPAÇOS TEMÁTICOS',
                       'subtitulo'  => 'Conexão para pequenas empresas.',
                       'destino'    => 'evento/espacos',
                       'texto'      => 'O CIAB reforça suas ações voltadas para as pequenas empresas, visando estimular a presença desse segmento no congresso.',
                       'classe'     => ''),
                
                array( 'imagem'     => 'mapa.png',
                       'titulo'     => 'PLANTA DA EXPOSIÇÃO',
                       'subtitulo'  => 'Confira as empresas expositoras.',
                       'destino'    => 'evento/planta',
                       'texto'      => 'Veja quem são as empresas expositoras das áreas de finanças, tecnologia da informação (TI) e telecomunicações do CIAB.',
                       'classe'     => ''),
                
                array( 'imagem'     => 'hospedagem.png',
                       'titulo'     => 'HOSPEDAGEM',
                       'subtitulo'  => 'Confira onde ficar hospedado.',
                       'destino'    => 'informacoes/hospedagem',
                       'texto'      => 'A comissão organizadora do CIAB negociou tarifas especiais com o hotel Transamérica São Paulo.',
                       'classe'     => ''),
                
                array( 'imagem'     => 'imprensa.png',
                       'titulo'     => 'SALA DE IMPRENSA',
                       'subtitulo'  => 'Atendimento exclusivo aos jornalistas.',
                       'destino'    => 'imprensa/assessoria',
                       'texto'      => 'Para receber press releases, participar de coletivas e entrevistas, clique aqui e faça seu cadastro ou entre em contato com nossa assessoria.',
                       'classe'     => ''),
           
                array( 'imagem'     => 'palestrante.png',
                       'titulo'     => 'PALESTRANTES',
                       'subtitulo'  => 'Os melhores estarão no CIAB.',
                       'destino'    => 'palestrantes',
                       'texto'      => 'Confira os palestrantes nacionais e internacionais presentes nos painéis da edição 2012 do CIAB FEBRABAN.',
                       'classe'     => ''),
                
                array( 'imagem'     => 'premio.png',
                       'titulo'     => 'PRÊMIO CIAB',
                       'subtitulo'  => 'Soluções para Eficiência Operacional.',
                       'destino'    => 'premios/ciab',
                       'texto'      => 'O Prêmio CIAB 2013 já está no ar com o tema Soluções para Eficiência Operacional. Confira.',
                       'classe'     => ''),
                
                array( 'imagem'     => 'publicacoes.png',
                       'titulo'     => 'PUBLICAÇÕES',
                       'subtitulo'  => 'As melhores entrevistas você confere aqui.',
                       'destino'    => 'publicacoes',
                       'texto'      => 'Fique por dentro dos números e do que pensam os melhores especialistas das áreas de TI, redes sociais, telecomunicações e finanças.',
                       'classe'     => ''),
           
                array( 'imagem'     => 'outros-anos.png',
                       'titulo'     => 'OUTRAS EDIÇÕES ',
                       'subtitulo'  => 'Você não pode perder nenhum assunto.',
                       'destino'    => 'evento/outrosanos',
                       'texto'      => 'Se você deixou de ler alguma edição da Revista CIAB, basta acessar as edições anteriores e conferir todo histórico.',
                       'classe'     => '')          
            );
            break;
        case 'en_':
            $links = array(
                array( 'imagem'     => 'o-ciab.png',
                       'titulo'     => 'CIAB FEBRABAN',
                       'subtitulo'  => 'Connect and know all about Ciab!',
                       'destino'    => 'evento/apresentacao',
                       'texto'      => 'Be a part of the biggest event in Latin America for both the financial sector and the IT area, CIAB FEBRABAN.',
                       'classe'     => ''),
           
                array( 'imagem'     => 'programacao.png',
                       'titulo'     => 'SCHEDULE',
                       'subtitulo'  => 'The most current themes in the sector.',
                       'destino'    => 'programacao/primeiroDia',
                       'texto'      => 'More than 20 talks and debates on the most current themes in the technology sector. Check the schedule.',
                       'classe'     => ''),
                
                array( 'imagem'     => 'noticias.png',
                       'titulo'     => 'NEWS',
                       'subtitulo'  => 'Check out the CIAB news.',
                       'destino'    => 'noticias',
                       'texto'      => 'Here you stay connected with all CIAB news and events.',
                       'classe'     => ''),
                
                array( 'imagem'     => 'quero-visitar.png',
                       'titulo'     => 'I WANT TO VISIT',
                       'subtitulo'  => 'Visit the CIAB expo.',
                       'destino'    => 'evento/querovisitar',
                       'texto'      => 'Come to the Transamérica Expo Center and check out the innovative solutions that the CIAB expositors bring to you.',
                       'classe'     => ''),
                
                array( 'imagem'     => 'quero-expor.png',
                       'titulo'     => 'I WANT TO EXPOSE',
                       'subtitulo'  => 'Want to be an expositor at the CIAB FEBRABAN?',
                       'destino'    => 'evento/queroexpor',
                       'texto'      => 'Be an expositor and connect with more than 20,000 people who participate in all three days of CIAB.',
                       'classe'     => ''),
           
                array( 'imagem'     => 'espacotematico.png',
                       'titulo'     => 'THEMATIC AREAS',
                       'subtitulo'  => 'Connection for small companies.',
                       'destino'    => 'evento/espacos',
                       'texto'      => 'The CIAB reinforces its actions to small companies, seeking to stimulate the presence of this congress segment. ',
                       'classe'     => ''),
                
                array( 'imagem'     => 'mapa.png',
                       'titulo'     => 'EXHIBITION PLANT',
                       'subtitulo'  => 'Check out the companies that will expose.',
                       'destino'    => 'evento/planta',
                       'texto'      => 'See which are the exhibiting companies of the financial area, information technology (IT) and telecommunications of CIAB.',
                       'classe'     => ''),
                
                array( 'imagem'     => 'hospedagem.png',
                       'titulo'     => 'ACCOMMODATION',
                       'subtitulo'  => 'Check out where to stay.',
                       'destino'    => 'informacoes/hospedagem',
                       'texto'      => 'The organizing commission of the CIAB has negotiated special rates with the Transamérica São Paulo hotel.',
                       'classe'     => ''),
                
                array( 'imagem'     => 'imprensa.png',
                       'titulo'     => 'PRESS ROOM',
                       'subtitulo'  => 'Exclusive service for journalists',
                       'destino'    => 'imprensa/assessoria',
                       'texto'      => 'To receive press releases, participate in press conferences and interviews, click here and register or enter into contact with our press office.',
                       'classe'     => ''),
           
                array( 'imagem'     => 'palestrante.png',
                       'titulo'     => 'PANELISTS',
                       'subtitulo'  => 'The best speakers will be at CIAB',
                       'destino'    => 'palestrantes',
                       'texto'      => 'Check out the national and international speakers present in the panels of the 2012 edition of the CIAB FEBRABAN.',
                       'classe'     => ''),
                
                array( 'imagem'     => 'premio.png',
                       'titulo'     => 'CIAB AWARD',
                       'subtitulo'  => 'Solutions for a Connected Society',
                       'destino'    => 'premios/ciab',
                       'texto'      => 'The CIAB 2012 Award brings something new – an award for prototypes for mobility with the theme ‘Solutions for a Connected Society’. Check it out.',
                       'classe'     => ''),
                
                array( 'imagem'     => 'publicacoes.png',
                       'titulo'     => 'PUBLICATIONS',
                       'subtitulo'  => 'You can find the best interviews here.',
                       'destino'    => 'publicacoes',
                       'texto'      => 'Keep up to date on the numbers and the thoughts of the best specialists in the IT, social networks, telecommunications and finance areas.',
                       'classe'     => ''),
           
                array( 'imagem'     => 'outros-anos.png',
                       'titulo'     => 'OTHER EDITIONS',
                       'subtitulo'  => 'Don’t miss out on any of the issues.',
                       'destino'    => 'evento/outrosanos',
                       'texto'      => 'If you miss any of the editions of the CIAB Magazine, access the previous editions and check out the entire background.',
                       'classe'     => '')          
            );        
            break;
        case 'es_':
            $links = array(
                array( 'imagem'     => 'o-ciab.png',
                       'titulo'     => 'CIAB FEBRABAN',
                       'subtitulo'  => '¡Conéctese y Sepa todo sobre Ciab!',
                       'destino'    => 'evento/apresentacao',
                       'texto'      => 'Participe del mayor evento de América Latina tanto para el sector financiero como para el área de TI, CIAB FEBRABAN. ',
                       'classe'     => ''),
           
                array( 'imagem'     => 'programacao.png',
                       'titulo'     => 'PROGRAMACIÓN',
                       'subtitulo'  => 'Los temas más actuales del sector.',
                       'destino'    => 'programacao/primeiroDia',
                       'texto'      => 'Más de 20 conferencias, debates y acerca de los temas más actuales del sector de tecnología. Vea la programación.',
                       'classe'     => ''),
                
                array( 'imagem'     => 'noticias.png',
                       'titulo'     => 'NOTICIAS',
                       'subtitulo'  => 'Vea las novedades de CIAB.',
                       'destino'    => 'noticias',
                       'texto'      => 'Aquí usted queda conectado a las novedades y a los acontecimientos de CIAB.',
                       'classe'     => ''),
                
                array( 'imagem'     => 'quero-visitar.png',
                       'titulo'     => 'QUIERO VISITAR',
                       'subtitulo'  => 'Visite la exposición de CIAB.',
                       'destino'    => 'evento/querovisitar',
                       'texto'      => 'Venga al Transamérica Expo Center y entérese de las soluciones innovadoras que los expositores de CIAB traen hasta usted.',
                       'classe'     => ''),
                
                array( 'imagem'     => 'quero-expor.png',
                       'titulo'     => 'QUIERO SER EXPOSITOR',
                       'subtitulo'  => 'Quiere ser un expositor en CIAB FEBRABAN?',
                       'destino'    => 'evento/queroexpor',
                       'texto'      => 'Sea un expositor y conéctese a más de 20.000 personas que frecuentan los tres días de CIAB.',
                       'classe'     => ''),
           
                array( 'imagem'     => 'espacotematico.png',
                       'titulo'     => 'ESPACIOS TEMÁTICOS',
                       'subtitulo'  => 'Conexión para pequeñas empresas.',
                       'destino'    => 'evento/espacos',
                       'texto'      => 'CIAB refuerza sus acciones orientadas a las pequeñas empresas, buscando estimular la presencia de ese segmento en el congreso.',
                       'classe'     => ''),
                
                array( 'imagem'     => 'mapa.png',
                       'titulo'     => 'PLANTA DE LA EXPOSICIÓN',
                       'subtitulo'  => 'Vea las empresas expositoras.',
                       'destino'    => 'evento/planta',
                       'texto'      => 'Vea quiénes son las empresas expositoras de las áreas de finanzas, tecnología de la información (TI) y telecomunicaciones de CIAB.',
                       'classe'     => ''),
                
                array( 'imagem'     => 'hospedagem.png',
                       'titulo'     => 'HOSPEDAJE',
                       'subtitulo'  => 'Vea los lugares de hospedaje.',
                       'destino'    => 'informacoes/hospedagem',
                       'texto'      => 'La comisión organizadora de CIAB negoció tarifas especiales con el hotel Transamérica São Paulo.',
                       'classe'     => ''),
                
                array( 'imagem'     => 'imprensa.png',
                       'titulo'     => 'SALA DE PRENSA',
                       'subtitulo'  => 'Atención exclusiva a los periodistas',
                       'destino'    => 'imprensa/assessoria',
                       'texto'      => 'Para recibir press releases, participar de las colectivas y entrevistas, clic aquí y haga su registro o sino contactase con nuestro servicio de asesoría.',
                       'classe'     => ''),
           
                array( 'imagem'     => 'palestrante.png',
                       'titulo'     => 'CONFERENCISTAS',
                       'subtitulo'  => 'Los mejores estarán en el CIAB',
                       'destino'    => 'palestrantes',
                       'texto'      => 'Vea los conferencias nacionales e internacionales presentes en los paneles de la edición 2012 del CIAB FEBRABAN.',
                       'classe'     => ''),
                
                array( 'imagem'     => 'premio.png',
                       'titulo'     => 'PREMIO CIAB',
                       'subtitulo'  => 'Soluciones para una Sociedad Conectada.',
                       'destino'    => 'premios/ciab',
                       'texto'      => 'El Premio CIAB 2012 viene con una novedad – premiación de prototipos para el movimiento con el tema Soluciones para una Sociedad Conectada. Vea.',
                       'classe'     => ''),
                
                array( 'imagem'     => 'publicacoes.png',
                       'titulo'     => 'PUBLICACIONES',
                       'subtitulo'  => 'Las mejores entrevistas puedes ver aquí.',
                       'destino'    => 'publicacoes',
                       'texto'      => 'Actualízate con los  números y de lo que piensan los mejores especialistas de las áreas de TI, redes sociales, telecomunicaciones y finanzas.',
                       'classe'     => ''),
           
                array( 'imagem'     => 'outros-anos.png',
                       'titulo'     => 'OTRAS EDICIONES',
                       'subtitulo'  => 'No puedes perder ningún asunto.',
                       'destino'    => 'evento/outrosanos',
                       'texto'      => 'Si dejaste de leer alguna edición de la Revista CIAB, basta ingresar a las ediciones anteriores y ver todas la secuencias del historial.',
                       'classe'     => '')          
            );        
            break;
        }
        
        $indices = array_rand($links, 3);
        foreach($indices as $val){
            if($links[$val]['destino'] != $this->input->post('origem'))
                $arr_retorno[] = $links[$val];
            else{
                if(isset($links[$val + 1]))
                    $arr_retorno[] = $links[$val + 1];
                else
                    $arr_retorno[] = $links[$val - 1];
            }
        }
        
        echo json_encode($arr_retorno);
    }

    function pegaPalestrantes(){
        $query = $this->db->order_by('id', 'random')->get('palestrantes')->result();
        echo json_encode($query);
    }
}

?>