<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    
class Busca extends MY_Frontcontroller{
 
    var $pag_options; 
    
    function __construct(){
        parent::__construct();
        
        $this->pag_options = array(
            'per_page' => '10',
            'first_link' => FALSE,
            'last_link' => FALSE,
            'next_tag_open' => "<div class='seta' id='nav-direita'>",
            'next_tag_close' => "</div>",
            'next_link' => '',
            'prev_tag_open' => "<div class='seta' id='nav-esquerda'>",
            'prev_tag_close' => "</div>",
            'prev_link' => '',
            'display_pages' => TRUE,
            'current_tag_open' => "<div class='pagina-ativa'>",
            'current_tag_close' => "</div>",
            'num_links' => 15
        );          
    }
    
    function index($pag = 0){
        if(isset($_POST['busca'])){
            $termo = $this->input->post('busca');
            $this->session->set_flashdata('termo-busca', $termo);
        }else{
            $termo = $this->session->flashdata('termo-busca');
            $this->session->set_flashdata('termo-busca', $termo);
        }
        
        if($termo == ''){
            redirect('home');
        }
        
        
        $query = "SELECT id, titulo, subtitulo, olho, data, 'noticias' as source, '' as arquivo, imagem FROM site_noticias WHERE MATCH(titulo, subtitulo, olho, texto)AGAINST('$termo*')
        UNION SELECT id, titulo, '' as subtitulo, descritivo as 'olho', data, 'clipping' as source, arquivo, '' as imagem FROM site_clippings WHERE MATCH(titulo, descritivo) AGAINST('$termo*')
        UNION SELECT id, titulo, '' as subtitulo, descritivo as 'olho', data, 'clipping' as source, arquivo, '' as imagem FROM site_releases WHERE MATCH(titulo, descritivo) AGAINST('$termo*')";
        
        $num_results = $this->db->query($query)->num_rows();
        
        if($num_results == 0){
            $query = "SELECT id, titulo, subtitulo, olho, data, 'noticias' as source, '' as arquivo, imagem FROM site_noticias WHERE titulo like '%$termo%' OR subtitulo like '%$termo%' OR olho like '%$termo%' OR texto like '%$termo%'UNION
            SELECT id, titulo, '' as subtitulo, descritivo as 'olho', data, 'clipping' as source, arquivo, '' as imagem FROM site_clippings WHERE titulo like '%$termo%' OR descritivo like '%$termo%'UNION
            SELECT id, titulo, '' as subtitulo, descritivo as 'olho', data, 'clipping' as source, arquivo, '' as imagem FROM site_releases WHERE titulo like '%$termo%' OR descritivo like'%$termo%'";
            $num_results = $this->db->query($query)->num_rows();
        }
        
        $query_limit = $query . ' LIMIT '. $pag . ', ' . $this->pag_options['per_page'];
        
        $this->pag_options['base_url'] = base_url("busca");
        $this->pag_options['total_rows'] = $num_results;
        $this->pag_options['uri_segment'] = 2;
        $this->load->library('pagination', $this->pag_options);
        
        $data['resultados'] = $this->db->query($query_limit);
        $data['pagination'] = $this->pagination->create_links();
        
        $menu['submenu'] = FALSE;
        $menu['titulo'] = 'BUSCA';
        $data['termo'] = $termo;
        $data['num_resultados'] = $this->pag_options['total_rows'];

        $this->load->view('common/menu-lateral', $menu);
        $this->load->view('busca', $data);
        $this->load->view('common/veja-mais');
    }
    
}
?>