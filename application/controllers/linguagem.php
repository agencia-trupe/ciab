<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Linguagem extends CI_controller {
    
    function __construct() {
        parent::__construct();
    }

    function index($ling = 'pt') {        
        if($ling == 'pt' || $ling == 'es' || $ling == 'en'){
            $this->session->set_userdata('language', $ling);
            $this->session->set_userdata('prefixo', $ling.'_');

            if($this->session->userdata('redirect')){
                redirect($this->session->userdata('redirect'));
            }else{
                redirect('home');    
            }
        }else{
            redirect('home');
        }
    }
    
    public function jstraduz(){
        echo traduz($this->input->post('string'), TRUE);
    }
    
}

?>