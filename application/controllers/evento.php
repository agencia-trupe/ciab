<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Evento extends MY_Frontcontroller {

    function __construct() {
        parent::__construct();
    }
    
    function index(){
        redirect('evento/apresentacao');
    }
    
    function apresentacao(){
        
        $this->headeropt['fb_title'] = 'Ciab 2013 - Apresentação';
        $this->headeropt['fb_description'] = 'O CIAB FEBRABAN - Congresso e Exposição de Tecnologia da Informação das
                                                Instituições Financeiras - é o maior evento da América Latina tanto
                                                para o setor financeiro quanto para a área de Tecnologia.';
        
        $this->load->view('evento/submenu-evento');
        $this->load->view('common/menu-lateral');
        $this->load->view('evento/apresentacao');
        $this->load->view('common/veja-mais');
    }
    
    function congresso(){
        
        $this->headeropt['fb_title'] = 'Ciab 2013 - O Congresso';
        $this->headeropt['fb_description'] = 'Realizado paralelamente à Exposição, o Congresso CIAB FEBRABAN será realizado
                                                em 3 auditórios que contarão com especialistas nacionais e internacionais de
                                                tecnologia e que debaterão sobre o tema principal do Congresso - “A Sociedade
                                                Conectada”.';
                                                
        $this->load->view('evento/submenu-evento');
        $this->load->view('common/menu-lateral');
        $this->load->view('evento/congresso');
        $this->load->view('common/veja-mais');
    }
    
    function espacos(){
        
        $this->headeropt['fb_title'] = 'Ciab 2013 - Espaços Temáticos';
        $this->headeropt['fb_description'] = 'Ciab FEBRABAN reforça, novamente, suas ações voltadas para as pequenas empresas,
                                                visando estimular a presença desse segmento no congresso.';
                                                
        $this->load->view('evento/submenu-evento');
        $this->load->view('common/menu-lateral');
        $this->load->view('evento/espacos');
        $this->load->view('common/veja-mais');
    }
    
    function exposicao(){
        
        $this->headeropt['fb_title'] = 'Ciab 2013 - Exposição';
        $this->headeropt['fb_description'] = 'Simultaneamente ao congresso, o CIAB FEBRABAN realiza sua exposição que conta
        com a participação de centenas de empresas expositoras das áreas de finanças, tecnologia da informação (TI) e
        telecomunicações.';
                                                
        $this->load->view('evento/submenu-evento');
        $this->load->view('common/menu-lateral');
        $this->load->view('evento/exposicao');
        $this->load->view('common/veja-mais');
    }
    
    function querovisitar(){
        
        $this->headeropt['fb_title'] = 'Ciab 2013 - Quero Visitar';
        $this->headeropt['fb_description'] = 'Você pode preencher o formulário abaixo e fazer agora mesmo seu cadastro.
        Nossos assessores entrarão em contato caso haja necessidade.';           

        $data['envio'] = $this->session->flashdata('envio');
        $data['status'] = $this->session->flashdata('status');
        
        $this->load->view('evento/submenu-evento');
        $this->load->view('common/menu-lateral');
        $this->load->view('evento/querovisitar', $data);
        $this->load->view('common/veja-mais');
    }
    
    function queroexpor(){
        
        $this->headeropt['fb_title'] = 'Ciab 2013 - Quero Expor';
        $this->headeropt['fb_description'] = 'Centenas de empresas das áreas de finanças, tecnologia da informação (TI) e
        telecomunicações, anualmente expõem no CIAB FEBRABAN e têm a oportunidade de estreitar seu relacionamento com
        milhares de pessoas que transitam passam pelo evento.';        

        $data['envio'] = $this->session->flashdata('envio');
        $data['status'] = $this->session->flashdata('status');
        
        $this->load->view('evento/submenu-evento');
        $this->load->view('common/menu-lateral');
        $this->load->view('evento/queroexpor', $data);
        $this->load->view('common/veja-mais');
    }
    
    function queropatrocinar(){
        
        $this->headeropt['fb_title'] = 'Ciab 2013 - Quero Patrocinar';
        $this->headeropt['fb_description'] = 'A sua empresa pode ser uma das patrocinadoras do maior congresso e exposição da
        América Latina em tecnologia da informação para instituições financeiras, e expor a sua marca para milhares de
        congressistas e visitantes que frequentam os 3 dias do CIAB FEBRABAN.';
        
        $this->load->view('evento/submenu-evento');
        $this->load->view('common/menu-lateral');
        $this->load->view('evento/queropatrocinar');
        $this->load->view('common/veja-mais');
    }
    
    function planta(){
        $data['expositores'] = $this->db->order_by('empresa')->get_where('stands', array('ano' => "2013"))->result();
        $data['num_expositores'] = sizeof($data['expositores']);
        $this->load->view('evento/mapa', $data);
        $this->load->view('common/veja-mais');
    }
    
    function outrosanos(){

        $this->headeropt['fb_title'] = 'Ciab 2013 - Eventos Anteriores';
        $this->headeropt['fb_description'] = 'Veja o que aconteceu nas edições anteriores do evento.';        
        
        $this->load->view('evento/submenu-evento');
        $this->load->view('common/menu-lateral');
        $this->load->view('evento/outrosanos');
        $this->load->view('common/veja-mais');
    }

    function enviar(){
        $emailconf['charset'] = 'utf-8';
        $emailconf['mailtype'] = 'html';

        $this->load->library('email');

        $this->email->initialize($emailconf);
        
        $origem = $this->input->post('origem');
        switch($origem){
            case 'querovisitar':
                $from = 'noreply@febraban.com.br';
                $fromname = 'Site Ciab';
                $to = 'fernanda.castillo@febraban.org.br';
                $bcc = 'bruno@trupe.net';
                $assunto = 'Ciab - Quero Visitar';       
                $u_nome = $this->input->post('nome');
                $u_email = $this->input->post('email');
                $u_empresa = $this->input->post('empresa');
                $u_empresa_site = $this->input->post('empresa-site');
                $u_area_atuacao = $this->input->post('area-atuacao');
                $u_telefone = $this->input->post('telefone');
                
                $email = "<html>
                            <head>
                                <style type='text/css'>
                                    .tit{
                                        font-weight:bold;
                                        font-size:16px;
                                        color:#00B69C;
                                        font-family:Verdana;
                                    }
                                    .val{
                                        color:#000;
                                        font-size:14px;
                                        font-family:Verdana;
                                    }
                                </style>
                            </head>
                            <body>
                            <span class='tit'>Nome :</span> <span class='val'>$u_nome</span><br>
                            <span class='tit'>E-mail :</span> <span class='val'>$u_email</span><br>
                            <span class='tit'>Empresa :</span> <span class='val'>$u_empresa</span><br>
                            <span class='tit'>Site da Empresa :</span> <span class='val'><a href='$u_empresa_site'>$u_empresa_site</a></span><br>
                            <span class='tit'>Área de Atuação :</span> <span class='val'>$u_area_atuacao</span><br>
                            <span class='tit'>Telefone :</span> <span class='val'>$u_telefone</span>
                            </body>
                            </html>";
                break;
            case 'queroexpor':
                $from = 'noreply@febraban.com.br';
                $fromname = 'Site Ciab';
                $to = 'fernanda.castillo@febraban.org.br';
                $bcc = 'bruno@trupe.net';
                $assunto = 'Ciab - Quero Expor';
                $u_assunto = $this->input->post('assunto');
                $u_nome = $this->input->post('nome');
                $u_email = $this->input->post('email');
                $u_empresa = $this->input->post('empresa');
                $u_empresa_site = $this->input->post('empresa-site');
                $u_area_atuacao = $this->input->post('area-atuacao');
                $u_telefone = $this->input->post('telefone');
                $u_interesse = $this->input->post('espaco');
                $email = "<html>
                            <head>
                                <style type='text/css'>
                                    .tit{
                                        font-weight:bold;
                                        font-size:16px;
                                        color:#00B69C;
                                        font-family:Verdana;
                                    }
                                    .val{
                                        color:#000;
                                        font-size:14px;
                                        font-family:Verdana;
                                    }
                                </style>
                            </head>
                            <body>
                            <span class='tit'>Assunto :</span> <span class='val'>$u_assunto</span><br />
                            <span class='tit'>Nome :</span> <span class='val'>$u_nome</span><br />
                            <span class='tit'>E-mail :</span> <span class='val'>$u_email</span><br />
                            <span class='tit'>Empresa :</span> <span class='val'>$u_empresa</span><br />
                            <span class='tit'>Site da Empresa :</span> <span class='val'><a href='$u_empresa_site'>$u_empresa_site</a></span><br />
                            <span class='tit'>Área de Atuação :</span> <span class='val'>$u_area_atuacao</span>
                            <span class='tit'>Telefone :</span> <span class='val'>$u_telefone</span>
                            <span class='tit'>Espaço/Metragem de interesse :</span> <span class='val'>$u_interesse</span>
                            </body>
                            </html>";                
                break;
        }


        $this->email->from($from, $fromname);
        $this->email->to($to);
        if($bcc)
            $this->email->bcc($bcc);
        $this->email->reply_to($u_email);

        $this->email->subject($assunto);
        $this->email->message($email);

        if(!$this->email->send())
            $this->session->set_flashdata('status', FALSE);
        else
            $this->session->set_flashdata('status', TRUE);

        $this->session->set_flashdata('envio', TRUE);
        redirect('evento/'.$origem);
    }    
}
?>
