<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Imprensa extends MY_Frontcontroller {

    function __construct() {
        parent::__construct();
    }
    
    function index(){
        redirect('imprensa/assessoria');
    }
    
    function assessoria(){
        
        $data['envio'] = $this->session->flashdata('envio');
        $data['status'] = $this->session->flashdata('status');        
        
        $this->load->view('imprensa/submenu-imprensa');
        $this->load->view('common/menu-lateral');
        $this->load->view('imprensa/assessoria', $data);
        $this->load->view('common/veja-mais');
    }
    
    function releases($pag = 0){
        
        $pag_options = array(
            'per_page' => 7,
            'first_link' => FALSE,
            'last_link' => FALSE,
            'next_tag_open' => "<div class='seta' id='nav-direita'>",
            'next_tag_close' => "</div>",
            'next_link' => '',
            'prev_tag_open' => "<div class='seta' id='nav-esquerda'>",
            'prev_tag_close' => "</div>",
            'prev_link' => '',
            'display_pages' => TRUE,
            'current_tag_open' => "<div class='pagina-ativa'>",
            'current_tag_close' => "</div>",
            'num_links' => 15,
            'base_url' => base_url('imprensa/releases'),
            'total_rows' => $this->db->count_all_results('releases'),
            'uri_segment' => 3
        );
        $this->load->library('pagination', $pag_options);
        
        $data['clippings'] = $this->db->order_by('data', 'desc')->order_by('id', 'DESC')->get('releases', $pag_options['per_page'], $pag)->result();
        $data['pagination'] = $this->pagination->create_links();
        
        $this->load->view('imprensa/submenu-imprensa');
        $this->load->view('common/menu-lateral');
        $this->load->view('imprensa/release', $data);
        $this->load->view('common/veja-mais');        
    }    
    
    function clipping($pag = 0){
        
        $pag_options = array(
            'per_page' => 10,
            'first_link' => FALSE,
            'last_link' => FALSE,
            'next_tag_open' => "<div class='seta' id='nav-direita'>",
            'next_tag_close' => "</div>",
            'next_link' => '',
            'prev_tag_open' => "<div class='seta' id='nav-esquerda'>",
            'prev_tag_close' => "</div>",
            'prev_link' => '',
            'display_pages' => TRUE,
            'current_tag_open' => "<div class='pagina-ativa'>",
            'current_tag_close' => "</div>",
            'num_links' => 15,
            'base_url' => base_url('imprensa/clipping'),
            'total_rows' => $this->db->count_all_results('clippings'),
            'uri_segment' => 3
        );
        $this->load->library('pagination', $pag_options);
        
        $data['clippings'] = $this->db->order_by('data', 'desc')->order_by('id', 'DESC')->get('clippings', $pag_options['per_page'], $pag)->result();
        $data['pagination'] = $this->pagination->create_links();
        
        $this->load->view('imprensa/submenu-imprensa');
        $this->load->view('common/menu-lateral');
        $this->load->view('imprensa/clipping', $data);
        $this->load->view('common/veja-mais');        
    }

    function enviar(){
        $emailconf['charset'] = 'utf-8';
        $emailconf['mailtype'] = 'html';

        $this->load->library('email');

        $this->email->initialize($emailconf);
        
        $from = 'noreply@febraban.com.br';
        $fromname = 'Site Ciab';
        $to = 'credenciamentociab@abce.com.br';
        $bcc = 'bruno@trupe.net';
        $assunto = 'Ciab - Press Release';
        
        $u_nome = $this->input->post('nome');
        $u_email = $this->input->post('email');
        $u_mtb = $this->input->post('mtb');
        $u_empresa = $this->input->post('empresa');
        $u_empresa_site = $this->input->post('empresa-site');
        $u_area_atuacao = $this->input->post('area-atuacao');
        $u_telefone = $this->input->post('telefone');
        
        $email = "<html>
                    <head>
                        <style type='text/css'>
                            .tit{
                                font-weight:bold;
                                font-size:16px;
                                color:#00B69C;
                                font-family:Verdana;
                            }
                            .val{
                                color:#000;
                                font-size:14px;
                                font-family:Verdana;
                            }
                        </style>
                    </head>
                    <body>
                    <span class='tit'>Nome :</span> <span class='val'>$u_nome</span><br>
                    <span class='tit'>E-mail :</span> <span class='val'>$u_email</span><br>
                    <span class='tit'>MTB :</span> <span class='val'>$u_mtb</span><br>
                    <span class='tit'>Empresa :</span> <span class='val'>$u_empresa</span><br>
                    <span class='tit'>Site da Empresa :</span> <span class='val'><a href='$u_empresa_site'>$u_empresa_site</a></span><br>
                    <span class='tit'>Área de Atuação :</span> <span class='val'>$u_area_atuacao</span><br>
                    <span class='tit'>Telefone :</span> <span class='val'>$u_telefone</span>
                    </body>
                    </html>";

        $this->email->from($from, $fromname);
        $this->email->to($to);
        if($bcc)
            $this->email->bcc($bcc);
        $this->email->reply_to($u_email);

        $this->email->subject($assunto);
        $this->email->message($email);

        if(!$this->email->send())
            $this->session->set_flashdata('status', FALSE);
        else
            $this->session->set_flashdata('status', TRUE);

        $this->session->set_flashdata('envio', TRUE);
        redirect('imprensa/assessoria');
    }        
}
?>
