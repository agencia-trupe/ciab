<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Noticias extends MY_Admin {

    var $titulo = 'Notícias';
    
    var $uploadconfig = array(
        'upload_path' => './_imgs/noticias',
        'allowed_types' => 'gif|jpg|png',
        'max_size' => '0',
        'max_width' => '0',
        'max_height' => '0');
    
    function  __construct() {
        parent::__construct();
        
        $this->load->library('upload', $this->uploadconfig);
    }

    function index() {
        $data['titulo'] = $this->titulo;
        $data['registros'] = $this->db->order_by('data', 'DESC')->order_by('id', 'desc')->get('noticias')->result();
        
        foreach($data['registros'] as $reg => $val){
            $query = $this->db->get_where('noticias_categorias', array('id' => $val->id_noticias_categorias))->result();
            if(isset($query[0]))
                $val->categoria = $query[0]->titulo;
            else
                $val->categoria = 'Categoria Não Encontrada';                
            $val->data = formataData($val->data, 'mysql2br');
        }

        $this->load->view('painel/common/header');
        $this->load->view('painel/common/menu');
        $this->load->view('painel/noticias/lista', $data);
        $this->load->view('painel/common/footer');
    }

    function form($id = FALSE){
        if($id){
            $data['titulo'] = 'Editar '. $this->titulo;
            $data['registro'] = $this->db->get_where('noticias', array('id' => $id))->result();
        }else{
            $data['titulo'] = 'Inserir '. $this->titulo;
        }
        
        $data['categorias'] = $this->db->get('noticias_categorias')->result();

        $this->load->view('painel/common/header');
        $this->load->view('painel/common/menu');
        $this->load->view('painel/noticias/form', $data);
        $this->load->view('painel/common/footer');
    }

    function inserir(){
        $this->db->set('titulo', $this->input->post('titulo'));
        $this->db->set('subtitulo', $this->input->post('subtitulo'));
        $this->db->set('data', formataData($this->input->post('data'), 'br2mysql'));
        $this->db->set('olho', $this->input->post('olho'));
        $this->db->set('texto', $this->input->post('texto'));
        $this->db->set('id_noticias_categorias', $this->input->post('categoria'));
        $this->db->set('timestamp', date('Y-m-d H:i:s'));
        $this->db->set('ultima_alteracao', date('Y-m-d H:i:s'));
        $this->db->set('imagem', $this->sobeImagem());
        $this->db->set('id_usuarios', $this->session->userdata('id'));
        $this->db->set('id_usuarios_alteracao', $this->session->userdata('id'));
        $this->db->insert('noticias');
        redirect('painel/noticias');
    }

    function editar($id){
        $this->db->set('titulo', $this->input->post('titulo'));
        $this->db->set('subtitulo', $this->input->post('subtitulo'));
        $this->db->set('data', formataData($this->input->post('data'), 'br2mysql'));
        $this->db->set('olho', $this->input->post('olho'));
        $this->db->set('texto', $this->input->post('texto'));
        $this->db->set('id_noticias_categorias', $this->input->post('categoria'));
        $this->db->set('ultima_alteracao', date('Y-m-d H:i:s'));
        
        $imagem = $this->sobeImagem();
        if(!empty($imagem)){
            @unlink('_imgs/noticias/thumbs/'.$this->input->post('imagem-atual'));
            @unlink('_imgs/noticias/'.$this->input->post('imagem-atual'));
            $this->db->set('imagem', $imagem);
        }
        if($this->input->post('excluir-imagem')){
            $this->db->set('imagem', '');
            @unlink('_imgs/noticias/thumbs/'.$this->input->post('imagem-atual'));
            @unlink('_imgs/noticias/'.$this->input->post('imagem-atual'));
        }
        
        $this->db->set('id_usuarios_alteracao', $this->session->userdata('id'));
        $this->db->where('id', $id);
        $this->db->update('noticias');
        redirect('painel/noticias');
    }

    function excluir($id){
        $query = $this->db->get_where('noticias', array('id' => $id))->result();
        @unlink('_imgs/noticias/thumbs/'.$query[0]->imagem);
        @unlink('_imgs/noticias/'.$query[0]->imagem);
        $this->db->delete('noticias', array('id' => $id));
        redirect('painel/noticias');
    }
    
    function categorias(){
        $data['titulo'] = 'Categorias';
        $data['registros'] = $this->db->order_by('titulo', 'ASC')->get('noticias_categorias')->result();

        $this->load->view('painel/common/header');
        $this->load->view('painel/common/menu');
        $this->load->view('painel/noticias/categorias/lista', $data);
        $this->load->view('painel/common/footer');        
    }
    
    function categoriasForm($id = FALSE){
        if($id){
            $data['titulo'] = 'Editar Categoria';
            $data['registro'] = $this->db->get_where('noticias_categorias', array('id' => $id))->result();
        }else{
            $data['titulo'] = 'Inserir Categoria';
        }

        $this->load->view('painel/common/header');
        $this->load->view('painel/common/menu');
        $this->load->view('painel/noticias/categorias/form', $data);
        $this->load->view('painel/common/footer');        
    }
    
    function categoriasInserir(){
        $this->db->set('titulo', $this->input->post('titulo'));
        $this->db->set('slug', strtolower(url_title($this->input->post('titulo'))));
        $this->db->insert('noticias_categorias');
        redirect('painel/noticias/categorias');
    }
    
    function categoriasEditar($id = FALSE){
        if(!$id)
            redirect('painel/noticias/categorias');
            
        $this->db->set('titulo', $this->input->post('titulo'));
        $this->db->set('slug', strtolower(url_title($this->input->post('titulo'))));
        $this->db->where('id', $id);
        $this->db->update('noticias_categorias');
        redirect('painel/noticias/categorias');
    }
    
    function categoriasExcluir($id){
        $this->db->delete('noticias_categorias', array('id' => $id));
        redirect('painel/noticias/categorias');
    }

    private function sobeImagem(){
        if($_FILES['userfile']['error'] != 4){
            if(!$this->upload->do_upload('userfile')){
                echo $this->upload->display_errors();
                return false;
            }else{
                $arquivo = $this->upload->data();
                $filename = url_title($arquivo['file_name']);
                rename('_imgs/noticias/'.$arquivo['file_name'] ,'_imgs/noticias/'.$filename);
                $this->image_moo
                        ->load('_imgs/noticias/'.$filename)
                        ->resize_crop(210,110)
                        ->save('_imgs/noticias/thumbs/'.$filename, TRUE);
                return $filename;
            }
        }else{
            $filename = '';
        }
    }
}
?>
