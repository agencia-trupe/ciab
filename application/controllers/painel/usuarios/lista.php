<div id="content" class="container_16">

    <div class="grid_16">
        <h2>
            <?= $titulo ?>
        </h2>
    </div>

    <div class="clearfix"></div>

    <div style="text-align:center;">
        <a href="<?= base_url() ?>painel/usuarios/form" class="add">Adicionar usuário <img src="<?= base_url() ?>_imgs/painel/add-icon.png"></a><br/><br/>
    </div>
    <table>
    <thead>
        <tr>
            <th>Usuario</th>
            <th>Email</th>
            <th></th>
            <th></th>
        </tr>
    </thead>

    <?if(isset($paginacao)):?>
    <tfoot>
        <tr>
            <td colspan="4" class="pagination">
<!--                <span class="active curved">1</span><a href="#" class="curved">2</a><a href="#" class="curved">3</a><a href="#" class="curved">4</a> ... <a href="#" class="curved">10 million</a>-->
                <?=$paginacao?>
            </td>
        </tr>
    </tfoot>
    <?endif;?>

    <tbody>
<?php
if(!empty($registros)){
    foreach($registros as $value){
        echo '<tr>';
        echo "<td>".$value->username."</td>";
        echo "<td>".$value->email."</td>";
        echo "<td><a class='edit' href='".base_url()."painel/usuarios/form/".$value->id."'>editar</a></td>";
        if($value->id != $this->session->userdata('id')){
            echo "<td><a class='delete' href='".base_url()."painel/usuarios/excluir/".$value->id."'>excluir</a></td>";
        }else{
            echo "<td></td>";
        }
        echo "</tr>";
    }
}else{
    echo "<tr><td colspan='4'>Nenhum usu�rio cadastrado</tr>";
}
?>
    </tbody>
    </table>


</div>