<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    
class Ajax extends CI_Controller{
    
    var $linguagem;    
    
    function __construct(){
        parent::__construct();
        
        $this->linguagem = FALSE;
        
        if(!$this->input->is_ajax_request())
            redirect('home/painel');
    }
    
    function index(){redirect('home/painel');}
    
    function salvaTema(){
      switch($this->input->post('language')){
        case 'es':
          $tabela = 'es_palestrantes_temas';
          break;
        default:
        case 'pt':
          $tabela = 'palestrantes_temas';
          break;
        case 'en':
          $tabela = 'en_palestrantes_temas';
          break;
      }
      $this->db->set('id_palestrantes', $this->input->post('id_palestrantes'));
      $this->db->set('titulo', $this->input->post('titulo'));
      $this->db->set('data', $this->input->post('data'));
      $this->db->set('texto', $this->input->post('texto'));
      if($this->db->insert($tabela))
        echo "<div class='sucesso mensagem-tema'>Tema inserido</div>";
      else
        echo "<div class='falha mensagem-tema'>Erro ao inserir</div>";
    }

    function listaTemas(){
      switch($this->input->post('language')){
        case 'es':
          $tabela = 'es_palestrantes_temas';
          break;
        default:
        case 'pt':
          $tabela = 'palestrantes_temas';
          break;
        case 'en':
          $tabela = 'en_palestrantes_temas';
          break;
      }      
      $id = $this->input->post('id_palestrantes');
      $temas = $this->db->order_by('data', 'ASC')->get_where($tabela, array('id_palestrantes' => $id))->result();
      echo json_encode($temas);
    }
    
    function excluiTema(){
        switch($this->input->post('language')){
          case 'es':
            $tabela = 'es_palestrantes_temas';
            break;
          default:
          case 'pt':
            $tabela = 'palestrantes_temas';
            break;
          case 'en':
            $tabela = 'en_palestrantes_temas';
            break;
        }      
        $id_temas = $this->input->post('id_temas');
        $this->db->where('id', $id_temas)->delete($tabela);
    }
}

?>