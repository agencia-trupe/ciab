<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Palestrantes extends MY_Admin {

    var $titulo = 'Palestrantes - Português';
    
    var $uploadconfig = array(
        'upload_path' => '_imgs/palestrantes/',
        'allowed_types' => 'jpg|png|gif',
        'max_size' => '0',
        'max_width' => '0',
        'max_height' => '0');
    
    function  __construct() {
        parent::__construct();
        
        $this->load->library('upload', $this->uploadconfig);
        if($this->session->userdata('username') != 'trupe')
            redirect('painel/home');
    }

    function up(){
        atualizaImagens();
        echo 'ok';
    }

    function addSlug(){
        $qry = $this->db->get('palestrantes')->result();
        foreach ($qry as $key => $value) {
            $this->db->set('slug', url_title($value->nome, 'dash', TRUE))->where('id', $value->id)->update('palestrantes');
        }
        echo "pt ok<br>";
        $qry = $this->db->get('es_palestrantes')->result();
        foreach ($qry as $key => $value) {
            $this->db->set('slug', url_title($value->nome, 'dash', TRUE))->where('id', $value->id)->update('es_palestrantes');
        }
        echo "es ok<br>";
        $qry = $this->db->get('en_palestrantes')->result();
        foreach ($qry as $key => $value) {
            $this->db->set('slug', url_title($value->nome, 'dash', TRUE))->where('id', $value->id)->update('en_palestrantes');
        }
        echo "en ok<br>";
    }    

    function index() {
        $data['titulo'] = $this->titulo;
        $data['registros'] = $this->db->order_by('nome', 'ASC')->get('palestrantes')->result();
        $data['lang'] = 'pt';
        
        $this->load->view('painel/common/header');
        $this->load->view('painel/common/menu');
        $this->load->view('painel/palestrantes/lista', $data);
        $this->load->view('painel/common/footer');
    }

    function form($id = FALSE){
        if($id){
            $data['titulo'] = 'Editar '. $this->titulo;
            $data['registro'] = $this->db->get_where('palestrantes', array('id' => $id))->result();
        }else{
            $data['titulo'] = 'Inserir '. $this->titulo;
        }

        $data['lingua'] = 'pt';
        
        $this->load->view('painel/common/header');
        $this->load->view('painel/common/menu');
        $this->load->view('painel/palestrantes/form', $data);
        $this->load->view('painel/common/footer');
    }

    function inserir(){
        $this->db->set('nome', $this->input->post('nome'));
        $this->db->set('origem', $this->input->post('origem'));
        $this->db->set('texto', $this->input->post('texto'));
        $this->db->set('imagem_on', $this->sobeArquivo('on', FALSE));
        $this->db->set('imagem_off', $this->sobeArquivo('off', FALSE));
        $this->db->set('imagem_ampliada', $this->sobeArquivo('ampliada', TRUE));
        $this->db->set('slug', url_title($this->input->post('nome'), 'dash', TRUE));
        $this->db->insert('palestrantes');
        atualizaImagens();
        redirect('painel/palestrantes');
    }

    function editar($id){
        $this->db->set('nome', $this->input->post('nome'));
        $this->db->set('origem', $this->input->post('origem'));
        $this->db->set('texto', $this->input->post('texto'));
        $this->db->set('slug', url_title($this->input->post('nome'), 'dash', TRUE));
        $on = $this->sobeArquivo('on', FALSE);
        if(!empty($on)){
            @unlink('_imgs/palestrantes/'.$this->input->post('on-atual'));
            $this->db->set('imagem_on', $on);
        }
        
        $off = $this->sobeArquivo('off', FALSE);
        if(!empty($off)){
            @unlink('_imgs/palestrantes/'.$this->input->post('off-atual'));
            $this->db->set('imagem_off', $off);
        }
        
        $ampliada = $this->sobeArquivo('ampliada', TRUE);
        if(!empty($ampliada)){
            @unlink('_imgs/palestrantes/'.$this->input->post('ampliada-atual'));
            $this->db->set('imagem_ampliada', $ampliada);
        }
        
        $this->db->where('id', $id);
        $this->db->update('palestrantes');
        atualizaImagens();
        redirect('painel/palestrantes');
    }

    function excluir($id){
        $query = $this->db->get_where('palestrantes', array('id' => $id))->result();
        if(strpos($query[0]->imagem_on, 'placeholder') === FALSE)
            @unlink('_imgs/palestrantes/'.$query[0]->imagem_on);
        if(strpos($query[0]->imagem_off, 'placeholder') === FALSE)
            @unlink('_imgs/palestrantes/'.$query[0]->imagem_off);
        if(strpos($query[0]->imagem_ampliada, 'placeholder') === FALSE)
            @unlink('_imgs/palestrantes/'.$query[0]->imagem_ampliada);
        $this->db->delete('palestrantes', array('id' => $id));
        redirect('painel/palestrantes');
    }
    
    private function sobeArquivo($campo, $resize){
        if($_FILES[$campo]['error'] != 4){
            if(!$this->upload->do_upload($campo)){
                exit($this->upload->display_errors());
            }else{
                $arquivo = $this->upload->data();
                $filename = url_title($arquivo['file_name']);
                rename('_imgs/palestrantes/'.$arquivo['file_name'] ,'_imgs/palestrantes/'.$filename);
                if($resize){
                    $this->image_moo->load('_imgs/palestrantes/'.$filename)
                                    ->resize_crop(242, 177)
                                    ->save('_imgs/palestrantes/'.$filename, TRUE);
                }
                return $filename;
            }
        }else{
            //if($resize)
            //    return 'big_placeholder11.jpg';
            //else
            //    return 'placeholder21.gif';
            return '';
        }        
    }
}
?>
