<div id="content" class="container_16">

    <div class="grid_16">
        <h2>
            <?= $titulo ?>
        </h2>
    </div>

    <div class="clearfix"></div>
    
    <div style="text-align:center;">
        <a href="<?= base_url() ?>painel/noticias/categoriasForm" class="add"><img src="<?= base_url() ?>_imgs/painel/add-icon.png"> Adicionar categoria</a> <br><br>
    </div>
    
    <table>
        <thead>
            <tr>
                <th>Titulo</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
    
        <?if(isset($paginacao)):?>
            <tfoot>
                <tr>
                    <td colspan="4" class="pagination">
                        <?=$paginacao?>
                    </td>
                </tr>
            </tfoot>
        <?endif;?>
    
        <tbody>
            <?php
            if(!empty($registros)){
                foreach($registros as $value){
                    echo '<tr>';
                    echo "<td>".$value->titulo."</td>";
                    echo "<td><a class='edit' href='".base_url()."painel/noticias/categoriasForm/".$value->id."'>editar</a></td>";
                    echo "<td><a class='delete' href='".base_url()."painel/noticias/categoriasExcluir/".$value->id."'>excluir</a></td>";
                    echo "</tr>";
                }
            }else{
                echo "<tr><td colspan='4'>Nenhuma categoria cadastrada</tr>";
            }
            ?>
        </tbody>
    </table>


</div>