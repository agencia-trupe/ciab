<div id="content" class="container_16">

    <div class="grid_16">
        <h2>
            <?= $titulo ?>
        </h2>
    </div>

    <div class="clearfix"></div>
    
    <div style="text-align:center;">
        <a href="<?= base_url() ?>painel/noticias/form" class="add"><img src="<?= base_url() ?>_imgs/painel/add-icon.png"> Adicionar notícia</a> | <a href="<?=base_url('painel/noticias/categorias')?>">Gerenciar categorias</a> <br><br>
    </div>
    
    <table>
        <thead>
            <tr>
                <th>Titulo</th>
                <th>Categoria</th>
                <th>Data</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
    
        <?if(isset($paginacao)):?>
            <tfoot>
                <tr>
                    <td colspan="4" class="pagination">
                        <?=$paginacao?>
                    </td>
                </tr>
            </tfoot>
        <?endif;?>
    
        <tbody>
            <?php
            if(!empty($registros)){
                foreach($registros as $value){
                    echo '<tr>';
                    echo "<td>".$value->titulo."</td>";
                    echo "<td>".$value->categoria."</td>";
                    echo "<td>".$value->data."</td>";
                    echo "<td><a class='edit' href='".base_url()."painel/noticias/form/".$value->id."'>editar</a></td>";
                    echo "<td><a class='delete' href='".base_url()."painel/noticias/excluir/".$value->id."'>excluir</a></td>";
                    echo "</tr>";
                }
            }else{
                echo "<tr><td colspan='4'>Nenhuma noticia cadastrada</tr>";
            }
            ?>
        </tbody>
    </table>


</div>