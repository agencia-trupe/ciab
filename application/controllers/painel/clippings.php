<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Clippings extends MY_Admin {

    var $titulo = 'Clipping';
    
    var $uploadconfig = array(
        'upload_path' => '_pdfs/clippings/',
        'allowed_types' => 'pdf',
        'max_size' => '0',
        'max_width' => '0',
        'max_height' => '0');
    
    function  __construct() {
        parent::__construct();
        
        $this->load->library('upload', $this->uploadconfig);
    }

    function index() {
        $data['titulo'] = $this->titulo;
        $data['registros'] = $this->db->order_by('data', 'DESC')->order_by('id', 'DESC')->get('clippings')->result();
        
        foreach($data['registros'] as $reg => $val){
            $val->data = formataData($val->data, 'mysql2br');
        }

        $this->load->view('painel/common/header');
        $this->load->view('painel/common/menu');
        $this->load->view('painel/clippings/lista', $data);
        $this->load->view('painel/common/footer');
    }

    function form($id = FALSE){
        if($id){
            $data['titulo'] = 'Editar '. $this->titulo;
            $data['registro'] = $this->db->get_where('clippings', array('id' => $id))->result();
        }else{
            $data['titulo'] = 'Inserir '. $this->titulo;
        }
        
        $this->load->view('painel/common/header');
        $this->load->view('painel/common/menu');
        $this->load->view('painel/clippings/form', $data);
        $this->load->view('painel/common/footer');
    }

    function inserir(){
        $this->db->set('titulo', $this->input->post('titulo'));
        $this->db->set('data', formataData($this->input->post('data'), 'br2mysql'));
        $this->db->set('descritivo', $this->input->post('descritivo'));
        $this->db->set('arquivo', $this->sobeArquivo());
        $this->db->set('timestamp', date('Y-m-d H:i:s'));
        $this->db->set('ultima_alteracao', date('Y-m-d H:i:s'));
        $this->db->set('id_usuarios', $this->session->userdata('id'));
        $this->db->set('id_usuarios_alteracao', $this->session->userdata('id'));
        $this->db->insert('clippings');
        redirect('painel/clippings');
    }

    function editar($id){
        $this->db->set('titulo', $this->input->post('titulo'));
        $this->db->set('data', formataData($this->input->post('data'), 'br2mysql'));
        $this->db->set('descritivo', $this->input->post('descritivo'));
        $this->db->set('ultima_alteracao', date('Y-m-d H:i:s'));
        $this->db->set('id_usuarios_alteracao', $this->session->userdata('id'));
        
        $arquivo = $this->sobeArquivo();
        if(!empty($arquivo)){
            @unlink('_pdfs/clippings/'.$this->input->post('arquivo-atual'));
            $this->db->set('arquivo', $arquivo);
        }
        
        $this->db->where('id', $id);
        $this->db->update('clippings');
        redirect('painel/clippings');
    }

    function excluir($id){
        $query = $this->db->get_where('clippings', array('id' => $id))->result();
        @unlink('_pdfs/clippings/'.$query[0]->arquivo);
        $this->db->delete('clippings', array('id' => $id));
        redirect('painel/clippings');
    }
    
    private function sobeArquivo(){
        if($_FILES['userfile']['error'] != 4){
            if(!$this->upload->do_upload('userfile')){
                exit($this->upload->display_errors());
            }else{
                $arquivo = $this->upload->data();
                $filename = url_title($arquivo['file_name']);
                rename('_pdfs/clippings/'.$arquivo['file_name'] ,'_pdfs/clippings/'.$filename);
                return $filename;
            }
        }else{
            $filename = '';
        }        
    }
}
?>
