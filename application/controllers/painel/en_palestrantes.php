<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class En_palestrantes extends MY_Admin {

    var $titulo = 'Palestrantes - Inglês';
    
    var $uploadconfig = array(
        'upload_path' => '_imgs/palestrantes/',
        'allowed_types' => 'jpg|png|gif',
        'max_size' => '0',
        'max_width' => '0',
        'max_height' => '0');
    
    function  __construct() {
        parent::__construct();
        
        $this->load->library('upload', $this->uploadconfig);
        if($this->session->userdata('username') != 'trupe')
            redirect('painel/home');
    }

    function index() {
        $data['titulo'] = $this->titulo;
        $data['registros'] = $this->db->order_by('nome', 'ASC')->get('en_palestrantes')->result();
        $data['lang'] = 'en';
        
        $this->load->view('painel/common/header');
        $this->load->view('painel/common/menu');
        $this->load->view('painel/palestrantes/lista', $data);
        $this->load->view('painel/common/footer');
    }

    function form($id = FALSE){
        if($id){
            $data['titulo'] = 'Editar '. $this->titulo;
            $data['registro'] = $this->db->get_where('en_palestrantes', array('id' => $id))->result();
        }else{
            $data['titulo'] = 'Inserir '. $this->titulo;
        }

        $data['lingua'] = 'en';
        
        $this->load->view('painel/common/header');
        $this->load->view('painel/common/menu');
        $this->load->view('painel/palestrantes/form_semimagem', $data);
        $this->load->view('painel/common/footer');
    }

    function inserir(){
        $this->db->set('nome', $this->input->post('nome'));
        $this->db->set('origem', $this->input->post('origem'));
        $this->db->set('texto', $this->input->post('texto'));
        $this->db->set('slug', url_title($this->input->post('nome'), 'dash', TRUE));
        $this->db->set('imagem_on', '');
        $this->db->set('imagem_off', '');
        $this->db->set('imagem_ampliada', '');
        $this->db->insert('en_palestrantes');
        atualizaImagens();
        redirect('painel/en_palestrantes');
    }

    function editar($id){
        $this->db->set('nome', $this->input->post('nome'));
        $this->db->set('origem', $this->input->post('origem'));
        $this->db->set('texto', $this->input->post('texto'));
        $this->db->set('slug', url_title($this->input->post('nome'), 'dash', TRUE));
        $this->db->set('imagem_on', '');
        $this->db->set('imagem_off', '');
        $this->db->set('imagem_ampliada', '');
        $this->db->where('id', $id);
        $this->db->update('en_palestrantes');
        atualizaImagens();
        redirect('painel/en_palestrantes');
    }

    function excluir($id){
        $this->db->delete('en_palestrantes', array('id' => $id));
        redirect('painel/en_palestrantes');
    }

    private function sobeArquivo($campo, $resize){
        if($_FILES[$campo]['error'] != 4){
            if(!$this->upload->do_upload($campo)){
                exit($this->upload->display_errors());
            }else{
                $arquivo = $this->upload->data();
                $filename = url_title($arquivo['file_name']);
                rename('_imgs/palestrantes/'.$arquivo['file_name'] ,'_imgs/palestrantes/'.$filename);
                if($resize){
                    $this->image_moo->load('_imgs/palestrantes/'.$filename)
                                    ->resize_crop(242, 177)
                                    ->save('_imgs/palestrantes/'.$filename, TRUE);
                }
                return $filename;
            }
        }else{
            //if($resize)
            //    return 'big_placeholder11.jpg';
            //else
            //    return 'placeholder21.gif';
            return '';
        }        
    }    
    
}
?>
