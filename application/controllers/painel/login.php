<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
                <meta name="robots" content="noindex, nofollow" />
		<title>Painel de Controle | <?=CLIENTE?></title>
                <link rel="stylesheet" href="<?= base_url() ?>css/tpl/960.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="<?= base_url() ?>css/tpl/template.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="<?= base_url() ?>css/tpl/colour.css" type="text/css" media="screen" charset="utf-8" />
                <script type="text/javascript" src="<?= base_url() ?>js/jquery-1.6.4.min.js"></script>
                <script type="text/javascript" src="<?= base_url() ?>js/jquery-ui-1.8.12.custom.min.js"></script>
                <script type="text/javascript" src="<?= base_url() ?>js/jquery.ui.datepicker-pt-BR.js"></script>
                <script type="text/javascript" src="<?= base_url() ?>js/tinymce/tiny_mce.js"></script>
                <script type="text/javascript" src="<?= base_url() ?>js/back.js"></script>
	</head>
	<body>

            <div id="content" class="container_16 clearfix">
                <div class="grid_5">&nbsp;</div>
                <div class="grid_6 login-box">

                    <div class="grid_6">
                        <h2>
                            Painel de controle | <?=CLIENTE?>
                        </h2>
                    </div>

                    <? if($errlogin == true): ?>
                    <div class="grid_6">
                        <p class="error">Erro no login</p>
                    </div>
                    <? endif; ?>

                    <form name='form-login' action='<?= base_url() ?>painel/home/login/' method='post'>
                        <div class="grid_6">
                            <p>
                                <label>usuário</label>
                                <input type='text' name='nome' />
                            </p>
                        </div>
                        <div class="grid_6">
                            <p>
                                <label>senha</label>
                                <input type='password' name='senha' />
                            </p>
                        </div>

                        <div class="grid_6">
                            <input type='submit' value='Login' />
                        </div>

                    </form>
                </div>
                <div class="grid_5">&nbsp;</div>
            </div>
    </body>
</html>

