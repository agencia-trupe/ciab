<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_controller {

    var $linguagem;

    public function  __construct() {
        parent::__construct();
        
        $this->linguagem = FALSE;
    }

    function index() {
        if($this->session->userdata('logged_in')){
            $data['usuario'] = $this->session->userdata('usuario');

            $this->load->view('painel/common/header');
            $this->load->view('painel/common/menu');
            $this->load->view('painel/home', $data);
            $this->load->view('painel/common/footer');
        }else{
            $data['errlogin'] = ($this->session->flashdata('errlogin') == true) ? true : null;
            $this->load->view('painel/login', $data);
        }
    }

    function login(){
        if($this->simplelogin->login($this->input->post('nome'), $this->input->post('senha'))){
            redirect('/painel/');
        }else{
            $this->session->set_flashdata('errlogin', true);
            redirect('/painel/');
        }
    }

    function logout(){
        $this->simplelogin->logout();
        redirect('/painel/');
    }


}

?>
