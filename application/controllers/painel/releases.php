<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Releases extends MY_Admin {

    var $titulo = 'Release';
    
    var $uploadconfig = array(
        'upload_path' => '_pdfs/releases/',
        'allowed_types' => 'pdf',
        'max_size' => '0',
        'max_width' => '0',
        'max_height' => '0');
    
    function  __construct() {
        parent::__construct();
        
        $this->load->library('upload', $this->uploadconfig);
    }

    function index() {
        $data['titulo'] = $this->titulo;
        $data['registros'] = $this->db->order_by('data', 'DESC')->order_by('id', 'DESC')->get('releases')->result();
        
        foreach($data['registros'] as $reg => $val){
            $val->data = formataData($val->data, 'mysql2br');
        }

        $this->load->view('painel/common/header');
        $this->load->view('painel/common/menu');
        $this->load->view('painel/releases/lista', $data);
        $this->load->view('painel/common/footer');
    }

    function form($id = FALSE){
        if($id){
            $data['titulo'] = 'Editar '. $this->titulo;
            $data['registro'] = $this->db->get_where('releases', array('id' => $id))->result();
        }else{
            $data['titulo'] = 'Inserir '. $this->titulo;
        }
        
        $this->load->view('painel/common/header');
        $this->load->view('painel/common/menu');
        $this->load->view('painel/releases/form', $data);
        $this->load->view('painel/common/footer');
    }

    function inserir(){
        $this->db->set('titulo', $this->input->post('titulo'));
        $this->db->set('data', formataData($this->input->post('data'), 'br2mysql'));
        $this->db->set('descritivo', $this->input->post('descritivo'));
        $this->db->set('arquivo', $this->sobeArquivo());
        $this->db->set('timestamp', date('Y-m-d H:i:s'));
        $this->db->set('ultima_alteracao', date('Y-m-d H:i:s'));
        $this->db->set('id_usuarios', $this->session->userdata('id'));
        $this->db->set('id_usuarios_alteracao', $this->session->userdata('id'));
        $this->db->insert('releases');
        redirect('painel/releases');
    }

    function editar($id){
        $this->db->set('titulo', $this->input->post('titulo'));
        $this->db->set('data', formataData($this->input->post('data'), 'br2mysql'));
        $this->db->set('descritivo', $this->input->post('descritivo'));
        $this->db->set('ultima_alteracao', date('Y-m-d H:i:s'));
        $this->db->set('id_usuarios_alteracao', $this->session->userdata('id'));
        
        $arquivo = $this->sobeArquivo();
        if(!empty($arquivo)){
            @unlink('_pdfs/releases/'.$this->input->post('arquivo-atual'));
            $this->db->set('arquivo', $arquivo);
        }
        
        $this->db->where('id', $id);
        $this->db->update('releases');
        redirect('painel/releases');
    }

    function excluir($id){
        $query = $this->db->get_where('releases', array('id' => $id))->result();
        @unlink('_pdfs/releases/'.$query[0]->arquivo);
        $this->db->delete('releases', array('id' => $id));
        redirect('painel/releases');
    }
    
    private function sobeArquivo(){
        if($_FILES['userfile']['error'] != 4){
            if(!$this->upload->do_upload('userfile')){
                exit($this->upload->display_errors());
            }else{
                $arquivo = $this->upload->data();
                $filename = url_title($arquivo['file_name']);
                rename('_pdfs/releases/'.$arquivo['file_name'] ,'_pdfs/releases/'.$filename);
                return $filename;
            }
        }else{
            $filename = '';
        }        
    }
}
?>
