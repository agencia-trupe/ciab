<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Usuarios extends MY_Admin {

    var $titulo = 'Usuários';

    function  __construct() {
        parent::__construct();

        $this->load->model('painel/usuarios_model');
    }

    function index() {
        $data['titulo'] = $this->titulo;
        $data['registros'] = $this->usuarios_model->dadosUsuarios();

        $this->load->view('painel/common/header');
        $this->load->view('painel/common/menu');
        $this->load->view('painel/usuarios/lista', $data);
        $this->load->view('painel/common/footer');
    }

    function form($id = null){
        if($id){
            $data['titulo'] = 'Editar '.$this->titulo;
            $data['registro'] = $this->usuarios_model->dadosUsuarios($id);
        }else{
            $data['titulo'] = 'Inserir '.$this->titulo;
        }

        $this->load->view('painel/common/header');
        $this->load->view('painel/common/menu');
        $this->load->view('painel/usuarios/form', $data);
        $this->load->view('painel/common/footer');
    }

    function inserir(){
        $this->usuarios_model->inserir();
        redirect('painel/usuarios/');
    }

    function editar($id){
        $this->usuarios_model->editar($id);
        redirect('painel/usuarios/');
    }

    function excluir($id){
        $this->usuarios_model->excluir($id);
        redirect('painel/usuarios/');
    }

}
?>
