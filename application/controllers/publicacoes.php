<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Publicacoes extends MY_Frontcontroller {

    var $linguagem;

    function __construct() {
        parent::__construct();
        
        $this->linguagem = TRUE;
    }
    
    function index(){
        $menu['submenu'] = FALSE;
        $menu['titulo'] = "PUBLICAÇÕES";
        
        $this->load->view('common/menu-lateral', $menu);
        $this->load->view('publicacoes/publicacoes');
        $this->load->view('common/veja-mais');
    }
}
?>
