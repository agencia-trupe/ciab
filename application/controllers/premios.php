<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Premios extends MY_Frontcontroller {

    function __construct() {
        parent::__construct();
    }
    
    function index(){
        redirect('premios/ciab');
    }
    
    function ciab(){

        $this->headeropt['fb_title'] = 'Prêmio Ciab 2013';
        $this->headeropt['fb_description'] = 'Com o tema “Soluções para uma Sociedade Conectada”, o Prêmio CIAB FEBRABAN 2012
        busca incluir os jovens e universitários no debate dessas propostas inovadoras. Os trabalhos poderão explorar
        assuntos como a utilização de redes sociais, convergência de tecnologias, smartphones, tablets, apps, novas
        formas de relacionamentos dos bancos com os clientes ou outros conteúdos relacionados ao tema do prêmio e
        deverão possuir a validação de empresas ou instituições de ensino, e ser aplicáveis no mercado financeiro.';        
        
        $this->load->view('premios/submenu-premios');
        $this->load->view('common/menu-lateral');
        $this->load->view('premios/ciab');
        $this->load->view('common/veja-mais');
    }

    function twitter(){
        redirect('premios/ciab');
        $this->headeropt['fb_title'] = 'Ciab 2012 - Prêmio Twitter';
        $this->headeropt['fb_description'] = 'Quem seguir o @CiabFEBRABAN e mandar seu slogan pode se dar bem! Para participar,
        bastava seguir o perfil @CiabFEBRABAN e enviar um slogan com até 140 caracteres.';        
        
        $this->load->view('premios/submenu-premios');
        $this->load->view('common/menu-lateral');
        $this->load->view('premios/twitter');
        $this->load->view('common/veja-mais');        
    }
    
    function vencedores(){
        $this->load->view('premios/submenu-premios');
        $this->load->view('common/menu-lateral');
        $this->load->view('premios/vencedores');
        $this->load->view('common/veja-mais');        
    }

    function vencedores2012(){
        $this->load->view('premios/submenu-premios');
        $this->load->view('common/menu-lateral');
        $this->load->view('premios/vencedores2012');
        $this->load->view('common/veja-mais');        
    }    
}
?>
