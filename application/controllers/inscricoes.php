<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Inscricoes extends MY_Frontcontroller {

    function __construct() {
        parent::__construct();
    }
    
    function index(){
        redirect('inscricoes/valores');
    }
    
    function valores(){
        $this->load->view('inscricoes/submenu-inscricoes');
        $this->load->view('common/menu-lateral');
        $this->load->view('inscricoes/valores');
        $this->load->view('common/veja-mais');
    }
    
    function informacoes(){
        $this->load->view('inscricoes/submenu-inscricoes');
        $this->load->view('common/menu-lateral');
        $this->load->view('inscricoes/informacoes');
        $this->load->view('common/veja-mais');
    }
}
?>
