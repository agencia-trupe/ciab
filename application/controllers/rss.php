<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    
class Rss extends CI_Controller{
    
    var $linguagem;    
    
    function __construct(){
        parent::__construct();
        
        $this->linguagem = FALSE;
    }
    
    function index(){
      $this->load->helper('xml');
      $data['feed_name'] = 'Ciab - Febraban 2012';  
      $data['feed_desc'] = 'Congresso e Exposição de Tecnologia da Informação das Instituições Financeiras - Últimas Notícias';
      $data['encoding'] = 'utf-8';  
      $data['posts'] = $this->db->order_by('timestamp')->get('noticias')->result();

      foreach($data['posts'] as $k => $v){
        $d = explode("-", $v->data); 
        $t = explode(":", '00:00:00');
        $v->data = date("r", mktime($t[0], $t[1], $t[2], $d[1], $d[2], $d[0]));
      }

      header("Content-Type: application/rss+xml"); 
      $this->load->view('rss', $data);
    }
}

?>