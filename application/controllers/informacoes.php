<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Informacoes extends MY_Frontcontroller {

    function __construct() {
        parent::__construct();
    }
    
    function index(){
        redirect('informacoes/local');
    }

    function local(){
        $this->load->view('informacoes/submenu-informacoes');
        $this->load->view('common/menu-lateral');
        $this->load->view('informacoes/local');
        $this->load->view('common/veja-mais');        
    }
    
    function hospedagem(){
        $this->load->view('informacoes/submenu-informacoes');
        $this->load->view('common/menu-lateral');
        $this->load->view('informacoes/hospedagem');
        $this->load->view('common/veja-mais');
    }

    function tam(){
        $this->load->view('informacoes/submenu-informacoes');
        $this->load->view('common/menu-lateral');
        $this->load->view('informacoes/tam');
        $this->load->view('common/veja-mais');
    }
}
?>
