<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Sobre extends MY_Frontcontroller {

    function __construct() {
        parent::__construct();
    }
    
    function index(){
        redirect('home');
    }
    
    function termos(){
        $menu['submenu'] = FALSE;
        $menu['titulo'] = FALSE;
        $this->load->view('common/menu-lateral', $menu);
        $this->load->view('sobre/termos-de-uso');
        $this->load->view('common/veja-mais');
    }
    
    function privacidade(){
        $menu['submenu'] = FALSE;
        $menu['titulo'] = FALSE;
        $this->load->view('common/menu-lateral', $menu);
        $this->load->view('sobre/politica-privacidade');
        $this->load->view('common/veja-mais');
    }
    
    function mapa(){
        $menu['submenu'] = FALSE;
        $menu['titulo'] = FALSE;
        $this->load->view('sobre/mapa-do-site');
        $this->load->view('common/veja-mais');
    }
}
?>
