<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 */
class Palestrantes extends MY_Frontcontroller {

    var $pag_options;

    function __construct() {
        parent::__construct();
        
        $this->pag_options = array(
            'per_page' => '5',
            'first_link' => FALSE,
            'last_link' => FALSE,
            'next_tag_open' => "<div class='seta' id='nav-direita'>",
            'next_tag_close' => "</div>",
            'next_link' => '',
            'prev_tag_open' => "<div class='seta' id='nav-esquerda'>",
            'prev_tag_close' => "</div>",
            'prev_link' => '',
            'display_pages' => TRUE,
            'current_tag_open' => "<div class='pagina-ativa'>",
            'current_tag_close' => "</div>",
            'num_links' => 15
        );             
    }

    function index($pag = 0){

        switch($this->session->userdata('language')){
            case 'es':
                $tabela = 'es_palestrantes';
                break;
            default:
            case 'pt':
                $tabela = 'palestrantes';
                break;
            case 'en':
                $tabela = 'en_palestrantes';
                break;
        }

        $this->pag_options['base_url'] = base_url('palestrantes/index');
        $this->pag_options['total_rows'] = $this->db->count_all_results($tabela);
        $this->pag_options['uri_segment'] = 3;
        $this->load->library('pagination', $this->pag_options);
        
        $data['palestrantes'] = $this->db->order_by('nome')->get($tabela, $this->pag_options['per_page'], $pag)->result();
        $data['pagination'] = $this->pagination->create_links();
        
        $this->load->view('palestrantes/submenu-palestrantes');
        $this->load->view('common/menu-lateral');
        $this->load->view('palestrantes/lista', $data);
        $this->load->view('common/veja-mais');
        
    }

    function detalhe($slug){
        if(!$slug)
            redirect('palestrantes');

        switch($this->session->userdata('language')){
            case 'es':
                $tabela = 'es_palestrantes';
                $tabela_temas = 'es_palestrantes_temas';
                break;
            default:
            case 'pt':
                $tabela = 'palestrantes';
                $tabela_temas = 'palestrantes_temas';
                break;
            case 'en':
                $tabela = 'en_palestrantes';
                $tabela_temas = 'en_palestrantes_temas';
                break;
        }        
        
        $this->load->helper('text');
        
        $query = $this->db->get_where($tabela, array('slug' => $slug))->result();

        if(!isset($query[0]))
            show_404(current_url(), FALSE);

        $data['palestrante'] = $query[0];
        
        $data['resultados'] = $this->db->order_by('id', 'random')->where('slug !=', $slug)->get($tabela, 2, 0)->result();
        
        $this->headeropt['fb_title'] = 'Ciab 2013 - Palestrante : ' . $query[0]->nome;
        $this->headeropt['fb_description'] = word_wrap(strip_tags($query[0]->texto), 40) . '...';
        $this->headeropt['fb_image'] = base_url('_imgs/palestrantes/'.$query[0]->imagem_on);

        $palest = $this->db->get_where('palestrantes', array('slug' => $slug))->result();

        $data['temas_1_dia'] = $this->db->get_where($tabela_temas, array('id_palestrantes' => $data['palestrante']->id, 'data' => '2013-06-12'))->result();
        $data['temas_2_dia'] = $this->db->get_where($tabela_temas, array('id_palestrantes' => $data['palestrante']->id, 'data' => '2013-06-13'))->result();
        $data['temas_3_dia'] = $this->db->get_where($tabela_temas, array('id_palestrantes' => $data['palestrante']->id, 'data' => '2013-06-14'))->result();
        
        $this->load->view('palestrantes/submenu-palestrantes');
        $this->load->view('common/menu-lateral');
        $this->load->view('palestrantes/detalhe', $data);
        $this->load->view('common/veja-mais');
    }
}
?>
