<?php
/* TOPO */
$lang['EVENTO'] = 'EVENT';
$lang['EVENTO_SUBMENU_1'] = 'Presentation';
$lang['EVENTO_SUBMENU_2'] = 'The Congress';
$lang['EVENTO_SUBMENU_3'] = 'Thematic Spaces';
$lang['EVENTO_SUBMENU_4'] = 'Exhibition';
$lang['EVENTO_SUBMENU_5'] = 'I Want To Visit';
$lang['EVENTO_SUBMENU_6'] = 'I Want To Exhibit';
$lang['EVENTO_SUBMENU_7'] = 'I Want To Sponsor';
$lang['EVENTO_SUBMENU_8'] = 'Exhibition Plan';
$lang['EVENTO_SUBMENU_9'] = 'Other Years';
$lang['PROGRAMACAO'] = 'PROGRAM';
$lang['PALESTRANTES'] = 'SPEAKERS';
$lang['PREMIOS'] = 'AWARDS';
$lang['PREMIOS_SUBMENU_1'] = 'Award Ciab 2012';
$lang['PREMIOS_SUBMENU_2'] = 'Ciab 2011 Winners';
$lang['PREMIOS_SUBMENU_3'] = 'Ciab 2012 Winners';
$lang['INSCRICOES'] = 'REGISTRATIONS';
$lang['INSCRICOES_SUBMENU_1'] = 'Values';
$lang['INSCRICOES_SUBMENU_2'] = 'More Information';
$lang['INSCRICOES_BANNER_TEXTO'] = 'Take advantage of promotional discounts until 10/06. Register your company and get group discounts.';
$lang['NOTICIAS'] = 'NEWS';
$lang['PUBLICACOES'] = 'PUBLICATIONS';
$lang['IMPRENSA'] = 'PRESS';
$lang['IMPRENSA_SUBMENU_1'] = 'Press Office';
$lang['INFORMACOES'] = 'INFORMATION';
$lang['INFORMACOES_SUBMENU_1'] = 'Event Location';
$lang['INFORMACOES_SUBMENU_2'] = 'Accommodation';
$lang['CONTATO'] = 'CONTACT';
$lang['BUSCA'] = 'SEARCH';
$lang['ling_portugues'] = 'português';
$lang['ling_ingles'] = 'english';
$lang['ling_espanhol'] = 'español';
$lang['VOLTAR'] = 'BACK';
$lang['Compartilhe'] = 'Share';
$lang['Quarta-Feira'] = 'Wednesday';
$lang['Quinta-Feira'] = 'Thursday';
$lang['Sexta-Feira'] = 'Friday';

/* FOOTER */
$lang['PATROCINADORES'] = 'SPONSORS';
$lang['DIAMANTE'] = 'DIAMOND';
$lang['OURO'] = 'GOLD';
$lang['PRATA'] = 'SILVER';
$lang['APOIO'] = 'SUPPORT';
$lang['FALE CONOSCO'] = 'CONTACT';
$lang['POLITICA DE PRIVACIDADE'] = 'PRIVACY POLICY';
$lang['TERMO DE USO'] = 'LEGAL INFORMATION';
$lang['MAPA DO SITE'] = 'SITE MAP';
$lang['address-linha1'] = '&copy; FEBRABAN - Federação Brasileira de Bancos - '.date('Y').' - All rights reserved';
$lang['address-linha2'] = 'Av. Brig. Faria Lima, 1.485 - 14º andar • São Paulo • PABX .: 55 11 3244 9800 / 3186 9800 • FAX.: 55 11 3031 4106 •  <a href="http://www.febraban.org.br">WWW.FEBRABAN.ORG.BR</a>';

/* VEJA MAIS */
$lang['VEJA_MAIS_TITULO'] = "SEE+MORE";

/* PALESTRANTES */
$lang['PALESTRANTES_TEMAS_TITULO'] = 'Themes presented during the congress';
$lang['PALESTRANTES_OUTROS'] = 'Other Speakers';

/* NOTÍCIAS */
$lang['NOTICIAS_ULTIMAS_TITULO'] = 'Latest News';
$lang['NOTICIAS_VER_TODAS'] = 'VIEW ALL';

/* HOME */
$lang['HOME_SLIDE_CMEP'] = 'Here comes CIAB FEBRABAN 2013<br>Save the date: June 12 - 14';
$lang['HOME_SLIDE_1'] = 'CIAB FEBRABAN – Congress and Expo of Information Technology for Financial Institutions – is the biggest event in Latin America for the financial sector and for technology.';
$lang['HOME_SLIDE_2'] = 'Gestão e Recuperação de Crédito - PJ / PF, Prevenção sobre Crimes de Lavagem de Dinheiro, Metodologias de Análise e Inspeção em Fraudes Bancárias, Excelência nos Serviços de Atendimento a Clientes, Análise Setorial com análise de riscos, e ...';
$lang['HOME_SLIDE_3'] = 'This unique event will provide an opportunity to discuss the key industry opportunities and issues specific to the Latin American community, together with an update on key SWIFT initiatives which impact the region.';
$lang['HOME_SLIDE_4'] = 'Em sua 13ª edição, o Congresso FEBRABAN de Auditoria Interna e Compliance traz à discussão a perspectiva tridimensional sobre o tema que compreende maior cobertura, maior profundidade nas análises e a garantia de qualidade ...';
$lang['BOX_INSCRICOES_TITULO'] = 'REGISTRATIONS';
$lang['BOX_INSCRICOES_TEXTO'] = 'Take advantage of promotional discounts until 10/06. Register your company and get group discounts.';
$lang['BOX_BLOG_TITULO'] = 'BLOG';
$lang['BOX_BLOG_TEXTO'] = 'New Ciab FEBRABAN blog. A place to discuss various themes related to Information Technology. Join it!';
$lang['QUERO_PATROCINAR_TITULO'] = 'I WANT TO SPONSOR';
$lang['QUERO_PATROCINAR_TEXTO'] = 'Sponsor the biggest event and exhibition in Latin America.';
$lang['QUERO_EXPOR_TITULO'] = 'I WANT TO EXHIBIT';
$lang['QUERO_EXPOR_TEXTO'] = 'Become an exhibitor and connect with more than 20,000 people that visit the three days of CIAB 2012.';
$lang['QUERO_VISITAR_TITULO'] = 'I WANT TO VISIT';
$lang['QUERO_VISITAR_TEXTO'] = 'Come to the Transamérica Expo Center and check out the innovative solutions that the CIAB exhibitors offer.';
$lang['AGENDA_TITULO'] = 'PROGRAM';
$lang['AGENDA_TEXTO'] = 'Check out the national and international speakers present in the panels of the 2012 edition of CIAB FEBRABAN';
$lang['NOTICIAS_TITULO'] = 'NEWS';
$lang['NENHUMA_NOTICIA'] = 'No recent news';
$lang['EVENTO_TITULO'] = 'THE EVENT';
$lang['EVENTO_SUBTITULO'] = 'What´s CIAB FEBRABAN?';
$lang['EVENTO_TEXTO'] = 'Congress and Expo of Information Technology for Financial Institutions – CIAB FEBRABAN is the biggest event in Latin America for the financial and technology sectors.';
$lang['PUBLICACOES_TITULO'] = 'PUBLICATIONS';
$lang['PUBLICACOES_SUBTITULO'] = 'Aposta de R$ 18 bilhões em TI';
$lang['PUBLICACOES_TEXTO'] = 'Pesquisa Ciab FEBRABAN revela que despesas e investimentos dos bancos em tecnologia, em 2011, foram de R$ 18 bilhões e também aponta grande evolução no uso do Mobile Banking.';
$lang['TWITTER_TITULO'] = 'TWITTER CIABFEBRABAN';
$lang['NEWSLETTER_TITULO'] = 'NEWSLETTER';
$lang['NEWSLETTER_TEXTO'] = 'Register and receive all news';
$lang['NEWSLETTER_PLACEHOLDER_NOME'] = 'Inform Your Name';
$lang['NEWSLETTER_PLACEHOLDER_EMAIL'] = 'Your email ';
$lang['NEWSLETTER_SUBMIT'] = 'REGISTER';

/* INSCRIÇÕES */
$lang['INSCRICOES_TITULO'] = 'Preços de Inscrições e Bonificações de Inscrições';
$lang['INSCRICOES_TITULO_TABELA_1'] = 'Valores por pessoa para inscrições nos 3 dias do Congresso';
$lang['DATA_ATE'] = 'Até';
$lang['DATA_APOS'] = 'Após';
$lang['INSCRICOES_TABELA_INSCRICOES'] = 'Inscrições';
$lang['INSCRICOES_TABELA_FILIADOS'] = 'Bancos filiados';
$lang['INSCRICOES_TABELA_NAO_FILIADOS'] = 'Não filiados';
$lang['INSCRICOES_TABELA_LINHA_1'] = 'Até 5';
$lang['INSCRICOES_TABELA_LINHA_2'] = 'de 6 a 10';
$lang['INSCRICOES_TABELA_LINHA_3'] = 'de 11 a 20';
$lang['INSCRICOES_TABELA_LINHA_4'] = 'mais de 20';
$lang['INSCRICOES_TABELA_RODAPE'] = 'EMPRESAS COM O MESMO CNPJ VALORES POR PARTICIPANTE';
$lang['INSCRICOES_TITULO_TABELA_2'] = 'Valores por pessoa para inscrições para 1 dia do Congresso';
$lang['INSCRICOES_OBSERVACAO_TITULO'] = 'Observação';
$lang['INSCRICOES_OBSERVACAO_TEXTO'] = 'No valor da inscrição estão incluídos: almoços, coffee breaks , coquetel do dia 20/06 e estacionamento nos 3 dias de realização do evento.';
$lang['INSCRICOES_OBSERVACAO_ITEM_1'] = 'Os descontos são válidos somente para inscrições na mesma categoria (participação nos 3 dias do evento ou participação em apenas um dia do evento).';
$lang['INSCRICOES_OBSERVACAO_ITEM_2'] = 'Manteremos o valor da 1ª. Fase para o banco que fizer mais do que o equivalente a 50 inscrições válidas para os três dias do evento. (ex.: 50 inscrições para participação nos 3 dias x R$ 2.178,00 = R$ 108.900,00 ou 124 inscrições para participação em um dia = R$ 108.872,00).';
$lang['INSCRICOES_BONIFICACAO_TITULO'] = 'Bonificação';
$lang['INSCRICOES_BONIFICACAO_ITEM_1'] = 'A cada 20 inscrições a instituição recebe 01 (uma) inscrição cortesia de um dia;';
$lang['INSCRICOES_BONIFICACAO_ITEM_2'] = 'Instituições que fizerem mais de 50 (cinqüenta) inscrições e aumentarem o total de inscritos em relação a 2011 serão bonificadas, conforme abaixo:';
$lang['INSCRICOES_BONIFICACAO_TABELA_TITULO_1'] = 'até 10%';
$lang['INSCRICOES_BONIFICACAO_TABELA_TITULO_2'] = 'Acima de 10% até 20%';
$lang['INSCRICOES_BONIFICACAO_TABELA_TITULO_3'] = 'Acima de 20% até 30%';
$lang['INSCRICOES_BONIFICACAO_TABELA_TITULO_4'] = 'Acima de 30%';
$lang['INSCRICOES_BONIFICACAO_TABELA_TEXTO_1'] = '5 inscr. de 1 dia';
$lang['INSCRICOES_BONIFICACAO_TABELA_TEXTO_2'] = '10 inscr. de 1 dia';
$lang['INSCRICOES_BONIFICACAO_TABELA_TEXTO_3'] = '15 inscr. de 1 dia';
$lang['INSCRICOES_BONIFICACAO_TABELA_TEXTO_4'] = '20 inscr. de 1 dia';
$lang['INSCRICOES_BONIFICACAO_TABELA_LEGENDA'] = '(*) A Tabela de Bonificação é válida exclusivamente para os acréscimos de inscrições em relação a 2011, conforme as faixas estabelecidas, e somente para empresas que fizeram acima de 50 (cinquenta) inscrições no Ciab 2012 .<br>(**) Para consultar a quantidade de inscrições realizadas em 2011, pedimos contatar a Diretoria de<br>Eventos pelo telefone (11) 3186-9860.';
$lang['INSCRICOES_EXEMPLO_TITULO'] = 'Exemplo';
$lang['INSCRICOES_BONIFICACAO_TEXTO'] = 'Instituição Y realizou em 2011 50 inscrições para os 3 dias do evento.<br> Em 2012, realizou 66 inscrições para os 3 dias.<br> Isso quer dizer que o banco Y tem direito à bonificação de 30%.<br>';
$lang['INSCRICOES_BONIFICACAO_TEXTO_NEGRITO'] = 'As bonificações adquiridas pela Instituição Y são';
$lang['INSCRICOES_BONIFICACAO_ITEM_EXEMPLO_1'] = 'Desconto de 35% para a mesma categoria (3 dias ou 1 dia) até o último dia de realização do Congresso, conforme disponibilidade de vagas.';
$lang['INSCRICOES_BONIFICACAO_ITEM_EXEMPLO_2'] = 'Gratuidade de 3 inscrições para participação em 1 dia do Congresso (a cada 20 inscrições 01 gratuidade).';
$lang['INSCRICOES_BONIFICACAO_ITEM_EXEMPLO_3'] = 'Gratuidade de 20 inscrições para participação em 1 dia do Congresso, conforme tabela de bonificação';
$lang['INSCRICOES_INFORMACOES_TITULO'] = 'IMPORTANT INFORMATION';
$lang['INSCRICOES_OPCOES_TITULO'] = 'Payment option';
$lang['INSCRICOES_OPCOES_ITEM_1'] = '- International Credit Cards';
$lang['INSCRICOES_OPCOES_ITEM_2'] = ' ';
$lang['INSCRICOES_REEMBOLSO_TITULO'] = 'Refunds and Cancellations';
$lang['INSCRICOES_REEMBOLSO_TEXTO'] = 'Requests for refunds or cancellations of registrations will be accepted when requested in writing and addressed to the email: eventos@febraban.org.br by May 31, 2012.<br> The refunds will be executed at the net amount, deducting all bank charges arising from bank transfers.<br> Requests for refunds or cancellations will not be accepted after June 1st, 2012.';
$lang['INSCRICOES_SUBSTITUICAO_TITULO'] = 'Substitution of Participant';
$lang['INSCRICOES_SUBSTITUICAO_TEXTO'] = 'FEBRABAN will only accept the substitution of a participant provided that the request is made by email eventos@febraban.org.br and sent before June 15, 2012.<br>Any replacement requested during the event, will be charged a fee of R$ 250.00, for re-issuing the name tag. The respective payment shall be made upon replacement request.';
$lang['INSCRICOES_CONFIRMACAO_TITULO'] = 'Registration Confirmation by Email';
$lang['INSCRICOES_CONFIRMACAO_TEXTO'] = 'After the registration is done, the system will send a confirmation by email.<br>If this confirmation is not received, the participant or area in charge shall contact the Events department of FEBRABAN by email eventos@febraban.org.br, requesting verification.';
$lang['INSCRICOES_ENCERRAMENTO_TITULO'] = 'Closing of Registrations';
$lang['INSCRICOES_ENCERRAMENTO_TEXTO'] = 'Registrations end on June 15, 2012 or when sold out.';

/* EVENTO */
$lang['EVENTO_APRESENTACAO_TITULO'] = 'Presentation';
$lang['EVENTO_APRESENTACAO_TEXTO'] = <<<TEXTO
<p>
CIAB FEBRABAN – Congress and Expo of Information Technology for Financial Institutions – is the biggest event in Latin America for the financial sector and for technology.
</p>
<p>
On the date it was launched in September 2011, 76% of the space set aside for the expo was reserved by 74 technology companies. That represents a sales increase of 12% over the launch for CIAB FEBRABAN 2011, once again consolidating its success.
</p>
<p>
Congress organization estimates that CIAB FEBRABAN 2012 will be visited by 19 thousand people, including financial institution representatives and IT companies, and will register 1.9 thousand congress members and enjoy the presence of 190 exhibiting companies.
</p>
<p>
The 22nd edition of the CIAB FEBRABAN will have “The Connected Society” as its core theme and it will be held June 20, 21 and 22, 2012.
</p>
<p>
2011 numbers, consolidating the success<br>
With the theme “Technology beyond the web”, the 21st edition of CIAB FEBRABAN 2011 recorded more than 18 thousand visitors, 1,795 congress members, representatives of financial institutions from Brazil and 27 other countries, as well as 174 exhibiting companies, 33 of which are international without bases in Brazil.
</p>
<p>
The congress also had the sponsorship and support of more than 20 companies, and a total of 33 panels, 81 lecturers, 5 debaters, 27 moderators and 6 keynote speakers.
</p>
TEXTO;
$lang['EVENTO_SOBRE_TITULO'] = 'Sobre a FEBRABAN';
$lang['EVENTO_SOBRE_TEXTO'] = <<<TEXTO
<p>
A Federação Brasileira de Bancos – FEBRABAN é a principal entidade representativa do setor bancário. Foi fundada em 1967 visando
o fortalecimento do sistema financeiro e de suas relações com a sociedade de modo. Reúne 120 bancos associados de um universo
de 172 instituições em operação no Brasil, os quais representam 97% dos ativos totais e 93% do patrimônio líquido do sistema 
bancário. Entre os objetivos estratégicos da Federação estão o desenvolvimento de iniciativas para a contínua melhoria da 
produtividade do sistema bancário e a redução dos seus níveis de risco, contribuindo dessa forma para o desenvolvimento econômico
e sustentável do País.
</p>
<p>
Os bancos também concentram esforços que viabilizam o crescente acesso da população aos seus produtos e serviços com qualidade
e transparência. A Federação consolidou em 2010 sua nova missão e seus novos valores, avançando em aspectos de governança corporativa.
São exemplos desse movimento, a posse do primeiro presidente não dirigente de instituição financeira de sua história, e o 
aprofundamento de discussões dos problemas brasileiros através do Conselho Consultivo da entidade, composto por membros representativos
de entidades empresariais ligadas ao comércio, indústria, educação e terceiro setor.
</p>
<p>
O diálogo com a sociedade vem se ampliando por meio de ações objetivas, como a criação do Sistema de Autorregulação Bancária e o
aperfeiçoamento de ferramentas de relacionamento com o consumidor: Orkut, Facebook, blogs e Twitter, e canais exclusivos de atendimento
como o Sac Bancos, Ouvidoria e Barômetro.
</p>
<p>
As novas exigências da sociedade, principalmente diante do novo estágio de desenvolvimento da economia brasileira, exigem que a
Federação e o setor bancário respondam aos novos desafios, valendo especial atenção à inclusão financeira de consumidores de baixa
renda e à educação financeira.
</p>
TEXTO;
$lang['EVENTO_CONGRESSO_TITULO'] = 'The Congress';
$lang['EVENTO_CONGRESSO_TEXTO'] = <<<TEXTO
<p>
Held parallel to the Expo, the CIAB FEBRABAN Congress will be held in 3 auditoriums that will have national and international specialists in technology and who will debated the Congress’ main theme - “The Connected Society”.
</p>
<p>
In 2011, the CIAB Congress had the participation of 1.8 thousand congress members, representatives of financial institutions in Brazil and 27 other countries, including: Germany, Argentina, Australia, Belgium, China, Chile, Spain, USA, France Israel, India, Italy, Japan, Mexico, Singapore, Taiwan and Uruguay. 
</p>
<p>
With the theme “Technology beyond the web”, the 2011 edition had 33 panels with nearly 100 keynote speakers, scholars and international writers like Michael D. Capellas, Tom Kelley and Tom Davenport, as well as the main executives from Brazil’s biggest financial institutions.
</p>
TEXTO;
$lang['EVENTO_ESPACOS_TITULO'] = 'Thematic Spaces';
$lang['EVENTO_ESPACOS_TEXTO'] = '<p>CIAB FEBRABAN once again reinforces its actions geared towards small companies aimed at stimulating the presence of this segment at the congress.</p>';
$lang['EVENTOS_ESPACOS_ITEM_1_TITULO'] = 'ENTREPREUNER AREA';
$lang['EVENTOS_ESPACOS_ITEM_1_TEXTO'] = 'Dedicated to the exhibition of companies with a 100% domestic capital, of a small size and sales limited to R$ 12 million. The exhibiting company has a package composed of a space in the trade fair and assembly of the stand at reduced prices.';
$lang['EVENTOS_ESPACOS_ITEM_2_TITULO'] = 'INNOVATION SPACE';
$lang['EVENTOS_ESPACOS_ITEM_2_TEXTO'] = 'A FEBRABAN e o ITS (Instituto de Tecnologia de Software e Serviços) vão promover durante o Ciab FEBRABAN 2012 a 8ª edição do “Espaço Inovação”. Divulgue esta iniciativa e participe do Processo de Seleção, inscrevendo sua inovação no site do ITS.';
$lang['EVENTOS_ESPACOS_ITEM_3_TITULO'] = 'INTERNATIONAL AREA';
$lang['EVENTOS_ESPACOS_ITEM_3_TEXTO'] = 'Dedicated to the exhibition of companies which are not headquartered in Brazil, with assembly of the stand and all the infrastructure.';
$lang['EVENTOS_ESPACOS_ITEM_4_TITULO'] = 'FRANCE PAVILION - UBIFRANCE';
$lang['EVENTOS_ESPACOS_ITEM_4_TEXTO'] = 'A FEBRABAN e a Embaixada da França no Brasil fecharam um acordo pelo qual empresas francesas da área de TI poderão participar de um espaço no 22º Ciab FEBRABAN. O Espaço França ocupará uma área de 27 metros quadrados, e abrigará até dez companhias. O intuito é intensificar o intercâmbio dos dois países.';
$lang['EVENTO_EXPOSICAO_TITULO'] = 'Exhibition';
$lang['EVENTO_EXPOSICAO_TEXTO'] = <<<TEXTO
<p>
At the same time as the congress, CIAB FEBRABAN carries out its exhibition counting on the participation of hundreds of exhibitor companies from the finance, information technology (IT) and telecommunications areas.
</p>
<p>
The visitor will have the opportunity to check out all of the news and technological solutions for 2012 and the coming years. You can also the Themed Spaces, a unique opportunity to honor national small businesses in the IT area.
</p>
<p>
Visiting the exhibition is free. Just register using the link below, or directly at the location, in the “Visitor Registration” area.
</p>
<p>
<span class="negrito">Important:</span> the visitor ID tag is only valid for access to the exhibition pavilion. Under no circumstances will visitors be allowed entry into the exclusive areas for those taking part in the congress and invites guests.
</p>

<a href="http://www.credenciamento.com.br/2012/VisitanteCiab/Default_por.aspx" target="_blank" class="botao" style="color:#FFF;">CLICK HERE AND REGISTER</a>
TEXTO;
$lang['EVENTO_QUERO_VISITAR_TITULO'] = 'I Want To Visit';
$lang['EVENTO_QUERO_VISITAR_TEXTO_1'] = <<<TEXTO
<p>
You can also visit the exhibition of the Ciab FEBRABAN. Entry is free. You only need to fill in the form below with your data and print your invitation. Go to the entrance of the Ciab FEBRABAN to obtain your visitor’s name tag and have access to the exhibition. 
</p>
<p>
<span class="negrito">Important:</span> the visitor’s name tag is only valid for access to the exhibition pavilion. Under no circumstances will visitors be allowed to enter the exclusive areas of entities taking part in the congress and guests.
</p>
TEXTO;
$lang['EVENTO_QUERO_VISITAR_TEXTO_2'] = <<<TEXTO
<p style="clear:left;">
If you prefer, click the button below and register. Our advisors team will contact you, if necessary.
</p>
<a href="http://www.credenciamento.com.br/2012/VisitanteCiab/Default_por.aspx" target="_blank" class="botao" style="color:#FFF;">CLICK HERE AND REGISTER</a>
TEXTO;
$lang['EVENTO_EXPOR_TITULO'] = 'I Want To Exhibit';
$lang['EVENTO_EXPOR_TEXTO'] = <<<TEXTO
<p>
Hundreds of companies in the finance, information technology (IT) and telecommunications businesses annually expose in CIAB FEBRABAN and have the opportunity to strengthen their relationship with thousands of people throughout the event.
</p>
<p>
In 2011, 174 of the largest companies in these sectors (such as IBM, Diebold, Microsoft, Itautec, HP, CA, Perto, Fujitsu and CPM Braxis) participated in the exhibition. At the launch of CIAB 2012, which occurred in September 2011, 76% of the exhibition area was already reserved.
</p>
TEXTO;
$lang['EVENTO_EXPOR_FORM_TEXTO'] = 'Fill out the form below,  register  now and reserve your company\'s space in the event. Our team will contact you to inform how to become an exhibitor.';
$lang['EVENTO_EXPOR_FORM_1'] = 'Choose The Topic';
$lang['EVENTO_EXPOR_FORM_2'] = 'Full Name';
$lang['EVENTO_EXPOR_FORM_3'] = 'E-mail ';
$lang['EVENTO_EXPOR_FORM_4'] = 'Company';
$lang['EVENTO_EXPOR_FORM_5'] = 'Company Website ';
$lang['EVENTO_EXPOR_FORM_6'] = 'Field';
$lang['EVENTO_EXPOR_FORM_7'] = 'Phone number';
$lang['EVENTO_EXPOR_FORM_8'] = 'Area/ Space of Interest';
$lang['EVENTO_EXPOR_FORM_SUBMIT'] = 'SEND';
$lang['EVENTO_EXPOR_FORM_OBS'] = 'For additional information and space reservations contact:';
$lang['EVENTO_EXPOR_FORM_OK'] = '<h1>Obrigado!<br>Email enviado com sucesso.entraremos em contato em breve.</h1>';
$lang['EVENTO_EXPOR_FORM_ERROR'] = '<h1>O email não pode ser enviado. <br> Tente novamente.</h1>';
$lang['EVENTO_PATROCINAR_TITULO'] = 'I Want To Sponsor';
$lang['EVENTO_PATROCINAR_TEXTO'] = <<<TEXTO
<p>
Your company can be one of the sponsors of Latin America’s biggest congress and expo in information technology for financial institutions, and exhibit your brand to thousands of congress members and visitors who will come to the 3-day CIAB FEBRABAN event.
</p>

<p>
The 2010 edition registered 129 exhibiting companies (like IBM, Diebold, Microsoft, Itautec, HP, CA, Perto, Fujitsu and CPM Braxis) and it welcomed a public of more than 16 thousand highly qualified people, comprised of presidents, directors and managers of financial institutions, who represented, in their majority, banks (70% of the public) and IT companies (15%).
</p>

<p>
These numbers increase in 2011. There were more than 18 thousand visitors, 1,795 congress members, representatives of financial institutions from Brazil and 27 other countries, as well as 174 exhibiting companies (33 of which are international without bases in Brazil).
</p>
TEXTO;
$lang['EVENTO_PATROCINAR_OBS'] = 'For additional information and space reservations contact:';
$lang['EVENTO_MAPA_EXPOSITORES'] = 'Expositores';

$lang['EVENTO_ANOS_TITULO'] = 'Other Years';
$lang['EVENTO_ANOS_SUBTITULO'] = 'See what happened in previous editions of the event';
$lang['EVENTO_ANOS_ITEM_1'] = 'Technology beyond the web';
$lang['EVENTO_ANOS_ITEM_2'] = 'Generation Y<br> A new bank for<br> a new consumer';
$lang['EVENTO_ANOS_ITEM_3'] = 'A broad discussion about<br> technology and the impact<br> on banking inclusion';  
$lang['EVENTO_ANOS_ITEM_4'] = 'Technology and Security';

/* PRÊMIOS */
$lang['PREMIOS_CIAB_TITULO'] = 'Award Ciab 2012';
$lang['PREMIOS_CIAB_SUBTITULO'] = 'This year’s CIAB FEBRABAN Award brings a new novelty: to award prototypes for mobility.';
$lang['PREMIOS_CIAB_TEXTO'] = <<<TEXTO
<p>
As the major exhibition of IT (Information Technology) of the country and one of the largest congresses for the financial sector, CIAB FEBRABAN brings something new each year.
</p>
<p>
For the 2012 edition of the CIAB FEBRABAN Award, the novelty is in the direct encouragement of technical innovation capacity of young people and undergraduate student who, for the first time, should submit applications for mobile connectivity or mobile platform.
</p>
<p>
With the theme "Solutions for a Connected Society," CIAB FEBRABAN Award 2012 seeks to include young people and college students in the discussion of these innovative proposals. The work will explore issues such as the use of social networks, convergence of technologies, smartphones, tablets, apps, new forms of relationships with bank's customers of banks or other content related to this theme of the award and must be validated by companies or educational institutions and be applicable in the financial market.
</p>
<p>
The main requirement for evaluation and classification of work is the originality and novelty of the proposal. Groups of up to six people are accepted. The results will be released June 04, 2012.
</p>
<p>The registration period is March 1<sup>st</sup> to May 21<sup>st</sup>, 2012.</p>
TEXTO;
$lang['PREMIOS_CIAB_CHAMADA'] = 'Prizes total R$ 25,000! Register now.';
$lang['PREMIOS_CIAB_BOTAO_1'] = 'CLICK HERE TO REGISTER';
$lang['PREMIOS_CIAB_BOTAO_2'] = 'READ THE REGULATION';
$lang['PREMIOS_VENCEDORES_TITULO'] = 'CIAB WINNERS 2011';
$lang['PREMIOS_VENCEDORES_SUBTITULO'] = 'Award Winners of CIAB FEBRABAN and Universia 2011 - Technology In The Web';
$lang['PREMIOS_VENCEDORES_TEXTO_1'] = '<p>The 2011 edition of the Award CIAB FEBRABAN and Universia (which had the theme "Technology In The Web") recorded against  611 versus 223 entries in the first edition of the award in 2010. Check out the winners.</p>';
$lang['PREMIOS_VENCEDORES_COMUNICADO'] = 'ANNOUNCEMENT - RECLASSIFICATION AWARD CIAB FEBRABAN and UNIVERSIA 2011';
$lang['Menção Honrosa'] = 'Honors Mention';
$lang['PREMIOS_VENCEDORES_TEXTO_2'] = <<<TEXTO
<p>
The organizing committee of Ciab FEBRABAN 2011 based on the criteria of Regulation Award Ciab FEBRABAN Universia and 2011, re-evaluating the competitors, concluded that the work previously classified in the first place (2Whish) was disqualified for not complying with requirements of regulation.
</p>

<p>
Therefore, the final award, shall be as follows:
</p>
TEXTO;
$lang['PREMIOS_VENCEDORES_TABELA_1'] = 'Classification';
$lang['PREMIOS_VENCEDORES_TABELA_2'] = 'Name';
$lang['PREMIOS_VENCEDORES_TABELA_3'] = 'Title of the work';
$lang['PREMIOS_VENCEDORES_TWITTER_TITULO'] = 'TWITTER 2011 Winners';
$lang['PREMIOS_VENCEDORES_TWITTER_SUBTITULO'] = 'Contest Cultural Best Slogan CIAB on Twitter';
$lang['PREMIOS_VENCEDORES_TWITTER_CHAMADA'] = 'Who followed the @ CiabFEBRABAN and sent his slogan did well!';
$lang['PREMIOS_VENCEDORES_TWITTER_TEXTO'] = <<<TEXTO
<p>Ciab FEBRABAN brought one more novelty for 2011 edition: the cultural contest "Best Slogan Ciab on Twitter." To participate, was enough follow the profile @ CiabFEBRABAN and send a slogan up to 140 characters.</p>

<p>37 slogans were received and three winners were awarded with tickets valid for three days of Ciab FEBRABAN 2011, entitled to participate in the exhibition and lectures at the congress.</p>

<p>Keep following @ CiabFEBRABAN and stay on top of everything that happens in the biggest Congress and Exhibition of Information Technology in Latin America.</p>
TEXTO;

$lang['PREMIOS_VENCEDORES_2012_TITULO'] = 'Confira os vencedores do Prêmio CIAB 2012';
$lang['PREMIOS_VENCEDORES_2012_PROJETO'] = 'PROJETO';
$lang['PREMIOS_VENCEDORES_2012_GRUPO'] = 'GRUPO';
$lang['PREMIOS_VENCEDORES_2012_PROJETO_1'] = 'ProDeaf – Software Tradutor (Português/LIBRAS – LIBRAS/Português)';
$lang['PREMIOS_VENCEDORES_2012_GRUPO_1'] = <<<LIST
<p>- Marcelo Lucio Correia de Amorim</p>
<p>- Lucas Araújo Mello Soares</p>
<p>- João Paulo dos Santos Oliveira</p>
<p>- Luca Bezerra Dias</p>
<p>- Roberta Agra Coutelo</p>
<p>- Victor Rafael Nascimento dos Santos</p>
LIST;
$lang['PREMIOS_VENCEDORES_2012_PROJETO_2'] = 'Bidd – Plataforma móvel para contratação de crédito de consumo.';
$lang['PREMIOS_VENCEDORES_2012_GRUPO_2'] = <<<LIST
<p>- Daniel César Vieira Radicchi</p>
<p>- André Luis Ferreira Marques</p>
<p>- Willy Stadnick Neto</p>
<p>- Vinicius Abouhatem</p>
<p>- Daniel Coelho</p>
LIST;
$lang['PREMIOS_VENCEDORES_2012_PROJETO_3'] = 'My Tag – Rede social de consumo.';
$lang['PREMIOS_VENCEDORES_2012_GRUPO_3'] = <<<LIST
<p>- Holisson Soares da Cunha</p>
<p>- Sergio Luis Sardi Mergen</p>
<p>- Luciana Hikari Kuamoto</p>
<p>- Leonardo Seiji</p>
LIST;





/* IMPRENSA */
$lang['IMPRENSA_ASSESSORIA_TITULO'] = 'Press Room';
$lang['IMPRENSA_ASSESSORIA_TEXTO_1'] = 'To receive press releases, participate of press conference and interviews, please contact us';
$lang['IMPRENSA_ASSESSORIA_TEXTO_2'] = 'If you want to cover the event, complete the form below and register. Our team will contact you if necessary.';
$lang['IMPRENSA_ASSESSORIA_FORM_1'] = 'Full Name';
$lang['IMPRENSA_ASSESSORIA_FORM_2'] = 'E-mail';
$lang['IMPRENSA_ASSESSORIA_FORM_3'] = 'MTB';
$lang['IMPRENSA_ASSESSORIA_FORM_4'] = 'Company';
$lang['IMPRENSA_ASSESSORIA_FORM_5'] = 'Company Website';
$lang['IMPRENSA_ASSESSORIA_FORM_6'] = 'Field';
$lang['IMPRENSA_ASSESSORIA_FORM_7'] = 'Phone number';
$lang['IMPRENSA_ASSESSORIA_FORM_SUBMIT'] = 'SEND';
$lang['IMPRENSA_ASSESSORIA_FORM_OK'] = '<h1>Obrigado! <br> Email enviado com  ssucesso. Entraremos em contato em breve.</h1>';
$lang['IMPRENSA_ASSESSORIA_FORM_ERROR'] = '<h1>O email não pode ser enviado. <br> Tente novamente.</h1>';

/* INFORMAÇÕES */
$lang['INFO_PAVILHOES'] = 'Pavillions A, B, C and D';
$lang['INFORMACOES_LOCAL_TITULO'] = 'Event Location';
$lang['INFORMACOES_LOCAL_VERMAPA'] = 'view larger map';
$lang['INFORMACOES_HOTEIS_TITULO'] = 'Hotels';
$lang['INFORMACOES_HOTEIS_SUBTITULO'] = 'Clique sobre o hotel para mais detalhes';
$lang['INFORMACOES_HOTEIS_LEGENDA'] = 'SGL: Acomoda 1 pessoa | DBL: Acomoda 2 pessoas';
$lang['INFORMACOES_HOTEIS_TABELA_TITULO_1'] = 'HOTEL';
$lang['INFORMACOES_HOTEIS_TABELA_TITULO_2'] = 'SGL SUPERIOR DAILY';
$lang['INFORMACOES_HOTEIS_TABELA_TITULO_3'] = 'DBL SUPERIOR DAILY';
$lang['INFORMACOES_HOTEIS_COMDESCONTO'] = 'including discount';
$lang['INFORMACOES_HOTEIS_LOCAL'] = 'Address';
$lang['INFORMACOES_HOTEIS_TEL'] = 'Phone number';
$lang['INFORMACOES_HOTEIS_COMO_CHEGAR'] = 'Click here to see how to get';
$lang['INFORMACOES_HOTEIS_CODIGO'] = 'Reservation code';
$lang['INFORMACOES_HOTEIS_DIARIA_SGL'] = 'Daily rate for Superior SGL apartment';
$lang['INFORMACOES_HOTEIS_DIARIA_DBL'] = 'Daily rate for Superior DBL apartment';
$lang['INFORMACOES_HOTEIS_DIARIA'] = 'The daily rate (by adherence) includes';
$lang['INFORMACOES_HOTEIS_OBSERVACOES'] = 'Notes';
$lang['INFORMACOES_HOTEIS_CHECK_1'] = '- Check-in from 14:00 PM<br>- Check-out by 12:00 PM';
$lang['INFORMACOES_HOTEIS_CHECK_2'] = 'Daily rates begin at 12:00 PM and finish at 12:00 PM';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_1'] = 'Breakfast served in the restaurant (06:00 AM to 10:00 AM)';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_2'] = '- Breakfast (when served in the restaurant)<br>- Transfer Hotel/Transamérica Expo Center/Hotel<br>- 01 garage space per apartment.';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_3'] = '- Breakfast (when served in the restaurant)<br>- Transfer Congonhas Airport / Hotel /<br> Congonhas Airport';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_4'] = '- Breakfast (when served in the restaurant).<br>- 1 garage space per apartment';
$lang['INFORMACOES_HOTEIS_DIARIA_HT_5'] = '-Breakfast (when served in the restaurant).';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_1'] = 'The value of the daily rate will be increased by 5% for ISS + R$ 4,50 tourism tax. The payment of the accommodation costs should be made directly at Hotel Transamérica São Paulo with cash or credit card. The apartments are subject to availability.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_2'] = 'The value of the daily rate will be increased by 5% for ISS + tourism tax. The payment of the accommodation costs should be made directly at Transamérica Executive Chácara Santo Antonio with cash or credit card. The apartments are subject to availability.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_3'] = 'The value of the daily rate will be increased by 5% for ISS + tourism tax. The payment of the accommodation costs should be made directly at Transamérica Executive Congonhas with cash or credit card. The apartments are subject to availability.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_4'] = 'The value of the daily rate will be increased by 5% for ISS + tourism tax. The payment of the accommodation costs should be made directly at Transamérica Prime International Plaza with cash or credit card. The apartments are subject to availability.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_5'] = 'The value of the daily rate will be increased by 5% for ISS + tourism tax. The payment of the accommodation costs should be made directly at Blue Tree Premium Morumbi with cash or credit card. The apartments are subject to availability.';
$lang['INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_6'] = 'The value of the daily rate will be increased by 5% for ISS + tourism tax. The payment of the accommodation costs should be made directly at Blue Tree Premium Verbo Divino with cash or credit card. The apartments are subject to availability.';

/* CONTATO */
$lang['CONTATO_INFORMACOES_GERAIS'] = 'General Information';
$lang['CONTATO_COMO_CHEGAR'] = 'HOW TO GET THERE';
$lang['CONTATO_RESERVAS'] = 'Reservations';
$lang['CONTATO_FORM_TITULO'] = 'Fill out the form below';
$lang['CONTATO_FORM_ASSUNTO_TITULO'] = 'Topic';
$lang['CONTATO_FORM_ASSUNTO_1'] = 'Other';
$lang['CONTATO_FORM_ASSUNTO_2'] = 'Panelist';
$lang['CONTATO_FORM_ASSUNTO_3'] = 'Exhibit';
$lang['CONTATO_FORM_ASSUNTO_4'] = 'Visitor';
$lang['CONTATO_FORM_ASSUNTO_5'] = 'Sponsor';
$lang['CONTATO_FORM_ASSUNTO_6'] = 'Press';
$lang['CONTATO_FORM_1'] = 'Full Name';
$lang['CONTATO_FORM_2'] = 'E-mail';
$lang['CONTATO_FORM_3'] = 'Company';
$lang['CONTATO_FORM_4'] = 'Company Site';
$lang['CONTATO_FORM_5'] = 'Field';
$lang['CONTATO_FORM_6'] = 'Phone number';
$lang['CONTATO_FORM_7'] = 'Message';
$lang['CONTATO_FORM_SUBMIT'] = 'SEND';
$lang['CONTATO_FORM_OK'] = '<h1>Obrigado! <br>Email enviado com sucesso. Entraremos em contato em breve.</h1>';
$lang['CONTATO_FORM_ERROR'] = '<h1>O email não pode ser enviado. <br>Tente novamente.</h1>';

/***************/
/* PROGRAMAÇÃO */
/***************/

/* DIA 1 */
$lang['1-sessao-1'] = <<<STR
1ª sessão manhã - das 09:00 às 10:30
STR;

$lang['auditorio-1'] = <<<STR
AUDITORIUM FEBRABAN
STR;

$lang['auditorio-2'] = <<<STR
AUDITORIUM BUSINESS LINE
STR;

$lang['auditorio-3'] = <<<STR
AUDITORIUM OPERATIONAL EFFICIENCY
STR;

$lang['1-sessao-1'] = <<<STR
1st section morning - from 09:00 to 10:30
STR;

$lang['1-sessao-2'] = <<<STR
2nd section morning - from 11:00 to 12:30
STR;

$lang['1-sessao-3'] = <<<STR
1st section afternoon - from 14:00 to 15:30
STR;

$lang['1-sessao-4'] = <<<STR
2nd section afternoon - from 16:00 to 18:00
STR;

$lang['obs-solenidade'] = <<<STR
Opening Ceremony of the Exhibition and visit to the Sponsors' booths / Coffee break
STR;

$lang['DIA_1_MANHA_1_A'] = <<<STR
Opening<br>
Murilo Portugal - FEBRABAN<br>
Luis Antonio Rodrigues - CIAB FEBRABAN
STR;

$lang['DIA_1_MANHA_1_B'] = <<<STR
Simultaneous broadcast of the Opening
STR;

$lang['DIA_1_MANHA_1_C'] = <<<STR
Simultaneous broadcast of the Opening
STR;


$lang['DIA_1_MANHA_2_A'] = <<<STR
The importance of IT in Financial System Development<br>
Roberto Egydio Setúbal - Itaú Unibanco<br>
Moderator: Murilo Portugal - FEBRABAN
STR;

$lang['DIA_1_MANHA_2_B'] = <<<STR
Simultaneous broadcast
STR;

$lang['DIA_1_MANHA_2_C'] = <<<STR
Simultaneous broadcast
STR;


$lang['DIA_1_TARDE_1_A'] = <<<STR
ArchiTechs: How to Live, Govern & Learn in a Hyper-connected World<br>
Rahaf Harfoush - Social Media Strategist<br>
Moderator: Cláudio Almeida Prado - Deutsche Bank
STR;

$lang['DIA_1_TARDE_1_B'] = <<<STR
Bank in the Cloud - Discussion on the use of Cloud Computing by Banks<br>
Peter Redshaw - Gartner<br>
Azeem Mohamed - HP<br>
Rodrigo Gazzaneo - EMC<br>
Moderator: Jair Vasconcelos Filho - CAIXA
STR;

$lang['DIA_1_TARDE_1_C'] = <<<STR
Consumerization<br>
Fernando Belfort - Frost & Sullivan<br>
Ghassan Dreibi Jr. - Cisco<br>
Moderator: Francimara Teixeira Garcia Viotti - Banco do Brasil 
STR;


$lang['DIA_1_TARDE_2_A'] = <<<STR
from 16:00 to 17:00<br>
Innovation, Information Technology and Communication<br>
Silvio Meira - Centro de Estudos e Sistemas Avançados do Recife (CESAR)<br> 
Moderator: Milton Shizuo Noguchi - Itautec<br>
====================================== <br>
from 17:00 to 18:00<br>
Delivering Convenience, Security and Innovation<br>
Thomas W. Swidarski - CEO Global da Diebold<br>
Moderator: Benito Luís Rossiti - TECBAN
STR;

$lang['DIA_1_TARDE_2_B'] = <<<STR
Big Data - Analytic Intelligence on unstructured data<br>
Donald Feinberg - Gartner<br>
George Tziahanas - HP<br>
Patrícia Florissi - EMC<br>
Moderator: Armando Corrêa - Citibank
STR;

$lang['DIA_1_TARDE_2_C'] = <<<STR
Mobile Technologies in the Connected Era<br>
Paulo Marcelo Lessa Moreira - CPM Braxis Capgemini<br>
Michihiko Yoden - NTT Data<br>
Nuno Gomes - Booz & Company<br>
José Domingos Favoretto Jr. - CPqD<br>
Moderator: Jorge Vacarini Júnior - Deutsche Bank
STR;



/* DIA 2 */
$lang['DIA_2_MANHA_1_A'] = <<<STR
The Bank and the Client of the Future<br>
John Bates - Progress Software<br>
Maurício Machado de Minas - Bradesco<br>
Moderator: Ricardo Ribeiro Mandacaru Guerra - Itaú Unibanco
STR;

$lang['DIA_2_MANHA_1_B'] = <<<STR
CIO's of Other Industries and the Connected Society<br>
Pedro Paulo A.C. Cunha - Alelo<br>
Anderson Figueiredo – IDC<br>
João Lencioni - GE<br>
Moderator: Keiji Sakai – BM&FBovespa
STR;

$lang['DIA_2_MANHA_1_C'] = <<<STR
OPERATIONAL EFFICIENCY<br>
Ian Malone - EMC<br>
Walter Tadeu Pinto de Faria - FEBRABAN<br>
Marcelo Atique - CAIXA<br>
Moderator: Joaquim Kiyoshi Kavakama - CIP - Câmara Interbancária de Pagamentos
STR;


$lang['DIA_2_MANHA_2_A'] = <<<STR
Innovation and Creativity<br>
Charles Bezerra - GAD'Innovation<br>
Moderator: Carlos Roberto Zanellato - HSBC
STR;

$lang['DIA_2_MANHA_2_B'] = <<<STR
The Bank in the Connected Era<br>
Rosie Fitzmaurice - RBR<br>
Dan Cohen - IBM<br>
Antranik Haroutiounian - Bradesco<br>
Moderator: David Alves de Melo - Itautec
STR;

$lang['DIA_2_MANHA_2_C'] = <<<STR
Challenges of training skilled human capital<br>
Prof. José Pastore<br>
Paulo Sérgio Sgobbi - Brasscom<br>
João Sabino - Fundação Bradesco<br>
Moderator: Fábio Cássio Costa Moraes - FEBRABAN
STR;


$lang['DIA_2_TARDE_1_A'] = <<<STR
The Business Areas of Banks talk about Trends<br>
Paulo Nergi Boeira de Oliveira - Caixa<br>
Hideraldo Dwight Leitão - Banco do Brasil<br>
Arnaldo Nissental - Bradesco<br>
Gilberto de Abreu - Santander Brasil<br>
Moderator: Alexandre Gouvea - McKinsey
STR;

$lang['DIA_2_TARDE_1_B'] = <<<STR
Digital Accessibility<br>
Delfino Natal de Souza - Ministry of Planning, Budget and Management<br>
Karen Myers  - W3C - World Wide Web Consortium <br>
Sidinei Rossoni - CAIXA<br>
Liliane Vieira Moraes - CNPq - Conselho Nacional de Desenvolvimento Científico e Tecnológico<br>
Moderator: Carlos Alberto Brocchi de Oliveira Pádua - Diebold
STR;

$lang['DIA_2_TARDE_1_C'] = <<<STR
Information Security<br>
Kris Lovejoy - IBM<br>
Alberto Fávero - Ernst Young<br>
Americo Lobo Neto - BioLógica<br>
Moderator: Jorge Fernando Krug - Banrisul
STR;


$lang['DIA_2_TARDE_2_A'] = <<<STR
from 16:00 to 17:00<br>
The Threat environment and the New Wave of IT<br>
Arthur W. Coviello Jr. - EMC<br>
Moderator: André Augusto de Lima Salgado - Citibank<br>
======================================<br>
from 17:00 to 18:00<br>
How IT Is Changing Our Future<br> 
Miha Kralj - Microsoft<br>
Moderator: João Antonio Dantas Bezerra Leite - Itaú Unibanco
STR;

$lang['DIA_2_TARDE_2_B'] = <<<STR
Analytical Platform for Social Networking<br> 
Katia Vaskys - IBM<br> 
Zachary Aron - Deloitte<br> 
Rodrigo Gonsales - Cisco<br> 
Katia Furie - Santander Brasil<br> 
Moderator: Gustavo de Souza Fosse - Banco do Brasil
STR;

$lang['DIA_2_TARDE_2_C'] = <<<STR
The Imperatives of the New Risk Management<br>
Luis Arturo Diaz- Oracle<br>
Oliver Cunningham - KPMG<br> 
Maria del Carmen Aleixandre - Accenture<br>
Moderator: Ricardo Orlando - Itaú Unibanco
STR;



/* DIA 3 */
$lang['3-sessao-1'] = <<<STR
1st section morning - from 09:00 to 10:30
STR;

$lang['3-sessao-2'] = <<<STR
2nd section morning - from 11:00 to 12:30
STR;

$lang['3-sessao-3'] = <<<STR
1st section afternoon - from 14:00 to 16:00
STR;

$lang['3-sessao-4'] = <<<STR
2nd section afternoon - from 16:30 to 18:00
STR;

$lang['DIA_3_MANHA_1_A'] = <<<STR
Innovation in IT Industry for Banks<br>
Carlos Cunha - EMC<br>
Fábio Pessoa - IBM<br>
J. Thomas Elbling - Perto<br>
João Abud Junior - Diebold<br>
Luciano Corsini - HP<br>
Rui José Schoenberger - NTT Data<br>
Wilton Ruas da Silva - Itautec<br>
Moderator: Cláudia Vassalo - Editora Abril
STR;

$lang['DIA_3_MANHA_1_B'] = <<<STR
Simultaneous broadcast
STR;

$lang['DIA_3_MANHA_1_C'] = <<<STR
Simultaneous broadcast
STR;


$lang['DIA_3_MANHA_2_A'] = <<<STR
IT Leaders discuss Trends<br>
Aurélio Conrado Boni - Bradesco<br>
Joaquim Lima de Oliveira - Caixa<br>
Luis Antonio Rodrigues - Itaú Unibanco<br> 
Marcelo Zerbinatti - Santander Brasil<br>
Moderator: Gustavo José Costa Roxo da Fonseca - Booz & Company
STR;

$lang['DIA_3_MANHA_2_B'] = <<<STR
Social Commerce<br>
Eduardo Galanternick - Magazine Luiza<br>
Luca Cavalcanti - Bradesco<br>
Gabriel Borges - Like Store<br>
Moderator: Ricardo Pomeranz - Rapp Brasil
STR;

$lang['DIA_3_MANHA_2_C'] = <<<STR
Agile Development<br>
Pedro Britto - IBM<br>
Eduardo Mazon - Banco BMG<br>
Ricardo Ribeiro Mandacaru Guerra - Itaú Unibanco<br>
Moderator: Alberto Luiz Gerardi - Banco do Brasil
STR;


$lang['DIA_3_TARDE_1_A'] = <<<STR
Brazil in Perspective<br>
Ilan Goldfajn - Itaú Unibanco<br>
Moderator: André Lahóz - Revista Exame
STR;

$lang['DIA_3_TARDE_1_B'] = <<<STR
 Innovation in Self-Service<br>
Carlos Alberto Brocchi de Oliveira Pádua - Diebold<br>
João Lo Ré Chagas - Itautec<br>
Fabrizio Pinna - Scopus<br>
Jorge Schtoltz - Banco do Brasil<br>
Moderator: Kátia Militello - Info Exame
STR;

$lang['DIA_3_TARDE_1_C'] = <<<STR
Gamification - How the game mechanics will transform your business and your behavior<br>
Peter Redshaw - Gartner<br>
Alcides de Francisco Ferreira - BM&Fbovespa<br>
Alexandre Winetzki - Woopi<br>
Moderator: Flávio Leomil Marietto - CPM Braxis Capgemini
STR;


$lang['DIA_3_TARDE_2_A'] = <<<STR
Closing Lecture
STR;

$lang['DIA_3_TARDE_2_B'] = <<<STR
Closing Lecture
STR;

$lang['DIA_3_TARDE_2_C'] = <<<STR
Closing Lecture
STR;

$lang['Auditórios Excelência em TI'] = 'Auditórios Excelência em TI';
$lang['Auditorios Horário'] = 'Horário';
$lang['Auditorios Palestrantes'] = 'Palestrantes';
$lang['Auditorios Tema'] = 'Tema';

/* DIA 1 */
$lang['dia_1 Auditorio 1'] = 'Auditório 3 - TALARIS';
$lang['dia_1 Auditorio 3'] = 'Auditório 3 - LÓGICA';
$lang['dia_1 Auditorio 2'] = 'Auditório 2 - Acesso Digital';

$lang['dia1_auditorio_2_horario_1'] = '14h30';
$lang['dia1_auditorio_2_horario_2'] = '14h40';
$lang['dia1_auditorio_2_horario_3'] = '15h00';
$lang['dia1_auditorio_2_horario_4'] = '15h30';
$lang['dia1_auditorio_2_horario_5'] = '16h00';
$lang['dia1_auditorio_2_palestrante_1'] = 'Antonio Augusto de Almeida Leite (Pancho), ACREFI';
$lang['dia1_auditorio_2_palestrante_2'] = 'Breno Costa, Sócio e Consultor da GoOn – A Evolução na Gestão do Risco';
$lang['dia1_auditorio_2_palestrante_3'] = 'Ricardo Batista, Superintendente de Crédito do Tribanco';
$lang['dia1_auditorio_2_palestrante_4'] = 'Roberto Jabali, Superintendente de Crédito do Grupo Citi - Credicard';
$lang['dia1_auditorio_2_palestrante_5'] = 'Alex Yamamoto,Consultor da Acesso Digital';
$lang['dia1_auditorio_2_tema_1'] = 'Abertura';
$lang['dia1_auditorio_2_tema_2'] = 'Case GoOn: Como aumentar o nível de decisão automática sem impacto na inadimplência.';
$lang['dia1_auditorio_2_tema_3'] = 'Case Tribanco: captura de documentos no varejo';
$lang['dia1_auditorio_2_tema_4'] = 'Case Grupo Citi: Pioneirismo na gestão eletrônica de documentos';
$lang['dia1_auditorio_2_tema_5'] = 'Formalização de crédito online nas instituições financeiras';

$lang['dia1_auditorio_3_horario_1'] = '09h00 - 10h00';
$lang['dia1_auditorio_3_horario_2'] = '11h00 – 12h00';
$lang['dia1_auditorio_3_horario_3'] = '14h00 - 15h10';
$lang['dia1_auditorio_3_horario_4'] = '15h20 – 16h30';
$lang['dia1_auditorio_3_horario_5'] = '16h40 – 17h50';
$lang['dia1_auditorio_3_horario_6'] = '17h50 -18h00';
$lang['dia1_auditorio_3_horario_7'] = ' ';
$lang['dia1_auditorio_3_palestrante_1'] = 'Paulo Martins, Diretor Comercial / Logica';
$lang['dia1_auditorio_3_palestrante_2'] = 'Reginaldo Silva, Portfólio / Logica';
$lang['dia1_auditorio_3_palestrante_3'] = 'Antonio Requejo, Regional Practice Leader Iberia & Latam – Security / Logica';
$lang['dia1_auditorio_3_palestrante_4'] = 'Júlio Gonçalves, Regional Practice Leader Iberia & Latam – IT Modernization / Logica';
$lang['dia1_auditorio_3_palestrante_5'] = 'Lode Snykers, Vice President, Global Financial Services / Logica';
$lang['dia1_auditorio_3_palestrante_6'] = 'Paulo Martins, Diretor Comercial / Logica';
$lang['dia1_auditorio_3_palestrante_7'] = ' ';
$lang['dia1_auditorio_3_tema_1'] = 'Compartilhando Idéias';
$lang['dia1_auditorio_3_tema_2'] = 'Mobilidade: Saiba como surpreender os seus clientes com soluções modernas e inovadoras';
$lang['dia1_auditorio_3_tema_3'] = 'De Securidad en TI para el Gerenciamento de Riesgo';
$lang['dia1_auditorio_3_tema_4'] = 'Modernização de Sistemas Legados';
$lang['dia1_auditorio_3_tema_5'] = 'The technological challenges of the Financial Sector in the XXI century';
$lang['dia1_auditorio_3_tema_6'] = 'Compartilhando Ideias II';
$lang['dia1_auditorio_3_tema_7'] = 'Sorteios para os participantes do dia';


/* DIA 2 */
$lang['dia_2 Auditorio 1'] = 'Auditório 1 - HP';
$lang['dia_2 Auditorio 2'] = 'Auditório 2 - Microsoft';
$lang['dia_2 Auditorio 3'] = 'Auditório 3 - CLM';

/* Auditorio 1 */
$lang['dia2_auditorio_1_horario_1'] = '10h00 – 11h00';
$lang['dia2_auditorio_1_horario_2'] = '11h10 – 12h10';
$lang['dia2_auditorio_1_horario_3'] = '14h30 – 15h30';
$lang['dia2_auditorio_1_horario_4'] = '15h40 – 16h00';
$lang['dia2_auditorio_1_palestrante_1'] = 'Mike Wright, HP';
$lang['dia2_auditorio_1_palestrante_2'] = 'Gladson Martins Russo, Nokia Siemens';
$lang['dia2_auditorio_1_palestrante_3'] = 'Andre Kalsing, HP';
$lang['dia2_auditorio_1_palestrante_4'] = 'Federico Grosso, Autonomy';
$lang['dia2_auditorio_1_tema_1'] = 'Tendências para Social CRM';
$lang['dia2_auditorio_1_tema_2'] = 'Novas tecnologias para Mobile Payment';
$lang['dia2_auditorio_1_tema_3'] = 'Benefícios da adoção de uma plataforma de serviços multicanal';
$lang['dia2_auditorio_1_tema_4'] = 'Uma nova Visão de Inovação e Big Data';


// Auditorio 2
$lang['dia2_auditorio_2_horario_1'] = '9h-10h';
$lang['dia2_auditorio_2_horario_2'] = '10h-11h30';
$lang['dia2_auditorio_2_horario_3'] = '11h30-12h30';
$lang['dia2_auditorio_2_horario_4'] = '14h-15h';
$lang['dia2_auditorio_2_horario_5'] = '15h-16h';
$lang['dia2_auditorio_2_horario_6'] = '16h-17h';
$lang['dia2_auditorio_2_horario_7'] = '17h-18h';
$lang['dia2_auditorio_2_palestrante_1'] = 'Fabio Souto';
$lang['dia2_auditorio_2_palestrante_2'] = 'Miha Krajl';
$lang['dia2_auditorio_2_palestrante_3'] = 'Jun Endo';
$lang['dia2_auditorio_2_palestrante_4'] = 'Eduardo Campos';
$lang['dia2_auditorio_2_palestrante_5'] = 'Roberto Prado';
$lang['dia2_auditorio_2_palestrante_6'] = 'Ricardo-Enrico Jahn';
$lang['dia2_auditorio_2_palestrante_7'] = 'Richard Chaves';
$lang['dia2_auditorio_2_tema_1'] = 'Sociedade Conectada e a Consumerização de TI';
$lang['dia2_auditorio_2_tema_2'] = 'Como TI está mudando nosso futuro';
$lang['dia2_auditorio_2_tema_3'] = 'Soluções Inovadoras para uma Experiência Única Multidevices & Consumerização de TI';
$lang['dia2_auditorio_2_tema_4'] = 'A nuvem nos seus termos - Estratégia de Computação na nuvem da Microsoft';
$lang['dia2_auditorio_2_tema_5'] = 'O profissional TI como protagonista da competitividade. Capacitação e Satisfação do cliente.';
$lang['dia2_auditorio_2_tema_6'] = 'Relacionamento e fidelização nas instituições financeiras com Dynamics CRM/XRM';
$lang['dia2_auditorio_2_tema_7'] = 'O poder da Inovação';

// Auditorio 3
$lang['dia2_auditorio_3_horario_1'] = '10h30 - 11h30';
$lang['dia2_auditorio_3_horario_2'] = '11h30 - 12h30';
$lang['dia2_auditorio_3_horario_3'] = '14h30 - 15h30';
$lang['dia2_auditorio_3_horario_4'] = '15h30 - 16h30';
$lang['dia2_auditorio_3_horario_5'] = '16h30 - 17h30';
$lang['dia2_auditorio_3_horario_6'] = '17h30 - 18h30';
$lang['dia2_auditorio_3_palestrante_1'] = 'Martin Roesch, SNORT';
$lang['dia2_auditorio_3_palestrante_2'] = 'Alex Silva, A10';
$lang['dia2_auditorio_3_palestrante_3'] = 'Carlos Fagundes, INTEGRALTRUST';
$lang['dia2_auditorio_3_palestrante_4'] = 'Sanjay Ramnath, BARRACUDA';
$lang['dia2_auditorio_3_palestrante_5'] = 'Dick Faulkner, SOPHOS';
$lang['dia2_auditorio_3_palestrante_6'] = 'Martin Roesch, SOURCEFIRE';
$lang['dia2_auditorio_3_tema_1'] = 'Open Source Security';
$lang['dia2_auditorio_3_tema_2'] = 'DDoS Attacks and Brute Force';
$lang['dia2_auditorio_3_tema_3'] = 'Segredos do Basileia III';
$lang['dia2_auditorio_3_tema_4'] = 'Application Security';
$lang['dia2_auditorio_3_tema_5'] = 'Cryptography and Mobile Security';
$lang['dia2_auditorio_3_tema_6'] = 'Next Generation Security';

/* DIA 3*/
$lang['dia_3 Auditorio 1'] = 'Auditório 1 - FEBRABAN';

$lang['dia3_auditorio_1_horario_1'] = '11h00 - 12h30';
$lang['dia3_auditorio_1_palestrante_1'] = 'Guilhermino Domiciano de Souza – Banco do Brasil;  Marcelo Ribeiro Câmara – Bradesco;  Cesar Faustino – Itaú Unibanco';
$lang['dia3_auditorio_1_tema_1'] = 'A Segurança Digital do Cidadão no Sistema Financeiro Brasileiro';
?>
