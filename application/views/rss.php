<?echo "<?xml version='1.0' encoding='$encoding'?>"?>
<rss xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:atom="http://www.w3.org/2005/Atom" version="2.0">
<channel>
<atom:link href="<?=base_url('rss')?>" rel="self" type="application/rss+xml" />
<title><?=xml_convert($feed_name)?></title>
<description><?=xml_convert($feed_desc)?></description>
<link><?=base_url('noticias')?></link>
<?foreach($posts as $k => $v):?>
<item>
<description><?=xml_convert(strip_tags($v->olho))?></description>
<link><?=base_url('noticias/detalhe/'.$v->id)?></link>
<guid><?=base_url('noticias/detalhe/'.$v->id)?></guid>
<pubDate><?=$v->data?></pubDate>
<title><?=xml_convert($v->titulo)?></title>
</item>
<?endforeach;?>
</channel>
</rss>