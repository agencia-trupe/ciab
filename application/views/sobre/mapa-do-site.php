<div class="centro main-<?=$this->router->class?>">
    
    <div id="container" class="conteudo conteudo-<?=$this->router->method?>">

        <h1>Mapa do Site</h1>
        
        <ul>
            <h2><a href="evento">Evento</a></h2>
                <li><a href="evento/apresentacao">&raquo; Apresentação</a></li>
                <li><a href="evento/congresso">&raquo; O congresso</a></li>
                <li><a href="evento/espacos">&raquo; Espaços Temáticos</a></li>
                <li><a href="evento/exposicao">&raquo; Exposição</a></li>
                <li><a href="evento/querovisitar">&raquo; Quero Visitar</a></li>
                <li><a href="evento/queroexpor">&raquo; Quero Expor</a></li>
                <li><a href="evento/queropatrocinar">&raquo; Quero Patrocinar</a></li>
                <li><a href="evento/planta">&raquo; Planta da Exposição</a></li>
                <li><a href="evento/outrosanos">&raquo; Outros Eventos</a></li>
        </ul>
        
        <ul>
            <h2><a href="programacao">Programação</a></h2>
                <li><a href="programacao/primeiroDia">&raquo; 20.06.12</a></li>
                <li><a href="programacao/segundoDia">&raquo; 21.06.12</a></li>
                <li><a href="programacao/terceiroDia">&raquo; 22.06.12</a></li>
        </ul>
        
        <ul>
            <h2><a href="palestrantes">Palestrantes</a></h2>
        </ul>
        
        <ul>
            <h2><a href="premios">Prêmios</a></h2>
                <li><a href="premios/ciab">&raquo; Prêmio Ciab 2012</a></li>
                <li><a href="premios/twitter">&raquo; Prêmio Twitter 2012</a></li>
                <li><a href="premios/vencedores">&raquo; Vencedores 2011</a></li>
        </ul>

        <ul>
            <h2><a href="inscricoes">Inscrições</a></h2>
                <li><a href="inscricoes/valores">&raquo; Valores</a></li>
                <li><a href="inscricoes/informacoes">&raquo; Informações<br> importantes</a></li>
        </ul>
        
        <ul>
            <h2><a href="noticias">Notícias</a></h2>
        </ul>        
        
        <ul>
            <h2><a href="publicacoes">Publicações</a></h2>
        </ul>
        
        <ul>
            <h2><a href="imprensa">Imprensa</a></h2>
                <li><a href="imprensa/assessoria">&raquo; Assessoria</a></li>
                <li><a href="imprensa/releases">&raquo; Releases</a></li>
                <li><a href="imprensa/clipping">&raquo; Clipping</a></li>
        </ul>        
        
        <ul>
            <h2><a href="informacoes">Informações</a></h2>
                <li><a href="informacoes/local">&raquo; Local</a></li>
                <li><a href="informacoes/hospedagem">&raquo; Hospedagem</a></li>
        </ul>
        
        <ul>
            <h2><a href="contato">Contato</a></h2>
        </ul>        
    </div>    
    
</div>