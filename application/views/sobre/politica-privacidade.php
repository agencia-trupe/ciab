        <div class="conteudo conteudo-<?=$this->router->method?>">
        
            <h1>Política de privacidade</h1>
            
            <p>Para garantir segurança e privacidade, a FEBRABAN, entidade responsável pelo Ciab, assume os seguintes
            compromissos com os seus usuários:</p>

            <p>- Não revelar sem prévia autorização os emails cadastrados no site</p>
            <p>- Não enviar mensagens eletrônicas que não tenham sido solicitadas mediante cadastro no site pelo usuário
                visitante</p>
            <p>- Fornecer sempre a possibilidade de cancelar o envio de mensagens</p>
            <p>- Todas as informações obtidas através do site são confidenciais e serão utilizadas apenas com fins estatísticos</p>
            <p>- Não coletar informações sem o consetimento do internauta</p>
            <p>- Utilizar cookies apenas para facilitar o login de usuários cadastrados ou para controle de audiência, nunca
                coletar informações não autorizadas</p>
            <p>- Não distribuir material ofensivo, discriminatório ou potencialmente danoso para os seus usuários</p>
            <p>Para dúvidas, sugestões, reclamações e maiores informações, use nosso canal direto com o usuário.</p>

        </div>

    </div>
</div>