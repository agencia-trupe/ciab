<div id="content" class="container_16 clearfix">

    <div class="grid_16">
        <h2><?= $titulo ?></h2>
    </div>
    

    <?php
    if(isset($registro[0]) AND !empty($registro[0])):
    ?>

<form name="form" id="form" method="post" action="<?= base_url() ?>painel/noticias/editar/<?= $registro[0]->id ?>" enctype="multipart/form-data">

    Categoria<br>
    <select name="categoria">
        <option></option>
        <?foreach($categorias as $cat):?>
            <option value="<?=$cat->id?>" <?if($cat->id == $registro[0]->id_noticias_categorias) echo " selected"?>><?=$cat->titulo?></option>
        <?endforeach;?>
    </select>
    
    <br><br>    

    Título<br>
    <input type="text" name="titulo" value="<?=$registro[0]->titulo?>" required> <br><br>
    
    Sub-Título<br>
    <input type="text" name="subtitulo" value="<?=$registro[0]->subtitulo?>"> <br><br>
    
    Data<br>
    <input type="text" name="data" id="datepicker" required maxlenght="10" value="<?=formataData($registro[0]->data, 'mysql2br')?>"><br><br>
    
    Olho<br>
    <textarea id="elm1" name="olho"><?=$registro[0]->olho?></textarea> <br><br>
    
    Texto<br>
    <textarea id="elm2" name="texto" class="mce"><?=$registro[0]->texto?></textarea> <br><br>
    
    Imagem<br>
    
    <?if(!empty($registro[0]->imagem)):?>
        <img src="<?=base_url('_imgs/noticias/thumbs/'.$registro[0]->imagem)?>"><br>
        <a href="" class="troca-imagem">Trocar Imagem</a> <br>
        <div class="troca-imagem-box" style="display:none;">
            <input type="file" name="userfile"> <br>
            <label style="font-size:14px; font-weight:normal"><input type="checkbox" name="excluir-imagem" value="1"> Somente Apagar Imagem</label>
            <input type="hidden" name="imagem-atual" value="<?=$registro[0]->imagem?>">
        </div>
    <?else:?>
        <input type="file" name="userfile">
    <?endif;?>
    
    <br><br>
    <input type="submit" value="Gravar" /> <input type="button" value="Voltar" class="btn-voltar" />
</form>

<?php else: ?>

<form name="form" id="form" method="post" action="<?= base_url() ?>painel/noticias/inserir/" enctype="multipart/form-data">

    Categoria<br>
    <select name="categoria">
        <option></option>
        <?foreach($categorias as $cat):?>
            <option value="<?=$cat->id?>"><?=$cat->titulo?></option>
        <?endforeach;?>
    </select>
    
    <br><br>    

    Título<br>
    <input type="text" name="titulo" required> <br><br>
    
    Sub-Título<br>
    <input type="text" name="subtitulo"> <br><br>
    
    Data<br>
    <input type="text" name="data" id="datepicker" required maxlenght="10"><br><br>
    
    Olho<br>
    <textarea id="elm1" name="olho"></textarea> <br><br>
    
    Texto<br>
    <textarea id="elm2" name="texto" class="mce"></textarea> <br><br>
    
    Imagem<br>
    <input type="file" name="userfile"><br><br>
    
    <input type="submit" value="Gravar" /> <input type="button" value="Voltar" class="btn-voltar" />
</form>

<?php endif; ?>

</div>

<!---------------------------------------------------------->

