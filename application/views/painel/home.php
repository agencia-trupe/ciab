<div id="content" class="container_16 clearfix home">
        <div class="grid_16">
                <?
                $meses = array(
                    '01' => 'Janeiro',
                    '02' => 'Fevereiro',
                    '03' => 'Março',
                    '04' => 'Abril',
                    '05' => 'Maio',
                    '06' => 'Junho',
                    '07' => 'Julho',
                    '08' => 'Agosto',
                    '09' => 'Setembro',
                    '10' => 'Outubro',
                    '11' => 'Novembro',
                    '12' => 'Dezembro'
                );
                ?>

                <h2>Bem Vindo(a), <?= $usuario ?></h2>
                <p>Dia <?= date('d') ?> de <?= $meses[date('m')] ?>  de <?= date('Y') ?>.</p>
                <p>No painel você pode controlar as principais funções do site.</p>
                <p>Qualquer Dúvida entrar em contato: <a class="mail-link" href="mailto:contato@felici.com.br">contato@felici.com.br</a></p>

        </div>
</div>

<script defer>
    $(document).ready( function(){
        $('body').keydown( function(e){
            if(e.which === 80){
                window.location = '<?=base_url('painel/palestrantes')?>'    
            }    
        });    
    });
</script>