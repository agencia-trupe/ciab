<div id="content" class="container_16 clearfix">

    <div class="grid_16">
        <h2><?= $titulo ?></h2>
    </div>
    

    <?php
    if(isset($registro[0]) AND !empty($registro[0])):
    ?>

<?if($this->router->class == 'palestrantes'):?>
    <form name="form" id="form" method="post" action="<?= base_url() ?>painel/palestrantes/editar/<?= $registro[0]->id ?>" enctype="multipart/form-data">
<?elseif($this->router->class == 'en_palestrantes'):?>
    <form name="form" id="form" method="post" action="<?= base_url() ?>painel/en_palestrantes/editar/<?= $registro[0]->id ?>" enctype="multipart/form-data">
<?elseif($this->router->class == 'es_palestrantes'):?>
    <form name="form" id="form" method="post" action="<?= base_url() ?>painel/es_palestrantes/editar/<?= $registro[0]->id ?>" enctype="multipart/form-data">
<?endif;?>

    Nome<br>
    <input type="text" name="nome" required value="<?=$registro[0]->nome?>"> <br><br>
    
    Origem<br>
    <input type="text" name="origem" value="<?=$registro[0]->origem?>"> <br><br>
    
    Texto<br>
    <textarea name="texto"><?=$registro[0]->texto?></textarea> <br><br>
    
    Imagem Colorida<br>
    <img src="<?=base_url('_imgs/palestrantes/'.$registro[0]->imagem_on)?>"><br>
    <a href="" class="troca-imagem">Trocar Arquivo</a> <br>
    <div class="troca-imagem-box" style="display:none;">
        <input type="file" name="on"> <br>
        <input type="hidden" name="on-atual" value="<?=$registro[0]->imagem_on?>">
    </div><br><br>

    Imagem PB<br>
    <img src="<?=base_url('_imgs/palestrantes/'.$registro[0]->imagem_off)?>"><br>
    <a href="" class="troca-imagem">Trocar Arquivo</a> <br>
    <div class="troca-imagem-box" style="display:none;">
        <input type="file" name="off"> <br>
        <input type="hidden" name="off-atual" value="<?=$registro[0]->imagem_off?>">
    </div><br><br>
    
    Imagem Ampliada<br>
    <img src="<?=base_url('_imgs/palestrantes/'.$registro[0]->imagem_ampliada)?>"><br>
    <a href="" class="troca-imagem">Trocar Arquivo</a> <br>
    <div class="troca-imagem-box" style="display:none;">
        <input type="file" name="ampliada"> <br>
        <input type="hidden" name="ampliada-atual" value="<?=$registro[0]->imagem_ampliada?>">
    </div><br><br>    
    
    <input type="submit" value="Gravar" /> <input type="button" value="Voltar" class="btn-voltar" />

    </form>

    <!-- PALESTRANTES -->

    <br>
    <br>
    <h3>Temas</h3>
    <script src="<?=base_url('js/temas.js')?>"></script>
    <style>
        #adicionar-tema{
            height: 220px;
        }
        #temas-target table{
            width:960px;
            max-width:960px;
            table-layout: fixed;
        }
        .mensagem-tema{
            width:260px;
            height: 190px;
            line-height: 190px;
            text-align:center;
            font-weight:bold;
        }
        .td-texto{
            word-wrap: break-word;
        }
    </style>

    <div id="secao-temas">

        <div id="temas-target"></div>

        <hr>
        Incluir tema :<br>
        <div id="adicionar-tema">
            <div class="tema-desc">
                <input type="text" class="input-small" name="titulo-tema" placeholder="titulo"><br>
                <select class="input-small" name="data-tema">
                    <option value="2013-06-12">1&ordm; dia - 12.06.13</option>
                    <option value="2013-06-13">2&ordm; dia - 13.06.13</option>
                    <option value="2013-06-14">3&ordm; dia - 14.06.13</option>
                </select><br>
                <textarea class="textarea-small" name="texto-tema" placeholder="texto"></textarea>
                <input type="hidden" name="id-palestrantes" id="id-palestrantes" value="<?=$registro[0]->id?>">
                <input type="hidden" name="language" id="language" value="<?=$lingua?>">
                <input type="button" id="gravar-tema" value="incluir">
            </div>
        </div>
    </div>

<?php else: ?>

<form name="form" id="form" method="post" action="<?= base_url() ?>painel/palestrantes/inserir/" enctype="multipart/form-data">

    Nome<br>
    <input type="text" name="nome" required> <br><br>
    
    Origem<br>
    <input type="text" name="origem"> <br><br>
    
    Texto<br>
    <textarea name="texto"></textarea> <br><br>
    
    Imagem Colorida<br>
    <input type="file" name="on"><br><br>

    Imagem PB<br>
    <input type="file" name="off"><br><br>
    
    Imagem Ampliada<br>
    <input type="file" name="ampliada"><br><br>    
    
    <input type="submit" value="Gravar" /> <input type="button" value="Voltar" class="btn-voltar" />
</form>

<?php endif; ?>

</div>

<!---------------------------------------------------------->

