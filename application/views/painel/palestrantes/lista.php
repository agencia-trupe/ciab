<div id="content" class="container_16">

    <div class="grid_16">
        <h2>
            <?= $titulo ?>
        </h2>
    </div>

    <div class="clearfix"></div>
    
    <div style="text-align:center;">
        <?
        switch($lang){
            case 'en':
                $lang = 'en_';
                break;
            case 'es':
                $lang = 'es_';
                break;
            case 'pt':
                $lang = '';
                break;
        }
        ?>
    <a href="<?=base_url('painel/en_palestrantes')?>">Versão Em Inglês</a> | <a href="<?= base_url() ?>painel/en_palestrantes/form" class="add">Adicionar palestrante em Inglês</a>
    <br>
    <a href="<?=base_url('painel/palestrantes')?>">Versão Em Português</a> | <a href="<?= base_url() ?>painel/palestrantes/form" class="add">Adicionar palestrante em Português</a>
    <br>
    <a href="<?=base_url('painel/es_palestrantes')?>">Versão Em Espanhol</a> | <a href="<?= base_url() ?>painel/es_palestrantes/form" class="add">Adicionar palestrante em Espanhol</a>
        
    </div>
    
    <br><br>
    
    <table>
        <thead>
            <tr>
                <th>Nome</th>
                <th>Origem</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
    
        <?if(isset($paginacao)):?>
            <tfoot>
                <tr>
                    <td colspan="4" class="pagination">
                        <?=$paginacao?>
                    </td>
                </tr>
            </tfoot>
        <?endif;?>
    
        <tbody>
            <?php
            if(!empty($registros)){
                foreach($registros as $value){
                    echo '<tr>';
                    echo "<td>".$value->nome."</td>";
                    echo "<td>".$value->origem."</td>";
                    if($this->router->class == 'palestrantes'):
                        echo "<td><a class='edit' href='".base_url()."painel/palestrantes/form/".$value->id."'>editar</a></td>";
                        echo "<td><a class='delete' href='".base_url()."painel/palestrantes/excluir/".$value->id."'>excluir</a></td>";
                    elseif($this->router->class == 'en_palestrantes'):
                        echo "<td><a class='edit' href='".base_url()."painel/en_palestrantes/form/".$value->id."'>editar</a></td>";
                        echo "<td><a class='delete' href='".base_url()."painel/en_palestrantes/excluir/".$value->id."'>excluir</a></td>";
                    elseif($this->router->class == 'es_palestrantes'):
                        echo "<td><a class='edit' href='".base_url()."painel/es_palestrantes/form/".$value->id."'>editar</a></td>";
                        echo "<td><a class='delete' href='".base_url()."painel/es_palestrantes/excluir/".$value->id."'>excluir</a></td>";
                    endif;
                    echo "</tr>";
                }
            }else{
                echo "<tr><td colspan='4'>Nenhum palestrante cadastrado</tr>";
            }
            ?>
        </tbody>
    </table>


</div>