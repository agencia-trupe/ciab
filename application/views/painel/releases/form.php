<div id="content" class="container_16 clearfix">

    <div class="grid_16">
        <h2><?= $titulo ?></h2>
    </div>
    

    <?php
    if(isset($registro[0]) AND !empty($registro[0])):
    ?>

<form name="form" id="form" method="post" action="<?= base_url() ?>painel/releases/editar/<?= $registro[0]->id ?>" enctype="multipart/form-data">

    Título<br>
    <input type="text" name="titulo" required value="<?=$registro[0]->titulo?>"> <br><br>
    
    Data<br>
    <input type="text" name="data" id="datepicker" required maxlenght="10" value="<?=formataData($registro[0]->data,'mysql2br')?>"><br><br>
    
    Descritivo<br>
    <input type="text" name="descritivo" value="<?=$registro[0]->descritivo?>"> <br><br>
    
    Arquivo<br>
    
    <?if(!empty($registro[0]->arquivo)):?>
        <a href="<?=base_url('_pdfs/releases/'.$registro[0]->arquivo)?>" target="_blank"><?=$registro[0]->arquivo?></a><br><br>
        <a href="" class="troca-imagem">Trocar Arquivo</a> <br>
        <div class="troca-imagem-box" style="display:none;">
            <input type="file" name="userfile"> <br>
            <input type="hidden" name="arquivo-atual" value="<?=$registro[0]->arquivo?>">
        </div>
    <?else:?>
        <input type="file" name="userfile">
    <?endif;?>
    
    <br><br>
    <input type="submit" value="Gravar" /> <input type="button" value="Voltar" class="btn-voltar" />
</form>

<?php else: ?>

<form name="form" id="form" method="post" action="<?= base_url() ?>painel/releases/inserir/" enctype="multipart/form-data">

    Título<br>
    <input type="text" name="titulo" required> <br><br>
    
    Data<br>
    <input type="text" name="data" id="datepicker" required maxlenght="10"><br><br>
    
    Descritivo<br>
    <input type="text" name="descritivo"> <br><br>
    
    Arquivo<br>
    <input type="file" name="userfile"><br><br>
    
    <input type="submit" value="Gravar" /> <input type="button" value="Voltar" class="btn-voltar" />
</form>

<?php endif; ?>

</div>

<!---------------------------------------------------------->

