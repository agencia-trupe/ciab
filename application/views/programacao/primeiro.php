        <div class="conteudo conteudo-<?=$this->router->class?> conteudo-<?=$this->router->class?>-<?=$this->router->method?>">
        
            <h1>12.06.13 - <?=traduz('Quarta-Feira')?></h1>
            
            <table>
                <thead>
                    <tr>
                        <th colspan="4"><?=traduz('AUDITÓRIO FEBRABAN')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="azul">
                        <td style="width:50px"><?=traduz('sessão')?></td>
                        <td style="width:90px"><?=traduz('horário')?></td>
                        <td style="width:216px"><?=traduz('palestra')?></td>
                        <td><?=traduz('palestrante')?></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('manhã')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>09:00 <?=traduz('às')?> 10:30</td>
                        <td>
                            <?=traduz('Abertura')?>
                        </td>
                        <td>
                            Murilo Portugal - FEBRABAN<br>
                            Virgílio Augusto Fernandes Almeida - MCTI<br>
                            Luis Antonio Rodrigues - CIAB FEBRABAN<br>
                            Geraldo Alckmin - Governador do Estado de SP<br>
                            Fernando Haddad - Prefeito de São Paulo
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>11:00 <?=traduz('às')?> 12:30</td>
                        <td></td>
                        <td>
                            Jorge Hereda - Presidente da CAIXA<br>
                            Moderador: Murilo Portugal - FEBRABAN
                        </td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('tarde')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>14:00 <?=traduz('às')?> 15:30</td>
                        <td></td>
                        <td>Stephanie Young - Walt Disney World</td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>16:00 <?=traduz('às')?> 17:00</td>
                        <td>Experiência do Cliente</td>
                        <td>Donald Feinberg  - Gartner<br>Peter Redshaw - Gartner</td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="4"><?=traduz('Solenidade de abertura da Exposição e visita aos estandes dos Patrocinadores / Coffee break')?></td>
                    </tr>
                </tfoot>
            </table>

            <table>
                <thead>
                    <tr>
                        <th colspan="4"><?=traduz('AUDITÓRIO LINHA DE NEGÓCIOS')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="azul">
                        <td style="width:50px"><?=traduz('sessão')?></td>
                        <td style="width:90px"><?=traduz('horário')?></td>
                        <td style="width:216px"><?=traduz('palestra')?></td>
                        <td><?=traduz('palestrante')?></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('manhã')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>09:00 <?=traduz('às')?> 10:30</td>
                        <td><?=traduz('Transmissão simultânea da Abertura')?></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>11:00 <?=traduz('às')?> 12:30</td>
                        <td><?=traduz('Transmissão simultânea')?></td>
                        <td></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('tarde')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>14:00 <?=traduz('às')?> 15:30</td>
                        <td>Interagindo com Clientes e Funcionários com Mobilidade</td>
                        <td>
                            Leandro Martins Ceccato - IBM (100) <br>
                            Wendy Arnott - TD Bank (Canadá) - 93<br>
                            Onildo Andrade Junior - Banco do Brasil
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>16:00 <?=traduz('às')?> 17:00</td>
                        <td>Impacto das Megatendências globais nos Serviços Financeiros (24)</td>
                        <td>Richard Sear - Frost & Sullivan</td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>17:00 <?=traduz('às')?> 17:15</td>
                        <td colspan="2" style="text-align:center;"><?=traduz('intervalo')?></td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>17:15 <?=traduz('às')?> 18:15</td>
                        <td>Os Grandes Projetos da BM&FBOVESPA que transformarão o Mercado Financeiro</td>
                        <td>Viviane El BanateBasso<br>Fabio Dutra</td>
                    </tr>
                </tbody>
            </table>

            <table>
                <thead>
                    <tr>
                        <th colspan="4"><?=traduz('AUDITÓRIO EFICIÊNCIA OPERACIONAL')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="azul">
                        <td style="width:50px"><?=traduz('sessão')?></td>
                        <td style="width:90px"><?=traduz('horário')?></td>
                        <td style="width:216px"><?=traduz('palestra')?></td>
                        <td><?=traduz('palestrante')?></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('manhã')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>09:00 <?=traduz('às')?> 10:30</td>
                        <td><?=traduz('Transmissão simultânea da Abertura')?></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>11:00 <?=traduz('às')?> 12:30</td>
                        <td><?=traduz('Transmissão simultânea')?></td>
                        <td></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('tarde')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>14:00 <?=traduz('às')?> 15:30</td>
                        <td>Cadastro Positivo e outros projetos da Comissão de Tecnologia e Automação Bancária da FEBRABAN</td>
                        <td>Luis Antonio Rodrigues - FEBRABAN</td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>16:00 <?=traduz('às')?> 17:00</td>
                        <td>Cloud Computing na Prática</td>
                        <td>
                            Carlos Alberto Duque - Capgemini (82)<br>
                            Felipe Avila - HP (71)<br>
                            Jose Luis Spagnuolo - IBM (168)<br>
                            Presidente da Intel Brasil - Cisco<br>
                            EMC<br><br>
                            Moderador: CTO do Santander
                        </td>
                    </tr>                   
                </tbody>                
            </table>



            <!-- 
            <h1 style="margin-top:30px;">20.06.12 - <traduz('Auditórios Excelência em TI')?></h1>
            <table>
                <thead>
                    <tr>
                        <th colspan="3"><traduz('dia_1 Auditorio 2')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><traduz('Auditorios Horário')?></td>
                        <td><traduz('Auditorios Palestrantes')?></td>
                        <td><traduz('Auditorios Tema')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_2_horario_1')?></td>
                        <td><traduz('dia1_auditorio_2_palestrante_1')?></td>
                        <td><traduz('dia1_auditorio_2_tema_1')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_2_horario_2')?></td>
                        <td><traduz('dia1_auditorio_2_palestrante_2')?></td>
                        <td><traduz('dia1_auditorio_2_tema_2')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_2_horario_3')?></td>
                        <td><traduz('dia1_auditorio_2_palestrante_3')?></td>
                        <td><traduz('dia1_auditorio_2_tema_3')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_2_horario_4')?></td>
                        <td><traduz('dia1_auditorio_2_palestrante_4')?></td>
                        <td><traduz('dia1_auditorio_2_tema_4')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_2_horario_5')?></td>
                        <td><traduz('dia1_auditorio_2_palestrante_5')?></td>
                        <td><traduz('dia1_auditorio_2_tema_5')?></td>
                    </tr>
                </tbody>
            </table>
            <table>
                <thead>
                    <tr>
                        <th colspan="3"><traduz('dia_1 Auditorio 3')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><traduz('Auditorios Horário')?></td>
                        <td><traduz('Auditorios Palestrantes')?></td>
                        <td><traduz('Auditorios Tema')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_1')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_1')?></td>
                        <td><traduz('dia1_auditorio_3_tema_1')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_2')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_2')?></td>
                        <td><traduz('dia1_auditorio_3_tema_2')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_3')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_3')?></td>
                        <td><traduz('dia1_auditorio_3_tema_3')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_4')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_4')?></td>
                        <td><traduz('dia1_auditorio_3_tema_4')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_5')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_5')?></td>
                        <td><traduz('dia1_auditorio_3_tema_5')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_6')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_6')?></td>
                        <td><traduz('dia1_auditorio_3_tema_6')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_7')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_7')?></td>
                        <td><traduz('dia1_auditorio_3_tema_7')?></td>
                    </tr>
                </tbody>
            </table>            
    -->
            
            <div style="height: 30px;"></div>
            
            <!-- $this->load->view('palestrantes/widget')?> -->
            
        </div>

    </div>
</div>