        <div class="conteudo conteudo-<?=$this->router->class?> conteudo-<?=$this->router->class?>-<?=$this->router->method?>">
        
            <h1>14.06.13 - <?=traduz('Sexta-Feira')?></h1>
            
            <table>
                <thead>
                    <tr>
                        <th colspan="4"><?=traduz('AUDITÓRIO FEBRABAN')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="azul">
                        <td style="width:50px"><?=traduz('sessão')?></td>
                        <td style="width:90px"><?=traduz('horário')?></td>
                        <td style="width:216px"><?=traduz('palestra')?></td>
                        <td><?=traduz('palestrante')?></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('manhã')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>09:00 <?=traduz('às')?> 10:30</td>
                        <td>Inovação na Indústria de TI para os Bancos</td>
                        <td>João Abud Júnior - Diebold <br>
                            Denoel Nicodemos Eller Junior - HP
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>11:00 <?=traduz('às')?> 12:30</td>
                        <td>Transformações econômicas no Brasil e no mundo e desafios e oportunidades para o setor financeiro</td>
                        <td>Ricardo Amorim  <br>
                            Moderador:  Rubens Sardenberg
                        </td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('tarde')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>14:00 <?=traduz('às')?> 16:00</td>
                        <td>CIOs debatem sobre a TI em tempos de Eficiência Operacional</td>
                        <td>Aurélio Conrado Boni - Bradesco <br>
                            Moderador: Gustavo Roxo - Booz & Company
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>16:30 <?=traduz('às')?> 18:00</td>
                        <td>Inovação: a criatividade na era digital</td>
                        <td>Marcelo Tas</td>
                    </tr>
                </tbody>                
            </table>

            <table>
                <thead>
                    <tr>
                        <th colspan="4"><?=traduz('AUDITÓRIO LINHA DE NEGÓCIOS')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="azul">
                        <td style="width:50px"><?=traduz('sessão')?></td>
                        <td style="width:90px"><?=traduz('horário')?></td>
                        <td style="width:216px"><?=traduz('palestra')?></td>
                        <td><?=traduz('palestrante')?></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('manhã')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>09:00 <?=traduz('às')?> 10:30</td>
                        <td><?=traduz('Transmissão Simultânea')?></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>11:00 <?=traduz('às')?> 12:30</td>
                        <td>Desafios para a Gestão de Risco e Capital</td>
                        <td>
                            Dermeval Bicalho Carvalho - Caixa<br>
                            Gerson Eduardo de Oliveria - Banco do Brasil<br>
                            Gedson Oliveira dos Santos - Bradesco<br><br>
                            Moderador: Carlos Donizeti Macedo Maia - Santander
                        </td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('tarde')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>14:00 <?=traduz('às')?> 16:00</td>
                        <td>Eficiência Operacional através das ferramentas de Segurança da Informação</td>
                        <td>
                            Renato Martini - ITI<br>
                            Francimara T. Viotti - Banco do Brasil<br>
                            José Ricardo Munhoz - Bull<br>
                            Moderador: Jorge Krug - Banrisul
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>16:30 <?=traduz('às')?> 18:00</td>
                        <td><?=traduz('Transmissão Simultânea')?></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>

            <table>
                <thead>
                    <tr>
                        <th colspan="4"><?=traduz('AUDITÓRIO EFICIÊNCIA OPERACIONAL')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="azul">
                        <td style="width:50px"><?=traduz('sessão')?></td>
                        <td style="width:90px"><?=traduz('horário')?></td>
                        <td style="width:216px"><?=traduz('palestra')?></td>
                        <td><?=traduz('palestrante')?></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('manhã')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>09:00 <?=traduz('às')?> 10:30</td>
                        <td><?=traduz('Transmissão Simultânea')?></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>11:00 <?=traduz('às')?> 12:30</td>
                        <td>
                            Appification - Das aplicações para aplicativos
                        </td>
                        <td>
                            Alexandre Winetzki -  Woopi/Stefanini<br>
                            Rodrigo Mulinari - Banco do Brasil<br>
                            Fabian Valverde - SAP <br>
                            <br>
                            Moderador: Keiji Sakai
                        </td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('tarde')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>14:00 <?=traduz('às')?> 16:00</td>
                        <td>Eficiência em Tecnologia Bancária</td>
                        <td>
                            Milton Shizuo Noguchi- Itautec<br>
                            Carlos Alberto Brocchi de Oliveira Pádua - Diebold <br>
                            Michael Bielamowicz - Glory <br>
                            <br>
                            Moderador: Anderson Itaborahy - Banco do Brasil
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>16:30 <?=traduz('às')?> 18:00</td>
                        <td><?=traduz('Transmissão Simultânea')?></td>
                        <td></td>
                    </tr>                   
                </tbody>                
            </table>



            <!-- 
            <h1 style="margin-top:30px;">20.06.12 - <traduz('Auditórios Excelência em TI')?></h1>
            <table>
                <thead>
                    <tr>
                        <th colspan="3"><traduz('dia_1 Auditorio 2')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><traduz('Auditorios Horário')?></td>
                        <td><traduz('Auditorios Palestrantes')?></td>
                        <td><traduz('Auditorios Tema')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_2_horario_1')?></td>
                        <td><traduz('dia1_auditorio_2_palestrante_1')?></td>
                        <td><traduz('dia1_auditorio_2_tema_1')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_2_horario_2')?></td>
                        <td><traduz('dia1_auditorio_2_palestrante_2')?></td>
                        <td><traduz('dia1_auditorio_2_tema_2')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_2_horario_3')?></td>
                        <td><traduz('dia1_auditorio_2_palestrante_3')?></td>
                        <td><traduz('dia1_auditorio_2_tema_3')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_2_horario_4')?></td>
                        <td><traduz('dia1_auditorio_2_palestrante_4')?></td>
                        <td><traduz('dia1_auditorio_2_tema_4')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_2_horario_5')?></td>
                        <td><traduz('dia1_auditorio_2_palestrante_5')?></td>
                        <td><traduz('dia1_auditorio_2_tema_5')?></td>
                    </tr>
                </tbody>
            </table>
            <table>
                <thead>
                    <tr>
                        <th colspan="3"><traduz('dia_1 Auditorio 3')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><traduz('Auditorios Horário')?></td>
                        <td><traduz('Auditorios Palestrantes')?></td>
                        <td><traduz('Auditorios Tema')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_1')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_1')?></td>
                        <td><traduz('dia1_auditorio_3_tema_1')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_2')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_2')?></td>
                        <td><traduz('dia1_auditorio_3_tema_2')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_3')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_3')?></td>
                        <td><traduz('dia1_auditorio_3_tema_3')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_4')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_4')?></td>
                        <td><traduz('dia1_auditorio_3_tema_4')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_5')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_5')?></td>
                        <td><traduz('dia1_auditorio_3_tema_5')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_6')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_6')?></td>
                        <td><traduz('dia1_auditorio_3_tema_6')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_7')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_7')?></td>
                        <td><traduz('dia1_auditorio_3_tema_7')?></td>
                    </tr>
                </tbody>
            </table>            
    -->
            
            <div style="height: 30px;"></div>
            
            <!-- $this->load->view('palestrantes/widget')?> -->
            
        </div>

    </div>
</div>