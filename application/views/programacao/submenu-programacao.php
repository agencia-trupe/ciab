<div class="centropad main-<?=$this->router->class?>">
    
    <div id="container">
    
        <div id="menu-lateral">
        
            <h3>PROGRAMAÇÃO</h3>
            
            <ul class="submenu-programacao">
                <li id="submn-primeiroDia"><a href="programacao/primeiroDia">12.06.13</a></li>
                <div class="faixa-sob-item-menu"><div></div></div>
                
                <li id="submn-segundoDia"><a href="programacao/segundoDia">13.06.13</a></li>
                <div class="faixa-sob-item-menu"><div></div></div>
                
                <li id="submn-terceiroDia"><a href="programacao/terceiroDia">14.06.13</a></li>
                <div class="faixa-sob-item-menu"><div></div></div>
            </ul>
            
            <script defer>$(document).ready( function(){ $('#submn-<?=$this->router->method?>').addClass('subativo'); });</script>
            