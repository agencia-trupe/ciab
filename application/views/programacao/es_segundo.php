        <div class="conteudo conteudo-<?=$this->router->class?> conteudo-<?=$this->router->class?>-<?=$this->router->method?>">
        
            <h1>13.06.13 - <?=traduz('Quinta-Feira')?></h1>
            
            <table>
                <thead>
                    <tr>
                        <th colspan="4"><?=traduz('AUDITORIO FEBRABAN')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="azul">
                        <td style="width:50px"><?=traduz('sesión')?></td>
                        <td style="width:90px"><?=traduz('horario')?></td>
                        <td style="width:216px"><?=traduz('conferencia')?></td>
                        <td><?=traduz('panelista')?></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('mañana')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>de las 09:00 <?=traduz('a las')?> 10:30</td>
                        <td>How We Win: Engagement, Empowerment, and Communications in an Interconnected World</td>
                        <td>Michael Slaby - TomorrowVentures <br>
                            Moderador: Leignes Jorge Andreatti - HSBC 
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>de las 11:00 <?=traduz('a las')?> 12:30</td>
                        <td>Desafíos, Tendencias y el papel de las TI para la industria de seguros</td>
                        <td>Marco Antonio Rossi - Bradesco Seguros</td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('tarde')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>de las 14:00 <?=traduz('a las')?> 15:30</td>
                        <td>Desafíos del Back Office de los Bancos</td>
                        <td>
                            Hideraldo Dwight Leitão - Banco do Brasil <br>
                            Pedro Paulo de Lima Gonçalves - CAIXA <br>
                            Maurício Minas - Bradesco <br>
                            <br>
                            Moderador: Sonia Penteado
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>de las 16:00 <?=traduz('a las')?> 17:00</td>
                        <td>Infraestructura de Brasil e Innovación</td>
                        <td>
                            Moderador: Murilo Portugal - FEBRABAN
                        </td>
                    </tr>
                </tbody>                
            </table>

            <table>
                <thead>
                    <tr>
                        <th colspan="4"><?=traduz('AUDITORIO LÍNEA DE NEGOCIO')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="azul">
                        <td style="width:50px"><?=traduz('sesión')?></td>
                        <td style="width:90px"><?=traduz('horario')?></td>
                        <td style="width:216px"><?=traduz('conferencia')?></td>
                        <td><?=traduz('panelista')?></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('mañana')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>de las 09:00 <?=traduz('a las')?> 10:30</td>
                        <td>BPO - Business Process Outsourcing</td>
                        <td>Mário Ferreira Neto - Caixa <br>
                            Anurag Shah - HP
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>de las 11:00 <?=traduz('a las')?> 12:30</td>
                        <td>Proyectos de Innovación o Cultura en Innovación</td>
                        <td>
                            Reginaldo Arakaki - Scopus Tecnologia <br>
                            Douglas Tevis Francisco - Bradesco  <br>
                            Ricardo Guerra - Itaú Unibanco<br>
                            Evandro Ferreira de Avelar - Caixa <br>
                            <br>
                            Moderador: Cirano Silveira - HP
                        </td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('tarde')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>de las 14:00 <?=traduz('a las')?> 15:30</td>
                        <td>CIO's de Otras Industrias</td>
                        <td>
                            Cláudio Prado - Editora Abril<br>
                            Marcos Vinicius Ferreira - SERPRO<br>
                            Moderador: Cassio Dreyfuss - Gartner
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>de las 16:00 <?=traduz('a las')?> 17:00</td>
                        <td>Experiencia de Clientes vs Fidelización</td>
                        <td>David Goslin  - Deloitte <br>
                            Moderador: Paulo Marcelo Moreira - Capgemini <br>
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>de las 17:00 <?=traduz('a las')?> 17:15</td>
                        <td colspan="2" style="text-align:center;"><?=traduz('intervalo')?></td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>de las 17:15 <?=traduz('a las')?> 18:15</td>
                        <td>Convertir datos en activo estratégico de negocios para los Bancos </td>
                        <td>
                            Hyong Kim - Ernst & Young
                        </td>
                    </tr>
                </tbody>
            </table>

            <table>
                <thead>
                    <tr>
                        <th colspan="4"><?=traduz('AUDITORIO LÍNEA DE NEGOCIO')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="azul">
                        <td style="width:50px"><?=traduz('sesión')?></td>
                        <td style="width:90px"><?=traduz('horario')?></td>
                        <td style="width:216px"><?=traduz('conferencia')?></td>
                        <td><?=traduz('panelista')?></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('mañana')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>de las 09:00 <?=traduz('a las')?> 10:30</td>
                        <td>Estrategia para el desarrollo de un modelo de Gobierno Corporativo de TI</td>
                        <td>
                            Mauro Cesar Bernardes - USP<br>
                            Ken Janssen - J.P. Morgan<br>
                            Joaquim Kavakama - CIP<br><br>
                            Moderador: Gustavo de Souza Fosse- Banco do Brasil
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>de las 11:00 <?=traduz('a las')?> 12:30</td>
                        <td>Canales de Relación con el Cliente</td>
                        <td></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('tarde')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>de las 14:00 <?=traduz('a las')?> 15:30</td>
                        <td>Big Data</td>
                        <td>
                            Marcos Panichi - IBM<br>
                            Jason White - HP <br>
                            Mike Huckaby - EMC <br>
                            <br>
                            Moderador: Ricardo Orlando - Itaú Unibanco
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>de las 16:00 <?=traduz('a las')?> 17:00</td>
                        <td>Infraestructura como fuente de ventaja competitiva en el Sector de Servicios Financieros</td>
                        <td>
                            Mary Knox - Gartner<br>
                            Gustavo Santana - Cisco<br>
                            José Isern - Itaú Unibanco<br>
                            Fernando Diaz - Santander <br>
                            Paulo Cesar de Almeida Toledo - Banco do Brasil <br>
                            Moderador: Waldemar Ruggiero - Bradesco\
                        </td>
                    </tr>                   
                </tbody>                
            </table>

            <div style="height: 30px;"></div>
            
            <!-- $this->load->view('palestrantes/widget')?> -->
            
        </div>

    </div>
</div>