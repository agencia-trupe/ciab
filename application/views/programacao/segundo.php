        <div class="conteudo conteudo-<?=$this->router->class?> conteudo-<?=$this->router->class?>-<?=$this->router->method?>">
        
            <h1>13.06.13 - <?=traduz('Quinta-Feira')?></h1>
            
            <table>
                <thead>
                    <tr>
                        <th colspan="4"><?=traduz('AUDITÓRIO FEBRABAN')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="azul">
                        <td style="width:50px"><?=traduz('sessão')?></td>
                        <td style="width:90px"><?=traduz('horário')?></td>
                        <td style="width:216px"><?=traduz('palestra')?></td>
                        <td><?=traduz('palestrante')?></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('manhã')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>09:00 <?=traduz('às')?> 10:30</td>
                        <td><?=traduz('Keynote Speaker')?></td>
                        <td>Michael Slaby - TomorrowVentures </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>11:00 <?=traduz('às')?> 12:30</td>
                        <td>Canais Digitais e Experiência do Cliente</td>
                        <td>Marco Tavares - HSBC</td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('tarde')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>14:00 <?=traduz('às')?> 15:30</td>
                        <td>Desafios do Back Office dos Bancos</td>
                        <td>
                            Hideraldo Dwight Leitão - Banco do Brasil<br>
                            Pedro Paulo de Lima Gonçalves - CAIXA<br>
                            Maurício Minas - Bradesco<br>
                            Santander Brasil<br>
                            Itaú Unibanco
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>16:00 <?=traduz('às')?> 17:00</td>
                        <td>Infraestrutura do Brasil e Inovação</td>
                        <td>
                            Ministro Paulo Bernardo<br>
                            Ministro Marco Antonio Raupp<br><br>
                            Moderador: Murilo Portugal - FEBRABAN
                        </td>
                    </tr>
                </tbody>                
            </table>

            <table>
                <thead>
                    <tr>
                        <th colspan="4"><?=traduz('AUDITÓRIO LINHA DE NEGÓCIOS')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="azul">
                        <td style="width:50px"><?=traduz('sessão')?></td>
                        <td style="width:90px"><?=traduz('horário')?></td>
                        <td style="width:216px"><?=traduz('palestra')?></td>
                        <td><?=traduz('palestrante')?></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('manhã')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>09:00 <?=traduz('às')?> 10:30</td>
                        <td>BPO - Business Process Outsourcing</td>
                        <td>Bradesco/Fidelity<br>Mário Ferreira Neto - CAIXA</td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>11:00 <?=traduz('às')?> 12:30</td>
                        <td>Projetos de Inovação ou Cultura em Inovação</td>
                        <td>Reginaldo Arakaki - Scopus Tecnologia (127)<br>Bradesco<br>Ricardo Guerra - Itaú Unibanco<br>CAIXA</td>                        
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('tarde')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>14:00 <?=traduz('às')?> 15:30</td>
                        <td>CIO's de Outras Indústrias</td>
                        <td>
                            Cláudio Prado - Editora Abril<br>
                            Carlos Katayama - USIMINAS<br>
                            Curt  Zimmerman - Brasil Foods<br><br>
                            Moderador: Cassio Dreyfuss
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>16:00 <?=traduz('às')?> 17:00</td>
                        <td><?=traduz('Experiencia de Clientes vs Fidelização')?></td>
                        <td>Brian Shniderman - Delloite </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>17:00 <?=traduz('às')?> 17:15</td>
                        <td colspan="2" style="text-align:center;"><?=traduz('intervalo')?></td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>17:15 <?=traduz('às')?> 18:15</td>
                        <td>Converting data into a strategic business asset for Banks </td>
                        <td>Hyong Kim - Ernst & Young</td>
                    </tr>
                </tbody>
            </table>

            <table>
                <thead>
                    <tr>
                        <th colspan="4"><?=traduz('AUDITÓRIO EFICIÊNCIA OPERACIONAL')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="azul">
                        <td style="width:50px"><?=traduz('sessão')?></td>
                        <td style="width:90px"><?=traduz('horário')?></td>
                        <td style="width:216px"><?=traduz('palestra')?></td>
                        <td><?=traduz('palestrante')?></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('manhã')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>09:00 <?=traduz('às')?> 10:30</td>
                        <td><?=traduz('Estratégia para o desenvolvimento de um modelo de Governança de TI')?></td>
                        <td>Mauro Cesar Bernardes - USP<br>
                            Joaquim Kawakama - CIP<br>
                            Ken Janssen - JP Morgan (Head of IT Cross Business for Latin America)<br><br>

                            Moderador: Anderson Itaborahy - Banco do Brasil</td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>11:00 <?=traduz('às')?> 12:30</td>
                        <td>Painel de Seguros</td>
                        <td>FENASEG<br>
                            Presidente da Bradesco Seguros<br>
                            Sul América Seguros/Porto Seguro
                        </td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('tarde')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>14:00 <?=traduz('às')?> 15:30</td>
                        <td>Big Data</td>
                        <td>
                            Marcos Panichi - IBM (92)<br>
                            Jason White - HP (72)<br>
                            EMC (159)<br><br>
                            Moderador: Banco
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>16:00 <?=traduz('às')?> 17:00</td>
                        <td>Infraestrutura como Fonte de Vantagem Competitiva no Setor de Serviços Financeiros</td>
                        <td>
                            Peter RedShaw -Gartner (52)<br>
                            Carlos Pereira - Cisco<br>
                            José Isern - Itaú Unibanco<br>
                            Fernando Diaz - Santander<br>
                            Paulo Cesar de Almeida Toledo - Banco do Brasil
                        </td>
                    </tr>                   
                </tbody>                
            </table>



            <!-- 
            <h1 style="margin-top:30px;">20.06.12 - <traduz('Auditórios Excelência em TI')?></h1>
            <table>
                <thead>
                    <tr>
                        <th colspan="3"><traduz('dia_1 Auditorio 2')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><traduz('Auditorios Horário')?></td>
                        <td><traduz('Auditorios Palestrantes')?></td>
                        <td><traduz('Auditorios Tema')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_2_horario_1')?></td>
                        <td><traduz('dia1_auditorio_2_palestrante_1')?></td>
                        <td><traduz('dia1_auditorio_2_tema_1')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_2_horario_2')?></td>
                        <td><traduz('dia1_auditorio_2_palestrante_2')?></td>
                        <td><traduz('dia1_auditorio_2_tema_2')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_2_horario_3')?></td>
                        <td><traduz('dia1_auditorio_2_palestrante_3')?></td>
                        <td><traduz('dia1_auditorio_2_tema_3')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_2_horario_4')?></td>
                        <td><traduz('dia1_auditorio_2_palestrante_4')?></td>
                        <td><traduz('dia1_auditorio_2_tema_4')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_2_horario_5')?></td>
                        <td><traduz('dia1_auditorio_2_palestrante_5')?></td>
                        <td><traduz('dia1_auditorio_2_tema_5')?></td>
                    </tr>
                </tbody>
            </table>
            <table>
                <thead>
                    <tr>
                        <th colspan="3"><traduz('dia_1 Auditorio 3')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><traduz('Auditorios Horário')?></td>
                        <td><traduz('Auditorios Palestrantes')?></td>
                        <td><traduz('Auditorios Tema')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_1')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_1')?></td>
                        <td><traduz('dia1_auditorio_3_tema_1')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_2')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_2')?></td>
                        <td><traduz('dia1_auditorio_3_tema_2')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_3')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_3')?></td>
                        <td><traduz('dia1_auditorio_3_tema_3')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_4')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_4')?></td>
                        <td><traduz('dia1_auditorio_3_tema_4')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_5')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_5')?></td>
                        <td><traduz('dia1_auditorio_3_tema_5')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_6')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_6')?></td>
                        <td><traduz('dia1_auditorio_3_tema_6')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_7')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_7')?></td>
                        <td><traduz('dia1_auditorio_3_tema_7')?></td>
                    </tr>
                </tbody>
            </table>            
    -->
            
            <div style="height: 30px;"></div>
            
            <!-- $this->load->view('palestrantes/widget')?> -->
            
        </div>

    </div>
</div>