        <div class="conteudo conteudo-<?=$this->router->class?> conteudo-<?=$this->router->class?>-<?=$this->router->method?>">
        
            <h1>14.06.13 - <?=traduz('Sexta-Feira')?></h1>
            
            <table>
                <thead>
                    <tr>
                        <th colspan="4"><?=traduz('AUDITORIUM FEBRABAN')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="azul">
                        <td style="width:50px"><?=traduz('session')?></td>
                        <td style="width:90px"><?=traduz('schedule')?></td>
                        <td style="width:216px"><?=traduz('lecture')?></td>
                        <td><?=traduz('panelist')?></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('morning')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>09:00 <?=traduz('to')?> 10:30</td>
                        <td>Innovation in the IT Industry for Banks</td>
                        <td>João Abud Júnior - Diebold <br>
                            Denoel Nicodemos Eller Junior - HP
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>11:00 <?=traduz('to')?> 12:30</td>
                        <td>Economic transformations in Brazil and worldwide: challenges and oportunities to the financial sector</td>
                        <td>Ricardo Amorim <br>
                            Moderator:  Rubens Sardenberg
                        </td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('afternoon')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>14:00 <?=traduz('to')?> 16:00</td>
                        <td>IT Leaders Discuss Trends</td>
                        <td>Aurélio Conrado Boni - Bradesco <br>
                            Moderator: Gustavo Roxo - Booz & Company
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>16:30 <?=traduz('to')?> 18:00</td>
                        <td>Innovation: creativity in the digital age</td>
                        <td>Marcelo Tas</td>
                    </tr>
                </tbody>                
            </table>

            <table>
                <thead>
                    <tr>
                        <th colspan="4"><?=traduz('AUDITORIUM BUSINESS LINE')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="azul">
                        <td style="width:50px"><?=traduz('session')?></td>
                        <td style="width:90px"><?=traduz('schedule')?></td>
                        <td style="width:216px"><?=traduz('lecture')?></td>
                        <td><?=traduz('panelist')?></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('morning')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>09:00 <?=traduz('to')?> 10:30</td>
                        <td>Simultaneous Translation</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>11:00 <?=traduz('to')?> 12:30</td>
                        <td>The Challenges for Capital and Risk Management</td>
                        <td>
                            Dermeval Bicalho Carvalho - Caixa<br>
                            Gerson Eduardo de Oliveria - Banco do Brasil<br>
                            Gedson Oliveira dos Santos - Bradesco<br><br>
                            Moderator: Carlos Donizeti Macedo Maia - Santander
                        </td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('afternoon')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>14:00 <?=traduz('to')?> 16:00</td>
                        <td>Operational Efficiency through the tools of the Information Security</td>
                        <td>
                            Renato Martini - ITI<br>
                            Francimara T. Viotti - Banco do Brasil<br>
                            José Ricardo Munhoz - Bull<br>
                            Moderator: Jorge Krug - Banrisul
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>16:30 <?=traduz('to')?> 18:00</td>
                        <td>Simultaneous Translation</td>
                        <td></td>
                    </tr>
                </tbody>
            </table>

            <table>
                <thead>
                    <tr>
                        <th colspan="4"><?=traduz('AUDITORIUM OPERATIONAL EFFICIENCY')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="azul">
                        <td style="width:50px"><?=traduz('session')?></td>
                        <td style="width:90px"><?=traduz('schedule')?></td>
                        <td style="width:216px"><?=traduz('lecture')?></td>
                        <td><?=traduz('panelist')?></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('morning')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>09:00 <?=traduz('to')?> 10:30</td>
                        <td>Simultaneous Translation</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>11:00 <?=traduz('to')?> 12:30</td>
                        <td>
                            Appification - applications to apps 
                        </td>
                        <td>
                            Alexandre Winetzki -  Woopi/Stefanini<br>
                            Rodrigo Mulinari - Banco do Brasil<br>
                            Fabian Valverde - SAP <br>
                            Moderator:  Keiji Sakai
                        </td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('afternoon')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>14:00 <?=traduz('to')?> 16:00</td>
                        <td>Efficiency in Banking Technology</td>
                        <td>
                            Milton Shizuo Noguchi- Itautec<br>
                            Carlos Alberto Brocchi de Oliveira Pádua - Diebold <br>
                            Michael Bielamowicz - Glory <br>
                            <br>
                            Moderator: Anderson Itaborahy - Banco do Brasil
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>16:30 <?=traduz('to')?> 18:00</td>
                        <td>Simultaneous Translation</td>
                        <td></td>
                    </tr>                   
                </tbody>                
            </table>



            <!-- 
            <h1 style="margin-top:30px;">20.06.12 - <traduz('Auditórios Excelência em TI')?></h1>
            <table>
                <thead>
                    <tr>
                        <th colspan="3"><traduz('dia_1 Auditorio 2')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><traduz('Auditorios Horário')?></td>
                        <td><traduz('Auditorios Palestrantes')?></td>
                        <td><traduz('Auditorios Tema')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_2_horario_1')?></td>
                        <td><traduz('dia1_auditorio_2_palestrante_1')?></td>
                        <td><traduz('dia1_auditorio_2_tema_1')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_2_horario_2')?></td>
                        <td><traduz('dia1_auditorio_2_palestrante_2')?></td>
                        <td><traduz('dia1_auditorio_2_tema_2')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_2_horario_3')?></td>
                        <td><traduz('dia1_auditorio_2_palestrante_3')?></td>
                        <td><traduz('dia1_auditorio_2_tema_3')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_2_horario_4')?></td>
                        <td><traduz('dia1_auditorio_2_palestrante_4')?></td>
                        <td><traduz('dia1_auditorio_2_tema_4')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_2_horario_5')?></td>
                        <td><traduz('dia1_auditorio_2_palestrante_5')?></td>
                        <td><traduz('dia1_auditorio_2_tema_5')?></td>
                    </tr>
                </tbody>
            </table>
            <table>
                <thead>
                    <tr>
                        <th colspan="3"><traduz('dia_1 Auditorio 3')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><traduz('Auditorios Horário')?></td>
                        <td><traduz('Auditorios Palestrantes')?></td>
                        <td><traduz('Auditorios Tema')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_1')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_1')?></td>
                        <td><traduz('dia1_auditorio_3_tema_1')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_2')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_2')?></td>
                        <td><traduz('dia1_auditorio_3_tema_2')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_3')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_3')?></td>
                        <td><traduz('dia1_auditorio_3_tema_3')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_4')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_4')?></td>
                        <td><traduz('dia1_auditorio_3_tema_4')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_5')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_5')?></td>
                        <td><traduz('dia1_auditorio_3_tema_5')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_6')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_6')?></td>
                        <td><traduz('dia1_auditorio_3_tema_6')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_7')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_7')?></td>
                        <td><traduz('dia1_auditorio_3_tema_7')?></td>
                    </tr>
                </tbody>
            </table>            
    -->
            
            <div style="height: 30px;"></div>
            
            <!-- $this->load->view('palestrantes/widget')?> -->
            
        </div>

    </div>
</div>