        <div class="conteudo conteudo-<?=$this->router->class?> conteudo-<?=$this->router->class?>-<?=$this->router->method?>">
        
            <h1>14.06.13 - <?=traduz('Sexta-Feira')?></h1>
            
            <table>
                <thead>
                    <tr>
                        <th colspan="4"><?=traduz('AUDITÓRIO FEBRABAN')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="azul">
                        <td style="width:50px"><?=traduz('sessão')?></td>
                        <td style="width:90px"><?=traduz('horário')?></td>
                        <td style="width:216px"><?=traduz('palestra')?></td>
                        <td><?=traduz('palestrante')?></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('manhã')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>09:00 <?=traduz('às')?> 10:30</td>
                        <td><?=traduz('Inovação na Indústria de TI para os Bancos')?></td>
                        <td>
                            Diebold<br>
                            EMC<br>
                            HP<br>
                            IBM<br>
                            Itautec<br>
                            Perto
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>11:00 <?=traduz('às')?> 12:30</td>
                        <td></td>
                        <td>Ricardo Amorim</td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('tarde')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>14:00 <?=traduz('às')?> 15:30</td>
                        <td>Lideranças de TI debatem Tendências</td>
                        <td>
                            Aurélio Conrado Boni - Bradesco<br>
                            Alexandre de Barros - Itaú Unibanco<br>
                            Joaquim Lima de Oliveira - Caixa<br>
                            Fernando Diaz - Santander Brasil<br>
                            Marco Tavares - HSBC<br>
                            Luiz Carlos Zambaldi - Safra<br><br>
                            Moderador: Gustavo Roxo - Booz & Company
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>16:00 <?=traduz('às')?> 17:00</td>
                        <td></td>
                        <td>
                            Marcelo Tas<br>
                            Bernardinho<br>
                            Arnaldo Jabor
                        </td>
                    </tr>
                </tbody>                
            </table>

            <table>
                <thead>
                    <tr>
                        <th colspan="4"><?=traduz('AUDITÓRIO LINHA DE NEGÓCIOS')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="azul">
                        <td style="width:50px"><?=traduz('sessão')?></td>
                        <td style="width:90px"><?=traduz('horário')?></td>
                        <td style="width:216px"><?=traduz('palestra')?></td>
                        <td><?=traduz('palestrante')?></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('manhã')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>09:00 <?=traduz('às')?> 10:30</td>
                        <td><?=traduz('Transmissão Simultânea')?></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>11:00 <?=traduz('às')?> 12:30</td>
                        <td><?=traduz('Basiléia III')?></td>
                        <td></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('tarde')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>14:00 <?=traduz('às')?> 15:30</td>
                        <td><?=traduz('Segurança da Informação')?></td>
                        <td>
                            Frank Meylan - KPMG (61)<br>
                            Bull (116)
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>16:00 <?=traduz('às')?> 17:00</td>
                        <td><?=traduz('Transmissão Simultânea')?></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>

            <table>
                <thead>
                    <tr>
                        <th colspan="4"><?=traduz('AUDITÓRIO EFICIÊNCIA OPERACIONAL')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="azul">
                        <td style="width:50px"><?=traduz('sessão')?></td>
                        <td style="width:90px"><?=traduz('horário')?></td>
                        <td style="width:216px"><?=traduz('palestra')?></td>
                        <td><?=traduz('palestrante')?></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('manhã')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>09:00 <?=traduz('às')?> 10:30</td>
                        <td><?=traduz('Transmissão Simultânea')?></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>11:00 <?=traduz('às')?> 12:30</td>
                        <td>
                            Appification - Das aplicações para aplicativos (3)
                        </td>
                        <td>
                            Alexandre Winetzki -  Woopi/Stefanini<br>
                            Rodrigo Mulinari - Banco do Brasil<br>
                            Fabian Valverde (Diretor de Mobilidade) - SAP
                        </td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('tarde')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>14:00 <?=traduz('às')?> 15:30</td>
                        <td>Eficiência em Tecnologia Bancária</td>
                        <td>
                            Itautec<br>
                            Carlos Pádua - Diebold <br>
                            Perto<br>
                            Glory<br><br>
                            Moderador: Gustavo Fosse - Banco do Brasil
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>16:00 <?=traduz('às')?> 17:00</td>
                        <td><?=traduz('Transmissão Simultânea')?></td>
                        <td></td>
                    </tr>                   
                </tbody>                
            </table>



            <!-- 
            <h1 style="margin-top:30px;">20.06.12 - <traduz('Auditórios Excelência em TI')?></h1>
            <table>
                <thead>
                    <tr>
                        <th colspan="3"><traduz('dia_1 Auditorio 2')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><traduz('Auditorios Horário')?></td>
                        <td><traduz('Auditorios Palestrantes')?></td>
                        <td><traduz('Auditorios Tema')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_2_horario_1')?></td>
                        <td><traduz('dia1_auditorio_2_palestrante_1')?></td>
                        <td><traduz('dia1_auditorio_2_tema_1')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_2_horario_2')?></td>
                        <td><traduz('dia1_auditorio_2_palestrante_2')?></td>
                        <td><traduz('dia1_auditorio_2_tema_2')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_2_horario_3')?></td>
                        <td><traduz('dia1_auditorio_2_palestrante_3')?></td>
                        <td><traduz('dia1_auditorio_2_tema_3')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_2_horario_4')?></td>
                        <td><traduz('dia1_auditorio_2_palestrante_4')?></td>
                        <td><traduz('dia1_auditorio_2_tema_4')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_2_horario_5')?></td>
                        <td><traduz('dia1_auditorio_2_palestrante_5')?></td>
                        <td><traduz('dia1_auditorio_2_tema_5')?></td>
                    </tr>
                </tbody>
            </table>
            <table>
                <thead>
                    <tr>
                        <th colspan="3"><traduz('dia_1 Auditorio 3')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><traduz('Auditorios Horário')?></td>
                        <td><traduz('Auditorios Palestrantes')?></td>
                        <td><traduz('Auditorios Tema')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_1')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_1')?></td>
                        <td><traduz('dia1_auditorio_3_tema_1')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_2')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_2')?></td>
                        <td><traduz('dia1_auditorio_3_tema_2')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_3')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_3')?></td>
                        <td><traduz('dia1_auditorio_3_tema_3')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_4')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_4')?></td>
                        <td><traduz('dia1_auditorio_3_tema_4')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_5')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_5')?></td>
                        <td><traduz('dia1_auditorio_3_tema_5')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_6')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_6')?></td>
                        <td><traduz('dia1_auditorio_3_tema_6')?></td>
                    </tr>
                    <tr>
                        <td><traduz('dia1_auditorio_3_horario_7')?></td>
                        <td><traduz('dia1_auditorio_3_palestrante_7')?></td>
                        <td><traduz('dia1_auditorio_3_tema_7')?></td>
                    </tr>
                </tbody>
            </table>            
    -->
            
            <div style="height: 30px;"></div>
            
            <!-- $this->load->view('palestrantes/widget')?> -->
            
        </div>

    </div>
</div>