        <div class="conteudo conteudo-<?=$this->router->class?> conteudo-<?=$this->router->class?>-<?=$this->router->method?>">
        
            <h1>12.06.13 - <?=traduz('Quarta-Feira')?></h1>
            
            <table>
                <thead>
                    <tr>
                        <th colspan="4"><?=traduz('AUDITORIO FEBRABAN')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="azul">
                        <td style="width:50px"><?=traduz('sesión')?></td>
                        <td style="width:90px"><?=traduz('horario')?></td>
                        <td style="width:216px"><?=traduz('conferencia')?></td>
                        <td><?=traduz('panelista')?></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('mañana')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>de las 09:00 <?=traduz('a las')?> 10:30</td>
                        <td>
                            Apertura
                        </td>
                        <td>
                            Murilo Portugal - FEBRABAN<br>
                            Virgílio Augusto Fernandes Almeida - Ministerio de Ciencia, Tecnología e Innovación
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>de las 11:00 <?=traduz('a las')?> 12:30</td>
                        <td></td>
                        <td>
                            Jorge Hereda - Presidente de la Caixa<br><br>
                            Moderador: Murilo Portugal - FEBRABAN
                        </td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('tarde')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>de las 14:00 <?=traduz('a las')?> 15:30</td>
                        <td>The sense of nonsense</td>
                        <td>Eric Haseltine - Haseltine Partners LLC
                            Moderador: Jorge Vacarini
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>de las 16:00 <?=traduz('a las')?> 17:00</td>
                        <td>El NEXO de las fuerzas: Cómo Mobile Technologies, Nube, Social y Big Data podrán ampliar la interacción digital y la interacción "face to face"</td>
                        <td>
                            Donald Feinberg  - Gartner<br>
                            Mary Knox  - Gartner
                        </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="4"><?=traduz('Ceremonia de apertura de la Exposición y visita a los estands de los Patrocinadores / Coffee break')?></td>
                    </tr>
                </tfoot>
            </table>

            <table>
                <thead>
                    <tr>
                        <th colspan="4"><?=traduz('AUDITORIO LÍNEA DE NEGOCIO')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="azul">
                        <td style="width:50px"><?=traduz('sesión')?></td>
                        <td style="width:90px"><?=traduz('horario')?></td>
                        <td style="width:216px"><?=traduz('conferencia')?></td>
                        <td><?=traduz('panelista')?></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('mañana')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>de las 09:00 <?=traduz('a las')?> 10:30</td>
                        <td>Transmisión simultánea de la Apertura</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>de las 11:00 <?=traduz('a las')?> 12:30</td>
                        <td>Transmisión simultánea</td>
                        <td></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('tarde')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>de las 14:00 <?=traduz('a las')?> 15:30</td>
                        <td>Interactuando con Clientes y Personal con Movilidad </td>
                        <td>
                            Onildo Andrade Junior - Banco do Brasil<br>
                            Wendy Arnott - TD Bank (Canadá)
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>de las 16:00 <?=traduz('a las')?> 17:00</td>
                        <td>Impacto de las Megatendencias globales en los Servicios Financieros</td>
                        <td>Richard Sear - Frost & Sullivan </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>de las 17:00 <?=traduz('a las')?> 17:15</td>
                        <td colspan="2" style="text-align:center;"><?=traduz('intervalo')?></td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>de las 17:15 <?=traduz('a las')?> 18:15</td>
                        <td>Los grandes proyectos del BMF&BOVESPA para transformar el mercado financiero</td>
                        <td>Viviane El Banate Basso - BMF&Bovespa</td>
                    </tr>
                </tbody>
            </table>

            <table>
                <thead>
                    <tr>
                        <th colspan="4"><?=traduz('AUDITÓRIO EFICIÊNCIA OPERACIONAL')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="azul">
                        <td style="width:50px"><?=traduz('sesión')?></td>
                        <td style="width:90px"><?=traduz('horario')?></td>
                        <td style="width:216px"><?=traduz('conferencia')?></td>
                        <td><?=traduz('panelista')?></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('mañana')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>de las 09:00 <?=traduz('a las')?> 10:30</td>
                        <td>Transmisión simultánea de la Apertura</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>de las 11:00 <?=traduz('a las')?> 12:30</td>
                        <td>Transmisión simultánea</td>
                        <td></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('tarde')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>de las 14:00 <?=traduz('a las')?> 15:30</td>
                        <td>BYOD - Bring Your On Device</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>de las 16:00 <?=traduz('a las')?> 17:00</td>
                        <td>Cloud Computing en la Práctica</td>
                        <td>
                            Carlos Alberto Duque - Capgemini <br>
                            Felipe Avila - Brasilcap <br>
                            Jose Luis Spagnuolo - IBM  <br>
                            Rodrigo Gazzaneo - EMC <br>
                            <br>
                            Moderador: Manuel Vaz - Santander
                        </td>
                    </tr>                   
                </tbody>                
            </table>

            <div style="height: 30px;"></div>
            
            <!-- $this->load->view('palestrantes/widget')?> -->
            
        </div>

    </div>
</div>