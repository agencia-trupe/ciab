        <div class="conteudo conteudo-<?=$this->router->class?> conteudo-<?=$this->router->class?>-<?=$this->router->method?>">
        
            <h1>13.06.13 - <?=traduz('Quinta-Feira')?></h1>
            
            <table>
                <thead>
                    <tr>
                        <th colspan="4"><?=traduz('AUDITORIUM FEBRABAN')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="azul">
                        <td style="width:50px"><?=traduz('session')?></td>
                        <td style="width:90px"><?=traduz('schedule')?></td>
                        <td style="width:216px"><?=traduz('lecture')?></td>
                        <td><?=traduz('panelist')?></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('morning')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>09:00 <?=traduz('to')?> 10:30</td>
                        <td>How We Win: Engagement, Empowerment, and Communications in an Interconnected World</td>
                        <td>Michael Slaby - TomorrowVentures <br>
                            Moderator: Leignes Jorge Andreatti - HSBC
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>11:00 <?=traduz('to')?> 12:30</td>
                        <td>Challenges, trends and the role of IT for  insurance industry</td>
                        <td>Marco Antonio Rossi - Bradesco Seguros</td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('afternoon')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>14:00 <?=traduz('to')?> 15:30</td>
                        <td>Bank Back Office Challenges</td>
                        <td>                            
                            Hideraldo Dwight Leitão - Banco do Brasil <br>
                            Pedro Paulo de Lima Gonçalves - CAIXA <br>
                            Maurício Minas - Bradesco <br>
                            Moderator: Sonia Penteado <br>
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>16:00 <?=traduz('to')?> 17:00</td>
                        <td>Brazil's Infrastructure and Innovation</td>
                        <td>
                            Moderator: Murilo Portugal - FEBRABAN
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>17:00 <?=traduz('to')?> 17:15</td>
                        <td colspan="2" style="text-align:center;"><?=traduz('break')?></td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>17:15 <?=traduz('to')?> 18:15</td>
                        <td>Brazil's Infrastructure and Innovation</td>
                        <td>
                            Moderator: Murilo Portugal - FEBRABAN
                        </td>
                    </tr>
                </tbody>                
            </table>

            <table>
                <thead>
                    <tr>
                        <th colspan="4"><?=traduz('AUDITORIUM BUSINESS LINE')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="azul">
                        <td style="width:50px"><?=traduz('session')?></td>
                        <td style="width:90px"><?=traduz('schedule')?></td>
                        <td style="width:216px"><?=traduz('lecture')?></td>
                        <td><?=traduz('panelist')?></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('morning')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>09:00 <?=traduz('to')?> 10:30</td>
                        <td>BPO - Business Process Outsourcing</td>
                        <td>Mário Ferreira Neto - Caixa <br>
                            Anurag Shah - HP
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>11:00 <?=traduz('to')?> 12:30</td>
                        <td>Innovation Projects or Culture in Innovation</td>
                        <td>
                            Reginaldo Arakaki - Scopus Tecnologia <br>
                            Douglas Tevis Francisco - Bradesco <br>
                            Ricardo Guerra - Itaú Unibanco<br>
                            Evandro Ferreira de Avelar - Caixa <br>
                            <br>
                            Moderator: Cirano Silveira - HP
                        </td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('afternoon')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>14:00 <?=traduz('to')?> 15:30</td>
                        <td>CIOs of other industries</td>
                        <td>
                            Cláudio Prado - Editora Abril<br>
                            Marcos Vinicius Ferreira - SERPRO <br>
                            Moderator: Cassio Dreyfuss - Gartner
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>16:00 <?=traduz('to')?> 17:00</td>
                        <td>Customer Experience vs. Loyalty</td>
                        <td>David Goslin  - Deloitte <br>
                            Moderador: Paulo Marcelo Moreira - Capgemini <br>
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>17:00 <?=traduz('to')?> 17:15</td>
                        <td colspan="2" style="text-align:center;"><?=traduz('break')?></td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>17:15 <?=traduz('to')?> 18:15</td>
                        <td>Converting data into a strategic business asset for Banks</td>
                        <td>
                            Hyong Kim - Ernst & Young <br>
                            Moderator: TBD
                        </td>
                    </tr>
                </tbody>
            </table>

            <table>
                <thead>
                    <tr>
                        <th colspan="4"><?=traduz('AUDITORIUM OPERATIONAL EFFICIENCY')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="azul">
                        <td style="width:50px"><?=traduz('session')?></td>
                        <td style="width:90px"><?=traduz('schedule')?></td>
                        <td style="width:216px"><?=traduz('lecture')?></td>
                        <td><?=traduz('panelist')?></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('morning')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>09:00 <?=traduz('to')?> 10:30</td>
                        <td>Strategy for development of an IT Governance Model  </td>
                        <td>
                           Mauro Cesar Bernardes - USP<br>
                           Ken Janssen - J.P. Morgan<br>
                           Joaquim Kavakama - CIP<br><br>
                           Moderator: Anderson Itaborahy- Banco do Brasil
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>11:00 <?=traduz('to')?> 12:30</td>
                        <td>Channels of Customer Relations</td>
                        <td></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('afternoon')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>14:00 <?=traduz('to')?> 15:30</td>
                        <td>Big Data</td>
                        <td>
                            Marcos Panichi - IBM <br>
                            Jason White - HP <br>
                            Mike Huckaby - EMC <br>
                            <br>
                            Moderator: Ricardo Orlando - Itaú Unibanco
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>16:00 <?=traduz('to')?> 17:00</td>
                        <td>Infrastructure as a Source of Competitive Advantage in Financial Services Sector</td>
                        <td>
                            Mary Knox - Gartner<br>
                            Gustavo Santana - Cisco<br>
                            José Isern - Itaú Unibanco<br>
                            Fernando Diaz - Santander <br>
                            Paulo Cesar de Almeida Toledo - Banco do Brasil <br>
                            Moderator: Waldemar Ruggiero - Bradesco
                        </td>
                    </tr>

                    <tr>
                        <td>2&ordf;</td>
                        <td>17:00 <?=traduz('to')?> 17:15</td>
                        <td colspan="2" style="text-align:center;"><?=traduz('break')?></td>
                    </tr>

                    <tr>
                        <td>2&ordf;</td>
                        <td>17:15 <?=traduz('to')?> 18:15</td>
                        <td>Infrastructure as a Source of Competitive Advantage in Financial Services Sector</td>
                        <td>
                            Mary Knox - Gartner<br>
                            Gustavo Santana - Cisco<br>
                            José Isern - Itaú Unibanco<br>
                            Fernando Diaz - Santander <br>
                            Paulo Cesar de Almeida Toledo - Banco do Brasil <br>
                            Moderator: Waldemar Ruggiero - Bradesco
                        </td>
                    </tr>                   
                </tbody>                
            </table>


            <div style="height: 30px;"></div>
            
            <!-- $this->load->view('palestrantes/widget')?> -->
            
        </div>

    </div>
</div>