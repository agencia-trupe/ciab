        <div class="conteudo conteudo-<?=$this->router->class?> conteudo-<?=$this->router->class?>-<?=$this->router->method?>">
        
            <h1>12.06.13 - <?=traduz('Quarta-Feira')?></h1>
            
            <table>
                <thead>
                    <tr>
                        <th colspan="4"><?=traduz('AUDITORIUM FEBRABAN')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="azul">
                        <td style="width:50px"><?=traduz('session')?></td>
                        <td style="width:90px"><?=traduz('schedule')?></td>
                        <td style="width:216px"><?=traduz('lecture')?></td>
                        <td><?=traduz('panelist')?></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('morning')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>09:00 <?=traduz('to')?> 10:30</td>
                        <td>
                            Opening
                        </td>
                        <td>
                            Murilo Portugal - FEBRABAN<br>
                            Virgílio Augusto Fernandes Almeida - Ministry of Science, Technology and Innovation
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>11:00 <?=traduz('to')?> 12:30</td>
                        <td></td>
                        <td>
                            Jorge Hereda - President of Caixa<br>
                            Moderator: Murilo Portugal - FEBRABAN
                        </td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('afternoon')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>14:00 <?=traduz('to')?> 15:30</td>
                        <td>The sense of nonsense</td>
                        <td>Eric Haseltine - Haseltine Partners LLC <br>
                            Moderator: Jorge Vacarini
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>16:00 <?=traduz('to')?> 17:00</td>
                        <td>The NEXUS OF FORCES: How Mobile, Cloud, Social and Big Data Technologies may extend digital interaction and "face-to-face" interaction</td>
                        <td>Donald Feinberg  - Gartner<br>Mary Knox  - Gartner</td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>17:00 <?=traduz('to')?> 17:15</td>
                        <td colspan="2" style="text-align:center;"><?=traduz('break')?></td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>17:15 <?=traduz('to')?> 18:15</td>
                        <td>The NEXO of the forces: How the Mobile Technologies, Cloud, Social and Big Data may expand digital interaction and the interaction "face-to-face"</td>
                        <td>Donald Feinberg  - Gartner<br>Mary Knox  - Gartner</td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="4"><?=traduz('Opening ceremony of the Exhibition and visit to the Sponsors students / Coffee break')?></td>
                    </tr>
                </tfoot>
            </table>

            <table>
                <thead>
                    <tr>
                        <th colspan="4"><?=traduz('AUDITORIUM BUSINESS LINE')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="azul">
                        <td style="width:50px"><?=traduz('session')?></td>
                        <td style="width:90px"><?=traduz('schedule')?></td>
                        <td style="width:216px"><?=traduz('lecture')?></td>
                        <td><?=traduz('panelist')?></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('morning')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>09:00 <?=traduz('to')?> 10:30</td>
                        <td>Simultaneous broadcast of the Opening</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>11:00 <?=traduz('to')?> 12:30</td>
                        <td>Simultaneous broadcast</td>
                        <td></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('afternoon')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>14:00 <?=traduz('to')?> 15:30</td>
                        <td>Interacting with Customers and Employees with Mobility</td>
                        <td>
                            Onildo Andrade Junior - Banco do Brasil<br>
                            Wendy Arnott - TD Bank (Canada)
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>16:00 <?=traduz('to')?> 17:00</td>
                        <td>Impact of Global Megatrends in Financial Services</td>
                        <td>Richard Sear - Frost & Sullivan</td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>17:00 <?=traduz('to')?> 17:15</td>
                        <td colspan="2" style="text-align:center;"><?=traduz('intervalo')?></td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>17:15 <?=traduz('to')?> 18:15</td>
                        <td>The major BMF&Bovespa projects that will transform the financial market</td>
                        <td>Viviane El Banate Basso - BMF&Bovespa</td>
                    </tr>
                </tbody>
            </table>

            <table>
                <thead>
                    <tr>
                        <th colspan="4"><?=traduz('AUDITORIUM OPERATIONAL EFFICIENCY')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="azul">
                        <td style="width:50px"><?=traduz('session')?></td>
                        <td style="width:90px"><?=traduz('schedule')?></td>
                        <td style="width:216px"><?=traduz('lecture')?></td>
                        <td><?=traduz('panelist')?></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('morning')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>09:00 <?=traduz('to')?> 10:30</td>
                        <td>Simultaneous broadcast of the Opening</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>11:00 <?=traduz('to')?> 12:30</td>
                        <td>Simultaneous broadcast</td>
                        <td></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('afternoon')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>14:00 <?=traduz('to')?> 15:30</td>
                        <td>BYOD - Bring Your On Device</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>16:00 <?=traduz('to')?> 17:00</td>
                        <td>Cloud Computing in Practice</td>
                        <td>
                            Carlos Alberto Duque - Capgemini <br>
                            Felipe Avila - HP<!--Brasilcap--><br>
                            <!--Jose Luis Spagnuolo - IBM-->
                            Rodrigo Gazzaneo - EMC <br>
                            <br>
                            Moderador: Manuel Vaz - Santander
                        </td>
                    </tr>                   
                </tbody>                
            </table>

            <div style="height: 30px;"></div>
            
            <!-- $this->load->view('palestrantes/widget')?> -->
            
        </div>

    </div>
</div>