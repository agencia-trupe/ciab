        <div class="conteudo conteudo-<?=$this->router->class?> conteudo-<?=$this->router->class?>-<?=$this->router->method?>">
        
            <h1>13.06.13 - <?=traduz('Quinta-Feira')?></h1>
            
            <table>
                <thead>
                    <tr>
                        <th colspan="4"><?=traduz('AUDITÓRIO FEBRABAN')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="azul">
                        <td style="width:50px"><?=traduz('sessão')?></td>
                        <td style="width:90px"><?=traduz('horário')?></td>
                        <td style="width:216px"><?=traduz('palestra')?></td>
                        <td><?=traduz('palestrante')?></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('manhã')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>09:00 <?=traduz('às')?> 10:30</td>
                        <td>How We Win: Engagement, Empowerment, and Communications in an Interconnected World</td>
                        <td>Michael Slaby - TomorrowVentures <br>
                            Moderador: Leignes Jorge Andreatti - HSBC
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>11:00 <?=traduz('às')?> 12:30</td>
                        <td>Desafios, Tendências e o papel da TI para o setor de Seguros</td>
                        <td>Marco Antonio Rossi - Bradesco Seguros</td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('tarde')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>14:00 <?=traduz('às')?> 15:30</td>
                        <td>Desafios do Back Office dos Bancos</td>
                        <td>Hideraldo Dwight Leitão - Banco do Brasil <br>
                            Pedro Paulo de Lima Gonçalves - CAIXA <br>
                            Maurício Minas - Bradesco <br> <br>
                            Moderador: Sonia Penteado 
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>16:00 <?=traduz('às')?> 17:00</td>
                        <td>Infraestrutura do Brasil e Inovação</td>
                        <td>
                            Moderador: Murilo Portugal - FEBRABAN
                        </td>
                    </tr>
                </tbody>                
            </table>

            <table>
                <thead>
                    <tr>
                        <th colspan="4"><?=traduz('AUDITÓRIO LINHA DE NEGÓCIOS')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="azul">
                        <td style="width:50px"><?=traduz('sessão')?></td>
                        <td style="width:90px"><?=traduz('horário')?></td>
                        <td style="width:216px"><?=traduz('palestra')?></td>
                        <td><?=traduz('palestrante')?></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('manhã')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>09:00 <?=traduz('às')?> 10:30</td>
                        <td>BPO - Business Process Outsourcing</td>
                        <td>Mário Ferreira Neto - Caixa <br>
                            Anurag Shah - HP
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>11:00 <?=traduz('às')?> 12:30</td>
                        <td>Projetos de Inovação ou Cultura em Inovação</td>
                        <td>
                            Reginaldo Arakaki - Scopus Tecnologia <br>
                            Ricardo Guerra - Itaú Unibanco<br>
                            Douglas Tevis Francisco - Bradesco <br>
                            Evandro Ferreira de Avelar - Caixa <br> 
                            Moderador: Cirano Silveira - HP<br>
                        </td>  
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('tarde')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>14:00 <?=traduz('às')?> 15:30</td>
                        <td>CIO's de Outras Indústrias</td>
                        <td>
                            Cláudio Prado - Editora Abril<br>
                            Marcos Vinicius Ferreira - SERPRO<br>
                            Moderador: Cassio Dreyfuss - Gartner
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>16:00 <?=traduz('às')?> 17:00</td>
                        <td><?=traduz('Experiencia de Clientes vs Fidelização')?></td>
                        <td>David Goslin  - Deloitte <br>
                            <br>
                            Moderador: Paulo Marcelo Moreira - Capgemini
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>17:00 <?=traduz('às')?> 17:15</td>
                        <td colspan="2" style="text-align:center;"><?=traduz('intervalo')?></td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>17:15 <?=traduz('às')?> 18:15</td>
                        <td>Converting data into a strategic business asset for Banks </td>
                        <td>Hyong Kim - Ernst & Young</td>
                    </tr>
                </tbody>
            </table>

            <table>
                <thead>
                    <tr>
                        <th colspan="4"><?=traduz('AUDITÓRIO EFICIÊNCIA OPERACIONAL')?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="azul">
                        <td style="width:50px"><?=traduz('sessão')?></td>
                        <td style="width:90px"><?=traduz('horário')?></td>
                        <td style="width:216px"><?=traduz('palestra')?></td>
                        <td><?=traduz('palestrante')?></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('manhã')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>09:00 <?=traduz('às')?> 10:30</td>
                        <td>Estratégia para o desenvolvimento de um modelo de Governança de TI </td>
                        <td>
                            Mauro Cesar Bernardes - USP<br>
                            Ken Janssens - J.P. Morgan<br>
                            Joaquim Kavakama - CIP<br><br>
                            Moderador: Gustavo de Souza Fosse- Banco do Brasil
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>11:00 <?=traduz('às')?> 12:30</td>
                        <td>Canais de Relacionamento com Clientes</td>
                        <td></td>
                    </tr>
                    <tr class="azul">
                        <td colspan="4"><?=traduz('tarde')?></td>
                    </tr>
                    <tr>
                        <td>1&ordf;</td>
                        <td>14:00 <?=traduz('às')?> 15:30</td>
                        <td>Big Data</td>
                        <td>
                            Marcos Panichi - IBM<br>
                            Jason White - HP <br>
                            Mike Huckaby - EMC <br>
                            Moderador: Ricardo Orlando - Itaú Unibanco
                        </td>
                    </tr>
                    <tr>
                        <td>2&ordf;</td>
                        <td>16:00 <?=traduz('às')?> 17:00</td>
                        <td>Infraestrutura como Fonte de Vantagem Competitiva no Setor de Serviços Financeiros</td>
                        <td>
                            Mary Knox  -Gartner<br>
                            Gustavo Santana - Cisco<br>
                            José Isern - Itaú Unibanco<br>
                            Fernando Diaz - Santander <br>                            
                            Paulo Cesar de Almeida Toledo - Banco do Brasil <br>
                            Moderador: Waldemar Ruggiero - Bradesco
                        </td>
                    </tr>                   
                </tbody>                
            </table>


            
            <div style="height: 30px;"></div>
            
            <!-- $this->load->view('palestrantes/widget')?> -->
            
        </div>

    </div>
</div>