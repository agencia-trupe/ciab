        <div class="conteudo conteudo-release">
        
            <h1><?=traduz('IMPRENSA_ASSESSORIA_TITULO')?></h1>
            
            <p><?=traduz('IMPRENSA_ASSESSORIA_TEXTO_1')?>:</p>
            
            <p class="destaque"><span class="azul-escuro">Diretoria de Comunicação da FEBRABAN</span><br>
                                (11) 3186-9832</p>
            
            <p class="destaque"><span class="azul-escuro">ABCE Comunicação</span> <br>
                                (11) 3862-0110</p>
            
            <p><?=traduz('IMPRENSA_ASSESSORIA_TEXTO_2')?></p>
            
            <div id="formulario-wrapper">
                <form id="form-release" method="post" action="imprensa/enviar">
                    
                    <label><?=traduz('IMPRENSA_ASSESSORIA_FORM_1')?><input type="text" name="nome" required></label>
                    
                    <label><?=traduz('IMPRENSA_ASSESSORIA_FORM_2')?><input type="email" name="email" required></label>
                    
                    <label><?=traduz('IMPRENSA_ASSESSORIA_FORM_3')?><input type="text" name="mtb" required></label>
                    
                    <label><?=traduz('IMPRENSA_ASSESSORIA_FORM_4')?><input type="text" name="empresa" required></label>
                    
                    <label><?=traduz('IMPRENSA_ASSESSORIA_FORM_5')?><input type="text" name="empresa-site"></label>
                    
                    <label><?=traduz('IMPRENSA_ASSESSORIA_FORM_6')?><input type="text" name="area-atuacao"></label>
                    
                    <label><?=traduz('IMPRENSA_ASSESSORIA_FORM_7')?><input type="text" name="telefone" required></label>
                    
                    <input type="submit" value="<?=traduz('IMPRENSA_ASSESSORIA_FORM_SUBMIT')?>" class="botao">
                </form>
            </div>
            <div id="resposta-target">
                <?if($envio):?>
                    <?if($status):?>
                        <?=traduz('IMPRENSA_ASSESSORIA_FORM_OK')?>
                    <?else:?>
                        <?=traduz('IMPRENSA_ASSESSORIA_FORM_ERROR')?>
                    <?endif;?>
                    
                    <script>
                        $(document).ready( function(){
                            $('#formulario-wrapper').hide(0, function(){
                                $('#resposta-target').delay(5000).hide('fade', function(){
                                    $('#formulario-wrapper').show('fade')
                                })
                            })
                        })
                    </script>
                <?endif;?>
            </div>
            
        </div>

    </div>
</div>