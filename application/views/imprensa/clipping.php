        <div class="conteudo conteudo-clipping">
        
            <h1>Clipping</h1>
            
            <?if($clippings):?>
            
                <div id="clips">
                
                    <?foreach($clippings as $clip):?>
                        <div class="lista-clipping">
                            <h2><?=$clip->titulo?></h2>
                            <?=str_replace('/','.', formataData($clip->data, 'mysql2br'))?> - <?=$clip->descritivo?><br>
                            <a href="_pdfs/clippings/<?=$clip->arquivo?>" target="_blank">CLIQUE AQUI E FAÇA O DOWNLOAD DO CLIPPING</a>
                        </div>
                    <?endforeach;?>

                </div>
                
            <?else:?>
                <h2>Nenhum Clipping cadastrado</h2>
            <?endif;?>                
            
            <?if($pagination):?>
                <div id="navegacao" class="sombra-pequena">
                    <?=$pagination?>
                </div>
            <?endif;?>          
            
        </div>

    </div>
</div>