<div class="centropad main-<?=$this->router->class?>">
    
    <div id="container">
    
        <div id="menu-lateral">
        
            <h3><?=traduz('IMPRENSA')?></h3>
            
            <ul class="submenu-imprensa">
                <li id="submn-assessoria"><a href="imprensa/assessoria"><?=traduz('IMPRENSA_SUBMENU_1')?></a></li>
                <div class="faixa-sob-item-menu"><div></div></div>
                
                <?if(prefixo('')=='pt_'):?>
                    <li id="submn-releases"><a href="imprensa/releases">Releases</a></li>
                    <div class="faixa-sob-item-menu"><div></div></div>
                    
                    <li id="submn-clipping"><a href="imprensa/clipping">Clipping</a></li>
                    <div class="faixa-sob-item-menu"><div></div></div>
                <?endif;?>
            </ul>
            
            <script defer>$(document).ready( function(){ $('#submn-<?=$this->router->method?>').addClass('subativo'); });</script>
            