        <div class="conteudo conteudo-clipping">
        
            <h1>Releases</h1>
            
            <?if($clippings):?>
            
                <div id="clips">
                
                    <?foreach($clippings as $clip):?>
                        <div class="lista-clipping">
                            <span class="data"><?=str_replace('/','.',formataData($clip->data, 'mysql2br'))?></span><br>
                            <h2><?=$clip->titulo?></h2>
                            <?=$clip->descritivo?>
                            <a href="_pdfs/releases/<?=$clip->arquivo?>" target="_blank">CLIQUE AQUI E FAÇA O DOWNLOAD DO RELEASE</a>
                        </div>
                    <?endforeach;?>

                </div>
                
            <?else:?>
                <h2>Nenhum Release cadastrado</h2>
            <?endif;?>                
            
            <?if($pagination):?>
                <div id="navegacao" class="sombra-pequena">
                    <?=$pagination?>
                </div>
            <?endif;?>          
            
        </div>

    </div>
</div>