<div class="centropad main-<?=$this->router->class?>">
    
    <div id="container">
    
        <div id="menu-lateral">
        
            <h3>NOTÍCIAS</h3>
            
            <nav>
                <select id="filtro-noticias">
                    <option value="0">Filtre</option>
                    <?foreach($categorias as $cat):?>
                        <option value="<?=$cat->slug?>" <?if(isset($slug) && $slug == $cat->slug) echo " selected"?>><?=$cat->titulo?></option>
                    <?endforeach;?>
                </select>
            </nav>
            
            <script>
                $(document).ready( function(){
                    $('#filtro-noticias').change( function(){
                        if($(this).val() != 0){
                            location = BASE + 'noticias/categoria/' + $(this).val()
                        }
                    });
                });
            </script>