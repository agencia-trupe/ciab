       
        <div class="conteudo conteudo-noticia">
            
            <img src="_imgs/noticias/thumbs/<?=$registro[0]->imagem?>" alt="<?=$registro[0]->titulo?>">
            
            <h1><?=$registro[0]->titulo?></h1>
            <h2><?=formataData($registro[0]->data,'mysql2br')?> <?=($registro[0]->subtitulo) ? ' - '.$registro[0]->subtitulo : ''?></h2>
            <div class="height-wrapper">
            <?=$registro[0]->texto?>
            </div>
            
            <?$this->load->view('common/compartilhe')?>
            
            <?if($ultimas):?>
            <div class="mais-noticias sombra-pequena">
            
                <a href="noticias" class="ver-todas"><?=traduz('NOTICIAS_VER_TODAS')?></a>
                
                <h1><?=traduz('NOTICIAS_ULTIMAS_TITULO')?></h1>
                
                <?foreach($ultimas as $ult):?>
                    <div class="noticia-list">
                        <a href="noticias/detalhe/<?=$ult->id?>">
                            <h2><?=formataData($ult->data, 'mysql2br')?> <?=($ult->titulo) ? ' - '.$ult->titulo : ''?></h2>
                            <p>
                                <?=$ult->olho?>
                            </p>
                        </a>
                    </div>                
                <?endforeach;?>
                
            </div>
            <?endif;?>
            
        </div>
    
    </div>
</div>