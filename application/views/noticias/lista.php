        <div class="conteudo">
            
            <?if($noticias):?>
            
                <?foreach($noticias as $val):?>
                    <div class="noticia">
                        <a href="noticias/detalhe/<?=$val->id?>">
                            <img src="_imgs/noticias/thumbs/<?=$val->imagem?>" alt="<?=$val->titulo?>">
                            <h1><?=$val->titulo?></h1>
                            <h2><?=formataData($val->data, 'mysql2br')?><?=($val->subtitulo) ? ' - '.$val->subtitulo : ''?></h2>
                            <p><?=$val->olho?></p>
                        </a>
                    </div>                    
                <?endforeach;?>
                
            <?else:?>
                <h1>Nenhuma notícia cadastrada</h1>
            <?endif;?>
            
            <?if($pagination):?>
                <div id="navegacao" class="sombra-pequena">
                    <?=$pagination?>
                </div>
            <?endif;?>
        </div>
    
    </div>
</div>