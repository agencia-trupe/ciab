        <div class="conteudo conteudo-premios conteudo-premios-vencedores">
        
            <h1><?=traduz('PREMIOS_VENCEDORES_TITULO')?></h1>
            
            <p class="strong"><?=traduz('PREMIOS_VENCEDORES_SUBTITULO')?></p>
            
            <?=traduz('PREMIOS_VENCEDORES_TEXTO_1')?>
            
            
            <p class="strong-cinza"><?=traduz('PREMIOS_VENCEDORES_COMUNICADO')?></p>
            
            <?=traduz('PREMIOS_VENCEDORES_TEXTO_2')?>            
            
            <table>
                <tr>
                    <th><?=traduz('PREMIOS_VENCEDORES_TABELA_1')?></th>
                    <th><?=traduz('PREMIOS_VENCEDORES_TABELA_2')?></th>
                    <th><?=traduz('PREMIOS_VENCEDORES_TABELA_3')?></th>
                </tr>
                <tr>
                    <td class="colocacao">1&deg;</td>
                    <td>
                        Calebe Luo<br>	
                        Raul Duque dos Santos<br>
                        Carolina da Cunha Kuroda<br>
                        Marco Flavio Trajano Mattos
                    </td>
                    <td style="font-weight:bold;">
                        Mobile Social Banking
                    </td>
                </tr>
                <tr>
                    <td class="colocacao">2&deg;</td>
                    <td>
                        Felipe Serra Peixoto dos Santos<br>
                        Igor Gomes da Silva<br>
                        Rafael Teruo Castilha Yamauchi<br>
                    </td>
                    <td style="font-weight:bold;">
                        A Computação Ubíqua
                    </td>
                </tr>
                <tr>
                    <td class="colocacao">3&deg;</td>
                    <td>
                        Márcia Cristina dos Santos
                    </td>
                    <td style="font-weight:bold;">
                        Bank to Go
                    </td>
                </tr>
                <tr>
                    <td class="colocacao" style="font-size:16px;" rowspan="2"><?=traduz('Menção Honrosa')?></td>
                    <td>
                        Leonardo Seiji Kuamoto<br>
                        Carlos Alberto Heuser<br>
                        Eduardo Leguisamo<br>
                        Lucas Sérgio<br>
                        Luis Sardi Mergen
                    </td>
                    <td style="font-weight:bold;">
                        Rompendo a Barreira do Firewall
                    </td>
                </tr>
                <tr>
                    <td>
                        Carlos Calvo Canabal<br>
                        Gabriela Rigon Calvoi
                    </td>
                    <td style="font-weight:bold;">
                        Tecnologia na Terceira Idade
                    </td>
                </tr>                  
            </table>
            
            <h1><?=traduz('PREMIOS_VENCEDORES_TWITTER_TITULO')?></h1>
            
            <p class="strong"><?=traduz('PREMIOS_VENCEDORES_TWITTER_SUBTITULO')?></p>
            
            <p class="cinza"><?=traduz('PREMIOS_VENCEDORES_TWITTER_CHAMADA')?></p>
            
            <?=traduz('PREMIOS_VENCEDORES_TWITTER_TEXTO')?>
            
        </div>

    </div>
</div>