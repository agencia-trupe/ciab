        <div class="conteudo conteudo-premios conteudo-premios-vencedores">
        
            <h1><?=traduz('PREMIOS_VENCEDORES_2012_TITULO')?></h1>
            
            <p class="strong"><?=traduz('PREMIOS_VENCEDORES_2012_PROJETO')?></p>
            
            <p class="strong-cinza" style="padding-left:15px;"><?=traduz('PREMIOS_VENCEDORES_2012_PROJETO_1')?></p>

            <p class="strong"><?=traduz('PREMIOS_VENCEDORES_2012_GRUPO')?></p>
            
            <div style="padding-left:15px;">
            <?=traduz('PREMIOS_VENCEDORES_2012_GRUPO_1')?>
            </div>
            
            <hr style="margin:20px 0;">            

            <p class="strong"><?=traduz('PREMIOS_VENCEDORES_2012_PROJETO')?></p>
            
            <p class="strong-cinza" style="padding-left:15px;"><?=traduz('PREMIOS_VENCEDORES_2012_PROJETO_2')?></p>

            <p class="strong"><?=traduz('PREMIOS_VENCEDORES_2012_GRUPO')?></p>
            
            <div style="padding-left:15px;">
            <?=traduz('PREMIOS_VENCEDORES_2012_GRUPO_2')?>            
            </div>
            
            <hr style="margin:20px 0;">

            <p class="strong"><?=traduz('PREMIOS_VENCEDORES_2012_PROJETO')?></p>
            
            <p class="strong-cinza" style="padding-left:15px;"><?=traduz('PREMIOS_VENCEDORES_2012_PROJETO_3')?></p>
            
            <p class="strong"><?=traduz('PREMIOS_VENCEDORES_2012_GRUPO')?></p>
            
            <div style="padding-left:15px;">
            <?=traduz('PREMIOS_VENCEDORES_2012_GRUPO_3')?>            
            </div>
            
        </div>

    </div>
</div>