<div class="centropad main-<?=$this->router->class?>">
    
    <div id="container">
    
        <div id="menu-lateral">
        
            <h3><?=traduz('PREMIOS')?></h3>
            
            <ul class="submenu-premios">
                <li id="submn-ciab"><a href="premios/ciab"><?=traduz('PREMIOS_SUBMENU_1')?></a></li>
                <div class="faixa-sob-item-menu"><div></div></div>
                
                <!--
                <li id="submn-twitter"><a href="premios/twitter">Prêmio Twitter 2012</a></li>
                <div class="faixa-sob-item-menu"><div></div></div>
                -->
                <li id="submn-vencedores2012"><a href="premios/vencedores2012"><?=traduz('PREMIOS_SUBMENU_3')?></a></li>
                <div class="faixa-sob-item-menu"><div></div></div>
                <li id="submn-vencedores"><a href="premios/vencedores"><?=traduz('PREMIOS_SUBMENU_2')?></a></li>
                <div class="faixa-sob-item-menu"><div></div></div>
            </ul>
            
            <script defer>$(document).ready( function(){ $('#submn-<?=$this->router->method?>').addClass('subativo'); });</script>
            