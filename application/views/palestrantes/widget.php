<link rel="stylesheet" href="css/widget.css">
<script defer>
    $(document).ready( function(){
        $.post(BASE + 'ajax/pegaPalestrantes', {}, function(retorno){
            retorno = JSON.parse(retorno)
            $('#widget-inner').html('')
            retorno.map( function(self){
                html = "<div class='palest'>"
                html += "<a href='palestrantes/detalhe/"+self.slug+"'>"
                html += "<img src='_imgs/palestrantes/"+self.imagem_on+"' class='on'>"
                html += "<img src='_imgs/palestrantes/"+self.imagem_off+"' class='off'>"
                html += "</a>"
                html += "<div class='palest-legenda'>"+self.nome+"</div>"
                html += "</div>"
                $('#widget-inner').append(html)
            })
            $('.palest').hover( function(){
                $('.palest-legenda').slideUp('fast');
                $(this).find('>a img.off').fadeOut('3000');
                $(this).find('.palest-legenda').slideDown('normal');
            }, function(){
                $(this).find('>a img.off').fadeIn('3000');
                $(this).find('.palest-legenda').slideUp('normal');    
            });
        })
    });
</script>

<div id="widget-nav">
    <div id="widget-nav-left" data-pad="156" data-move="palest"></div>
    <div id="widget-nav-right" data-pad="156" data-move="palest"></div>
    <div id="widget-inner">

    </div>
</div>