        <div class="conteudo detalhe-palestrante">
            
            <div id="palestrante-detalhe">
                
                <img src="_imgs/palestrantes/<?=$palestrante->imagem_ampliada?>">
                
                <div class="texto">
                    <h1><?=$palestrante->nome?></h1>
                    <?=$palestrante->texto?>
                </div>
                
            </div>
            
            <?if($temas_1_dia || $temas_2_dia || $temas_3_dia):?>

                <br><br>

                <h2><?=traduz('PALESTRANTES_TEMAS_TITULO')?>:</h2>

                <?if($temas_1_dia):?>
                    <h2 class="data">12/06/13</h2>  

                    <?foreach($temas_1_dia as $tem):?>
                        <h3><?=$tem->titulo?></h3>
                        <?if($tem->texto):?>
                            <p><?=$tem->texto?></p>        
                        <?endif;?>
                    <?endforeach;?>
                <?endif;?>

                <?if($temas_2_dia):?>
                    <h2 class="data">13/06/13</h2>  

                    <?foreach($temas_2_dia as $tem):?>
                        <h3><?=$tem->titulo?></h3>
                        <?if($tem->texto):?>
                            <p><?=$tem->texto?></p>        
                        <?endif;?>     
                    <?endforeach;?>                
                <?endif;?>

                <?if($temas_3_dia):?>
                    <h2 class="data">14/06/13</h2>  

                    <?foreach($temas_3_dia as $tem):?>
                        <h3><?=$tem->titulo?></h3>
                        <?if($tem->texto):?>
                            <p><?=$tem->texto?></p>        
                        <?endif;?>  
                    <?endforeach;?>                
                <?endif;?>                                

            <?endif;?>
            
            <?$this->load->view('common/compartilhe');?>
            
            <div id="outros-palestrantes" class="sombra-pequena">
                <?if($resultados):?>
                    <h1 class="titulo"><?=traduz('PALESTRANTES_OUTROS')?></h1>
                    <?foreach($resultados as $result):?>
                    
                        <div class="lista-palestrante img-switch">
                            <a href="palestrantes/detalhe/<?=$result->slug?>">
                                <img src="_imgs/palestrantes/<?=$result->imagem_on?>" alt="<?=$result->nome?>">
                                <img src="_imgs/palestrantes/<?=$result->imagem_off?>" class="img-off" alt="<?=$result->nome?>">
                                <h1><?=$result->nome?></h1>
                                <?=substr(strip_tags($result->texto), 0, 330)?> ...
                            </a>
                        </div>    
                    
                    <?endforeach;?>
                <?endif;?>
            </div>
        </div>

    </div>
</div>