        <div class="conteudo">
        
            <?if($palestrantes):?>

                <?foreach($palestrantes as $palest):?>
                    <div class="lista-palestrante img-switch">
                        <a href="palestrantes/detalhe/<?=$palest->slug?>">
                            <img src="_imgs/palestrantes/<?=$palest->imagem_on?>" alt="<?=$palest->nome?>">
                            <img src="_imgs/palestrantes/<?=$palest->imagem_off?>" class="img-off" alt="<?=$palest->nome?>">
                            <h1><?=$palest->nome?></h1>
                            <?=substr(strip_tags($palest->texto), 0, 330)?> ...
                        </a>
                    </div>                    
                <?endforeach;?>

                <?if($pagination):?>
                <div id="navegacao" class="sombra-pequena">
                    <?=$pagination?>
                </div>

            <?endif;?>

            <?else:?>
                <h2 style="border-radius:3px; background-color:#E5E5E5; font-weight:bold; padding:3px; color:#00427a; margin-top:50px; text-align:center;">Aguarde, em breve você conhecerá os palestrantes da edição 2013.</h2>
            <?endif;?>            
            
        </div>

    </div>
</div>