<div class="centro main-<?=$this->router->class?>">

<div id="slide-wrapper">
    <div class="slideshow">



        <?if(prefixo('') == 'en_'):?>

            <div class="slide">
                <a href="evento">
                    <img class="img-fundo" src="_imgs/home/banner1en.jpg">

                    <div class="legenda azul">
                        2013 CIAB FEBRABAN - Congress and Exhibition of IT for financial institutions, largest event in Latin America for both technology and financial sectors, will discuss the challenges of the financial area. Be a part of it!
                    </div>
                </a>
            </div>

        <?elseif(prefixo('') == 'es_'):?>

            <div class="slide">
                <a href="evento">
                    <img class="img-fundo" src="_imgs/home/banner1es.jpg">

                    <div class="legenda azul">
                        CIAB FEBRABAN 2013 - Congreso y Exposición de Tecnología de la Información de las Instituciones Financieras es el mayor evento de América Latina tanto para el sector financiero como para el área de Tecnología, se discutirán los desafíos del sector financiero. Participe!
                    </div>
                </a>
            </div>

        <?else:?>

            <div class="slide">
                <a href="publicacoes">
                    <img class="img-fundo" src="_imgs/home/BannerHome-Revista45.jpg">

                    <div class="legenda azul">
                        Pela primeira vez o evento terá a participação de empresas como Cisco, Glory e New Space. Além de outros patrocinadores como Diebold, IBM, Itautec, que prestigiam o CIAB desde 1991.
                    </div>
                </a>
            </div>

            <div class="slide">
                <a href="evento">
                    <img class="img-fundo" src="_imgs/home/banner1.jpg">

                    <div class="legenda azul">
                        CIAB FEBRABAN 2013 - Congresso e Exposição de Tecnologia da Informação das Instituições Financeiras, maior evento da América Latina tanto para o setor financeiro quanto e a área de Tecnologia irá discutir os desafios do setor financeiro. Participe!
                    </div>
                </a>
            </div>

            <div class="slide">
                <a href="publicacoes">
                    <img class="img-fundo" src="_imgs/home/BannerHome-Revista44.jpg">

                    <div class="legenda vermelho">
                        Bancos, operadoras de cartões e de telefonia investem e apresentam novos projetos de m-payment. O Banco Central já anunciou a intenção de criar um sistema de pagamento móvel. Agora, a onda deve pegar de vez no Brasil.
                    </div>
                </a>
            </div>



        <?endif;?>
    </div>

    <div class="box-superior box-superior-tam box">
        <h3><?=traduz('VANTAGENS')?></h3>
        <a href="informacoes/tam">
            <div class="texto">
                <?=traduz('ATAM')?>
            </div>
        </a>
    </div>

    <div class="box-inferior box">
        <h3><?=traduz('PROMOÇÃO')?></h3>
        <a href="http://www.jdconsultores.com.br/siteJD/noticias.asp?id=116&tipo=1" target="_blank">
            <div class="texto">
                <?=traduz('PROMOÇÃO2')?>
            </div>
        </a>
    </div>

    <!--<div class="box-inferior box">
        <h3><?=traduz('BOX_BLOG_TITULO')?></h3>
        <a href="http://www.ciab.com.br/blog/">
            <div class="texto">
                <?=traduz('BOX_BLOG_TEXTO')?>
            </div>
        </a>
    </div>  -->

    <div id="nav-wrapper">
        <div id="navegacao">
        </div>
    </div>
</div>



</div>

</div> <!-- fim da div superior -->

<div id="intermediaria">
    <div class="centro large-widget">

        <!-- $this->load->view('palestrantes/widget')?> -->

        <div id="eu-quero">

            <div class="coluna primeira">
                <a href="evento/queropatrocinar">
                    <h3><?=traduz('QUERO_PATROCINAR_TITULO')?></h3>
                    <div class="texto">
                        <img src="_imgs/home/patrocinio.jpg" alt="<?=traduz('QUERO_PATROCINAR_TITULO')?>">
                        <p><?=traduz('QUERO_PATROCINAR_TEXTO')?></p>
                    </div>
                </a>
            </div>

            <div class="coluna">
                <a href="evento/querovisitar">
                    <h3><?=traduz('QUERO_VISITAR_TITULO')?></h3>
                    <div class="texto">
                        <img src="_imgs/home/expor.jpg" alt="<?=traduz('QUERO_VISITAR_TITULO')?>">
                        <p><?=traduz('QUERO_VISITAR_TEXTO')?></p>
                    </div>
                </a>
            </div>

            <div class="coluna">
                <a href="http://www.ciab.com.br/blog/">
                    <h3><?=traduz('BLOGCIAB_TITULO')?></h3>
                    <div class="texto">
                        <img src="_imgs/home/blog_box.jpg" alt="<?=traduz('BLOGCIAB_TITULO')?>">
                        <p><?=traduz('BLOGCIAB_TEXTO')?></p>
                    </div>
                </a>
            </div>

            <div class="coluna ultima">
                <a href="programacao/primeiroDia">
                    <h3><?=traduz('AGENDA_TITULO')?></h3>
                    <div class="texto">
                        <img src="_imgs/home/agenda.jpg" alt="<?=traduz('AGENDA_TITULO')?>">
                        <p><?=traduz('AGENDA_TEXTO')?></p>
                    </div>
                </a>
            </div>

        </div>

        <div id="colunas-home">

            <div class="coluna">
                <h3 style="margin-bottom:15px;"><?=traduz('NOTICIAS')?></h3>

                <?if($noticias):?>
                    <?foreach($noticias as $not):?>
                        <div class="noticia">
                            <a href="noticias/detalhe/<?=$not->id?>">
                                <h4><?=$not->titulo?></h4>
                                <p class="sub"><?=formataData($not->data,'mysql2br')?> <?if($not->subtitulo) echo ' - '.$not->subtitulo?></p>
                                <?=$not->olho?>
                            </a>
                        </div>
                    <?endforeach;?>
                <?else:?>
                    <h4><?=traduz('NENHUMA_NOTICIA')?></h4>
                <?endif;?>

            </div>

            <div class="coluna meio">
                <h3 style="margin-bottom:15px;"><?=traduz('EVENTO_TITULO')?></h3>
                <a href="evento">
                    <img src="_imgs/home/eventos.jpg" alt="<?=traduz('EVENTO_TITULO')?>">
                    <h4><?=traduz('EVENTO_SUBTITULO')?></h4>
                    <?=traduz('EVENTO_TEXTO')?>
                </a>

                <h3 style="margin-top:28px;"><?=traduz('PUBLICACOES_TITULO')?></h3>
                <a href="publicacoes">
                    <img src="_imgs/home/publicacoes-home.jpg" alt="<?=traduz('PUBLICACOES_TITULO')?>">
                    <h4>Soluções ao Vivo</h4>
                    Mais de cem expositores preencherão os espaços do Ciab 2013. Saiba quais nomes já reservaram seus lugares e o que pretendem apresentar na 23ª edição do evento
                </a>
            </div>

            <div class="coluna">
                <h3 style="margin-bottom:15px;"><?=traduz('TWITTER_TITULO')?></h3>

                <a class="twitter-timeline" data-dnt="true" href="https://twitter.com/CiabFEBRABAN" data-widget-id="345220758580113408">Tweets de @CiabFEBRABAN</a>
                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

                <h3 style="margin-top:10px;"><?=traduz('NEWSLETTER_TITULO')?></h3>
                <h4 style="margin:10px 0 5px 0;"><?=traduz('NEWSLETTER_TEXTO')?></h4>
                <div id="form-wrapper">
                    <form method="post" id="newsletter-form" action="">
                        <input type="text" name="nome" class="placeholder" placeholder="<?=TRADUZ('NEWSLETTER_PLACEHOLDER_NOME')?>" required>
                        <input type="email" name="email" class="placeholder" placeholder="<?=TRADUZ('NEWSLETTER_PLACEHOLDER_EMAIL')?>" required>
                        <input type="submit" value="<?=TRADUZ('NEWSLETTER_SUBMIT')?>">
                    </form>
                    <div id="form-resposta"></div>
                </div>
            </div>
        </div>

    </div>
</div>