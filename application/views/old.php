<div class="centro main-<?=$this->router->class?>">

    <div class="slideshow">
        <div class="slide">
            
                <div class="box-superior box">
                    <h3>INSCRIÇÕES</h3>
                    <a href="">
                        <div class="texto">
                            Aproveite os descontos promocionais até
                            31/03 para filiados e não filiados. Inscreva
                            sua empresa e ganhe descontos por grupo.
                        </div>
                    </a>
                </div>
                
                <div class="box-inferior box">
                    <h3>PRÊMIO</h3>
                    <a href="">
                        <div class="texto">
                            Motivada pelo sucesso de segunda edição
                            Ciab FEBRABAN e Universia lançaram a 
                            terceira edição de prêmio para jovens.
                        </div>
                    </a>
                </div>
            
            <a href="">   
                <img class="img-fundo" src="_imgs/slides/slide-1.jpg">
                
                <div class="legenda">
                    O CIAB FEBRABAN - Congresso e Exposição de Tecnologia da Informação das Instituições Financeiras  é o maior evento da América Latina tanto para o setor financeiro quanto para a área de Tecnologia. 
                </div>
            </a>    
        </div>
        
        <div class="slide">    
            
                <div class="box-superior box">
                    <h3>INSCRIÇÕES</h3>
                    <a href="">
                        <div class="texto">
                            Aproveite os descontos promocionais até
                            31/03 para filiados e não filiados. Inscreva
                            sua empresa e ganhe descontos por grupo.
                        </div>
                    </a>
                </div>
                
                <div class="box-inferior box">
                    <h3>PRÊMIO</h3>
                    <a href="">
                        <div class="texto">
                            Motivada pelo sucesso de segunda edição
                            Ciab FEBRABAN e Universia lançaram a 
                            terceira edição de prêmio para jovens.
                        </div>
                    </a>
                </div>
            
            <a href="">
                <img class="img-fundo" src="_imgs/slides/slide-1.jpg">
                
                <div class="legenda">
                    O CIAB FEBRABAN - Congresso e Exposição de Tecnologia da Informação das Instituições Financeiras  é o maior evento da América Latina tanto para o setor financeiro quanto para a área de Tecnologia. 
                </div>
            </a>    
        </div>        
        
    </div>
    
    <div id="nav-wrapper">
        <div id="navegacao">
        </div>
    </div>

</div>

</div> <!-- fim da div superior -->

<div id="intermediaria">
    <div class="centro">
    
        <!-- $this->load->view('palestrantes/widget')?> -->
        
        <div id="eu-quero">
        
            <div class="coluna primeira">
                <a href="">
                    <h3><?=traduz('QUERO PATROCINAR')?></h3>
                    <div class="texto">
                        <p>Sua empresa pode patrocinar o maior congresso e exposição da América Latina.</p>
                        <img src="_imgs/home/patrocinio.jpg" alt="<?=traduz('QUERO PATROCINAR')?>">
                        <?traduz('TXT_PATROCINIO_HOME')?>
                    </div>
                </a>
            </div>
            
            <div class="coluna">
                <a href="">
                    <h3><?=traduz('QUERO EXPOR')?></h3>
                    <div class="texto">
                        <p>CIAB FEBRABAN conta com a participação de centenas de empresas expositoras.</p>
                        <img src="_imgs/home/expor.jpg" alt="<?=traduz('QUERO EXPOR')?>">
                        <?traduz('TXT_PATROCINIO_HOME')?>
                    </div>
                </a>                
            </div>
            
            <div class="coluna">
                <a href="">
                    <h3><?=traduz('QUERO VISITAR')?></h3>
                    <div class="texto">
                        <p>Va ao Transamérica Expo Center e faça seu cadastro de visitante do evento.</p>
                        <img src="_imgs/home/visitar.jpg" alt="<?=traduz('QUERO VISITAR')?>">
                        <?traduz('TXT_PATROCINIO_HOME')?>
                    </div>
                </a>                
            </div>
            
            <div class="coluna ultima">
                <a href="">
                    <h3><?=traduz(' AGENDA')?></h3>
                    <div class="texto">
                        <p>Confira a agenda de palestrantes nacionais e internacionais que estarão presentes.</p>
                        <img src="_imgs/home/agenda.jpg" alt="<?=traduz('QUERO VER A AGENDA')?>">
                        <?traduz('TXT_PATROCINIO_HOME')?>
                    </div>
                </a>                
            </div>
            
        </div>
        
        <div id="colunas-home">
        
            <div class="coluna">
                <h3 style="margin-bottom:15px;"><?=traduz('NOTICIAS')?></h3>
                
                <div class="noticia">
                    <a href="">
                        <h4>Adultos passam mais tempo com uso de dispositivos móveis</h4>
                        <p class="sub">13/01 - Uso supera o de jornais e revistas</p>
                        Um estudo feito pela consultoria de mercado norte-americana eMarketer apontou que um adulto gasta em média 65 minutos por dia com um dispositivo móvel (tablet ou smartphone)
                    </a>
                </div>
                
                <div class="noticia">
                    <a href="">
                        <h4>Adultos passam mais tempo com uso de dispositivos móveis</h4>
                        <p class="sub">13/01 - Uso supera o de jornais e revistas</p>
                        Um estudo feito pela consultoria de mercado norte-americana eMarketer apontou que um adulto gasta em média 65 minutos por dia com um dispositivo móvel (tablet ou smartphone)
                    </a>
                </div>
                
                <div class="noticia">
                    <a href="">
                        <h4>Adultos passam mais tempo com uso de dispositivos móveis</h4>
                        <p class="sub">13/01 - Uso supera o de jornais e revistas</p>
                        Um estudo feito pela consultoria de mercado norte-americana eMarketer apontou que um adulto gasta em média 65 minutos por dia com um dispositivo móvel (tablet ou smartphone)
                    </a>
                </div>
            </div>
            
            <div class="coluna meio">
                <h3 style="margin-bottom:15px;"><?=traduz('O EVENTO')?></h3>
                <a href="">
                    <img src="_imgs/home/eventos.jpg" alt="<?=traduz('O EVENTO')?>">
                    <h4>O que é CIAB FEBRABAN</h4>
                    Congresso e Exposição de Tecnologia da Informação das Instituições Financeiras - o maior evento da América Latina...
                </a>
                
                <h3 style="margin-top:28px;"><?=traduz('PUBLICACOES')?></h3>
                <a href="">
                    <img src="_imgs/home/publicacoes.jpg" alt="<?=traduz('O EVENTO')?>">
                    <h4>O que é CIAB FEBRABAN</h4>
                    Congresso e Exposição de Tecnologia da Informação das Instituições Financeiras - o maior evento da América Latina...
                </a>                
            </div>            
            
            <div class="coluna">
                <h3 style="margin-bottom:15px;"><?=traduz('TWITTER CIABFEBRABAN')?></h3>
                
                <script charset="utf-8" src="js/twitter-widget.js"></script>
                <script>
                new TWTR.Widget({
                  version: 2,
                  type: 'profile',
                  rpp: 2,
                  interval: 30000,
                  width: 'auto',
                  height: 160,
                  theme: {
                    shell: {
                      background: '#D6D6D6',
                      color: '#00417a'
                    },
                    tweets: {
                      background: '#e2e2e2',
                      color: '#6a7175',
                      links: '#00417a'
                    }
                  },
                  features: {
                    scrollbar: false,
                    loop: false,
                    live: false,
                    behavior: 'all'
                  }
                }).render().setUser('ciabfebraban').start();
                </script>                
                
                <h3 style="margin-top:10px;"><?=traduz('NEWSLETTER')?></h3>
                <h4 style="margin:10px 0 5px 0;">Assine e receba todas as notividades</h4>
                <form method="post" action="">
                    <input type="text" name="nome" class="placeholder" placeholder="Informe seu nome">
                    <input type="text" name="email" class="placeholder" placeholder="seu e-mail">
                    <input type="submit" value="ENVIAR CADASTRO">
                </form>
            </div>               
        </div>
        
    </div>
</div>