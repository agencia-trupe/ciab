        <div class="conteudo conteudo-<?=$this->router->class?>">
            
            <div class="coluna">
                <h1><?=traduz('CONTATO_INFORMACOES_GERAIS')?>:</h1>
                <strong>Diretoria de Eventos da Febraban</strong><br>
                Av. Brig. Faria Lima, 1.485 - 14º andar<br>
                CEP 01452-921 - São Paulo- SP<br>
                PABX 55 11 3244 9860 / 3186 9860<br>
                Fax 55 11 3031 4106 <br>
                <a href="mailto:eventos@febraban.org.br">eventos@febraban.org.br</a><br>
                <a href="informacoes/local" class="botao bt-azul-claro"><?=traduz('CONTATO_COMO_CHEGAR')?></a>
            </div>
            
            <div class="coluna">
                <h1><?=traduz('CONTATO_RESERVAS')?>:</h1>
                <strong>Fernanda Castillo</strong><br>
                <a href="mailto:fernanda.castillo@febraban.org.br">fernanda.castillo@febraban.org.br</a><br>
                (11) 3244-9897<br>
                <br>
                <strong>Marcelo Assumpção</strong><br>
                <a href="mailto:marcelo.assumpcao@febraban.org.br">marcelo.assumpcao@febraban.org.br</a><br>
                (11) 3244-9941
            </div>
            
            <div id="formulario-wrapper">
                <form id="form-contato" method="post" action="contato/enviar">
                
                    <p><?=traduz('CONTATO_FORM_TITULO')?>:</p>
                    
                    <label><?=traduz('CONTATO_FORM_ASSUNTO_TITULO')?>: <select name="assunto">
                        <option value=""></option>
                        <option value="Outros"><?=traduz('CONTATO_FORM_ASSUNTO_1')?></option>
                        <option value="Congressista"><?=traduz('CONTATO_FORM_ASSUNTO_2')?></option>
                        <option value="Expositor"><?=traduz('CONTATO_FORM_ASSUNTO_3')?></option>
                        <option value="Visitante"><?=traduz('CONTATO_FORM_ASSUNTO_4')?></option>
                        <option value="Patrocinador"><?=traduz('CONTATO_FORM_ASSUNTO_5')?></option>
                        <option value="Imprensa"><?=traduz('CONTATO_FORM_ASSUNTO_6')?></option>
                    </select></label>
                    
                    <label><?=traduz('CONTATO_FORM_1')?>: <input type="text" name="nome" required></label>
                    <label><?=traduz('CONTATO_FORM_2')?>: <input type="text" name="email" required></label>
                    <label><?=traduz('CONTATO_FORM_3')?>: <input type="text" name="empresa"></label>
                    <label><?=traduz('CONTATO_FORM_4')?>: <input type="text" name="empresa-site"></label>
                    <label><?=traduz('CONTATO_FORM_5')?>: <input type="text" name="area-atuacao"></label>
                    <label><?=traduz('CONTATO_FORM_6')?>: <input type="text" name="telefone"></label>
                    <label><?=traduz('CONTATO_FORM_7')?>: <textarea name="mensagem" required></textarea></label>
                    <input type="submit" value="<?=traduz('CONTATO_FORM_SUBMIT')?>" class="botao botao-escuro">
                </form>
            </div>
            <div id="resposta-target">
                <?if($envio):?>
                    <?if($status):?>
                        <?=traduz('CONTATO_FORM_OK')?>
                    <?else:?>
                        <?=traduz('CONTATO_FORM_ERROR')?>
                    <?endif;?>
                    
                    <script>
                        $(document).ready( function(){
                            $('#formulario-wrapper').hide(0, function(){
                                $('#resposta-target').delay(5000).hide('fade', function(){
                                    $('#formulario-wrapper').show('fade')
                                })
                            })
                        })
                    </script>
                <?endif;?>
            </div>
        </div>

    </div>
</div>