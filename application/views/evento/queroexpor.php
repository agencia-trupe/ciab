        <div class="conteudo conteudo-<?=$this->router->class?> conteudo-<?=$this->router->class?>-<?=$this->router->method?>">
        
            <h1><?=traduz('EVENTO_EXPOR_TITULO')?></h1>
            
            <?=traduz('EVENTO_EXPOR_TEXTO')?>
            
            <div class="spacer">
                <?$this->load->view('common/compartilhe')?>
            </div>
            
            <a href="evento/mapa"><img id="link-mapa" src="_imgs/evento/<?=prefixo('link-mapa.png')?>" alt="Clique para ver o mapa da exposição"></a>
            
            <div id="formulario-wrapper">
                <p style="clear:left;">
                <?=traduz('EVENTO_EXPOR_FORM_TEXTO')?>
                </p>
                
                <form method="post" action="evento/enviar" id="form-evento">
                    <label><?=traduz('EVENTO_EXPOR_FORM_1')?> <input type="text" name="assunto" required></label>
                
                    <label><?=traduz('EVENTO_EXPOR_FORM_2')?><input type="text" name="nome" required></label>
                    
                    <label><?=traduz('EVENTO_EXPOR_FORM_3')?><input type="email" name="email" required></label>
                    
                    <label><?=traduz('EVENTO_EXPOR_FORM_4')?><input type="text" name="empresa" required></label>
                    
                    <label><?=traduz('EVENTO_EXPOR_FORM_5')?><input type="text" name="empresa-site"></label>
                    
                    <label><?=traduz('EVENTO_EXPOR_FORM_6')?><input type="text" name="area-atuacao"></label>
                    
                    <label><?=traduz('EVENTO_EXPOR_FORM_7')?><input type="text" name="telefone"></label>
                    
                    <input type="hidden" name="origem" value="<?=$this->router->method?>">
                    
                    <label class="label-textarea"><textarea name="espaco" required></textarea><?=traduz('EVENTO_EXPOR_FORM_8')?></label>
                    
                    <input type="submit" value="<?=traduz('EVENTO_EXPOR_FORM_SUBMIT')?>" class="botao">
                    
                    <p class="negrito" style="clear:both; padding-top:15px;">
                        <?=traduz('EVENTO_EXPOR_FORM_OBS')?>
                    </p>
                    <p>
                        <span>Fernanda Castillo</span><br>
                        (11) 3244-9897<br>
                        <a class="mailto" href="mailto:fernanda.castillo@febraban.org.br">fernanda.castillo@febraban.org.br</a>
                    </p>
                    
                    <p>
                        <span>Marcelo Assumpção</span><br>
                        (11) 3244-9941<br>
                        <a class="mailto" href="mailto:marcelo.assumpcao@febraban.org.br">marcelo.assumpcao@febraban.org.br</a>
                    </p>
                </form>
            </div>
            <div id="resposta-target">
                <?if($envio):?>
                    <?if($status):?>
                        <?=traduz('EVENTO_EXPOR_FORM_OK')?>
                    <?else:?>
                        <?=traduz('EVENTO_EXPOR_FORM_ERROR')?>
                    <?endif;?>
                    
                    <script>
                        $(document).ready( function(){
                            $('#formulario-wrapper').hide(0, function(){
                                $('#resposta-target').delay(5000).hide('fade', function(){
                                    $('#formulario-wrapper').show('fade')
                                })
                            })
                        })
                    </script>
                <?endif;?>                
            </div>
            
        </div>

    </div>
</div>