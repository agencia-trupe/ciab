        <div class="conteudo conteudo-<?=$this->router->class?> conteudo-<?=$this->router->class?>-<?=$this->router->method?>">
        
            <h1><?=traduz('EVENTO_ANOS_TITULO')?></h1>
            
            <p>
                <?=traduz('EVENTO_ANOS_SUBTITULO')?>:
            </p>
            
            <ul id="lista-outrosanos">
                <li>
                    <a href="http://www.ciab.org.br/ciab2011/" target="_blank">
                        <img src="_imgs/evento/ciab2011.png" alt="Ciab 2011">
                        <div class="ano">2011</div>
                        <div class="texto"><?=traduz('EVENTO_ANOS_ITEM_1')?></div>
                    </a>
                </li>
                <li>
                    <a href="http://www.ciab.org.br/ciab2010/pt/index.html" target="_blank">
                        <img src="_imgs/evento/ciab2010.png" alt="Ciab 2010">
                        <div class="ano">2010</div>
                        <div class="texto multilinha"><?=traduz('EVENTO_ANOS_ITEM_2')?></div>
                    </a>
                </li>
                <li>
                    <a href="http://www.ciab.org.br/ciab2009/" target="_blank">
                        <img src="_imgs/evento/ciab2009.png" alt="Ciab 2009">
                        <div class="ano">2009</div>
                        <div class="texto multilinha"><?=traduz('EVENTO_ANOS_ITEM_3')?></div>
                    </a>
                </li>
                <li>
                    <a href="http://www.ciab.org.br/ciab2008/" target="_blank">
                        <img src="_imgs/evento/ciab2008.png" alt="Ciab 2008">
                        <div class="ano">2008</div>
                        <div class="texto"><?=traduz('EVENTO_ANOS_ITEM_4')?></div>
                    </a>
                </li>
            </ul>
            
            <div class="spacer">
                <?$this->load->view('common/compartilhe')?>
            </div>
      
        </div>

    </div>
</div>