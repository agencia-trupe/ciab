<div class="centro centro-<?=$this->router->class?>">

    <div class="conteudo conteudo-<?=$this->router->class?> conteudo-<?=$this->router->class?>-<?=$this->router->method?>">
    
        <img src="_imgs/evento/<?=prefixo('mapa-topo.png')?>" alt="">
        
        <div class="menu-mapa">
            <a href="" id="botao-voltar"><?=traduz('VOLTAR')?></a>
            <a href="<?=base_url('_pdfs/mapa/PlantaCiab2013.pdf')?>" target="_blank" class="pdf-download">DOWNLOAD PDF</a>
        </div>
        
        <div id="zoom">
            <div class="small">
                <img src="_imgs/evento/PlantaCiab2013.jpg?nocache=<?=time()?>" style="width:985px; height:447px;" alt="Mapa do Evento">
            </div>
            <div class="large">
                <img src="_imgs/evento/PlantaCiab2013.jpg?nocache=<?=time()?>" alt="Mapa do Evento">
            </div>
        </div>
        
        
        <div id="lista-expositores">

            <h1><?=traduz('EVENTO_MAPA_EXPOSITORES')?></h1>
            
            <ul>
            <?
            $exp_porlista = ceil($num_expositores / 4);
            $cont = 0;
            ?>
            <?foreach($expositores as $exp):?>
                <li><?=$exp->empresa . ' - ' . $exp->stand?></li>
                <?$cont++?>
                <?
                if($cont == $exp_porlista){
                    echo "</ul><ul>";
                    $cont = 0;
                }
                ?>
            <?endforeach;?>
            </ul>
            
        </div>
    
    </div>

</div>
<script defer>
    $(document).ready( function(){

        $('#zoom').anythingZoomer({
            smallArea   : 'small',
            largeArea   : 'large',
            clone       : false,
            switchEvent : false,
            edge        : 10
        })
    
    });
</script>