<script defer src="js/jquery.anythingzoomer.min.js"></script>

<div class="centro centro-<?=$this->router->class?>">

    <div class="conteudo conteudo-<?=$this->router->class?> conteudo-<?=$this->router->class?>-<?=$this->router->method?>">
    
        <img src="_imgs/evento/mapa-topo.png" alt="">
        
        <div class="menu-mapa">
            <a href="" id="botao-voltar">&laquo; voltar</a>
            <a href="" class="pdf-download">DOWNLOAD PDF</a>
        </div>
        
        <a href="_imgs/evento/mapa-ampliado.png" class="fancy">
            <img src="_imgs/evento/mapa-reduzido.png" class="mapa-reduzido" alt="Mapa do Evento">
        </a>
        
        <div id="lista-expositores">

            <h1>EXPOSITORES</h1>
        
            <ul>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
            </ul>
            <ul>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
            </ul>
            <ul>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
            </ul>
            <ul>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
                <li>NOME</li>
            </ul>
        </div>
    
    </div>

</div>
<script defer>
    $(document).ready( function(){
        $('.fancy').fancybox({
            autoScale : 0,
            type : 'image',
            hideOnContentClick : 0,
            onStart : function(){
                $('body').css('overflow', 'hidden')
            },
            onCleanup : function(){$('body').css('overflow', 'auto')},
            onComplete : function(){
                view_w = parseInt($(window).width()) - 100;
                view_h = parseInt($(window).height()) - 100;
                $('#fancybox-wrap').css({overflow :'hidden', width : view_w, height : view_h})
                $('#fancybox-content').css({overflow :'hidden', width : view_w, height : view_h, backgroundColor : '#FFF'})
                $('#fancybox-content img').css({width : 'auto', height : 'auto', border : '3px #cecece solid'}).hover( function(){
                    $('body').css('cursor', 'move');
                }, function(){
                    $('body').css('cursor', 'normal');
                }).draggable();
            }
        });
    });
</script>