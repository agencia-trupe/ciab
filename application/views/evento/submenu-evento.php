<div class="centropad main-<?=$this->router->class?>">
    
    <div id="container">
    
        <div id="menu-lateral">
        
            <h3><?=traduz('EVENTO')?></h3>
            
            <ul class="submenu-premios">
                <li id="submn-apresentacao"><a href="evento/apresentacao"><?=traduz('EVENTO_SUBMENU_1')?></a></li>
                <div class="faixa-sob-item-menu"><div></div></div>

                <li id="submn-congresso"><a href="evento/congresso"><?=traduz('EVENTO_SUBMENU_2')?></a></li>
                <div class="faixa-sob-item-menu"><div></div></div>
                
                <li id="submn-espacos"><a href="evento/espacos"><?=traduz('EVENTO_SUBMENU_3')?></a></li>
                <div class="faixa-sob-item-menu"><div></div></div>
                
                <li id="submn-exposicao"><a href="evento/exposicao"><?=traduz('EVENTO_SUBMENU_4')?></a></li>
                <div class="faixa-sob-item-menu"><div></div></div>
                
                <li id="submn-querovisitar"><a href="evento/querovisitar"><?=traduz('EVENTO_SUBMENU_5')?></a></li>
                <div class="faixa-sob-item-menu"><div></div></div>
                
                <li id="submn-queroexpor"><a href="evento/queroexpor"><?=traduz('EVENTO_SUBMENU_6')?></a></li>
                <div class="faixa-sob-item-menu"><div></div></div>
                
                <li id="submn-queropatrocinar"><a href="evento/queropatrocinar"><?=traduz('EVENTO_SUBMENU_7')?></a></li>
                <div class="faixa-sob-item-menu"><div></div></div>
                
                <li id="submn-mapa"><a href="evento/planta"><?=traduz('EVENTO_SUBMENU_8')?></a></li>
                <div class="faixa-sob-item-menu"><div></div></div>
                
                <li id="submn-outrosanos"><a href="evento/outrosanos"><?=traduz('EVENTO_SUBMENU_9')?></a></li>
                <div class="faixa-sob-item-menu"><div></div></div>                
            </ul>
            
            <script defer>$(document).ready( function(){ $('#submn-<?=$this->router->method?>').addClass('subativo'); });</script>
            