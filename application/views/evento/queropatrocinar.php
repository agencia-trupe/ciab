        <div class="conteudo conteudo-<?=$this->router->class?> conteudo-<?=$this->router->class?>-<?=$this->router->method?>">
        
            <h1><?=traduz('EVENTO_PATROCINAR_TITULO')?></h1>
            
            <?=traduz('EVENTO_PATROCINAR_TEXTO')?>
            
            <div class="spacer">
                <?$this->load->view('common/compartilhe')?>
            </div>
            
            <a href="evento/mapa"><img src="_imgs/evento/<?=prefixo('link-mapa.png')?>" alt="Clique para ver o mapa da exposição"></a>
            
            <p class="negrito" style="clear:both; padding-top:15px;">
                <?=traduz('EVENTO_PATROCINAR_OBS')?>
            </p>
            <p>
                <span>Fernanda Castillo</span><br>
                (11) 3244-9897<br>
                <a class="mailto" href="mailto:fernanda.castillo@febraban.org.br">fernanda.castillo@febraban.org.br</a>
            </p>
            
            <p>
                <span>Marcelo Assumpção</span><br>
                (11) 3244-9941<br>
                <a class="mailto" href="mailto:marcelo.assumpcao@febraban.org.br">marcelo.assumpcao@febraban.org.br</a>
            </p>
        </div>

    </div>
</div>