        <div class="conteudo conteudo-<?=$this->router->class?> conteudo-<?=$this->router->class?>-<?=$this->router->method?>">
        
            <h1>Aposta de R$ 18 bilhões em TI</h1>
            
            <div id="publicacoes-banner">
                <div id="float-wrapper">
                    <p>
                        Pesquisa Ciab FEBRABAN revela que despesas e investimentos dos bancos em tecnologia, em 2011,
                         foram de R$ 18 bilhões e também aponta grande evolução no uso do Mobile Banking.
                    </p>
                    <p>
                        Brasil tem participação importante nos gastos com TI e está à frente de Índia, México e Austrália.
                        Em primeiro lugar estão os EUA, com investimento de US$ 94,2 bilhões em 2011.
                    </p>
                    
                    <a href="<?=base_url('_pdfs/publicacoes/2012/39-Mai2012.pdf')?>" target="_blank" id="down-publi"><img src="_imgs/publicacoes/download-link-grande-laranja.png" alt="Download da Publicação em PDF">
                    <img class="tofade" src="_imgs/publicacoes/download-link-grande.png" alt="Download da Publicação em PDF"></a>
                </div>
            </div>

            <div id="publi-destaques">
                <h3>Destaques da edição:</h3>
                <ul>
                    <li>
                        <img src="_imgs/publicacoes/destaque2-thumb1.jpg">
                        <div class="destaque-texto">
                            <strong>Prêmio Ciab FEBRABAN 2012 vai distribuir R$ 25 mil</strong><br>
                            A nova edição do prêmio incentiva a capacidade de inovação técnica solicitando, pela primeira vez, que
                            sejam inscritos programas aplicativos de negócios ou programas de sistemas com soluções em ambiente móvel.
                             O prazo de inscrição vai até 21 de maio de 2012.
                        </div>
                    </li>
                    <li>
                        <img src="_imgs/publicacoes/destaque2-thumb2.jpg">
                        <div class="destaque-texto">
                            <strong>O Bradesco está no Facebook</strong><br>
                            Luca Cavalcanti , diretor dos Canais Digitais Dia & Noite do Bradesco, fala da iniciativa do banco em
                            atender seus clientes pelo Facebook. O novo canal permite consulta ao saldo de contas, investimentos e
                            limites de crédito.
                        </div>
                    </li>
                    <li>
                        <img src="_imgs/publicacoes/destaque2-thumb3.jpg">
                        <div class="destaque-texto">
                            <strong>Antonio Augusto Coutinho</strong><br>
                            Em artigo exclusivo, o CIO do Santander afirma que a instituição está em momento de expansão e fala do
                             planejamento estratégico elaborado em 2011 para transformar a organização no primeiro banco de seus
                              clientes, até 2013.
                        </div>
                    </li>
                </ul>
            </div>
            
            <a id="link-todas" href="" data-target="0">&raquo; VER TODAS</a>
            <h2>EDIÇÕES ANTERIORES&raquo;</h2> <span id="link-anos"><a href="" data-target="2012">2012</a> / <a href="" data-target="2011">2011</a> / <a href="" data-target="2010">2010</a> / <a href="" data-target="2009">2009</a>
            / <a href="" data-target="2008">2008</a> / <a href="" data-target="2007">2007</a> / <a href="" data-target="2006">2006</a> / <a href="" data-target="2005">2005</a></span>
            
            <link rel="stylesheet" href="css/widget.css">
            <div id="widget-nav" class="public-widget">
                <div id="widget-nav-left" data-pad="107" data-move="thumbs"></div>
                <div id="widget-nav-right" data-pad="107" data-move="thumbs"></div>
                <div id="widget-inner">
                    <script defer>
                        $(document).ready( function(){
                             $.post( BASE + 'ajax/pegaThumbs', { pasta : '0'} , function(retorno){
                                var er = RegExp(/^[0-9]{4}\/[0-9]{2}-([a-zA-Z]{3}|[a-zA-Z]{5})([0-9]{4}).pdf$/)
                                retorno = JSON.parse(retorno)
                                $('#widget-inner').html('')
                                resultado = retorno.map( function(self){
                                    exp = self.arquivo.match(er)
                                    if(exp == null)
                                        alert(self.arquivo)
                                    titulo_arquivo = exp[1] + '/' + exp[2]
                                    $('#widget-inner').append("<div class='thumbs'><a target='_blank' href='"+BASE+"_pdfs/publicacoes/"+self.arquivo+"' class='fancy' title='"+titulo_arquivo+"'><img src='_imgs/publicacoes/capas/"+self.capa+"' alt='"+titulo_arquivo+"'></a></div>")
                                })
                                
                            })
                        });
                    </script>
                </div>
            </div>
            
        </div>
        
        <div id="veja-tambem" class="sombra-grande">
            <h1>VEJA TAMBÉM//</h1>
            
            <div class="veja-tambem-link catalogo">
                <a href="<?=base_url('_pdfs/publicacoes/Catalogo2012.pdf')?>" target="_blank">
                    <h2>CATÁLOGO CIAB</h2>
                    <p>Resumo completo do evento com biografias dos conferencistas, programação das palestras, relação de expositores 
                        e a pesquisa “O setor bancário em números”.<br>Clique aqui e confira!</p>
                    <img src="_imgs/publicacoes/download-link-grande-laranja.png" class="dwnlink" alt="Download do PDF">
                    <img class="tofade dwnlink" src="_imgs/publicacoes/download-link-grande.png" alt="Download do PDF">
                </a>
            </div>
            
            <div class="veja-tambem-link pesquisa">
                <a href="<?=base_url('_pdfs/publicacoes/Pesquisa2012.pdf')?>" target="_blank">
                    <h2>PESQUISA O SETOR EM NÚMEROS</h2>
                    <p>Uma ferramenta útil nas discussões sobre tecnologia no meio acadêmico, entidades de classe, órgãos do governo
                     e na mídia, contribuindo para a continua evolução do setor. Clique e consulte.</p>
                    <img src="_imgs/publicacoes/download-link-grande-laranja.png" class="dwnlink" alt="Download do PDF">
                    <img class="tofade dwnlink" src="_imgs/publicacoes/download-link-grande.png" alt="Download do PDF">
                </a>                
            </div>
            
        </div>        

    </div>
</div>