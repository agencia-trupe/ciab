        <div class="conteudo conteudo-<?=$this->router->class?> conteudo-<?=$this->router->class?>-<?=$this->router->method?>">
        
            <h1>ÚLTIMA EDIÇÃO &bull; Abr/2013 &bull; Nº 45</h1>
            
            <div id="publicacoes-banner">
                <div id="float-wrapper">
                    <p>
                       Mais de cem expositores preencherão os espaços do CIAB 2013!
                    </p>
                    <p>
                       Saiba quais nomes já reservaram seus lugares e o que pretendem apresentar na 23º edição do evento.
                    </p>
                    
                    <a href="<?=base_url('_pdfs/publicacoes/2013/45-Abr2013.pdf')?>" target="_blank" id="down-publi"><img src="_imgs/publicacoes/download-link-grande-laranja.png" alt="Download da Publicação em PDF">
                    <img class="tofade" src="_imgs/publicacoes/download-link-grande.png" alt="Download da Publicação em PDF"></a>
                </div>
            </div>

            <div id="publi-destaques">
                <h3>Destaques da edição:</h3>
                <ul>
                    <li>
                        <img src="_imgs/publicacoes/1-apps.jpg">
                        <div class="destaque-texto">
                            <strong>Apps impulsionam mobile banking</strong><br>
                            Número crescente de smartphones e tablets no Brasil tem agitado esse segmento
                        </div>
                    </li>
                    <li>
                        <img src="_imgs/publicacoes/2-premio.jpg">
                        <div class="destaque-texto">
                            <strong>Prêmio CIAB estimula a eficiência operacional</strong><br>
                            Quarta edição do concurso irá distribuir R$ 25 mil para os vencedores
                        </div>
                    </li>
                    <li>
                        <img src="_imgs/publicacoes/3-empresas.jpg">
                        <div class="destaque-texto">
                            <strong>Conheça as empresas participantes do CIAB 2013</strong><br>
                            Empresas mostram o que será tendência nos próximos anos
                        </div>
                    </li>
                </ul>
            </div>
            
            <a id="link-todas" href="" data-target="0">&raquo; VER TODAS</a>
            <h2>EDIÇÕES ANTERIORES&raquo;</h2> <span id="link-anos"><a href="" data-target="2013">2013</a> / <a href="" data-target="2012">2012</a> / <a href="" data-target="2011">2011</a> / <a href="" data-target="2010">2010</a> / <a href="" data-target="2009">2009</a>
            / <a href="" data-target="2008">2008</a> / <a href="" data-target="2007">2007</a> / <a href="" data-target="2006">2006</a> / <a href="" data-target="2005">2005</a></span>
            
            <link rel="stylesheet" href="css/widget.css">
            <div id="widget-nav" class="public-widget">
                <div id="widget-nav-left" data-pad="107" data-move="thumbs"></div>
                <div id="widget-nav-right" data-pad="107" data-move="thumbs"></div>
                <div id="widget-inner">
                    <script defer>
                        $(document).ready( function(){
                             $.post( BASE + 'ajax/pegaThumbs', { pasta : '0'} , function(retorno){
                                var er = RegExp(/^[0-9]{4}\/[0-9]{2}-([a-zA-Z]{3}|[a-zA-Z]{5})([0-9]{4}).pdf$/)
                                retorno = JSON.parse(retorno)
                                $('#widget-inner').html('')
                                resultado = retorno.map( function(self){
                                    exp = self.arquivo.match(er)
                                    if(exp == null)
                                        alert(self.arquivo)
                                    titulo_arquivo = exp[1] + '/' + exp[2]
                                    $('#widget-inner').append("<div class='thumbs'><a target='_blank' href='"+BASE+"_pdfs/publicacoes/"+self.arquivo+"' class='fancy' title='"+titulo_arquivo+"'><img src='_imgs/publicacoes/capas/"+self.capa+"' alt='"+titulo_arquivo+"'></a></div>")
                                })
                                
                            })
                        });
                    </script>
                </div>
            </div>
            
        </div>
        
        <div id="veja-tambem" class="sombra-grande">
            <h1>VEJA TAMBÉM//</h1>
            
            <div class="veja-tambem-link catalogo">
                <a href="http://www.avmb.com.br/ciab" target="_blank">
                    <h2>Indicadores FEBRABAN de Tecnologia Bancária</h2>
                    <p>Consulte todos os indicadores do setor bancário atualizados constantemente para que você possa realizar suas pesquisas. Clique aqui e confira!</p>
                    <img src="_imgs/publicacoes/acesse-link-grande-laranja.png" class="dwnlink2" alt="Clique aqui e confira!">
                    <img class="tofade dwnlink2" src="_imgs/publicacoes/acesse-link-grande-azul.png" alt="Clique aqui e confira!">
                </a>
            </div>
            
            <div class="veja-tambem-link pesquisa">
                <a href="<?=base_url('_pdfs/publicacoes/AnuarioFebraban.pdf')?>" target="_blank">
                    <h2>Anuário Setor Bancário em Números</h2>
                    <p>Uma ferramenta útil nas discussões sobre tecnologia no meio acadêmico, entidades de classe, órgãos do governo
                     e na mídia, contribuindo para a continua evolução do setor. Clique e consulte.</p>
                    <img src="_imgs/publicacoes/download-link-grande-laranja.png" class="dwnlink" alt="Download do PDF">
                    <img class="tofade dwnlink" src="_imgs/publicacoes/download-link-grande.png" alt="Download do PDF">
                </a>                
            </div>
            
        </div>        

    </div>
</div>