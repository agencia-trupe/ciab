        <div class="conteudo conteudo-<?=$this->router->class?> conteudo-<?=$this->router->class?>-<?=$this->router->method?>">
        
            <h1>Exclusivo: tendências que movem as corporações nos tempos da indisciplina</h1>
            
            <div id="publicacoes-banner">
                <div id="float-wrapper">
                    <p>
                        O estado de permanente mudança nas corporações Paradigma da ameaça oportunidade como motivação para 
                        inovação pode estar em xeque, alerta pesquisa do Gartner
                    </p>
                    <p>
                        A dinâmica do mundo empresarial mostra que as companhias só alteram seu ‘modus operandi’ diante das
                        clássicas situações de ameaça-oportunidade.
                    </p>
                    
                    <a href="<?=base_url('_pdfs/publicacoes/2012/38-Fev2012.pdf')?>" target="_blank" id="down-publi"><img src="_imgs/publicacoes/download-link-grande-laranja.png" alt="Download da Publicação em PDF">
                    <img class="tofade" src="_imgs/publicacoes/download-link-grande.png" alt="Download da Publicação em PDF"></a>
                </div>
            </div>

            <div id="publi-destaques">
                <h3>Destaques da edição:</h3>
                <ul>
                    <li>
                        <img src="_imgs/publicacoes/destaque-thumb1.jpg">
                        <div class="destaque-texto">
                            <strong>O que muda nas corporações</strong><br>
                            Adiantamos resultados inéditos de estudos do Gartner a respeito de tendências mundiais em 
                            Tecnologia. Companhias terão de identificar a mudança como o seu estado permanente, antes
                            que sejam surpreendidas.
                        </div>
                    </li>
                    <li>
                        <img src="_imgs/publicacoes/destaque-thumb2.jpg">
                        <div class="destaque-texto">
                            <strong>Contas vencidas no DDA</strong><br>
                            A partir de março, será possível quitar contas vencidas pelo Débito Direto Autorizado (DDA). 
                            É mais um avanço do sistema que hoje processa, em média, um milhão de boletos por dia,
                            segundo Célia Okazawa, da CIP (foto).
                        </div>
                    </li>
                    <li>
                        <img src="_imgs/publicacoes/destaque-thumb3.jpg">
                        <div class="destaque-texto">
                            <strong>Aurélio Conrado Boni</strong><br>
                            Em artigo exclusivo, o diretor vice-presidente executivo do Bradesco (foto) trata de 
                            globalização, tecnologias, inovação e, principalmente, como a instituição tem agido para se 
                            adaptar aos novos cenários.
                        </div>
                    </li>
                </ul>
            </div>
            
            <a id="link-todas" href="" data-target="0">&raquo; VER TODAS</a>
            <h2>EDIÇÕES ANTERIORES&raquo;</h2> <span id="link-anos"><a href="" data-target="2012">2012</a> / <a href="" data-target="2011">2011</a> / <a href="" data-target="2010">2010</a> / <a href="" data-target="2009">2009</a>
            / <a href="" data-target="2008">2008</a> / <a href="" data-target="2007">2007</a> / <a href="" data-target="2006">2006</a> / <a href="" data-target="2005">2005</a></span>
            
            <link rel="stylesheet" href="css/widget.css">
            <div id="widget-nav" class="public-widget">
                <div id="widget-nav-left" data-pad="107" data-move="thumbs"></div>
                <div id="widget-nav-right" data-pad="107" data-move="thumbs"></div>
                <div id="widget-inner">
                    <script defer>
                        $(document).ready( function(){
                             $.post( BASE + 'ajax/pegaThumbs', { pasta : '0'} , function(retorno){
                                var er = RegExp(/^[0-9]{4}\/[0-9]{2}-([a-zA-Z]{3}|[a-zA-Z]{5})([0-9]{4}).pdf$/)
                                retorno = JSON.parse(retorno)
                                $('#widget-inner').html('')
                                resultado = retorno.map( function(self){
                                    exp = self.arquivo.match(er)
                                    if(exp == null)
                                        alert(self.arquivo)
                                    titulo_arquivo = exp[1] + '/' + exp[2]
                                    $('#widget-inner').append("<div class='thumbs'><a target='_blank' href='"+BASE+"_pdfs/publicacoes/"+self.arquivo+"' class='fancy' title='"+titulo_arquivo+"'><img src='_imgs/publicacoes/capas/"+self.capa+"' alt='"+titulo_arquivo+"'></a></div>")
                                })
                                
                            })
                        });
                    </script>
                </div>
            </div>
            
        </div>
        
        <div id="veja-tambem" class="sombra-grande">
            <h1>VEJA TAMBÉM//</h1>
            
            <div class="veja-tambem-link catalogo">
                <a href="<?=base_url('_pdfs/publicacoes/CatalogoCiab.pdf')?>" target="_blank">
                    <h2>CATÁLOGO CIAB</h2>
                    <p>Aguarde, em breve a edição de 2012 do catálogo estará disponível para download.
                        Enquanto isso, faça o download da edição de 2011.<br> Um panorama completo do último evento realizado.</p>
                    <img src="_imgs/publicacoes/download-link-grande-laranja.png" class="dwnlink" alt="Download do PDF">
                    <img class="tofade dwnlink" src="_imgs/publicacoes/download-link-grande.png" alt="Download do PDF">
                </a>
            </div>
            
            <div class="veja-tambem-link pesquisa">
                <a href="<?=base_url('_pdfs/publicacoes/Pesquisa2012.pdf')?>" target="_blank">
                    <h2>PESQUISA O SETOR EM NÚMEROS</h2>
                    <p>Uma ferramenta útil nas discussões sobre tecnologia no meio acadêmico, entidades de classe, órgãos do governo
                     e na mídia, contribuindo para a continua evolução do setor. Clique e consulte.</p>
                    <img src="_imgs/publicacoes/download-link-grande-laranja.png" class="dwnlink" alt="Download do PDF">
                    <img class="tofade dwnlink" src="_imgs/publicacoes/download-link-grande.png" alt="Download do PDF">
                </a>                
            </div>
            
        </div>        

    </div>
</div>