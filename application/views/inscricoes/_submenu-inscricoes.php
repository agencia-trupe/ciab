<div class="centropad main-<?=$this->router->class?>">
    
    <div id="container">
    
        <div id="menu-lateral">
        
            <h3>INSCRIÇÕES</h3>
            
            <ul class="submenu-inscricoes">
                <li id="submn-valores"><a href="inscricoes/valores">Valores</a></li>
                <div class="faixa-sob-item-menu"><div></div></div>
                
                <li id="submn-informacoes"><a href="inscricoes/informacoes">Informações Importantes</a></li>
                <div class="faixa-sob-item-menu"><div></div></div>
                
            </ul>
            
            <script defer>$(document).ready( function(){ $('#submn-<?=$this->router->method?>').addClass('subativo'); });</script>
            