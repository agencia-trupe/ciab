        <div class="conteudo conteudo-<?=$this->router->class?> conteudo-<?=$this->router->class?>-<?=$this->router->method?>">
        
            <h1><?=traduz('INSCRICOES_INFORMACOES_TITULO')?>:</h1>
            
            <ul id="lista-info">
                <li id="pagamento">
                    <strong><?=traduz('INSCRICOES_OPCOES_TITULO')?></strong><br>
                    <?=traduz('INSCRICOES_OPCOES_ITEM_1')?><br>
                    <?=traduz('INSCRICOES_OPCOES_ITEM_2')?>
                </li>
                <li id="reembolso">
                    <strong><?=traduz('INSCRICOES_REEMBOLSO_TITULO')?></strong><br>
                    <?=traduz('INSCRICOES_REEMBOLSO_TEXTO')?>
                </li>
                <li id="substituicao">
                    <strong><?=traduz('INSCRICOES_SUBSTITUICAO_TITULO')?></strong><br>
                    <?=traduz('INSCRICOES_SUBSTITUICAO_TEXTO')?>
                </li>
                <li id="confirmacao">
                    <strong><?=traduz('INSCRICOES_CONFIRMACAO_TITULO')?></strong><br>
                    <?=traduz('INSCRICOES_CONFIRMACAO_TEXTO')?>
                </li>
                <li id="encerramento">
                    <strong><?=traduz('INSCRICOES_ENCERRAMENTO_TITULO')?></strong><br>
                    <?=traduz('INSCRICOES_ENCERRAMENTO_TEXTO')?>
                </li>
            </ul>

            
        </div>

    </div>
</div>