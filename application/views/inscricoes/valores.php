        <div class="conteudo conteudo-<?=$this->router->class?> conteudo-<?=$this->router->class?>-<?=$this->router->method?>">
        
            <?if(prefixo('')=='pt_'):?>

                <h1>TABELA DE PREÇOS DE INSCRIÇÕES E BONIFICAÇÕES</h1>

                <a href="http://www.febraban.org.br/ar/eventos/Inscricoes/Inscricao1-4.asp?id_evento=5534" class="botao btn-azul" target="_blank">INSCREVA-SE</a>
                
                <h2>Valores para participação nos 3 dias do evento</h2>
                <table>
                    <tbody>
                        <tr>
                            <td><?=traduz('INSCRICOES_TABELA_INSCRICOES')?></td>
                            <td><?=traduz('INSCRICOES_TABELA_FILIADOS')?></td>
                            <td><?=traduz('INSCRICOES_TABELA_NAO_FILIADOS')?></td>
                        </tr>
                        <tr>
                            <td><?=traduz('INSCRICOES_TABELA_LINHA_1')?></td>
                            <td>R$ 4.990,00</td>
                            <td>R$ 6.234,00</td>
                        </tr>
                        <tr>
                            <td><?=traduz('INSCRICOES_TABELA_LINHA_2')?></td>
                            <td>R$ 4.695,00</td>
                            <td>R$ 5.870,00</td>
                        </tr>
                        <tr>
                            <td><?=traduz('INSCRICOES_TABELA_LINHA_3')?></td>
                            <td>R$ 4.080,00</td>
                            <td>R$ 5.100,00</td>
                        </tr>
                        <tr>
                            <td><?=traduz('INSCRICOES_TABELA_LINHA_4')?></td>
                            <td>R$ 3.563,00</td>
                            <td>R$ 4.414,00</td>
                        </tr>                    
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3">Empresas com o mesmo CNPJ – VALORES POR PARTICIPANTE</td>
                        </tr>
                    </tfoot>
                </table>
                
                <h2>Valores para participação em apenas 1 dia do evento</h2>
                <table>
                    <tbody>
                        <tr>
                            <td><?=traduz('INSCRICOES_TABELA_INSCRICOES')?></td>
                            <td><?=traduz('INSCRICOES_TABELA_FILIADOS')?></td>
                            <td><?=traduz('INSCRICOES_TABELA_NAO_FILIADOS')?></td>
                        </tr>
                        <tr>
                            <td><?=traduz('INSCRICOES_TABELA_LINHA_1')?></td>
                            <td>R$ 1.998,00</td>
                            <td>R$ 2.488,00</td>
                        </tr>
                        <tr>
                            <td><?=traduz('INSCRICOES_TABELA_LINHA_2')?></td>
                            <td>R$ 1.858,00</td>
                            <td>R$ 2.319,00</td>
                        </tr>
                        <tr>
                            <td><?=traduz('INSCRICOES_TABELA_LINHA_3')?></td>
                            <td>R$ 1.620,00</td>
                            <td>R$ 2.041,00</td>
                        </tr>
                        <tr>
                            <td><?=traduz('INSCRICOES_TABELA_LINHA_4')?></td>
                            <td>R$ 1.418,00</td>
                            <td>R$ 1.780,00</td>
                        </tr>                    
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3">Empresas com o mesmo CNPJ – VALORES POR PARTICIPANTE</td>
                        </tr>
                    </tfoot>
                </table>
                
                <p class="negrito">Observações:</p>
                
                <p>No valor da inscrição estão incluídos: almoços, coffee breaks , coquetel do dia 12/06 e estacionamento nos 3 dias de realização do evento.</p>
                
                <ul>
                    <li><b>1.</b> Os descontos são válidos somente para inscrições na mesma categoria (participação nos 3 dias do evento ou participação em apenas um dia do evento).</li>
                    <li><b>2.</b> Manteremos o valor da 1ª. Fase para o banco que fizer mais do que o equivalente a 50 inscrições válidas para os três dias do evento. (ex.: 50 inscrições para participação nos 3 dias x R$ 2.287,00 = R$ 114.350,00 ou 124 inscrições para participação em um dia = R$ 114.328,00).</li>
                </ul>
                
                <h3>BONIFICAÇÃO</h3>
                
                <ul>
                    <li><b>1.</b> A cada 20 inscrições efetuadas a instituição recebe 01 (uma) inscrição cortesia de um dia; </li>
                    <li><b>2.</b> Instituições que fizerem mais de 50 (cinqüenta) inscrições e aumentarem o total de inscritos em relação a 2012 serão bonificadas, conforme abaixo:</li>
                </ul>
                
                <table class="tabela-bonificacao">
                    <thead>
                        <tr>
                            <th><?=traduz('INSCRICOES_BONIFICACAO_TABELA_TITULO_1')?></th>
                            <th><?=traduz('INSCRICOES_BONIFICACAO_TABELA_TITULO_2')?></th>
                            <th><?=traduz('INSCRICOES_BONIFICACAO_TABELA_TITULO_3')?></th>
                            <th><?=traduz('INSCRICOES_BONIFICACAO_TABELA_TITULO_4')?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="td-destaque"><?=traduz('INSCRICOES_BONIFICACAO_TABELA_TEXTO_1')?></td>
                            <td class="td-destaque"><?=traduz('INSCRICOES_BONIFICACAO_TABELA_TEXTO_2')?></td>
                            <td class="td-destaque"><?=traduz('INSCRICOES_BONIFICACAO_TABELA_TEXTO_3')?></td>
                            <td class="td-destaque"><?=traduz('INSCRICOES_BONIFICACAO_TABELA_TEXTO_4')?></td>
                        </tr>
                    </tbody>
                </table>
                
                <div class="legenda">
                    (*) A Tabela de Bonificação é válida exclusivamente para os acréscimos de inscrições em relação a  2012, conforme as faixas estabelecidas, e somente para empresas que fizeram acima de 50 (cinquenta) inscrições no Ciab  2013.<br>
                    (**) Para consultar a quantidade de inscrições realizadas em  2012, pedimos contatar a Diretoria de Eventos pelo telefone (11) 3186-9860.
                </div>
                
                <p class="negrito">
                   Exemplo:
                </p>
                
                <p>
                    Instituição Y realizou em  2012  50 inscrições para os 3 dias do evento. Em  2013, realizou 66 inscrições para os 3 dias. Isso quer dizer que a Instituição Y tem direito à bonificação de acima de 30%.<br>
                    <br>
                    <b>As bonificações adquiridas pela Instituição Y são :</b>
                    <br>
                    <ul>
                        <li>Desconto de 35% para a mesma categoria (3 dias ou 1 dia) até o último dia de realização do Congresso, conforme disponibilidade de vagas.</li>
                        <li>Gratuidade de 3 inscrições para participação em 1 dia do Congresso (a cada 20 inscrições 01 gratuidade).</li>
                        <li>Gratuidade de 20 inscrições para participação em 1 dia do Congresso, conforme tabela de bonificação.</li>
                    </ul>
                </p>

            <?elseif(prefixo('')=='en_'):?>

                <h1>INTERNATIONAL REGISTRATIONS</h1>

                <a href="http://www.febraban.org.br/ar/eventos/Inscricoes/Inscricao1-4.asp?id_evento=5534" class="botao btn-azul" target="_blank">INSCREVA-SE</a>

                <h2>Value For 3-Day Congress - US$ 600,00</h2>

                <p>Note: The Value of the inscription includes: lunch, coffee break, cocktail of day June 12 and parking in the three days during which the event is held.</p>

                <br>

                <p>
                <a href="http://www.febraban.org.br/ar/eventos/Inscricoes/Inscricao1-4.asp?id_evento=5534" target="_blank" class="botao bt-azul-escuro">REGISTER NOW</a>
                </p>

            <?elseif(prefixo('')=='es_'):?>

                <h1>INSCRIPCIONES INTERNACIONALES</h1>

                <a href="http://www.febraban.org.br/ar/eventos/Inscricoes/Inscricao1-4.asp?id_evento=5534" class="botao btn-azul" target="_blank">INSCREVA-SE</a>

                <h2>Inscripción para los 3 días del Congreso  – US$ 600,00</h2>

                <p>Dentro del valor de la inscripción están incluidos: almuerzos, coffee breaks, cocktail del día 12 de junio y aparcamiento durante los 3 días de realización del evento.</p>

                <br>

                <p>
                <a href="http://www.febraban.org.br/ar/eventos/Inscricoes/Inscricao1-4.asp?id_evento=5534" target="_blank" class="botao bt-azul-escuro">INSCRIPCIÓN</a>
                </p>

            <?endif;?>
            
        </div>

    </div>
</div>