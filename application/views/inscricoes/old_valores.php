        <div class="conteudo conteudo-<?=$this->router->class?> conteudo-<?=$this->router->class?>-<?=$this->router->method?>">
        
            <?if(prefixo('')=='pt_'):?>

                <h1><?=traduz('INSCRICOES_TITULO')?></h1>
                
                <!-- <a href="http://www.febraban.org.br/ar/eventos/inscricoes/inscricao1-4.asp?id_evento=4916" target="_blank"><img id="banner-desconto" src="_imgs/inscricoes/desconto.png" alt="35% desconto - clique aqui"></a> -->
                <a class="botao bt-azul-escuro" style="margin-bottom:30px;" href="http://www.febraban.org.br/ar/eventos/inscricoes/inscricao1-4.asp?id_evento=4916" target="_blank">CLIQUE AQUI E INSCREVA-SE</a>
                
                <h2><?=traduz('INSCRICOES_TITULO_TABELA_1')?></h2>
                <table>
                    <thead>
                        <tr>
                            <th colspan="3"><?=traduz('DATA_ATE')?> 10/06/2012</th>
                            <th colspan="2"><?=traduz('DATA_APOS')?> 11/06/2012</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?=traduz('INSCRICOES_TABELA_INSCRICOES')?></td>
                            <td><?=traduz('INSCRICOES_TABELA_FILIADOS')?></td>
                            <td><?=traduz('INSCRICOES_TABELA_NAO_FILIADOS')?></td>
                            <td><?=traduz('INSCRICOES_TABELA_FILIADOS')?></td>
                            <td><?=traduz('INSCRICOES_TABELA_NAO_FILIADOS')?></td>
                        </tr>
                        <tr>
                            <td><?=traduz('INSCRICOES_TABELA_LINHA_1')?></td>
                            <td>R$ 3.564,00</td>
                            <td>R$ 4.453,00</td>
                            <td>R$ 4.752,00</td>
                            <td>R$ 5.937,00</td>
                        </tr>
                        <tr>
                            <td><?=traduz('INSCRICOES_TABELA_LINHA_2')?></td>
                            <td>R$ 3.354,00</td>
                            <td>R$ 4.193,00</td>
                            <td>R$ 4.471,00</td>
                            <td>R$ 5.590,00</td>
                        </tr>
                        <tr>
                            <td><?=traduz('INSCRICOES_TABELA_LINHA_3')?></td>
                            <td>R$ 2.914,00</td>
                            <td>R$ 3.644,00</td>
                            <td>R$ 3.885,00</td>
                            <td>R$ 4.858,00</td>
                        </tr>
                        <tr>
                            <td><?=traduz('INSCRICOES_TABELA_LINHA_4')?></td>
                            <td>R$ 2.545,00</td>
                            <td>R$ 3.153,00</td>
                            <td>R$ 3.393,00</td>
                            <td>R$ 4.204,00</td>
                        </tr>                    
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="5"><?=traduz('INSCRICOES_TABELA_RODAPE')?></td>
                        </tr>
                    </tfoot>
                </table>
                
                <h2><?=traduz('INSCRICOES_TITULO_TABELA_2')?></h2>
                <table>
                    <thead>
                        <tr>
                            <th colspan="3"><?=traduz('DATA_ATE')?> 10/06/2012</th>
                            <th colspan="2"><?=traduz('DATA_APOS')?> 11/06/2012</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?=traduz('INSCRICOES_TABELA_INSCRICOES')?></td>
                            <td><?=traduz('INSCRICOES_TABELA_FILIADOS')?></td>
                            <td><?=traduz('INSCRICOES_TABELA_NAO_FILIADOS')?></td>
                            <td><?=traduz('INSCRICOES_TABELA_FILIADOS')?></td>
                            <td><?=traduz('INSCRICOES_TABELA_NAO_FILIADOS')?></td>
                        </tr>
                        <tr>
                            <td><?=traduz('INSCRICOES_TABELA_LINHA_1')?></td>
                            <td>R$ 1.427,00</td>
                            <td>R$ 1.778,00</td>
                            <td>R$ 1.903,00</td>
                            <td>R$ 2.370,00</td>
                        </tr>
                        <tr>
                            <td><?=traduz('INSCRICOES_TABELA_LINHA_2')?></td>
                            <td>R$ 1.328,00</td>
                            <td>R$ 1.657,00</td>
                            <td>R$ 1.770,00</td>
                            <td>R$ 2.209,00</td>
                        </tr>
                        <tr>
                            <td><?=traduz('INSCRICOES_TABELA_LINHA_3')?></td>
                            <td>R$ 1.157,00</td>
                            <td>R$ 1.458,00</td>
                            <td>R$ 1.543,00</td>
                            <td>R$ 1.944,00</td>
                        </tr>
                        <tr>
                            <td><?=traduz('INSCRICOES_TABELA_LINHA_4')?></td>
                            <td>R$ 1.013,00</td>
                            <td>R$ 1.272,00</td>
                            <td>R$ 1.351,00</td>
                            <td>R$ 1.696,00</td>
                        </tr>                    
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="5"><?=traduz('INSCRICOES_TABELA_RODAPE')?></td>
                        </tr>
                    </tfoot>
                </table>
                
                <p class="negrito"><?=traduz('INSCRICOES_OBSERVACAO_TITULO')?>:</p>
                
                <p><?=traduz('INSCRICOES_OBSERVACAO_TEXTO')?></p>
                
                <ul>
                    <li><b>1.</b> <?=traduz('INSCRICOES_OBSERVACAO_ITEM_1')?></li>
                    <li><b>2.</b> <?=traduz('INSCRICOES_OBSERVACAO_ITEM_2')?></li>
                </ul>
                
                <h3><?=traduz('INSCRICOES_BONIFICACAO_TITULO')?></h3>
                
                <ul>
                    <li><b>1.</b> <?=traduz('INSCRICOES_BONIFICACAO_ITEM_1')?></li>
                    <li><b>2.</b> <?=traduz('INSCRICOES_BONIFICACAO_ITEM_2')?></li>
                </ul>
                
                <table class="tabela-bonificacao">
                    <thead>
                        <tr>
                            <th><?=traduz('INSCRICOES_BONIFICACAO_TABELA_TITULO_1')?></th>
                            <th><?=traduz('INSCRICOES_BONIFICACAO_TABELA_TITULO_2')?></th>
                            <th><?=traduz('INSCRICOES_BONIFICACAO_TABELA_TITULO_3')?></th>
                            <th><?=traduz('INSCRICOES_BONIFICACAO_TABELA_TITULO_4')?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="td-destaque"><?=traduz('INSCRICOES_BONIFICACAO_TABELA_TEXTO_1')?></td>
                            <td class="td-destaque"><?=traduz('INSCRICOES_BONIFICACAO_TABELA_TEXTO_2')?></td>
                            <td class="td-destaque"><?=traduz('INSCRICOES_BONIFICACAO_TABELA_TEXTO_3')?></td>
                            <td class="td-destaque"><?=traduz('INSCRICOES_BONIFICACAO_TABELA_TEXTO_4')?></td>
                        </tr>
                    </tbody>
                </table>
                
                <div class="legenda">
                    <?=traduz('INSCRICOES_BONIFICACAO_TABELA_LEGENDA')?>                
                </div>
                
                <p class="negrito">
                    <?=traduz('INSCRICOES_EXEMPLO_TITULO')?>:
                </p>
                
                <p>
                    <?=traduz('INSCRICOES_BONIFICACAO_TEXTO')?>                
                    <br>
                    <b><?=traduz('INSCRICOES_BONIFICACAO_TEXTO_NEGRITO')?>:</b>
                    <br>
                    <ul>
                        <li><?=traduz('INSCRICOES_BONIFICACAO_ITEM_EXEMPLO_1')?></li>
                        <li><?=traduz('INSCRICOES_BONIFICACAO_ITEM_EXEMPLO_2')?></li>
                        <li><?=traduz('INSCRICOES_BONIFICACAO_ITEM_EXEMPLO_3')?></li>
                    </ul>
                </p>

            <?elseif(prefixo('')=='en_'):?>

                <h1>INTERNATIONAL REGISTRATIONS</h1>
                <h2>Value For 3-Day Congress - US$ 600,00</h2>

                <p>Note: The Value of the registration mentioned above includes: lunch, coffee break, and parking for the three days of the event. </p>

                <br>

                <p>
                <a href="http://www.febraban.org.br/ar/eventos/inscricoes/inscricao1-4.asp?id_evento=4916&id_idioma=2&pais=BRAZIL" target="_blank" class="botao bt-azul-escuro">REGISTER NOW</a>
                </p>

            <?elseif(prefixo('')=='es_'):?>

                <h1>INSCRIPCIONES INTERNACIONALES</h1>
                <h2>Inscripción para los 3 días del Congreso – US$ 600,00</h2>

                <p>Dentro del valor de la inscripción están incluidos: almuerzos, coffee breaks y aparcamiento durante los 3 días de realización del evento.</p>

                <br>

                <p>
                <a href="http://www.febraban.org.br/ar/eventos/inscricoes/inscricao1-4.asp?id_evento=4916&id_idioma=3&pais=BRAZIL" target="_blank" class="botao bt-azul-escuro">REGISTER NOW</a>
                </p>

            <?endif;?>
            
        </div>

    </div>
</div>