        <div class="conteudo conteudo-<?=$this->router->class?> conteudo-<?=$this->router->class?>-<?=$this->router->method?>">
        
        <?if(prefixo('')=='pt_'):?>

            <h1>Informações Importantes:</h1>
            
            <ul id="lista-info">
                <li id="pagamento">
                    <strong>OPÇÕES DE PAGAMENTO</strong><br>
                    - Boleto Bancário<br>
                    - Cartões de Crédito Nacionais e Internacionais
                </li>
                <li id="reembolso">
                    <strong>REEMBOLSOS E CANCELAMENTOS</strong><br>
                    Os pedidos de reembolso ou cancelamento de inscrições serão acatados, mediante solicitação por escrito endereçada para o e-mail <a href="mailto:eventos@febraban.org.br">eventos@febraban.org.br</a> até o dia 31 de maio de 2013.<br>
                    Os reembolsos serão efetuados pelo valor líquido, deduzidas todas as despesas bancárias provenientes da transferência bancária.<br>
                    A partir de 1º de junho de 2013 não serão aceitos pedidos de reembolso ou cancelamentos.
                </li>
                <li id="substituicao">
                    <strong>SUBSTITUIÇÃO DE PARTICIPANTE</strong><br>
                    A FEBRABAN somente aceitará a substituição de participante desde que a solicitação seja feita por e-mail dirigido ao endereço eletrônico <a href="mailto:eventos@febraban.org.br">eventos@febraban.org.br</a> até o dia 24.05.2013.<br>
                    Para substituições solicitadas durante a realização do evento a Organização cobrará uma taxa de R$ 250,00 pela reemissão do crachá. O respectivo pagamento deverá ser efetuado no ato da solicitação. 
                </li>
                <li id="confirmacao">
                    <strong>CONFIRMAÇÃO DE INSCRIÇÃO VIA E-MAIL</strong><br>
                    Após realizada a inscrição, o participante receberá automaticamente por e-mail disparado pelo sistema de inscrições a sua confirmação. <br>
                    Caso não receba essa confirmação , o participante ou a área responsável pela inscrição poderá entrar em contato com a Diretoria de Eventos da FEBRABAN pelo e-mail <a href="mailto:eventos@febraban.org.br">eventos@febraban.org.br</a>, solicitando a verificação.
                </li>
                <li id="encerramento">
                    <strong>ENCERRAMENTO DAS INSCRIÇÕES</strong><br>
                    24.05.2013 ou até o encerramento das vagas.
                </li>
            </ul>

        <?elseif(prefixo('')=='es_'):?>

            <h1>Información Importante:</h1>
            
            <ul id="lista-info">
                <li id="pagamento">
                    <strong>OPCIONES DE PAGO</strong><br>
                    - Tarjeta de Crédito Internacional
                </li>
                <li id="reembolso">
                    <strong>REEMBOLSOS Y CANCELACIONES</strong><br>
                    Los pedidos de reembolso o de cancelaciones de inscripciones serán recibidos, a través de una solicitación por escrito destinado para el e-mail <a href="mailto:eventos@febraban.org.br">eventos@febraban.org.br</a> hasta el día 31 de mayo de 2013.<br>
                    Los reembolsos serán efectuados en el valor neto, descontadas los gastos bancarios provenientes de la transferencia bancaria.<br>
                    A partir del 1º de junio de 2013 no serán más aceptados los pedidos de reembolso ó de cancelaciones.
                </li>
                <li id="substituicao">
                    <strong>SUSTITUCIÓN DE PARTICIPANTE</strong><br>
                    FEBRABAN, solamente aceptará la  sustitución del participante desde que la solicitación sea hecha vía e-mail y dirigido para la dirección electrónica: <a href="mailto:eventos@febraban.org.br">eventos@febraban.org.br</a> hasta el día 24 de mayo de 2013.<br>
                    Para  las sustituciones solicitadas durante la realización del evento la Organización cobrará una tasa de R$ 250,00 por la reemisión del gafete.  El respectivo pago deberá ser efectuado en el acto de la solicitación. 
                </li>
                <li id="confirmacao">
                    <strong>CONFIRMACIÓN DE LA INSCRIPCIÓN VÍA E-MAIL</strong><br>
                    Una vez realizada la inscripción, el sistema de inscripciones de la FEBRABAN enviará un e-mail de confirmación de inscripción del participante. <br>
                    En caso de que no se tenga una respuesta de confirmación de la inscripción, el participante o el área responsable por la inscripción debe entrar en contacto con la Dirección de Eventos de la FEBRABAN a través del e-mail: <a href="mailto:eventos@febraban.org.br">eventos@febraban.org.br</a>, solicitando la verificación de la inscripción.
                </li>
                <li id="encerramento">
                    <strong>ENCERRAMIENTO DE LAS INSCRIPCIONES</strong><br>
                    Las inscripciones serán encerradas el 24 de mayo de 2013 o hasta que el número de vacantes sea ocupado.
                </li>
            </ul>

        <?elseif(prefixo('')=='en_'):?>

            <h1>IMPORTANT INFORMATION:</h1>
            
            <ul id="lista-info">
                <li id="pagamento">
                    <strong>PAYMENT OPTION:</strong><br>
                    - International credit cards
                </li>
                <li id="reembolso">
                    <strong>REFUNDS AND CANCELLATIONS:</strong><br>
                    The requests for refunds or cancellations of inscriptions will be accepted, through request in writing addressed to email <a href="mailto:eventos@febraban.org.br">eventos@febraban.org.br</a> by May 31, 2013.<br>
                    The refunds will be executed at the net amount, deducting all bank charges arising from bank transfers. From June 1, 2013 will not be accepted requests for refunds or cancellations.
                </li>
                <li id="substituicao">
                    <strong>SUBSTITUTION OF PARTICIPANT</strong><br>
                    FEBRABAN will only accept the substitution of a participant provided that the request is made by email sent to the electronic address <a href="mailto:eventos@febraban.org.br">eventos@febraban.org.br</a> by May 24, 2013.<br>
                    For substitutions requested during the holding of the event the Organization will charge R$ 250.00 for re-issuing the name tag. The respective payment shall be made upon making the request. 
                </li>
                <li id="confirmacao">
                    <strong>INSCRIPTION CONFIRMATION BY EMAIL</strong><br>
                    After inscription done, the inscription system of FEBRABAN will send an email confirming the participant’s inscription.<br>
                    If this confirmation is not received, the participant or area in charge of the inscription will be able to contact the Directory of Events of FEBRABAN by email <a href="mailto:eventos@febraban.org.br">eventos@febraban.org.br</a>, requesting verification.
                </li>
                <li id="encerramento">
                    <strong>CLOSING OF INSCRIPTIONS</strong><br>
                    The inscriptions close on May 24, 2013 or when all the vacancies are filled.
                </li>
            </ul>
            
        <?endif;?>

        </div>

    </div>
</div>

<style type="text/css">
.conteudo #lista-info li a{
    text-decoration:none;
    color:#686D72;
    font-weight:bold;
}
.conteudo #lista-info li a:active, .conteudo #lista-info li a:visited{ color:#686D72; }
.conteudo #lista-info li a:hover{ text-decoration:underline; }
</style>