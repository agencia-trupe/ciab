<div class="centropad main-<?=$this->router->class?>">
    
    <div id="container">
    
        <div id="menu-lateral">
        
            <h3><?=traduz('INSCRICOES')?></h3>
            
            <ul class="submenu-inscricoes">
                <li id="submn-valores"><a href="inscricoes/valores"><?=traduz('INSCRICOES_SUBMENU_1')?></a></li>
                <div class="faixa-sob-item-menu"><div></div></div>
                
                <li id="submn-informacoes"><a href="inscricoes/informacoes"><?=traduz('INSCRICOES_SUBMENU_2')?></a></li>
                <div class="faixa-sob-item-menu"><div></div></div>
                
            </ul>
            
            <script defer>$(document).ready( function(){ $('#submn-<?=$this->router->method?>').addClass('subativo'); });</script>
            