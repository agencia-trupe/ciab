<div class="centro">
    <div id="veja-mais" class="sombra-grande">
            <h1><?=traduz('VEJA_MAIS_TITULO')?>//</h1>
            
            <div id="links-target"></div>
            <script>
                $(document).ready( function(){
                    $.post(BASE + 'ajax/linksVejaMais', { origem : '<?=uri_string()?>', lang : '<?=prefixo('')?>' }, function(retorno){
                        retorno = JSON.parse(retorno)
                        var conta_links = 0
                        retorno.map( function(self){
                            if(conta_links == 1)
                                html = "<div class='veja-mais-link "+self.classe+"' style='background:url(_imgs/vejamais/"+self.imagem+") #FFF center top no-repeat; margin:0 21px;'>"
                            else
                                html = "<div class='veja-mais-link "+self.classe+"' style='background:url(_imgs/vejamais/"+self.imagem+") #FFF center top no-repeat;'>"
                            html += "<a href='"+self.destino+"'>"
                            html += "<h2>"+self.titulo+"</h2>"
                            html += "<h3>"+self.subtitulo+"</h3>"
                            html += "<p>"+self.texto+"</p>"
                            html += "</a></div>"
                            $('#links-target').append(html)
                            conta_links++
                        })
                    })    
                })
            </script>
        
    </div>    
</div>