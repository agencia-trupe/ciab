<footer>
    <div class="centropad">
        <h1 class="titulo-patrocinio"><?=traduz('PATROCINADORES')?></h1>
        
        <h2><?=traduz('DIAMANTE')?></h2>
        <div class="sombra-grande-fundo">
            <img src="_imgs/patrocinio/diamante.png" alt="Patrocinadores Diamante">
        </div>
        
        <h2><?=traduz('OURO')?></h2>
        <div class="sombra-grande-fundo">
            <img src="_imgs/patrocinio/ouro.png" alt="Patrocinadores Ouro">
        </div>

        <h2><?=traduz('PRATA')?><span style="margin-left:101px"><?=traduz('APOIO')?><span style="margin-left:458px"><?=traduz('CIA AÉREA OFICIAL')?></span></h2>
        <div class="sombra-grande-fundo">
            <img src="_imgs/patrocinio/apoio.png" alt="Patrocinadores Ouro">
        </div>        
        
    </div>
    <address class="centro">
        <a id="link-febraban" href="http://www.febraban.org.br/" target="_blank" title="FEBRABRAN"></a>
        <div class="borda-wrapper">
            <ul>
                <li <?if($this->router->class == 'contato') echo "class='footerativo'"?>>
                    <a href="contato"><?=traduz('FALE CONOSCO')?></a>&nbsp;&#124;&nbsp;
                </li>
                <li <?if($this->router->method == 'privacidade') echo "class='footerativo'"?>>
                    <a href="sobre/privacidade"><?=traduz('POLITICA DE PRIVACIDADE')?></a>&nbsp;&#124;&nbsp;
                </li>
                <li <?if($this->router->method == 'termos') echo "class='footerativo'"?>>
                    <a href="sobre/termos"><?=traduz('TERMO DE USO')?></a>&nbsp;&#124;&nbsp;
                </li>
                <li <?if($this->router->class == 'sobre' && $this->router->method == 'mapa') echo "class='footerativo'"?>>
                    <a href="sobre/mapa"><?=traduz('MAPA DO SITE')?></a>
                </li>
            </ul>
            
            <?=traduz('address-linha1')?> <br>
            AV. BRIGADEIRO FARIA LIMA, 1.485 - 14º ANDAR • SÃO PAULO • PABX +5511 3244 9800 / 3186 9800 • WWW.FEBRABAN.ORG.BR
        </div>
    </address>
</footer>

<script defer>
    $(document).ready( function(){
        /*Adiciona a classe de selecionado e dá um hover para acionar no css pie*/
        $('#mn-<?=$marcar_menu?>').addClass('ativo').find('>a').addClass('ativo');
        if(jQuery.browser.msie){
            $('#mn-<?=$marcar_menu?>').removeClass('ativo').addClass('ativo');        
        }
    });
</script>
<!-- GOOGLE ANALYTICS -->
<script type="text/javascript">

    var _gaq = _gaq || [];

    _gaq.push(['_setAccount', 'UA-3461385-24']);

    _gaq.push(['_trackPageview']);

    (function () {

        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

    })();

</script>

</body>
</html>