<div id="compartilhe">
    <span class="label"><?=traduz('Compartilhe')?>//</span>
    <a href="https://twitter.com/share" class="twitter-share-button" data-lang="pt" data-count="none">Tweet</a>
    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
    <span class="espacador"></span>
    <script src="//platform.linkedin.com/in.js" type="text/javascript"></script><script type="IN/Share"></script>
    <span class="espacador"></span>
    <g:plusone size="medium" annotation="none"></g:plusone>
    <span class="espacador"></span>
    <div class="fb-like" data-href="<?=current_url()?>" data-send="false" data-layout="button_count" data-width="55" data-show-faces="false" data-font="trebuchet ms"></div>
</div>