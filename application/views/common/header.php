<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="pt-BR"> <![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  
  <title>CIAB 2013</title>
  <meta name="description" content="Congresso e Exposição de Tecnologia da Informação das Instituições Financeiras">
  <meta name="keywords" content="Congresso de TI, TI para Instituições Financeiras, Febraban, Ciab, " />
  <meta name="robots" content="index, follow" />
  
    <!-- FACEBOOK -->
    <meta property="og:title" content="<? echo isset($fb_title) ? $fb_title : 'CIAB Febraban 2013' ?>" />
    <meta property="og:description" content="<? echo isset($fb_description) ? $fb_description : 'Congresso e Exposição de Tecnologia da Informação das Instituições Financeiras' ?>" />
    <meta property="og:image" content="<? echo isset($fb_image) ? $fb_image : base_url('_imgs/layout/logomarca2013.png') ?>" />    
    
    <meta property="og:type" content="activity" />
    <meta property="og:url" content="<?=current_url()?>" />
    <meta property="og:site_name" content="CIAB 2013" />
    <meta property="fb:admins" content="100002297057504" />
    <?
    switch(prefixo('')){
      case 'pt_':
        $lang_str_url = 'pt_BR';
        $lang_str = 'pt-BR';
        break;
      case 'en_':
        $lang_str_url = 'en_US';
        $lang_str = 'en-US';
        break;
      case 'es_':
        $lang_str_url = 'es_ES';
        $lang_str = 'es-ES';
        break;
      default:
        $lang_str_url = 'pt_BR';
        $lang_str = 'pt-BR';
    }
    ?>
    
    <!-- GOOGLE PLUS -->
    <script type="text/javascript">
      window.___gcfg = {lang: '<?=$lang_str?>'};
    
      (function() {
        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
        po.src = 'https://apis.google.com/js/plusone.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
      })();
    </script>  
  
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <base href="<?= base_url() ?>">

  <script>
    var BASE = '<?=base_url()?>'
  </script>
  
  <!-- CSS -->
  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="css/fontface/stylesheet.css">
  <link rel="stylesheet" href="css/base.css">
  <link rel="stylesheet" href="css/<?=$this->router->class?>.css">
  <link rel="stylesheet" href="css/fancybox/jquery.fancybox-1.3.4.css">
  <!--[if lt IE 9]><link rel="stylesheet" href="css/ie8.css"><![endif]-->
  <link rel='stylesheet' href='css/ingles.css'>
  <!--[if gt IE 8]><link rel="stylesheet" href="css/ingles_ie9.css"><![endif]-->
  
  <!-- JAVASCRIPT -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="js/jquery-1.6.4.min.js"><\/script>')</script>
  <script src="js/jquery-ui-1.8.18.custom.min.js"></script>
  <script src="js/modernizr-2.0.6.min.js"></script>
  <script>Array.prototype.map || document.write('<script src="js/map.js"><\/script>')</script>
  <script src="js/jquery.fancybox-1.3.4.js"></script>
  <script src="js/placeholder.js"></script>
  <script src="js/cycle.js"></script>
  <script defer src="js/front.js?a=<?=time()?>"></script>
  <?if($this->router->class == 'evento' && ($this->router->method == 'planta' || $this->router->method == 'mapa')):?>
    <script src="js/jquery.anythingzoomer.min.js"></script>
  <?endif;?>
    
</head>

<body class="<?=prefixo('body')?>">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/<?=$lang_str_url?>/all.js#xfbml=1&appId=179472998836941";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="faixa-topo">
    <div class="verde cor"></div><div class="azul cor"></div>
    <div class="vermelho cor"></div><div class="laranja cor"></div>
</div>

<div id="superior">
    
    <div id="topo" class="centro <?=prefixo('centro')?>">
    
        <!-- <img src="_imgs/layout/<=prefixo('img-topo.png')?>" alt="Febraban" class="imagem-topo"> -->
    
        <a href="home" class="logo" title="Página Inicial"></a>

        <img src="_imgs/layout/marca-febraban.png" alt="Febraban" class="img-fbb">
        
        <div>        
            <form method="post" action="busca">
                <input type="text" name="busca" autocomplete="off" class="placeholder" placeholder="<?=traduz('BUSCA')?>//">
                <input type="submit" value="">
            </form>
            
            <a href="<?=base_url('rss')?>" target="_blank" id="mn-rss" class="icones"><img src="_imgs/layout/rss-btn.png" alt="RSS Feed"></a>
            <a href="http://www.twitter.com/CiabFEBRABAN" id="mn-twitter" class="icones divisao"><img src="_imgs/layout/twitter-btn.png" alt="Twitter"></a>
            <a href="linguagem/pt" id="mn-pt" class="icones"><img src="_imgs/layout/lang-btn-pt.png" alt="<?=traduz('ling_portugues')?>"></a>
            <a href="linguagem/es" id="mn-es" class="icones"><img src="_imgs/layout/lang-btn-es.png" alt="<?=traduz('ling_espanhol')?>"></a>
            <a href="linguagem/en" id="mn-en" class="icones ultimo"><img src="_imgs/layout/lang-btn-en.png" alt="<?=traduz('ling_ingles')?>"></a>
        </div>
        
    </div>
    
    <nav class="centro">
        <ul>
            <li id="mn-evento"><a href="evento"><?=traduz('EVENTO')?></a></li><li id="mn-programacao"><a href="programacao"><?=traduz('PROGRAMACAO')?></a></li><li id="mn-palestrantes"><a href="palestrantes"><?=traduz('PALESTRANTES')?></a></li><li id="mn-premios"><a href="premios"><?=traduz('PREMIOS')?></a></li><li id="mn-inscricoes"><a href="inscricoes"><?=traduz('INSCRICOES')?></a></li><li id="mn-noticias"><a href="noticias"><?=traduz('NOTICIAS')?></a></li><li id="mn-publicacoes"><a href="publicacoes"><?=traduz('PUBLICACOES')?></a></li><li id="mn-imprensa"><a href="imprensa"><?=traduz('IMPRENSA')?></a></li><li id="mn-informacoes"><a href="informacoes"><?=traduz('INFORMACOES')?></a></li><li id="mn-contato"><a href="contato"><?=traduz('CONTATO')?></a></li>
        </ul>        
    </nav>