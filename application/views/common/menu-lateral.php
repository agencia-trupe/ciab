<?if(isset($submenu) AND $submenu === FALSE):?>

    <div class="centropad main-<?=$this->router->class?>">

        <div id="container">

            <div id="menu-lateral">

                <?if(isset($titulo) && $titulo):?>
                    <h3><?=$titulo?></h3>
                <?endif;?>
<?endif;?>

    <div class="box">
        <h4><?=traduz('BOX_INSCRICOES_TITULO')?></h4>
        <a href="http://www.febraban.org.br/ar/eventos/inscricoes/loginpalestras.asp" target="_blank">
            <div class="texto"><?=traduz('BOX_INSCRICOES_TEXTO')?></div>
        </a>
    </div>

    <div class="box">
        <h4><?=traduz('VANTAGENS')?></h4>
        <a href="informacoes/tam">
            <div class="texto tam">
                <?=traduz('A TAM é a CIA Aérea Oficial do Ciab 2013 e quem ganha o desconto é você. Clique aqui e veja as condições especiais oferecidas.')?>
            </div>
        </a>
    </div>

    <div class="box-inferior box">
        <h4><?=traduz('PROMOÇÃO')?></h4>
        <a href="http://www.jdconsultores.com.br/siteJD/noticias.asp?id=116&tipo=1" target="_blank">
            <div class="texto" style="background: url('../_imgs/layout/blog.png') no-repeat; background-position: 13px 5px;">
                <?=traduz('PROMOÇÃO2')?>
            </div>
        </a>
    </div>

    <h4><?=traduz('TWITTER_TITULO')?></h4>
    <a class="twitter-timeline" data-dnt="true" href="https://twitter.com/CiabFEBRABAN" data-widget-id="345220758580113408">Tweets de @CiabFEBRABAN</a>
    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

    <div class="faixa-sobre-titulo"><div></div></div>

    <h4><?=traduz('NEWSLETTER_TITULO')?></h4>
    <p><?=traduz('NEWSLETTER_TEXTO')?></p>
    <div id="form-wrapper">
        <form method="post" id="newsletter-form" action="">
            <input type="text" name="nome" class="placeholder" placeholder="<?=TRADUZ('NEWSLETTER_PLACEHOLDER_NOME')?>" required>
            <input type="email" name="email" class="placeholder" placeholder="<?=TRADUZ('NEWSLETTER_PLACEHOLDER_EMAIL')?>" required>
            <input type="submit" value="<?=TRADUZ('NEWSLETTER_SUBMIT')?>">
        </form>
        <div id="form-resposta"></div>
    </div>
</div>