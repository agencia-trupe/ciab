        <div class="conteudo conteudo-<?=$this->router->class?> conteudo-<?=$this->router->class?>-<?=$this->router->method?>">
        
            <h1><?=traduz('Parceria TAM')?></h1>
            
            <h2><?=traduz('A TAM é a Cia Aérea Oficial do Ciab 2013 e quem ganha o desconto é você!')?></h2>
            
            <br>

            <p>
                <?=traduz('Em parceria com o Ciab 2013, a TAM como Cia Aérea Oficial do congresso disponibiliza passagens aéreas com condições especiais para você comparecer ao Ciab e ficar por dentro de tudo que as empresas de ponta em tecnologia estão preparando para superar “Os Novos Desafios do Setor Financeiro”.')?>
            </p>

            <br>

            <p>
                <?=traduz('Confira abaixo as condições que preparamos para você, válidos para todos os trechos <strong>Brasil/São Paulo/Brasil</strong> no período de <strong>embarque</strong> entre <strong>09 e 17.06.2013:</strong>')?>
            </p>

            <br>

            <p style="padding-left:10px;">
                <?=traduz('- 25%* de desconto na tarifa da classe disponível<br><br><strong>ou</strong><br><br>- 10%* de desconto nas tarifas Mega Promo e nas classes W e N')?>
            </p>

            <br>

            <p>
                <?=traduz("Para conseguir o seu desconto é fácil:<br>Clique no botão abaixo ou acesse <a href='http://www.tam.com.br' title='TAM' target='_blank'>www.tam.com.br</a> e ao escolher o <strong>trecho</strong> e a <strong>data</strong>, digite o número do Código Promocional <strong>413709</strong>.")?>
            </p>
            
            <br>

            <a class="botao bt-azul btn-esquerda" target="_blank" href="http://www.tam.com.br"><?=traduz('Clique e Compre sua Passagem')?></a>

            <br><br><br>

            <p>
                <?=traduz('* valores sujeitos à disponibilidade de assentos e regras/restrições específicas de cada tarifa, válidos para embarque três dias antes e três após a data do evento no trecho Brasil/São Paulo/Brasil.')?>
            </p>

        </div>

    </div>
</div>