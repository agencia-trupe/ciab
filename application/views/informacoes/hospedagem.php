        <div class="conteudo conteudo-<?=$this->router->class?> conteudo-<?=$this->router->class?>-<?=$this->router->method?>">
        
            <h1><?=traduz('INFORMACOES_HOTEIS_TITULO')?></h1>
            
            <p><?=traduz('INFORMACOES_HOTEIS_SUBTITULO')?>:</p>
            
            <div id="legenda"><?=traduz('INFORMACOES_HOTEIS_LEGENDA')?></div>
            
            <table id="tabela-hoteis">
                <tr>
                    <th><?=traduz('INFORMACOES_HOTEIS_TABELA_TITULO_1')?></th>
                    <th><?=traduz('INFORMACOES_HOTEIS_TABELA_TITULO_2')?></th>
                    <th><?=traduz('INFORMACOES_HOTEIS_TABELA_TITULO_3')?></th>
                </tr>
                
                <tr data-target="transamerica">
                    <td>Hotel Transamérica São Paulo</td>
                    <td>R$ 468,00</td>
                    <td>R$ 520,00</td>
                </tr>
                
                <tr data-target="chacara">
                    <td>Transamérica Chácara Santo Antônio</td>
                    <td>R$ 283,00</td>
                    <td>R$ 313,00</td>
                </tr>
                
                <tr data-target="congonhas">
                    <td>Transamérica Executive Congonhas</td>
                    <td>R$ 315,00</td>
                    <td>R$ 345,00</td>
                </tr>
                <!--
                <tr data-target="prime">
                    <td>Transamérica Prime International Plaza</td>
                    <td>R$ 355,00 (<=traduz('INFORMACOES_HOTEIS_COMDESCONTO')?>)</td>
                    <td>R$ 385,00 (<=traduz('INFORMACOES_HOTEIS_COMDESCONTO')?>)</td>
                </tr>
                -->
                <tr data-target="morumbi">
                    <td>Blue Tree Premium Morumbi</td>
                    <td>R$ 421,00</td>
                    <td>R$ 491,00</td>
                </tr>
                
                <tr data-target="verbo">
                    <td>Blue Tree Premium Verbo Divino</td>
                    <td>R$ 306,00</td>
                    <td>R$ 391,00</td>
                </tr>

                <tr data-target="berrini">
                    <td>HOTEL quality berrini</td>
                    <td>R$ 325,00</td>
                    <td>R$ 375,00</td>
                </tr>
            </table>
            
            <section id="transamerica">
                <h2>HOTEL TRANSAMÉRICA SÃO PAULO</h2>
                
                <div class="coluna prim">
                    <strong><?=traduz('INFORMACOES_HOTEIS_LOCAL')?>:</strong><br>
                    Av. das Nações Unidas, 18.591 – Santo Amaro – SP<br>
                    E-mail: <a href="mailto:gruposhtsp@transamerica.com.br">gruposhtsp@transamerica.com.br</a><br>
                    <?=traduz('INFORMACOES_HOTEIS_TEL')?>: (55) 11 5693-4050<br>
                    <a href="http://maps.google.com.br/maps?q=Av.+das+Na%C3%A7%C3%B5es+Unidas,+18.591+%E2%80%93+Santo+Amaro+%E2%80%93+SP&hl=pt-BR&ie=UTF8&sll=-14.239424,-53.186502&sspn=56.118961,93.076172&hnear=Av.+das+Na%C3%A7%C3%B5es+Unidas,+18591+-+Santo+Amaro,+S%C3%A3o+Paulo,+04795-100&t=m&z=17" target="_blank"><?=traduz('INFORMACOES_HOTEIS_COMO_CHEGAR')?></a>
                </div>
                
                <div class="coluna seg">
                    <strong><?=traduz('INFORMACOES_HOTEIS_CODIGO')?>: CIAB FEBRABAN 2013</strong><br>
                    <?=traduz('INFORMACOES_HOTEIS_DIARIA_SGL')?> - R$ 468,00<br>
                    <?=traduz('INFORMACOES_HOTEIS_DIARIA_DBL')?> - R$ 520,00<br>
                </div>
                
                <br>
                
                <div class="coluna checkin prim">
                    <strong>Check-in/out: </strong><br>
                    <?=traduz('INFORMACOES_HOTEIS_CHECK_3')?>
                </div>
                <div class="coluna diaria seg">
                    <strong><?=traduz('INFORMACOES_HOTEIS_DIARIA')?>:</strong><br>
                    - <?=traduz('INFORMACOES_HOTEIS_DIARIA_HT_1')?>
                </div>
                
                <div class="observacoes">
                    <strong><?=traduz('INFORMACOES_HOTEIS_OBSERVACOES')?>:</strong><br>
                    <?=traduz('INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_1')?>
                </div>
            </section>

        
            <section id="chacara">
                <h2>TRANSAMÉRICA CHÁCARA SANTO ANTONIO</h2>
                
                <div class="coluna prim">
                    <strong><?=traduz('INFORMACOES_HOTEIS_LOCAL')?>:</strong><br>
                    Rua Américo Brasiliense, 2163 - Chácara Sto Antonio - SP<br>
                    E-mail: <a href="mailto:reservasthg@transamerica.com.br">reservasthg@transamerica.com.br</a><br>
                    <?=traduz('INFORMACOES_HOTEIS_TEL')?>: (55) 11 5547-1166<br>
                    <a href="http://maps.google.com.br/maps?q=Rua+Am%C3%A9rico+Brasiliense,+2163+-+Ch%C3%A1cara+Santo+Antonio+-+SP&hl=pt-BR&ie=UTF8&sll=-23.651522,-46.722816&sspn=0.006732,0.011362&hnear=R.+Am%C3%A9rico+Brasiliense,+2163+-+Santo+Amaro,+S%C3%A3o+Paulo,+04715-005&t=m&z=17" target="_blank"><?=traduz('INFORMACOES_HOTEIS_COMO_CHEGAR')?></a>
                </div>
                
                <div class="coluna seg">
                    <strong><?=traduz('INFORMACOES_HOTEIS_CODIGO')?>: CIAB FEBRABAN 2013</strong>
                    <br>
                    <?=traduz('INFORMACOES_HOTEIS_DIARIA_SGL')?> - R$ 283,00<br>
                    <?=traduz('INFORMACOES_HOTEIS_DIARIA_DBL')?> - R$ 313,00<br>
                </div>
                
                <br>
                
                <div class="coluna checkin prim">
                    <strong>Check-in/out: </strong><br>
                    - <?=traduz('INFORMACOES_HOTEIS_CHECK_2')?>
                </div>
                <div class="coluna diaria seg">
                    <strong><?=traduz('INFORMACOES_HOTEIS_DIARIA')?>:</strong><br>
                    <?=traduz('INFORMACOES_HOTEIS_DIARIA_HT_2')?>
                </div>
                
                <div class="observacoes">
                    <strong><?=traduz('INFORMACOES_HOTEIS_OBSERVACOES')?>:</strong><br>
                    <?=traduz('INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_2')?>
                </div>
            </section>
            
            <section id="congonhas">
                <h2>TRANSAMÉRICA EXECUTIVE CONGONHAS</h2>
                
                <div class="coluna prim">
                    <strong><?=traduz('INFORMACOES_HOTEIS_LOCAL')?>:</strong><br>
                    Rua Vieira de Moraes, 1960 - Congonhas - SP<br>
                    E-mail: <a href="mailto:reservasthg@transamerica.com.br">reservasthg@transamerica.com.br</a><br>
                    <?=traduz('INFORMACOES_HOTEIS_TEL')?>: (55) 11 5547-1166<br>
                    <a href="http://maps.google.com.br/maps?q=Rua+Vieira+de+Moraes,+1960+-+Congonhas+-+SP&hl=pt-BR&ie=UTF8&sll=-23.628584,-46.706832&sspn=0.006733,0.011362&hnear=R.+Vieira+de+Morais,+1960+-+Campo+Belo,+S%C3%A3o+Paulo,+04617-007&t=m&z=17" target="_blank"><?=traduz('INFORMACOES_HOTEIS_COMO_CHEGAR')?></a>
                </div>
                
                <div class="coluna seg">
                    <strong><?=traduz('INFORMACOES_HOTEIS_CODIGO')?>: CIAB FEBRABAN 2013</strong><br>
                    <?=traduz('INFORMACOES_HOTEIS_DIARIA_SGL')?> - R$ 315,00<br>
                    <?=traduz('INFORMACOES_HOTEIS_DIARIA_DBL')?> - R$ 345,00<br>
                </div>
                
                <br>
                
                <div class="coluna checkin prim">
                    <strong>Check-in/out: </strong><br>
                    - <?=traduz('INFORMACOES_HOTEIS_CHECK_2')?>
                </div>
                <div class="coluna diaria seg">
                    <strong><?=traduz('INFORMACOES_HOTEIS_DIARIA')?>:</strong><br>
                    <?=traduz('INFORMACOES_HOTEIS_DIARIA_HT_3')?>
                </div>
                
                <div class="observacoes">
                    <strong><?=traduz('INFORMACOES_HOTEIS_OBSERVACOES')?>:</strong><br>
                    <?=traduz('INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_3')?>
                </div>
            </section>

            <section id="morumbi">
                <h2>BLUE TREE PREMIUM MORUMBI</h2>
                
                <div class="coluna prim">
                    <strong><?=traduz('INFORMACOES_HOTEIS_LOCAL')?>:</strong><br>
                    Av. Roque Petroni Jr., 1.000 – Brooklin – SP<br>
                    E-mail: <a href="mailto:reservas.morumbi@bluetree.com.br">reservas.morumbi@bluetree.com.br</a><br>
                    <?=traduz('INFORMACOES_HOTEIS_TEL')?>: (55) 11 5187-1269<br>
                    <a href="http://maps.google.com.br/maps?q=Av.+Roque+Petroni+Jr.,+1.000+%E2%80%93+Brooklin+%E2%80%93+SP&hl=pt-BR&ie=UTF8&sll=-23.566218,-46.652667&sspn=0.006736,0.011362&hnear=Av.+Roque+Petroni+J%C3%BAnior,+1000+-+Itaim+Bibi,+S%C3%A3o+Paulo,+04707-000&t=m&z=17" target="_blank"><?=traduz('INFORMACOES_HOTEIS_COMO_CHEGAR')?></a>
                </div>
                
                <div class="coluna seg">
                    <strong><?=traduz('INFORMACOES_HOTEIS_CODIGO')?>: CIAB FEBRABAN 2013</strong><br>
                    <?=traduz('INFORMACOES_HOTEIS_DIARIA_SGL')?> - R$ 421,00<br>
                    <?=traduz('INFORMACOES_HOTEIS_DIARIA_DBL')?> - R$ 491,00<br>
                </div>
                
                <br>
                
                <div class="coluna checkin prim">
                    <strong>Check-in/out: </strong><br>
                    - <?=traduz('INFORMACOES_HOTEIS_CHECK_3')?>
                </div>
                <div class="coluna diaria seg">
                    <strong><?=traduz('INFORMACOES_HOTEIS_DIARIA')?>:</strong><br>
                    <?=traduz('INFORMACOES_HOTEIS_DIARIA_HT_5')?>
                </div>
                
                <div class="observacoes">
                    <strong><?=traduz('INFORMACOES_HOTEIS_OBSERVACOES')?>:</strong><br>
                    <?=traduz('INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_5')?>
                </div>
            </section>            
            
            <section id="verbo">
                <h2>BLUE TREE PREMIUM VERBO DIVINO</h2>
                
                <div class="coluna prim">
                    <strong><?=traduz('INFORMACOES_HOTEIS_LOCAL')?>:</strong><br>
                    Rua Verbo Divino, 1323 - Santo Amaro - SP<br>
                    E-mail: <a href="mailto:reservas2.verbodivino@bluetree.com.br">reservas2.verbodivino@bluetree.com.br</a><br>
                    <?=traduz('INFORMACOES_HOTEIS_TEL')?>: (55) 11 3018-1848<br>
                    <a href="http://maps.google.com.br/maps?q=Rua+Verbo+Divino,+1323+-+Santo+Amaro+-+SP&hl=pt-BR&ie=UTF8&sll=-23.622376,-46.697145&sspn=0.006733,0.011362&hnear=R.+Verbo+Divino,+1323+-+Santo+Amaro,+S%C3%A3o+Paulo,+04719-002&t=m&z=17" target="_blank"><?=traduz('INFORMACOES_HOTEIS_COMO_CHEGAR')?></a>
                </div>
                
                <div class="coluna seg">
                    <strong><?=traduz('INFORMACOES_HOTEIS_CODIGO')?>: CIAB FEBRABAN 2013</strong><br>
                    <?=traduz('INFORMACOES_HOTEIS_DIARIA_SGL')?> - R$ 306,00<br>
                    <?=traduz('INFORMACOES_HOTEIS_DIARIA_DBL')?> - R$ 391,00<br>
                </div>
                
                <br>
                
                <div class="coluna checkin prim">
                    <strong>Check-in/out: </strong><br>
                    - <?=traduz('INFORMACOES_HOTEIS_CHECK_3')?>
                </div>
                <div class="coluna diaria seg">
                    <strong><?=traduz('INFORMACOES_HOTEIS_DIARIA')?>:</strong><br>
                    <?=traduz('INFORMACOES_HOTEIS_DIARIA_HT_5')?>
                </div>
                
                <div class="observacoes">
                    <strong><?=traduz('INFORMACOES_HOTEIS_OBSERVACOES')?>:</strong><br>
                    <?=traduz('INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_6')?>
                </div>
            </section>

            <section id="berrini">
                <h2>HOTEL QUALITY BERRINI</h2>
                
                <div class="coluna prim">
                    <strong><?=traduz('INFORMACOES_HOTEIS_LOCAL')?>:</strong><br>
                    Rua Heinrich Hertz, 14 – Brooklin Novo – São Paulo<br>
                    E-mail: <a href="mailto:reservas.qbe@atlanticahotels.com.br">reservas.qbe@atlanticahotels.com.br</a><br>
                    <?=traduz('INFORMACOES_HOTEIS_TEL')?>: (55) 11 3573-3900<br>
                    <a href="https://maps.google.com.br/maps?q=Rua+Heinrich+Hertz,+14&ie=UTF8&hnear=R.+Heinrich+Hertz,+14+-+Itaim+Bibi,+S%C3%A3o+Paulo,+04575-000&gl=br&t=m&z=14" target="_blank"><?=traduz('INFORMACOES_HOTEIS_COMO_CHEGAR')?></a>
                </div>
                
                <div class="coluna seg">
                    <strong><?=traduz('INFORMACOES_HOTEIS_CODIGO')?>: CIAB FEBRABAN 2013</strong><br>
                    <?=traduz('INFORMACOES_HOTEIS_DIARIA_SGL')?> - R$ 325,00<br>
                    <?=traduz('INFORMACOES_HOTEIS_DIARIA_DBL')?> - R$ 375,00<br>
                </div>
                
                <br>
                
                <div class="coluna checkin prim">
                    <strong>Check-in/out: </strong><br>
                    - <?=traduz('INFORMACOES_HOTEIS_CHECK_3')?>
                </div>
                <div class="coluna diaria seg">
                    <strong><?=traduz('INFORMACOES_HOTEIS_DIARIA')?>:</strong><br>
                    <?=traduz('INFORMACOES_HOTEIS_DIARIA_HT_6')?>
                </div>
                
                <div class="observacoes">
                    <strong><?=traduz('INFORMACOES_HOTEIS_OBSERVACOES')?>:</strong><br>
                    <?=traduz('INFORMACOES_HOTEIS_OBSERVACOES_TEXTO_7')?>
                </div>
            </section>
        </div>

    </div>
</div>