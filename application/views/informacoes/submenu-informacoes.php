<div class="centropad main-<?=$this->router->class?>">
    
    <div id="container">
    
        <div id="menu-lateral">
        
            <h3><?=traduz('INFORMACOES')?></h3>
            
            <ul class="submenu-informacoes">
                <li id="submn-local"><a href="informacoes/local"><?=traduz('INFORMACOES_SUBMENU_1')?></a></li>
                <div class="faixa-sob-item-menu"><div></div></div>
                
                <li id="submn-hospedagem"><a href="informacoes/hospedagem"><?=traduz('INFORMACOES_SUBMENU_2')?></a></li>
                <div class="faixa-sob-item-menu"><div></div></div>

                <li id="submn-tam"><a href="informacoes/tam"><?=traduz('Parceria TAM')?></a></li>
                <div class="faixa-sob-item-menu"><div></div></div>
                
            </ul>
            
            <script defer>$(document).ready( function(){ $('#submn-<?=$this->router->method?>').addClass('subativo'); });</script>
            