        <div class="conteudo">
        
            <h1>Busca por : '<?=$termo?>'</h1>
            
            <?if($num_resultados > 0):?>
            
                <h1>Resultados : <?=$num_resultados?></h1>
            
                <?foreach($resultados->result() as $val):?>
                    
                    <?if($val->source == 'noticias'):?>
                    
                        <div class="noticia">
                            <a href="noticias/detalhe/<?=$val->id?>">
                                <img src="_imgs/noticias/thumbs/<?=$val->imagem?>" alt="<?=$val->titulo?>">
                                <h1><?=$val->titulo?></h1>
                                <h2><?=formataData($val->data, 'mysql2br')?><?=($val->subtitulo) ? ' - '.$val->subtitulo : ''?></h2>
                                <p><?=$val->olho?></p>
                            </a>
                        </div>
                        
                    <?else:?>
                    
                        <div class="lista-clipping">
                            <a href="_pdfs/clippings/<?=$val->arquivo?>" target="_blank">
                                <h2><?=formataData($val->data, 'mysql2br')?> - <?=$val->titulo?></h2>
                                <?=$val->olho?>
                            </a>
                        </div>
                        
                    <?endif;?>
                    
                <?endforeach;?>
                
            <?else:?>
                <h1>Nenhum resultado encontrado</h1>
            <?endif;?>
            
            <?if($pagination):?>
                <div id="navegacao" class="sombra-pequena">
                    <?=$pagination?>
                </div>
            <?endif;?>
        </div>
    
    </div>
</div>