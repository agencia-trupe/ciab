<?php

$lang['profiler_database'] = 'BASE DE DADOS';
$lang['profiler_controller_info'] = 'CLASSE/M&Eacute;TODO';
$lang['profiler_benchmarks'] = 'BENCHMARKS';
$lang['profiler_queries'] = 'COLSULTAS';
$lang['profiler_get_data'] = 'DADOS GET';
$lang['profiler_post_data'] = 'DADOS POST';
$lang['profiler_uri_string'] = 'URI STRING';
$lang['profiler_memory_usage'] = 'USO DA MEM&Oacute;RIA';
$lang['profiler_config'] = 'VARI&Aacute;VEIS DE CONFIGURA&Ccedil;&Atilde;O';
$lang['profiler_headers'] = 'CABE&Ccedil;ALHOS HTTP';
$lang['profiler_no_db'] = 'O driver da base de dados n&atilde;o foi carregado';
$lang['profiler_no_queries'] = 'Nenhuma consulta foi executada';
$lang['profiler_no_post'] = 'N&atilde;o existem dados do tipo POST';
$lang['profiler_no_get'] = 'N&atilde;o existem dados do tipo GET';
$lang['profiler_no_uri'] = 'N&atilde;o existem dados do tipo URI';
$lang['profiler_no_memory'] = 'Uso da mem&oacute;ria n&atilde;o est&aacute; dispon&iacute;vel';
$lang['profiler_no_profiles'] = 'N�o h&aacute; dados do perfil - todas as se&ccedil;&otilde;es de perfis foram desativados.';
?>